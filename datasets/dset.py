from __future__ import print_function
import builtins
import torch.utils.data as data
from PIL import Image
import os
import os.path
import errno
import torch
import codecs
import tqdm
import pickle
import torchvision.datasets
import random

import hashlib
import sys
import tarfile
from PIL import Image

from six.moves import urllib
import ipdb
import numpy

import datasets.voc as voc



import json
import glob
import codecs
import numpy as np


# LAMBDA FUNCTIONS
filenameToPILImage = lambda x: Image.open(x).convert('L')
PiLImageResize = lambda x: x.resize((cargs.nr,cargs.nc))
PiLImageResizeNearest = lambda x: x.resize((cargs.nr,cargs.nc), Image.NEAREST)
#np_reshape = lambda x: np.reshape(np.array(x, dtype=float), (cargs.nr, cargs.nc, cargs.nch))
np_reshape = lambda x: np.reshape(np.array(x, dtype=float), (cargs.nr, cargs.nc, 1))
np_reshape3 = lambda x: np.reshape(np.array(x, dtype=float), (cargs.nr, cargs.nc, 3))
np_reshapef = lambda x: 255-np.reshape(np.array(x, dtype=float), (cargs.nr, cargs.nc, 1))
np_reshapef3 = lambda x: 255-np.reshape(np.array(x, dtype=float), (cargs.nr, cargs.nc, 3))



def np_reshapergb(x):
    #n = np.array(x.convert('YCbCr'), dtype=float)
    n = np.array(x, dtype=float)
    return n



class MyScale(object):
    def __init__(self, size, interpolation=Image.BILINEAR):
        self.size = size
        self.interpolation = interpolation

    def __call__(self, img):
        if isinstance(self.size, int):
            w, h = img.size
            if (w <= h and w == self.size) or (h <= w and h == self.size):
                return img
            if w < h:
                ow = self.size
                oh = int(self.size * h / w)
                return img.resize((ow, oh), self.interpolation)
            else:
                oh = self.size
                ow = int(self.size * w / h)
                return img.resize((ow, oh), self.interpolation)
        else:
            return img.resize(self.size, self.interpolation)

class MyInvert(object):
    def __call__(self, img):
        return 1-img

def get_int(b):
    return int(codecs.encode(b, 'hex'), 16)


def parse_byte(b):
    if isinstance(b, str):
        return ord(b)
    return b


def read_label_file(path):
    with open(path, 'rb') as f:
        data = f.read()
        assert get_int(data[:4]) == 2049
        length = get_int(data[4:8])
        labels = [parse_byte(b) for b in data[8:]]
        assert len(labels) == length
        return torch.LongTensor(labels)


def read_image_file(path):
    with open(path, 'rb') as f:
        data = f.read()
        assert get_int(data[:4]) == 2051
        length = get_int(data[4:8])
        num_rows = get_int(data[8:12])
        num_cols = get_int(data[12:16])
        images = []
        idx = 16
        for l in range(length):
            img = []
            images.append(img)
            for r in range(num_rows):
                row = []
                img.append(row)
                for c in range(num_cols):
                    row.append(parse_byte(data[idx]))
                    idx += 1
        assert len(images) == length
        return torch.ByteTensor(images).view(-1, 28, 28)




class KeepOrigImg(object):
    """
    keep original image
    """
    def __init__(self, t):
        self.t = t
    def __call__(self, tensor):
        """
        """
        return {'origdata': tensor.clone(), 'data':self.t(tensor)}



class VOCSegmentation(data.Dataset):
    CLASSES = [
        'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus',
        'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse',
        'motorbike', 'person', 'potted-plant', 'sheep', 'sofa', 'train',
        'tv/monitor', 'ambigious'
    ]

    URL = "http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar"
    FILE = "VOCtrainval_11-May-2012.tar"
    MD5 = '6cd6e144f989b92b3379bac3b3de84fd'
    BASE_DIR = 'VOCdevkit/VOC2012'

    def __init__(self,
                 root,
                 train=True,
                 transform=None,
                 target_transform=None,
                 download=False,
                 crop_image=False):
        self.crop_image = crop_image
        self.root = root
        _voc_root = os.path.join(self.root, self.BASE_DIR)
        self._voc_root = _voc_root
        _mask_dir = os.path.join(_voc_root, 'SegmentationClass')
        _image_dir = os.path.join(_voc_root, 'JPEGImages')
        self.transform = transform
        self.target_transform = target_transform
        self.train = train

        if download:
            self._download()

        if not self._check_integrity():
            raise RuntimeError('Dataset not found or corrupted.' +
                               ' You can use download=True to download it')
        # train/val/test splits are pre-cut
        _splits_dir = os.path.join(_voc_root, 'ImageSets/Segmentation')
        _split_f = os.path.join(_splits_dir, 'train.txt')
        if not self.train:
            _split_f = os.path.join(_splits_dir, 'trainval.txt')

        self.images = []
        self.masks = []
        with open(os.path.join(_split_f), "r") as lines:
            for line in lines:
                _image = os.path.join(_image_dir, line.rstrip('\n') + ".jpg")
                _mask = os.path.join(_mask_dir, line.rstrip('\n') + ".png")
                assert os.path.isfile(_image)
                assert os.path.isfile(_mask)
                self.images.append(_image)
                self.masks.append(_mask)

        self.preprocess()
        assert (len(self.images) == len(self.masks))

        self.ppClassMembers = [[] for c in VOCSegmentation.CLASSES]
        for i,c in enumerate(self.ppclasses):
            self.ppClassMembers[c].append(i)

        nsum = sum([len(x) for x in self.ppClassMembers])
        cargs.classWeights = [1 - len(x)/nsum for x in self.ppClassMembers]

    def preprocess(self):
        if os.path.exists(os.path.join(self._voc_root, 'ObjBBSegs', 'pp.pkl')):
            with open(os.path.join(self._voc_root, 'ObjBBSegs', 'pp.pkl'), 'rb') as f:
                [self.ppimgs, self.ppsegs, self.ppclasses] = pickle.load(f)
            return
        if not os.path.exists(os.path.join(self._voc_root, 'ObjBBSegs')):
            os.makedirs(os.path.join(self._voc_root, 'ObjBBSegs'))

        imgs = []
        segs = []
        classes = []

        oidx = 0
        print('Preprocessing VOC')
        for index in tqdm.tqdm(range(len(self.images))):
            _annfname = os.path.join(self._voc_root, 'Annotations', os.path.basename(self.images[index])[:-4]+'.xml')
            ann = voc.load_annotation(_annfname)
            _img = Image.open(self.images[index]).convert('RGB')
            _target = Image.open(self.masks[index])

            for obj in ann.find_all('object'):
                idx = voc.cat_name_to_cat_id(obj.find('name').contents[0])
                xmax = min(int(obj.xmax.contents[0])+4, _img.size[0])
                ymax = min(int(obj.ymax.contents[0])+4, _img.size[1])
                xmin = max(0, int(obj.xmin.contents[0])-4)
                ymin = max(0, int(obj.ymin.contents[0])-4)

                ## make square
                if (xmax-xmin) > (ymax-ymin):
                    dlt = ((xmax-xmin) - (ymax-ymin)) // 2
                    ymin = max(0, ymin-dlt)
                    ymax = min(_img.size[1], ymax+dlt)
                else:
                    dlt = ((ymax-ymin) - (xmax-xmin)) // 2
                    xmin = max(0, xmin-dlt)
                    xmax = min(_img.size[0], xmax+dlt)


                img = _img
                tgt = numpy.array(_target) == (idx)
                if self.crop_image:
                    img = _img.crop((xmin, ymin, xmax, ymax)).resize((cargs.nr, cargs.nc), Image.BILINEAR)
                    tgt = numpy.array(_target)[ymin:ymax, xmin:xmax] == (idx)
                if (tgt.sum() == 0): continue
                tgt = Image.fromarray(numpy.uint8(tgt)*255).resize((cargs.nr, cargs.nc), Image.BILINEAR)


                ifname =os.path.join(self._voc_root, 'ObjBBSegs', 'IMG%05d.png'%(oidx))
                sfname =os.path.join(self._voc_root, 'ObjBBSegs', 'SEG%05d.png'%(oidx))
                img.save(ifname)
                tgt.save(sfname)
                _img.save(os.path.join(self._voc_root, 'ObjBBSegs', 'ORIG%05d.png'%(oidx)))
                _target.save(os.path.join(self._voc_root, 'ObjBBSegs', 'ORIGS%05d.png'%(oidx)))


                imgs.append(ifname)
                segs.append(sfname)
                classes.append(idx)
                oidx += 1

        self.ppimgs = imgs
        self.ppsegs = segs
        self.ppclasses = classes

        assert(len(self.ppimgs) == len(self.ppsegs))
        assert(len(self.ppimgs) == len(self.ppclasses))

        print('Extracted %d images'%(len(self.ppimgs)))
        with open(os.path.join(self._voc_root, 'ObjBBSegs', 'pp.pkl'), 'wb') as f:
            pickle.dump([self.ppimgs, self.ppsegs, self.ppclasses], f)

    def __getitem__(self, index):
        img = Image.open(self.ppimgs[index]) #.convert('YCbCr')
        tgt = Image.open(self.ppsegs[index]).convert('L')
        idx = self.ppclasses[index]


        if self.transform is not None:
            img = torch.Tensor(self.transform(img))
        if self.target_transform is not None:
            tgt = torch.Tensor(self.target_transform(tgt))

        img = {'image': img}
        # img should already be a dict
        img['actualSeg'] = tgt.squeeze().unsqueeze(0)
        return img, {'target': torch.LongTensor([idx]), 'origidx': torch.LongTensor([index])}


    def __len__(self):
        return len(self.ppimgs)

    def _check_integrity(self):
        _fpath = os.path.join(self.root, self.FILE)
        if not os.path.isfile(_fpath):
            print("{} does not exist".format(_fpath))
            return False
        _md5c = hashlib.md5(open(_fpath, 'rb').read()).hexdigest()
        if _md5c != self.MD5:
            print(" MD5({}) did not match MD5({}) expected for {}".format(
                _md5c, self.MD5, _fpath))
            return False
        return True

    def _download(self):
        _fpath = os.path.join(self.root, self.FILE)

        try:
            os.makedirs(self.root)
        except OSError as e:
            if e.errno == errno.EEXIST:
                pass
            else:
                raise

        if self._check_integrity():
            print('Files already downloaded and verified')
            return
        else:
            print('Downloading ' + self.URL + ' to ' + _fpath)

            def _progress(count, block_size, total_size):
                sys.stdout.write('\r>> %s %.1f%%' %
                                 (_fpath, float(count * block_size) /
                                  float(total_size) * 100.0))
                sys.stdout.flush()

            urllib.request.urlretrieve(self.URL, _fpath, _progress)

        # extract file
        cwd = os.getcwd()
        print('Extracting tar file')
        tar = tarfile.open(_fpath)
        os.chdir(self.root)
        tar.extractall()
        tar.close()
        os.chdir(cwd)
        print('Done!')

class FilenameToPILImage(object):
    """
    Load a PIL RGB Image from a filename.
    """
    def __call__(self,filename):
        img=Image.open(filename).convert('YCbCr')
        return img



def find_classes(root_dir):
    retour=[]
    for (root,dirs,files) in os.walk(root_dir):
        for f in files:
            if (f.endswith("png")):
                r=root.split('/')
                lr=len(r)
                retour.append((f,r[lr-2]+"/"+r[lr-1],root))
    print("== Found %d items "%len(retour))
    return retour

def index_classes(items):
    idx={}
    for i in items:
        if (not i[1] in idx):
            idx[i[1]]=len(idx)
    print("== Found %d classes"% len(idx))
    return idx


class RandomVOCMask(data.Dataset):
    CLASSES = [
        'background', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus',
        'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse',
        'motorbike', 'person', 'potted-plant', 'sheep', 'sofa', 'train',
        'tv/monitor', 'ambigious'
    ]

    URL = "http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar"
    FILE = "VOCtrainval_11-May-2012.tar"
    MD5 = '6cd6e144f989b92b3379bac3b3de84fd'
    BASE_DIR = 'VOCdevkit/VOC2012'

    def __init__(self, root, transform=None):
        self.root = root
        _voc_root = os.path.join(self.root, self.BASE_DIR)
        self._voc_root = _voc_root
        _mask_dir = os.path.join(_voc_root, 'SegmentationObject')
        self.transform = transform

        self.masks = glob.glob(_mask_dir + '/*.png')
        self.objIdxs = []
        for i, mi in enumerate(self.masks):
            img = numpy.array(filenameToPILImage(mi))
            vs = numpy.unique(img)
            vs = vs[(vs != 0) & (vs != 220)]
            # vs = [x for x in vs if (img == x).sum() >= (0.1 * img.size)]
            # if len(vs) == 0:
            #     self.masks[i] = None
            #     self.objIdxs.append(None)
            # else:
            #     self.objIdxs.append(vs.tolist())
            self.objIdxs.append(vs.tolist())

    def __getitem__(self, idx):
        idx = idx % len(self.objIdxs)
        obj = self.objIdxs[idx]
        obji = random.sample(obj, 1)[0]
        img = self.transform(self.masks[idx]) == obji
        return img
