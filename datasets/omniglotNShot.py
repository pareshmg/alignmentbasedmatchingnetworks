##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import builtins
import torch
from datasets import omniglot
import torchvision
import torchvision.transforms as transforms
from PIL import Image
import os.path
import pickle
import numpy as np
np.random.seed(1234)  # for reproducibility
from datasets.dset import *
from skimage import data, segmentation, filters, color
import tqdm
from torch.autograd import Variable
from models.LinearAdditiveModel import weight_boundary, merge_boundary
from skimage.future import graph
from tu.imageUtils import normImg
import GPUtil

import datasets.voc as voc

class ImageFolder(torchvision.datasets.ImageFolder):
    """A generic data loader where the images are arranged in this way: ::

        root/dog/xxx.png
        root/dog/xxy.png
        root/dog/xxz.png

        root/cat/123.png
        root/cat/nsdf3.png
        root/cat/asd932_.png

    Args:
        root (string): Root directory path.
        transform (callable, optional): A function/transform that  takes in an PIL image
            and returns a transformed version. E.g, ``transforms.RandomCrop``
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        loader (callable, optional): A function to load an image given its path.

     Attributes:
        classes (list): List of the class names.
        class_to_idx (dict): Dict with items (class_name, class_index).
        imgs (list): List of (image path, class_index) tuples
    """

    def __init__(self, root, transform=None, target_transform=None,
                 loader=torchvision.datasets.folder.default_loader, classes=None):
        class_to_idx = None
        if classes is None:
            classes, class_to_idx = find_classes(root)
        else:
            class_to_idx = {x:i for (i,x) in enumerate(classes)}
        imgs = torchvision.datasets.folder.make_dataset(root, class_to_idx)
        if len(imgs) == 0:
            raise(RuntimeError("Found 0 images in subfolders of: " + root + "\n"
                               "Supported image extensions are: " + ",".join(IMG_EXTENSIONS)))

        self.root = root
        self.imgs = imgs
        self.classes = classes
        self.class_to_idx = class_to_idx
        self.transform = transform
        self.target_transform = target_transform
        self.loader = loader



class OmniglotNShotDataset(object):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):

        if not os.path.isfile(os.path.join(dataroot,'data.npy')):
            self.x = omniglot.OMNIGLOT(dataroot, download=True,
                                       transform=transforms.Compose([filenameToPILImage,
                                                                     PiLImageResize,
                                                                     np_reshape]))

            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            for classes in temp:
                self.x.append(np.array(temp[classes]))
            self.x = np.array(self.x)
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data.npy'),self.x)
        else:
            self.x = np.load(os.path.join(dataroot,'data.npy'))

        """
        Constructs an N-Shot omniglot Dataset
        :param batch_size: Experiment batch_size
        :param classes_per_set: Integer indicating the number of classes per set
        :param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
        """

        shuffle_classes = np.arange(self.x.shape[0])
        np.random.shuffle(shuffle_classes)
        self.x = self.x[shuffle_classes]
        self.x_train, self.x_test, self.x_val  = self.x[:1200], self.x[1200:1500], self.x[1500:]
        self.normalization()

        self.batch_size = batch_size
        self.n_classes = self.x.shape[0]
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class

        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": self.x_train, "val": self.x_val, "test": self.x_test} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}

    def normalization(self):
        """
        Normalizes our data, to have a mean of 0 and sdt of 1
        """
        self.mean = np.mean(self.x_train)
        self.std = np.std(self.x_train)
        self.max = np.max(self.x_train)
        self.min = np.min(self.x_train)
        print("train_shape", self.x_train.shape, "test_shape", self.x_test.shape, "val_shape", self.x_val.shape)
        #print("before_normalization", "mean", self.mean, "max", self.max, "min", self.min, "std", self.std)
        self.x_train = (self.x_train - self.mean) / self.std
        self.x_val = (self.x_val - self.mean) / self.std
        self.x_test = (self.x_test - self.mean) / self.std
        #self.mean = np.mean(self.x_train)
        #self.std = np.std(self.x_train)
        #self.max = np.max(self.x_train)
        #self.min = np.min(self.x_train)
        print("after_normalization", "mean", self.mean, "max", self.max, "min", self.min, "std", self.std)

    def load_data_cache(self, data_pack):
        """
        Collects 1000 batches data for N-shot learning
        :param data_pack: Data pack to use (any one of train, val, test)
        :return: A list with [support_set_x, support_set_y, target_x, target_y] ready to be fed to our networks
        """
        if cargs.test: np.random.seed(cargs.seed)
        n_samples = self.samples_per_class * self.classes_per_set
        data_cache = []
        for sample in range(64):
            support_set_x = np.zeros((self.batch_size, n_samples, cargs.nr, cargs.nc, cargs.nch))
            support_set_y = np.zeros((self.batch_size, n_samples))
            support_set_idx = np.zeros((self.batch_size, n_samples, 2))
            target_x = np.zeros((self.batch_size, self.samples_per_class, cargs.nr, cargs.nc, cargs.nch), dtype=np.float)
            target_y = np.zeros((self.batch_size, self.samples_per_class), dtype=np.int)
            target_idx = np.zeros((self.batch_size, self.samples_per_class, 2), dtype=np.int)
            for i in range(self.batch_size):
                pinds = np.random.permutation(n_samples)
                assert (data_pack.shape[0] >= self.classes_per_set),  'given {} but need {}'.format(data_pack.shape[0], self.classes_per_set)
                classes = np.random.choice(data_pack.shape[0], self.classes_per_set, False)
                # select 1-shot or 5-shot classes for test with repetition
                x_hat_class = np.random.choice(classes, self.samples_per_class, True)
                pinds_test = np.random.permutation(self.samples_per_class)
                ind = 0
                ind_test = 0
                for j, cur_class in enumerate(classes):  # each class
                    if cur_class in x_hat_class:
                        # Count number of times this class is inside the meta-test
                        n_test_samples = np.sum(cur_class == x_hat_class)
                        example_inds = np.random.choice(data_pack.shape[1], self.samples_per_class + n_test_samples, False)
                    else:
                        example_inds = np.random.choice(data_pack.shape[1], self.samples_per_class, False)

                    # meta-training
                    for eind in example_inds[:self.samples_per_class]:
                        support_set_x[i, pinds[ind], :, :, :] = data_pack[cur_class][eind]
                        support_set_y[i, pinds[ind]] = j
                        support_set_idx[i, pinds[ind], 0] = cur_class
                        support_set_idx[i, pinds[ind], 1] = eind
                        ind = ind + 1
                    # meta-test
                    for eind in example_inds[self.samples_per_class:]:
                        target_x[i, pinds_test[ind_test], :, :, :] = data_pack[cur_class][eind]
                        target_y[i, pinds_test[ind_test]] = j
                        target_idx[i, pinds_test[ind_test], 0] = cur_class
                        target_idx[i, pinds_test[ind_test], 1] = eind
                        ind_test = ind_test + 1

            data_cache.append([support_set_x, support_set_y, target_x, target_y, support_set_idx, target_idx])
        return data_cache

    def __get_batch(self, dataset_name):
        """
        Gets next batch from the dataset with name.
        :param dataset_name: The name of the dataset (one of "train", "val", "test")
        :return:
        """
        if self.indexes[dataset_name] >= len(self.datasets_cache[dataset_name]):
            self.indexes[dataset_name] = 0
            self.datasets_cache[dataset_name] = self.load_data_cache(self.datasets[dataset_name])
        next_batch = self.datasets_cache[dataset_name][self.indexes[dataset_name]]
        self.indexes[dataset_name] += 1
        #x_support_set, y_support_set, x_target, y_target = next_batch
        #return x_support_set, y_support_set, x_target, y_target
        return next_batch

    def get_batch(self,str_type, rotate_flag = False):

        """
        Get next batch
        :return: Next batch
        """
        x_support_set, y_support_set, x_target, y_target, x_ind, y_ind = self.__get_batch(str_type)
        if rotate_flag:
            k = int(np.random.uniform(low=0, high=4))
            # Iterate over the sequence. Extract batches.
            for i in np.arange(x_support_set.shape[0]):
                x_support_set[i,:,:,:,:] = self.__rotate_batch(x_support_set[i,:,:,:,:],k)
            # Rotate all the batch of the target images
            for i in np.arange(x_target.shape[0]):
                x_target[i,:,:,:,:] = self.__rotate_batch(x_target[i,:,:,:,:], k)
        return x_support_set, y_support_set, x_target, y_target, x_ind, y_ind


    def __rotate_data(self, image, k):
        """
        Rotates one image by self.k * 90 degrees counter-clockwise
        :param image: Image to rotate
        :return: Rotated Image
        """

        return np.rot90(image, k)


    def __rotate_batch(self, batch_images, k):
        """
        Rotates a whole image batch
        :param batch_images: A batch of images
        :param k: integer degree of rotation counter-clockwise
        :return: The rotated batch of images
        """
        batch_size = len(batch_images)
        for i in np.arange(batch_size):
            batch_images[i] = self.__rotate_data(batch_images[i], k)
        return batch_images





class MiniImageNetNShotDataset(OmniglotNShotDataset):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):
        classes = None
        imf = 'images{}'.format(cargs.nr)
        self.batch_size = batch_size
        self.n_classes = 100
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class
        self.dataroot = dataroot
        self.imf = imf

        with open(os.path.join(dataroot, imf, 'classes.pkl'), 'rb') as f:
            classes = pickle.load(f)
        if not os.path.isfile(os.path.join(dataroot, imf, 'norm_test.npy')):
            if not os.path.isfile(os.path.join(dataroot, imf, 'data_test.npy')):
                self.x_train = ImageFolder(root=os.path.join(dataroot,imf, 'train'),
                                           transform=transforms.Compose([PiLImageResize,
                                                                         np_reshapergb]),
                                           classes=classes)

                self.x_val = ImageFolder(root=os.path.join(dataroot, imf,'val'),
                                           transform=transforms.Compose([PiLImageResize,
                                                                         np_reshapergb]),
                                         classes=classes)
                self.x_test = ImageFolder(root=os.path.join(dataroot, imf,'test'),
                                           transform=transforms.Compose([PiLImageResize,
                                                                         np_reshapergb]),
                                          classes=classes)

                """
                # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
                """
                for dataset, x in zip(['train', 'val', 'test'], [self.x_train, self.x_val, self.x_test]):
                    temp = dict()
                    for (img, label) in x:
                        if label in temp:
                            temp[label].append(img)
                        else:
                            temp[label]=[img]
                    x = [] # Free memory

                    for classes in temp:
                        x.append(np.array(temp[classes]))
                    x = np.array(x)
                    temp = [] # Free memory
                    np.save(os.path.join(dataroot,imf,'data_{}.npy'.format(dataset)),x)


            self.x_train = np.load(os.path.join(dataroot,imf,'data_train.npy'))
            self.x_val = np.load(os.path.join(dataroot,imf,'data_val.npy'))
            self.x_test = np.load(os.path.join(dataroot,imf,'data_test.npy'))

            """
            Constructs an N-Shot omniglot Dataset
            :param batch_size: Experiment batch_size
            :param classes_per_set: Integer indicating the number of classes per set
            :param samples_per_class: Integer indicating samples per class
            e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
                 For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
            """

            self.normalization()
            np.save(os.path.join(dataroot, imf, 'norm_train.npy'), self.x_train)
            np.save(os.path.join(dataroot, imf, 'norm_val.npy'), self.x_val)
            np.save(os.path.join(dataroot, imf, 'norm_test.npy'), self.x_test)
            np.save(os.path.join(dataroot, imf, 'norm_params.npy'), numpy.array([self.mean/255, self.std/255]))


        self.x_train = np.load(os.path.join(dataroot,imf,'norm_train.npy'))
        self.x_val = np.load(os.path.join(dataroot,imf,'norm_val.npy'))
        self.x_test = np.load(os.path.join(dataroot,imf,'norm_test.npy'))
        self.mean, self.std = np.load(os.path.join(dataroot,imf,'norm_params.npy'))

        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": self.x_train, "val": self.x_val, "test": self.x_test} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}





class MiniImageNetSLICNShotDataset(MiniImageNetNShotDataset):
    def __init__(self, *args, **kwargs):
        super(MiniImageNetSLICNShotDataset, self).__init__(*args, **kwargs)
        for dataset, dx in zip(['train', 'val', 'test'], [self.x_train, self.x_val, self.x_test]):
            nunique = []
            if not os.path.isfile(os.path.join(self.dataroot, self.imf, 'slic_{}.npz'.format(dataset))):
                ncl, nex, nr, nr, nch = dx.shape
                segs = []
                ulabs = []
                for incl in tqdm.tqdm(range(ncl)):
                    ts = []
                    tu = []
                    for inex in range(nex):
                        image = dx[incl, inex]
                        segments = segmentation.slic(image, n_segments = 50, compactness=10, sigma = 1)
                        #if len(numpy.unique(segments)) == 1:
                        #    segments = segmentation.slic(image, n_segments = 50, compactness=50, sigma = 1)
                        edge_map = filters.sobel(color.rgb2gray(image))

                        minThresh = 0.0
                        maxThresh = 1.0


                        if False: #while True:
                            try:
                                adjGraph = graph.rag_boundary(segments, edge_map)
                            except:
                                import ipdb; ipdb.set_trace()
                            thresh = (minThresh + maxThresh)/2
                            seg2 = graph.merge_hierarchical(labels=segments,
                                                            rag=adjGraph,
                                                            thresh=thresh,
                                                            rag_copy=True,
                                                            in_place_merge=True,
                                                            merge_func=merge_boundary,
                                                            weight_func=weight_boundary) # [nr, nc]
                            # if (len(unq2) >= 5) and (len(unq2) <= 10): break
                            # if abs(minThresh-maxThresh) < 1e-3: break

                            # if len(unq2) > 10: minThresh = thresh
                            # elif len(unq2) < 5: maxThresh = thresh
                        else:
                            seg2 = segments


                        unq2 = numpy.unique(seg2)
                        ts.append(seg2) # [nexamples, nr, nc]
                        unq2 = numpy.unique(seg2)
                        nunique.append(len(unq2))
                        unq = numpy.zeros(50, dtype=int) + 999
                        unq[:len(unq2)] = unq2
                        tu.append(unq)

                    nc, nn = numpy.unique(nunique, return_counts=True)
                    segs.append(numpy.array(ts)) # [nclasses, nexamples, nr, nc]
                    ulabs.append(tu)
                nc, nn = numpy.unique(nunique, return_counts=True)
                print(list(zip(nc, nn)))
                segs = numpy.array(segs)
                ulabs = numpy.array(ulabs)
                print(dataset, segs.shape, ulabs.shape)
                numpy.savez_compressed(os.path.join(self.dataroot, self.imf, 'slic_{}.npz'.format(dataset)), segs=segs, ulabs=ulabs)
        self.slic = {}
        for x in ['train', 'val', 'test']:
            tmp = numpy.load(os.path.join(self.dataroot, self.imf, 'slic_{}.npz'.format(x)))
            self.slic[x] = [torch.from_numpy(tmp['segs']), torch.from_numpy(tmp['ulabs'])]


    def get_rag(self, mode, ind2):
        # ind :: np.zeros((self.batch_size, n_samples, 2))
        segs, ulabs = self.slic[mode]
        ress = []
        resu = []
        ind = ind2.data.cpu()
        for bi in range(ind.size(0)):
            for si in range(ind.size(1)):
                ress.append(segs[ind[bi,si,0], ind[bi,si,1]])
                resu.append(ulabs[ind[bi,si,0], ind[bi,si,1]])

        segs = torch.stack(ress)
        ulabs = torch.stack(resu)

        segs = segs.view(ind.size(0), ind.size(1), segs.size(1), segs.size(2))
        ulabs = ulabs.view(ind.size(0), ind.size(1), 50)

        nmax = (ulabs == 999).sum(2).min()
        ulabs = ulabs.narrow(2,0, ulabs.size(2)-nmax)

        if ind2.is_cuda:
            segs = segs.cuda()
            ulabs = ulabs.cuda()

        return Variable(segs, requires_grad=False), Variable(ulabs, requires_grad=False)

    def make_rag(self, img, segs, ulabs):
        """
        img :: [nb, ns, nch, nr, nc]
        """
        uimgs = (segs.unsqueeze(2) == ulabs.unsqueeze(3).unsqueeze(4)) # [nb, ns, nseg, nr, nc]
        uimgs = uimgs.unsqueeze(3).float() # [nb, ns, nseg, 1, nr, nc]
        uimgs = torch.cat([img.unsqueeze(2) * uimgs, uimgs], dim=3)
        return uimgs # [nb, ns, nseg, ndim+1, nr, nc]


    def create_slic(self, imgs, nsegments=50, compactness=50,sigma=1):
        """
        imgs :: [nb, nt, nch, nr, nc]
        """
        segs = []
        ulabs = []
        nb, nt, nch, nr, nc = imgs.size()
        for bi in range(nb):
            for ti in range(nt):
                image = imgs[bi, ti]
                seg = segmentation.slic(normImg(image).cpu().permute(1,2,0).numpy(),
                                        n_segments = nsegments, compactness=compactness, sigma = sigma)
                unq2 = numpy.unique(seg)
                segs.append(seg)
                unq = numpy.zeros(nsegments, dtype=int) + 999
                unq[:len(unq2)] = unq2
                ulabs.append(unq)
        segs = torch.from_numpy(numpy.array(segs)).view(nb, nt, nr, nc)
        ulabs = torch.from_numpy(numpy.array(ulabs)).view(nb, nt, nsegments)
        nmax = (ulabs != 999).long().sum(2).max()
        ulabs = ulabs.narrow(2,0,nmax)

        return segs, ulabs


class MNISTNShotDataset(OmniglotNShotDataset):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):
        if not os.path.isfile(os.path.join(dataroot,'data_test.npy')):
            self.x = torchvision.datasets.MNIST(dataroot, train=True, download=True,
                                                      transform=transforms.Compose([PiLImageResize,
                                                                                    np_reshapef]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_train.npy'),self.x)

            #####
            self.x = torchvision.datasets.MNIST(dataroot, train=False, download=True,
                                                      transform=transforms.Compose([PiLImageResize,
                                                                                    np_reshapef]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_test.npy'),self.x)

        self.x_train = np.load(os.path.join(dataroot,'data_train.npy'))
        self.x_val = self.x_train[5:]
        self.x_train = self.x_train[:5]
        self.x_test = np.load(os.path.join(dataroot,'data_test.npy'))[5:]

        """
        Constructs an N-Shot omniglot Dataset
        :param batch_size: Experiment batch_size
        :param classes_per_set: Integer indicating the number of classes per set
        :param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
        """

        self.normalization()

        self.batch_size = batch_size
        self.n_classes = 10
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class


        # from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg
        # for d,myx in zip(['train', 'val', 'test'],[self.x_train, self.x_val, self.x_test]):
        #     myx = torch.from_numpy(myx)
        #     allImgs = []
        #     for i in range(5):
        #         allImgs.append(hstackImgs([normImg(myx[i][j]) for j in range(10)]))
        #     allImgs = vstackImgs(allImgs)
        #     allImgs = makeImg(allImgs)
        #     allImgs.save('/home/pareshmg/outs/logs/{}.png'.format(d))
        # exit(1)


        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": self.x_train, "val": self.x_val, "test": self.x_test} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}

class CIFAR10NShotDataset(OmniglotNShotDataset):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):
        if not os.path.isfile(os.path.join(dataroot,'data_test.npy')):
            self.x = torchvision.datasets.CIFAR10(dataroot, train=True, download=True,
                                                      transform=transforms.Compose([PiLImageResize,
                                                                                    np_reshapef3]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_train.npy'),self.x)

            #####
            self.x = torchvision.datasets.CIFAR10(dataroot, train=False, download=True,
                                                      transform=transforms.Compose([PiLImageResize,
                                                                                    np_reshapef3]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_test.npy'),self.x)

        self.x_train = np.load(os.path.join(dataroot,'data_train.npy'))
        self.x_val = self.x_train[5:]
        self.x_train = self.x_train[:5]
        self.x_test = np.load(os.path.join(dataroot,'data_test.npy'))[5:]

        """
        Constructs an N-Shot omniglot Dataset
        :param batch_size: Experiment batch_size
        :param classes_per_set: Integer indicating the number of classes per set
        :param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
        """

        self.normalization()

        self.batch_size = batch_size
        self.n_classes = 10
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class


        # from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg
        # for d,myx in zip(['train', 'val', 'test'],[self.x_train, self.x_val, self.x_test]):
        #     myx = torch.from_numpy(myx)
        #     allImgs = []
        #     for i in range(5):
        #         allImgs.append(hstackImgs([normImg(myx[i][j]) for j in range(10)]))
        #     allImgs = vstackImgs(allImgs)
        #     allImgs = makeImg(allImgs)
        #     allImgs.save('/home/pareshmg/outs/logs/{}.png'.format(d))
        # exit(1)


        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": self.x_train, "val": self.x_val, "test": self.x_test} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}


class CIFAR100NShotDataset(OmniglotNShotDataset):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):
        if not os.path.isfile(os.path.join(dataroot,'data_test.npy')):
            self.x = torchvision.datasets.CIFAR100(dataroot, train=True, download=True,
                                                   transform=transforms.Compose([PiLImageResize,
                                                                                 np_reshapef3]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_train.npy'),self.x)

            #####
            self.x = torchvision.datasets.CIFAR100(dataroot, train=False, download=True,
                                                   transform=transforms.Compose([PiLImageResize,
                                                                                 np_reshapef3]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_test.npy'),self.x)

        classes_perm = np.random.permutation(100)
        train_classes = classes_perm[:60]
        val_classes = classes_perm[60:75]
        test_classes = classes_perm[75:]

        self.x_train = np.load(os.path.join(dataroot,'data_train.npy'))
        self.x_val = self.x_train[val_classes]
        self.x_train = self.x_train[train_classes]
        self.x_test = np.load(os.path.join(dataroot,'data_test.npy'))[test_classes]

        """
        Constructs an N-Shot omniglot Dataset
        :param batch_size: Experiment batch_size
        :param classes_per_set: Integer indicating the number of classes per set
        :param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
        """

        self.normalization()

        self.batch_size = batch_size
        self.n_classes = 100
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class


        # from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg
        # for d,myx in zip(['train', 'val', 'test'],[self.x_train, self.x_val, self.x_test]):
        #     myx = torch.from_numpy(myx)
        #     allImgs = []
        #     for i in range(5):
        #         allImgs.append(hstackImgs([normImg(myx[i][j]) for j in range(10)]))
        #     allImgs = vstackImgs(allImgs)
        #     allImgs = makeImg(allImgs)
        #     allImgs.save('/home/pareshmg/outs/logs/{}.png'.format(d))
        # exit(1)


        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": self.x_train, "val": self.x_val, "test": self.x_test} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}


class MNISTNShotDatasetFake(OmniglotNShotDataset):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):
        if not os.path.isfile(os.path.join(dataroot,'data.npy')):
            self.x = torchvision.datasets.MNIST(dataroot, train=True, download=True,
                                                      transform=transforms.Compose([PiLImageResize,
                                                                                    np_reshapef]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            for classes in temp:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_train.npy'),self.x)

            #####
            self.x = torchvision.datasets.MNIST(dataroot, train=False, download=True,
                                                      transform=transforms.Compose([PiLImageResize,
                                                                                    np_reshapef]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            for classes in temp:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_test.npy'),self.x)



        self.x_train = np.load(os.path.join(dataroot,'data_train.npy'))
        self.x_test = np.load(os.path.join(dataroot,'data_test.npy'))

        """
        Constructs an N-Shot omniglot Dataset
        :param batch_size: Experiment batch_size
        :param classes_per_set: Integer indicating the number of classes per set
        :param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
        """

        shuffle_classes = np.arange(self.x_train.shape[0])
        np.random.shuffle(shuffle_classes)
        self.x_train = self.x_train[shuffle_classes]
        self.x_test = self.x_test[shuffle_classes]
        self.x_val = self.x_train.copy()
        self.normalization()

        self.batch_size = batch_size
        self.n_classes = 10
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class

        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": self.x_train, "val": self.x_val, "test": self.x_test} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}


class SVHNNShotDataset(OmniglotNShotDataset):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):
        if not os.path.isfile(os.path.join(dataroot,'data_test.npy')):
            self.x = torchvision.datasets.SVHN(dataroot, split='train', download=True,
                                               transform=transforms.Compose([PiLImageResize,
                                                                             np_reshape3]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                #label=(label[0] + 1) % 10
                label=label[0] % 10
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            print(allClasses)
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_train.npy'),self.x)

            #####
            self.x = torchvision.datasets.SVHN(dataroot, split='test', download=True,
                                               transform=transforms.Compose([PiLImageResize,
                                                                             np_reshape3]))
            """
            # Convert to the format of AntreasAntoniou. Format [nClasses,nCharacters,32,32,1]
            """
            temp = dict()
            for (img, label) in self.x:
                #label=(label[0] + 1) % 10
                label=label[0] % 10
                if label in temp:
                    temp[label].append(img)
                else:
                    temp[label]=[img]
            self.x = [] # Free memory

            allClasses = [x for x in temp]
            allClasses.sort()
            for classes in allClasses:
                self.x.append(np.array(temp[classes]))
            nc = min([x.shape[0] for x in self.x])
            self.x = np.array([x[:nc] for x in self.x])
            temp = [] # Free memory
            np.save(os.path.join(dataroot,'data_test.npy'),self.x)



        self.x_train = np.load(os.path.join(dataroot,'data_train.npy'))
        self.x_val = self.x_train[5:]
        self.x_train = self.x_train[:5]
        self.x_test = np.load(os.path.join(dataroot,'data_test.npy'))[5:]






        """
        Constructs an N-Shot omniglot Dataset
        :param batch_size: Experiment batch_size
        :param classes_per_set: Integer indicating the number of classes per set
        :param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
        """
        self.normalization()

        self.batch_size = batch_size
        self.n_classes = 10
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class


        # from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg
        # ipdb.set_trace()
        # for d,myx in zip(['train', 'val', 'test'],[self.x_train, self.x_val, self.x_test]):
        #     myx = torch.from_numpy(myx)
        #     allImgs = []
        #     for i in range(5):
        #         allImgs.append(hstackImgs([normImg(myx[i][j]) for j in range(10)]))
        #     allImgs = vstackImgs(allImgs)
        #     allImgs = makeImg(allImgs)
        #     allImgs.save('/home/pareshmg/outs/logs/{}.png'.format(d))
        # exit(1)


        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": self.x_train, "val": self.x_val, "test": self.x_test} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}


class CrossNShotDataset():
    def __init__(self, d1, d2, batch_size = 100, classes_per_set=10, samples_per_class=1):
        self.d1 = d1
        self.d2 = d2
        self.batch_size = batch_size
        self.n_classes = self.d1.x_train.shape[0]
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class

        self.d1.datasets_cache = None
        self.d2.datasets_cache = None

        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets_cache = {"train": self.load_data_cache(self.d1.datasets["train"], self.d2.datasets['train']),
                               "val": self.load_data_cache(self.d1.datasets["val"], self.d2.datasets['val']),
                               "test": self.load_data_cache(self.d1.datasets["test"], self.d2.datasets['test'])}


    def load_data_cache(self, data_pack1, data_pack2):
        """
        Collects 1000 batches data for N-shot learning
        :param data_pack: Data pack to use (any one of train, val, test)
        :return: A list with [support_set_x, support_set_y, target_x, target_y] ready to be fed to our networks
        """
        n_samples = self.samples_per_class * self.classes_per_set
        data_cache = []
        for sample in range(256):
            support_set_x = np.zeros((self.batch_size, n_samples, cargs.nr, cargs.nc, cargs.nch))
            support_set_y = np.zeros((self.batch_size, n_samples))
            target_x = np.zeros((self.batch_size, self.samples_per_class, cargs.nr2, cargs.nc2, cargs.nch2), dtype=np.float)
            target_y = np.zeros((self.batch_size, self.samples_per_class), dtype=np.int)
            for i in range(self.batch_size):
                pinds = np.random.permutation(n_samples)
                classes = np.random.choice(data_pack1.shape[0], self.classes_per_set, False)
                # select 1-shot or 5-shot classes for test with repetition
                x_hat_class = np.random.choice(classes, self.samples_per_class, True)
                pinds_test = np.random.permutation(self.samples_per_class)
                ind = 0
                ind_test = 0
                for j, cur_class in enumerate(classes):  # each class
                    if cur_class in x_hat_class:
                        # Count number of times this class is inside the meta-test
                        n_test_samples = np.sum(cur_class == x_hat_class)
                        example_inds = np.random.choice(min(data_pack1.shape[1], data_pack2.shape[1]),
                                                        self.samples_per_class + n_test_samples, False)
                    else:
                        example_inds = np.random.choice(min(data_pack1.shape[1], data_pack2.shape[1]),
                                                        self.samples_per_class, False)

                    # meta-training
                    for eind in example_inds[:self.samples_per_class]:
                        support_set_x[i, pinds[ind], :, :, :] = data_pack1[cur_class][eind]
                        support_set_y[i, pinds[ind]] = j
                        ind = ind + 1
                    # meta-test
                    for eind in example_inds[self.samples_per_class:]:
                        target_x[i, pinds_test[ind_test], :, :, :] = data_pack2[cur_class][eind]
                        target_y[i, pinds_test[ind_test]] = j
                        ind_test = ind_test + 1

            data_cache.append([support_set_x, support_set_y, target_x, target_y])
        return data_cache

    def __get_batch(self, dataset_name):
        """
        Gets next batch from the dataset with name.
        :param dataset_name: The name of the dataset (one of "train", "val", "test")
        :return:
        """
        if self.indexes[dataset_name] >= len(self.datasets_cache[dataset_name]):
            self.indexes[dataset_name] = 0
            self.datasets_cache[dataset_name] = self.load_data_cache(self.d1.datasets[dataset_name],
                                                                     self.d2.datasets[dataset_name])
        next_batch = self.datasets_cache[dataset_name][self.indexes[dataset_name]]
        self.indexes[dataset_name] += 1
        x_support_set, y_support_set, x_target, y_target = next_batch
        return x_support_set, y_support_set, x_target, y_target

    def get_batch(self,str_type, rotate_flag = False):

        """
        Get next batch
        :return: Next batch
        """
        x_support_set, y_support_set, x_target, y_target = self.__get_batch(str_type)
        if rotate_flag:
            k = int(np.random.uniform(low=0, high=4))
            # Iterate over the sequence. Extract batches.
            for i in np.arange(x_support_set.shape[0]):
                x_support_set[i,:,:,:,:] = self.__rotate_batch(x_support_set[i,:,:,:,:],k)
            # Rotate all the batch of the target images
            for i in np.arange(x_target.shape[0]):
                x_target[i,:,:,:,:] = self.__rotate_batch(x_target[i,:,:,:,:], k)
        return x_support_set, y_support_set, x_target, y_target


    def __rotate_data(self, image, k):
        """
        Rotates one image by self.k * 90 degrees counter-clockwise
        :param image: Image to rotate
        :return: Rotated Image
        """

        return np.rot90(image, k)


    def __rotate_batch(self, batch_images, k):
        """
        Rotates a whole image batch
        :param batch_images: A batch of images
        :param k: integer degree of rotation counter-clockwise
        :return: The rotated batch of images
        """
        batch_size = len(batch_images)
        for i in np.arange(batch_size):
            batch_images[i] = self.__rotate_data(batch_images[i], k)
        return batch_images



class VOCNShotDataset(OmniglotNShotDataset):
    def __init__(self, dataroot, batch_size = 100, classes_per_set=10, samples_per_class=1):
        self.img_df = voc.load_data_multilabel('train')
        self.smask = voc.has_segmentation_mask(self.img_df)
        self.img_df = self.img_df[self.smask]
        self.img_df.reset_index(drop=True)
        self.multi_label_images = self.img_df[self.img_df.iloc[:,1:].sum(1) > 1]
        self.batch_size = batch_size
        self.n_classes = 10
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class


        # self.x_train = np.load(os.path.join(dataroot,imf,'norm_train.npy'))
        # self.x_val = np.load(os.path.join(dataroot,imf,'norm_val.npy'))
        # self.x_test = np.load(os.path.join(dataroot,imf,'norm_test.npy'))
        # self.mean, self.std = np.load(os.path.join(dataroot,imf,'norm_params.npy'))

        self.indexes = {"train": 0, "val": 0, "test": 0}
        self.datasets = {"train": None, "val": None, "test": None} #original data cached
        self.datasets_cache = {"train": self.load_data_cache(self.datasets["train"]),  #current epoch data cached
                               "val": self.load_data_cache(self.datasets["val"]),
                               "test": self.load_data_cache(self.datasets["test"])}

    def __get_batch(self, dataset_name):
        """
        Gets next batch from the dataset with name.
        :param dataset_name: The name of the dataset (one of "train", "val", "test")
        :return:
        """
        if self.indexes[dataset_name] >= len(self.datasets_cache[dataset_name]):
            self.indexes[dataset_name] = 0
            self.datasets_cache[dataset_name] = self.load_data_cache(self.datasets[dataset_name])
        next_batch = self.datasets_cache[dataset_name][self.indexes[dataset_name]]
        self.indexes[dataset_name] += 1
        #x_support_set, y_support_set, x_target, y_target = next_batch
        #return x_support_set, y_support_set, x_target, y_target
        return next_batch

    def get_batch(self,str_type, rotate_flag = False):

        """
        Get next batch
        :return: Next batch
        """
        x_support_set, y_support_set, x_target, y_target, x_ind, y_ind, m_target = self.__get_batch(str_type)
        # rotate flag is disabled
        return x_support_set, y_support_set, x_target, y_target, x_ind, y_ind, m_target


    def load_data_cache(self, data_pack):
        """
        Collects 1000 batches data for N-shot learning
        :param data_pack: Data pack to use (any one of train, val, test)
        :return: A list with [support_set_x, support_set_y, target_x, target_y] ready to be fed to our networks

        1) sample target image that has two or more objects with different labels in it
        2) sample support set that has labels in the target image / other labels
        """
        # FIXME
        print('FIXME!!!')
        if os.path.exists('/home/pareshmg/outs/dcache_remove.t7'):
            data_cache = torch.load('/home/pareshmg/outs/dcache_remove.t7')
            return data_cache
        assert(self.samples_per_class == 1)
        if cargs.test: np.random.seed(cargs.seed)
        n_samples = self.samples_per_class * self.classes_per_set
        data_cache = []
        for sample in range(64):
            support_set_x = np.zeros((self.batch_size, n_samples, cargs.nr, cargs.nc, cargs.nch))
            support_set_y = np.zeros((self.batch_size, n_samples))
            support_set_idx = np.zeros((self.batch_size, n_samples, 2))
            target_x = np.zeros((self.batch_size, self.samples_per_class, cargs.nr, cargs.nc, cargs.nch), dtype=np.float)
            target_mask = np.zeros((self.batch_size, self.samples_per_class, cargs.nr, cargs.nc), dtype=np.float)
            target_y = np.zeros((self.batch_size, self.samples_per_class), dtype=np.int)
            target_idx = np.zeros((self.batch_size, self.samples_per_class, 2), dtype=np.int)
            for i in range(self.batch_size):
                # sample target class
                tclass = random.sample(voc.list_image_sets()[1:-1], 1)[0]
                possibleSamps = self.multi_label_images[self.multi_label_images[tclass] == True]
                tind = possibleSamps.iloc[np.random.choice(len(possibleSamps))].name

                sclasses = [tclass]
                cols = list(self.img_df.columns[2:-1])
                random.shuffle(cols)
                for col in cols:
                    if len(sclasses) >= self.classes_per_set: break
                    if self.img_df[col][tind] == True: sclasses.append(col)

                for tmpi in range(len(cols)):
                    if len(sclasses) >= self.classes_per_set: break
                    sclasses.append(col)

                random.shuffle(sclasses)
                del cols


                target_x[i,0,:,:,:] = self.get_torch_voc_image(self.img_df.filename[tind])
                target_y[i,0] = sclasses.index(tclass)
                target_idx[i,0,0] = list(self.img_df.columns).index(tclass)
                target_idx[i,0,1] = tind
                target_mask[i,0,:,:] = self.get_torch_voc_mask(self.img_df.filename[tind], tclass)
                assert(target_mask[i,0].sum() > 0)
                for j,cur_class in enumerate(sclasses):
                    # sample support set image with cur class, but without
                    # other classes in object (other than bkgnd / ambigious)
                    possibleSamps = None
                    if cur_class == tclass:
                        possibleSamps = self.img_df[cur_class] == True
                    else:
                        possibleSamps = (self.img_df[cur_class] == True) & (self.img_df[tclass] == False)

                    possibleSamps = self.img_df[possibleSamps]
                    sind = possibleSamps.iloc[np.random.choice(len(possibleSamps))].name


                    support_set_x[i,j,:,:,:] = self.get_torch_voc_image(self.img_df.filename[sind])
                    support_set_y[i,j] = j
                    support_set_idx[i,j,0] = list(self.img_df.columns).index(cur_class)
                    support_set_idx[i,j,1] = sind

            data_cache.append([support_set_x, support_set_y, target_x, target_y, support_set_idx, target_idx, target_mask])

        torch.save(data_cache,'/home/pareshmg/outs/dcache_remove.t7')
        return data_cache


    def get_torch_voc_image(self, imgf):
        img = voc.load_img(imgf) # [nr, nc, nch]
        img = numpy.array(img*255, dtype=numpy.uint8)
        img = Image.fromarray(img)
        img = img.resize((84,84),resample=Image.BICUBIC)
        img = torch.from_numpy(numpy.array(img))
        return img

    def get_torch_voc_mask(self, imgf, objClass):
        objClassID = voc.cat_name_to_cat_id(objClass)
        img = voc.load_segmentation_class(imgf)
        img = numpy.array(img == objClassID, dtype=numpy.uint8)
        img = Image.fromarray(img)
        img = img.resize((84,84),resample=Image.NEAREST)
        img = torch.from_numpy(numpy.array(img))
        return img




class CIFAR10Dataset(object):
    def __init__(self,
                 root,
                 batch_size = None,
                 train=True,
                 transform=None,
                 target_transform=None,
                 download=True,
                 crop_image=False):
        self.batch_size = batch_size
        self.crop_image = crop_image
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.train = train
        self.nClasses= 10

        self.x , self.y = self.load_data()
        self.normalization()
        self.xmin = self.x['train'].min()
        self.xmax = self.x['train'].max()


    def load_data(self):
        if not os.path.exists(os.path.join(self.root, 'classification_data_y.pkl')):
            tmpx = torchvision.datasets.CIFAR10(self.root, train=True, download=True,
                                                transform=transforms.Compose([PiLImageResize,
                                                                              np_reshapef3]))
            xs = []
            ys = []
            for (img, label) in tmpx:
                xs.append(img)
                ys.append(label)

            xs = numpy.array(xs)
            ys = numpy.array(ys)
            shuf = np.arange(xs.shape[0])
            np.random.shuffle(shuf)
            x_train = xs[shuf[:-len(shuf)//10]]
            x_valid = xs[shuf[-len(shuf)//10:]]
            y_train = ys[shuf[:-len(shuf)//10]]
            y_valid = ys[shuf[-len(shuf)//10:]]

            tmpx = torchvision.datasets.CIFAR10(self.root, train=False, download=True,
                                                transform=transforms.Compose([PiLImageResize,
                                                                              np_reshapef3]))
            xs = []
            ys = []
            for (img, label) in tmpx:
                xs.append(img)
                ys.append(label)

            xs = numpy.array(xs)
            ys = numpy.array(ys)
            x_test = xs
            y_test = ys

            with open(os.path.join(self.root, 'split.pkl'), 'wb') as fp:
                pickle.dump(shuf, fp)
            with open(os.path.join(self.root, 'classification_data_y.pkl'), 'wb') as fp:
                pickle.dump({'train':y_train, 'val':y_valid, 'test':y_test}, fp)
            with open(os.path.join(self.root, 'classification_data_x.pkl'), 'wb') as fp:
                pickle.dump({'train':x_train, 'val':x_valid, 'test':x_test}, fp)

        print(self.root)
        with open(os.path.join(self.root, 'classification_data_y.pkl'), 'rb') as fp:
            y = pickle.load(fp)
        with open(os.path.join(self.root, 'classification_data_x.pkl'), 'rb') as fp:
            x = pickle.load(fp)
        return x, y


    def get_batch(self,str_type, **kwargs):
        """
        Get next batch
        :return: Next batch
        """
        xs = self.x[str_type]
        ys = self.y[str_type]

        idxs = [i for i in range(len(self.y[str_type]))]
        numpy.random.shuffle(idxs)
        idxs = idxs[:self.batch_size]


        xs = xs[idxs]
        ys = ys[idxs]
        xs = torch.from_numpy(numpy.array(xs)).permute(0,3,1,2).float()
        ys = torch.from_numpy(ys).long()

        one_hot = torch.eye(self.batch_size)

        xs = torch.stack(xs)

        if True: #str_type == 'train':
            if True:
                # Attempt 1: knock out a percentage of pixels
                idxs = (torch.rand(xs.size()) < (1*1e-2)).float()
                xs_low = xs * (1-idxs) + self.xmin * idxs
                xs_high = xs * (1-idxs) + self.xmax * idxs
            elif False:
                # Attempt 2: knock out 1 pixel
                idxs = torch.rand(xs.size(0), xs.size(2)*xs.size(3))
                idxs = (idxs == idxs.max(1, keepdim=True)[0]).float().view(xs.size(0), 1, xs.size(2), xs.size(3))
                xs_low = xs * (1-idxs) + self.xmin * idxs
                xs_high = xs * (1-idxs) + self.xmax * idxs
            elif False:
                # all pixels have a small delta range
                xs_low = xs - 1e-1
                xs_high = xs + 1e-1
            elif False:
                # all pixels have a small delta range
                xs_low = xs - 1e-3
                xs_high = xs + 1e-3
            elif False:
                xs_low = xs
                xs_high = xs
        else:
            xs_low = xs
            xs_high = xs




        return xs_low, xs_high, ys, one_hot


    def normalization(self):
        if not os.path.exists(os.path.join(self.root, 'classification_norm_params.npy')):
            curSum = torch.from_numpy(self.x['train'])
            self.mean = curSum.mean()
            self.std = curSum.std()
            numpy.save(os.path.join(self.root, 'classification_norm_params.npy'), numpy.array([self.mean, self.std]))


        self.mean, self.std = numpy.load(os.path.join(self.root,'classification_norm_params.npy'))
        for k in self.x:
            self.x[k] = (self.x[k] - self.mean) / self.std



class MNISTDataset(CIFAR10Dataset):
    def __init__(self,
                 root,
                 batch_size = None,
                 train=True,
                 transform=None,
                 target_transform=None,
                 download=True,
                 crop_image=False):
        self.batch_size = batch_size
        self.crop_image = crop_image
        self.root = root
        self.transform = transform
        self.target_transform = target_transform
        self.train = train
        self.nClasses= 10

        if not os.path.exists(os.path.join(self.root, 'classification_data_y.pkl')):
            tmpx = torchvision.datasets.MNIST(self.root, train=True, download=True,
                                              transform=transforms.Compose([PiLImageResize,
                                                                            np_reshapef]))
            xs = []
            ys = []
            for (img, label) in tmpx:
                xs.append(img)
                ys.append(label)

            xs = numpy.array(xs)
            ys = numpy.array(ys)
            shuf = np.arange(xs.shape[0])
            np.random.shuffle(shuf)
            self.x_train = xs[shuf[:-len(shuf)//10]]
            self.x_valid = xs[shuf[-len(shuf)//10:]]
            self.y_train = ys[shuf[:-len(shuf)//10]]
            self.y_valid = ys[shuf[-len(shuf)//10:]]

            tmpx = torchvision.datasets.MNIST(self.root, train=False, download=True,
                                              transform=transforms.Compose([PiLImageResize,
                                                                            np_reshapef]))
            xs = []
            ys = []
            for (img, label) in tmpx:
                xs.append(img)
                ys.append(label)

            xs = numpy.array(xs)
            ys = numpy.array(ys)
            self.x_test = xs
            self.y_test = ys

            with open(os.path.join(self.root, 'split.pkl'), 'wb') as fp:
                pickle.dump(shuf, fp)
            with open(os.path.join(self.root, 'classification_data_y.pkl'), 'wb') as fp:
                pickle.dump({'train':self.y_train, 'val':self.y_valid, 'test':self.y_test}, fp)
            with open(os.path.join(self.root, 'classification_data_x.pkl'), 'wb') as fp:
                pickle.dump({'train':self.x_train, 'val':self.x_valid, 'test':self.x_test}, fp)

        print(self.root)
        with open(os.path.join(self.root, 'classification_data_y.pkl'), 'rb') as fp:
            self.y = pickle.load(fp)
        with open(os.path.join(self.root, 'classification_data_x.pkl'), 'rb') as fp:
            self.x = pickle.load(fp)

        self.normalization()





class MiniImageNetDataset(CIFAR10Dataset):
    def __init__(self,
                 root,
                 batch_size = None,
                 train=True,
                 transform=None,
                 target_transform=None,
                 download=True,
                 crop_image=False):
        imf = 'images{}'.format(cargs.nr)
        super(MiniImageNetDataset, self).__init__(os.path.join(root, imf), batch_size, train, transform, target_transform, download, crop_image)
        self.nClasses= 100

    def load_data(self):
        with open(os.path.join(self.root, 'classes.pkl'), 'rb') as f:
            classes = pickle.load(f)

        if not os.path.exists(os.path.join(self.root, 'classification_data_y.pkl')):
            x_train = ImageFolder(root=os.path.join(self.root, 'train'),
                                       transform=transforms.Compose([PiLImageResize,
                                                                     np_reshapergb]),
                                       classes=classes)
            x_val = ImageFolder(root=os.path.join(self.root,'val'),
                                       transform=transforms.Compose([PiLImageResize,
                                                                     np_reshapergb]),
                                     classes=classes)
            x_test = ImageFolder(root=os.path.join(self.root,'test'),
                                       transform=transforms.Compose([PiLImageResize,
                                                                     np_reshapergb]),
                                      classes=classes)
            temp = dict()
            for dataset, x in zip(['train', 'val', 'test'], [x_train, x_val, x_test]):
                for (img, label) in x:
                    if label in temp:
                        temp[label].append(img)
                    else:
                        temp[label]=[img]
            xs = []
            ys = []
            for ci, classes in enumerate(temp):
                xs.append(np.array(temp[classes]))
                ys.append(np.array([ci for x in temp[classes]], dtype=int))

            xs = numpy.concatenate(xs, axis=0)
            ys = numpy.concatenate(ys, axis=0)



            shuf = np.arange(xs.shape[0])
            np.random.shuffle(shuf)
            x_train = xs[shuf[len(shuf)//10:-len(shuf)//5]]
            x_valid = xs[shuf[:len(shuf)//10]]
            x_test = xs[shuf[-len(shuf)//5:]]
            y_train = ys[shuf[len(shuf)//10:-len(shuf)//5]]
            y_valid = ys[shuf[:len(shuf)//10]]
            y_test = ys[shuf[-len(shuf)//5:]]

            with open(os.path.join(self.root, 'classification_split.pkl'), 'wb') as fp:
                pickle.dump(shuf, fp)
            with open(os.path.join(self.root, 'classification_data_y.pkl'), 'wb') as fp:
                pickle.dump({'train':y_train, 'val':y_valid, 'test':y_test}, fp, protocol=4)
            with open(os.path.join(self.root, 'classification_data_x.pkl'), 'wb') as fp:
                pickle.dump({'train':x_train, 'val':x_valid, 'test':x_test}, fp, protocol=4)

        with open(os.path.join(self.root, 'classification_data_y.pkl'), 'rb') as fp:
            y = pickle.load(fp)
        with open(os.path.join(self.root, 'classification_data_x.pkl'), 'rb') as fp:
            x = pickle.load(fp)

        return x,y


    def create_slic(self, imgs, nsegments=50, compactness=50,sigma=1):
        """
        imgs :: [nb, nch, nr, nc]
        """
        segs = []
        ulabs = []
        nb, nch, nr, nc = imgs.size()
        for bi in range(nb):
            image = imgs[bi]
            seg = segmentation.slic(normImg(image).cpu().permute(1,2,0).numpy(),
                                    n_segments = nsegments, compactness=compactness, sigma = sigma)
            unq2 = numpy.unique(seg)
            segs.append(seg)
            unq = numpy.zeros(nsegments, dtype=int) + 999
            unq[:len(unq2)] = unq2
            ulabs.append(unq)
        segs = torch.from_numpy(numpy.array(segs)).view(nb, nr, nc)
        ulabs = torch.from_numpy(numpy.array(ulabs)).view(nb, nsegments)
        nmax = (ulabs != 999).long().sum(1).max()
        ulabs = ulabs.narrow(1,0,nmax)

        return segs, ulabs
