import builtins
import torch
from datasets import omniglot
import torchvision
import torchvision.transforms as transforms
from PIL import Image
import os.path
import pickle
import ipdb
import numpy
numpy.random.seed(1234)  # for reproducibility
from datasets.dset import VOCSegmentation
# LAMBDA FUNCTIONS
filenameToPILImage = lambda x: Image.open(x).convert('L')
PiLImageResize = lambda x: x.resize((args.nr,args.nc))
#np_reshape = lambda x: numpy.reshape(numpy.array(x, dtype=float), (args.nr, args.nc, args.nch))
np_reshape = lambda x: numpy.reshape(numpy.array(x, dtype=float), (args.nr, args.nc, 1))
np_reshape3 = lambda x: numpy.reshape(numpy.array(x, dtype=float), (args.nr, args.nc, 3))
np_reshapef = lambda x: 255-numpy.reshape(numpy.array(x, dtype=float), (args.nr, args.nc, 1))
np_reshapef3 = lambda x: 255-numpy.reshape(numpy.array(x, dtype=float), (args.nr, args.nc, 3))


class MNISTClassificationDataset():
    def __init__(self, dataroot, batch_size = 100):
        if not os.path.isfile(os.path.join(dataroot,'data_class.npy')):
            x = torchvision.datasets.MNIST(dataroot, train=True, download=True,
                                    transform=transforms.Compose([PiLImageResize,
                                                                  np_reshapef]))
            tmp = numpy.array([y[0] for y in x])
            del x
            self.mean = numpy.mean(tmp)
            self.std = numpy.std(tmp)

            numpy.save(os.path.join(dataroot, 'data_class.npy'), (self.mean, self.std))


        (self.mean, self.std) = numpy.load(os.path.join(dataroot, 'data_class.npy'))
        self.batch_size = batch_size
        self.nClasses = 10

        tform = transforms.Compose([PiLImageResize,
                                    np_reshapef,
                                    transforms.ToTensor(),
                                    #transforms.Normalize((self.mean,), (self.std,)),

                                    ])
        self.x_train = torchvision.datasets.MNIST(dataroot, train=True, download=True,
                                      transform=tform)
        shuf = numpy.arange(0,len(self.x_train.train_labels))
        numpy.random.shuffle(shuf)
        shuf = torch.LongTensor(shuf)
        self.x_train.train_data = self.x_train.train_data[shuf[:-len(shuf)//10]]
        self.x_train.train_labels = self.x_train.train_labels[shuf[:-len(shuf)//10]]

        self.x_val = torchvision.datasets.MNIST(dataroot, train=True, download=True,
                                                transform=tform)
        self.x_val.train_data = self.x_val.train_data[shuf[-len(shuf)//10:]]
        self.x_val.train_labels = self.x_val.train_labels[shuf[-len(shuf)//10:]]


        self.x_test = torchvision.datasets.MNIST(dataroot, train=False, download=True,
                                                 transform=tform)


        self.indexes = {"train": 0, "val": 0, "test": 0}

        dsamp = lambda x: x #torch.utils.data.DataLoader(x, batch_size=batch_size, shuffle=True, drop_last=True).__iter__()
        self.datasets = {"train": dsamp(self.x_train),
                         "val": dsamp(self.x_val),
                         "test": dsamp(self.x_test)}

    def normalization(self):
        """
        Normalizes our data, to have a mean of 0 and sdt of 1
        """
        self.mean = numpy.mean(self.x_train)
        self.std = numpy.std(self.x_train)
        self.max = numpy.max(self.x_train)
        self.min = numpy.min(self.x_train)
        print("train_shape", self.x_train.shape, "test_shape", self.x_test.shape, "val_shape", self.x_val.shape)
        #print("before_normalization", "mean", self.mean, "max", self.max, "min", self.min, "std", self.std)
        self.x_train = (self.x_train - self.mean) / self.std
        self.x_val = (self.x_val - self.mean) / self.std
        self.x_test = (self.x_test - self.mean) / self.std
        #self.mean = numpy.mean(self.x_train)
        #self.std = numpy.std(self.x_train)
        #self.max = numpy.max(self.x_train)
        #self.min = numpy.min(self.x_train)

        print("after_normalization", "mean", self.mean, "max", self.max, "min", self.min, "std", self.std)


    def get_batch(self,str_type, rotate_flag = False):

        """
        Get next batch
        :return: Next batch
        """
        xs, ys = next(torch.utils.data.DataLoader(self.datasets[str_type], batch_size=self.batch_size,
                                                  shuffle=True, drop_last=True).__iter__())
        if False: #rotate_flag:
            k = int(numpy.random.uniform(low=0, high=4))
            xs = self.__rotate_batch(xs, k)
        return xs,ys


    def __rotate_data(self, image, k):
        """
        Rotates one image by self.k * 90 degrees counter-clockwise
        :param image: Image to rotate
        :return: Rotated Image
        """

        return numpy.rot90(image, k)


    def __rotate_batch(self, batch_images, k):
        """
        Rotates a whole image batch
        :param batch_images: A batch of images
        :param k: integer degree of rotation counter-clockwise
        :return: The rotated batch of images
        """
        batch_size = len(batch_images)
        for i in numpy.arange(batch_size):
            batch_images[i] = torch.from_numpy(self.__rotate_data(batch_images[i], k))
        return batch_images
