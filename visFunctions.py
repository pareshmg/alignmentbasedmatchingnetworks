import torch
from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg, txtImg
import builtins
import visdom
import numpy
import torch.nn.functional as F
from torch.autograd import Variable, Function
from models.DistanceNetwork import *
from skimage.future import graph
from skimage.segmentation import mark_boundaries
import networkx

def addImg(visImgs, label, img):
    if label not in visImgs: visImgs[label] = []
    visImgs[label].append(img)


def debugModelSLIC(obj_Builder):
    model = obj_Builder.nnet
    dataset = obj_Builder.data
    VIS = visdom.Visdom(env=('main' if builtins.cargs.visMain else builtins.cargs.log_base ), use_incoming_socket=False)
    VIS_wins = {}
    visImgs = {}

    if True: ## override slic
        ##################################################
        ### make slic superpixels more round
        target_image = model.forVis['target_image'] # [nb, nt, nch, nr, nc]
        support_set_images = model.forVis['support_set_images']
        nt, ns = target_image.size(1), support_set_images.size(1)
        imgs = torch.cat([target_image, support_set_images], dim=1)
        segs, ulabs = dataset.create_slic(imgs, nsegments=50)
        segs = segs.cuda()
        ulabs = ulabs.cuda()
        rags = dataset.make_rag(imgs, segs, ulabs)

        model.forVis['target_segs'] = segs.narrow(1,0,nt)
        model.forVis['support_segs'] = segs.narrow(1,nt,ns)

        model.forVis['target_rag'] = rags.narrow(1,0,nt)
        model.forVis['support_rag'] = rags.narrow(1,nt,ns)

    support_rag = Variable(model.forVis['support_rag'], requires_grad=False) # [nb, ns, nsegs, ndim, nr, nc]
    target_rag = Variable(model.forVis['target_rag'], requires_grad=False) # [nb, nt, nsegt, ndim, nr, nc]
    support_set_images = model.forVis['support_set_images']
    target_image = model.forVis['target_image']

    support_match = (model.forVis['support_set_labels_one_hot'].max(2)[1] == model.forVis['target_label']).max(1)[1]

    if True:
        ##################################################
        ### show slic
        nb, ns, nsegs, nch, nr, nc = support_rag.size()
        nb, nt, nsegt, nch, nr, nc = target_rag.size()

        tmp = target_rag.sum(2).unsqueeze(2)
        target_rag = torch.cat([tmp, target_rag, tmp - target_rag], dim=2)
        tmp = support_rag.sum(2).unsqueeze(2)
        support_rag = torch.cat([tmp, support_rag, tmp - support_rag], dim=2)

        support_rag_enc = model.encoders[0](support_rag.view(nb * ns * (2*nsegs+1), nch, nr, nc))[-1].view(nb, ns, 2*nsegs+1, -1) # [nb, ns, 2*nsegs+1, ndim]

        target_rag_enc = model.encoders[0](target_rag.view(nb * nt * (2*nsegt+1), nch, nr, nc))[-1].view(nb, nt, 2*nsegt+1, -1) # [nb, nt, 2*nsegt+1, ndim]

        ndim = target_rag_enc.size(3)

        q = CosineSimilaritiesPerDim()(target_rag_enc.select(2,0).permute(0,2,1),
                                       support_rag_enc.select(2,0).permute(0,2,1)) # [nb, ndim, nt*nsegt, ns*nsegs]

        #diffFn = lambda a,b: CosineDistance()(a.unsqueeze(2), b.unsqueeze(2)).squeeze().data[0]
        diffFn = lambda a,b: torch.abs(F.sigmoid(a).log() - F.sigmoid(b).log()).data.sum()
        # diffFn = lambda a,b: (a-b).pow(2).sum(1).data[0]
        def getG(fI, fIl, fI_l):
            """
            all inps: [nb, nt, nsegt, ndim]
            """
            #return torch.abs(fI - (fIl + fI_l))
            #return torch.pow(fI - (fIl + fI_l), 2)
            if False:
                #res = torch.pow(fI - (fIl + fI_l), 2)
                #res = torch.abs(fI - (fIl + fI_l)) * 5
                res = torch.abs(F.sigmoid(fI).log() - F.sigmoid(fI_l).log()) * 5
                rmean = (res - res.max(2,keepdim=True)[0]).exp()
                return rmean
            if True:
                #res = torch.abs(fI - (fIl + fI_l))
                res = torch.abs(F.sigmoid(fI).log() - F.sigmoid(fI_l).log()) * 5
                res = F.softmax(res, dim=2)
                return res


        target_g = getG(target_rag_enc.narrow(2,0,1),
                        target_rag_enc.narrow(2,1,nsegt),
                        target_rag_enc.narrow(2,nsegt+1, nsegt)) # [nb, nt, nsegt, ndim]
        support_g = getG(support_rag_enc.narrow(2,0,1),
                         support_rag_enc.narrow(2,1,nsegt),
                         support_rag_enc.narrow(2,nsegt+1, nsegt)) # [nb, ns, nsegs, ndim]
        ## per dim, find the most important SLICs
        target_wt = target_rag.narrow(2,1,nsegt) # [nb, nt, nsegt, nch, nr, nc]
        target_wt = target_wt.unsqueeze(3)# [nb, nt, nsegt, 1, nch, nr, nc]
        target_wt = target_wt * target_g.unsqueeze(4).unsqueeze(5).unsqueeze(6) # [nb, nt, nsegt, ndim, nch, nr, nc]
        target_wt = target_wt.sum(2).narrow(3,0,3) # [nb, nt, ndim, nch, nr, nc]

        visImgs['im_target_g'] = []
        for bi in range(target_wt.size(0)):
            for ti in range(target_wt.size(1)):
                print(target_wt[bi,ti,0].size())
                visImgs['im_target_g'].append(normImg(hstackImgs([padImg(target_image[bi,ti])] +
                                                                 [padImg(target_wt[bi,ti,di]) for di in range(target_wt.size(2))])))


        support_wt = support_rag.narrow(2,1,nsegt) # [nb, nt, nsegt, nch, nr, nc]
        support_wt = support_wt.unsqueeze(3)# [nb, nt, nsegt, 1, nch, nr, nc]
        support_wt = support_wt * support_g.unsqueeze(4).unsqueeze(5).unsqueeze(6) # [nb, nt, nsegt, ndim, nch, nr, nc]
        support_wt = support_wt.sum(2).narrow(3,0,3) # [nb, nt, ndim, nch, nr, nc]

        visImgs['im_support_g'] = []
        for bi in range(support_wt.size(0)):
            for ti in range(support_wt.size(1)):
                visImgs['im_support_g'].append(normImg(hstackImgs([padImg(support_set_images[bi,ti])] +
                                                                  [padImg(support_wt[bi,ti,di]) for di in range(support_wt.size(2))])))


        ## show individual rag components
        visImgs['target_rag'] = []
        target_rag = model.forVis['target_rag'] # [nb, nt, nsegt, ndim, nr, nc]
        for bi in range(target_rag.size(0)):
            for segi in range(target_rag.size(2)):
                img = hstackImgs([txtImg(target_rag[bi,0,segi,:3], str(segi))] + [
                    target_rag[bi,ti,segi,:3] for ti in range(target_rag.size(1))
                ])
                visImgs['target_rag'].append(normImg(img))

        if True:
            ##################################################
            ### test neighbor SLIC for f(A) + f(B) vs f(A+B): target
            for ti in range(target_rag.size(1)):
                resNbr = [] # superpixel vs nbr
                resMatch = [] # superpixel vs mean(full img , full img match)
                resSelf = [] # superpixel vs full img
                for bi in range(target_rag.size(0)):
                    segs = model.forVis['target_segs'][bi,ti]
                    img = model.forVis['target_image'][bi,ti]
                    imgS = model.forVis['support_set_images'][bi, support_match[bi]]
                    fI0 = model.encoders[0](Variable(img.unsqueeze(0), requires_grad=False))[-1].squeeze(3).squeeze(2)
                    fI0S = model.encoders[0](Variable(imgS.unsqueeze(0), requires_grad=False))[-1].squeeze(3).squeeze(2)
                    adjGraph = graph.RAG(segs.cpu().numpy())
                    maxadj = max([len(adjGraph[x]) for x in adjGraph.nodes])
                    print([len(adjGraph[x]) for x in adjGraph.nodes])
                    for segi in adjGraph.nodes:
                        iA = ((segs == int(segi)).float().unsqueeze(0)  * img).unsqueeze(0) # [1, nch, nr, nc]
                        fA = model.encoders[0](Variable(iA, requires_grad=False))[-1].squeeze(3).squeeze(2)
                        resSelf.append((diffFn(fA, fI0) , bi, segi, None))
                        resMatch.append((diffFn(fA, (fI0+fI0S)/2), bi, segi, None))
                        for nbri in adjGraph[segi].keys():
                            iB = ((segs == int(nbri)).float().unsqueeze(0)  * img).unsqueeze(0) # [1, nch, nr, nc]
                            iAB = iA + iB
                            fB = model.encoders[0](Variable(iB, requires_grad=False))[-1].squeeze(3).squeeze(2)
                            fAB = model.encoders[0](Variable(iAB, requires_grad=False))[-1].squeeze(3).squeeze(2)

                            diff = diffFn((fA+fB), fAB)
                            resNbr.append((diff, bi,segi, nbri))

                resNbr.sort(key=lambda x: x[0])
                resMatch.sort(key=lambda x: x[0])
                resSelf.sort(key=lambda x: x[0])
                for res, lab in zip([resNbr, resMatch, resSelf], ['nbr', 'match','self']):
                    resImg = []
                    for pi, (diff, bi, segi, nbri) in enumerate(res):
                        segs = model.forVis['target_segs'][bi,ti]
                        img = normImg(model.forVis['target_image'][bi,ti])
                        adjGraph = graph.RAG(segs.cpu().numpy())
                        iA = ((segs == int(segi)).float().unsqueeze(0)  * img) # [nch, nr, nc]
                        nodeMap = {k:k for k in adjGraph.nodes}

                        if nbri is not None:
                            iB = ((segs == int(nbri)).float().unsqueeze(0)  * img) # [nch, nr, nc]
                            iC = iA + iB
                            iD = iA * 0

                            nodeMap[nbri] = segi
                            for (d2, bi2, segi2, nbri2) in res[pi+1:]:
                                if bi2 != bi: continue
                                kold = nodeMap[nbri2]
                                nodeMap[nbri2] = nodeMap[segi2]
                                for k in nodeMap:
                                    if nodeMap[k] == nbri2 or nodeMap[k] == kold:
                                        nodeMap[k] = nodeMap[nbri2]
                            segs2 = segs.clone()
                            for k in nodeMap:
                                k = int(k)
                                v = int(nodeMap[k])
                                a = (segs2 == k).long()
                                segs2 = a* v + segs2 * (1-a)
                            img2 = normImg(img.cpu()).permute(1,2,0).numpy()
                            islic = torch.from_numpy(mark_boundaries(img2, segs2.cpu().numpy(),  color=[1.0,1.0,0.1])).permute(2,0,1).float().cuda()
                            iD = ((segs2 == int(nodeMap[segi])).float().unsqueeze(0)  * img)

                            img = vstackImgs([
                                txtImg(target_image[bi,ti], str(diff)),
                                iA, iB, iC, iD, islic
                            ])
                        else:
                            for (d2, bi2, segi2, _) in res[pi:]:
                                if bi2 != bi: continue
                                nodeMap[segi2] = segi
                            for k in nodeMap:
                                k = int(k)
                                v = int(nodeMap[k])
                                a = (segs2 == k).long()
                                segs2 = a* v + segs2 * (1-a)

                            img2 = normImg(img.cpu()).permute(1,2,0).numpy()
                            islic = torch.from_numpy(mark_boundaries(img2, segs2.cpu().numpy(),  color=[1.0,1.0,0.1])).permute(2,0,1).float().cuda()
                            iD = ((segs2 == int(segi)).float().unsqueeze(0)  * img)

                            img = vstackImgs([padImg(x) for x in [
                                txtImg(target_image[bi,ti], str(diff)),
                                iA, iD, islic
                            ]])

                        resImg.append(normImg(img))
                    resImg = hstackImgs(resImg)
                    addImg(visImgs, 'im_nbr_join_{}_target'.format(lab), resImg)

        if True:
            ##################################################
            ### test neighbor SLIC for f(A) + f(B) vs f(A+B) : support
            support_image = support_set_images
            visImgs['im_nbr_join_support'] = []
            visImgs['im_nbr_subtract_support'] = []
            for ti in range(support_rag.size(1)):
                res = [] # combine adjacent pairs and check diff of f(A)+f(B) vs f(A+B)
                res2 = [] # just check F(I) vs F(I-A)
                for bi in range(support_rag.size(0)):
                    segs = model.forVis['support_segs'][bi,ti]
                    img = model.forVis['support_set_images'][bi,ti]
                    adjGraph = graph.RAG(segs.cpu().numpy())
                    maxadj = max([len(adjGraph[x]) for x in adjGraph.nodes])
                    print([len(adjGraph[x]) for x in adjGraph.nodes])
                    for segi in adjGraph.nodes:
                        mseg = (segs == int(segi)).float().unsqueeze(0)
                        imA = ((1-mseg) * img).unsqueeze(0)
                        im0 = img.unsqueeze(0)
                        fmA = model.encoders[0](Variable(imA, requires_grad=False))[-1].squeeze(3).squeeze(2)
                        fI0 = model.encoders[0](Variable(im0, requires_grad=False))[-1].squeeze(3).squeeze(2)
                        diff2 = diffFn(fmA.unsqueeze(2), fI0.unsqueeze(2))
                        res2.append((diff, bi, segi))
                        for nbri in adjGraph[segi].keys():
                            mnbr = (segs == int(nbri)).float().unsqueeze(0)
                            iA = (mseg  * img).unsqueeze(0) # [1, nch, nr, nc]
                            iB = (mnbr  * img).unsqueeze(0) # [1, nch, nr, nc]
                            iAB = iA + iB

                            iA2 = iA + (1-mseg) * 0.5
                            iB2 = iA + (1-mnbr) * 0.5
                            iAB2 = iAB + (1-mseg - mnbr) * 0.5

                            iA3 = iA - (1-mseg) * 0.5
                            iB3 = iA - (1-mnbr) * 0.5
                            iAB3 = iAB - (1-mseg - mnbr) * 0.5


                            fA = model.encoders[0](Variable(iA, requires_grad=False))[-1].squeeze(3).squeeze(2)
                            fB = model.encoders[0](Variable(iB, requires_grad=False))[-1].squeeze(3).squeeze(2)
                            fAB = model.encoders[0](Variable(iAB, requires_grad=False))[-1].squeeze(3).squeeze(2)

                            fA2 = model.encoders[0](Variable(iA2, requires_grad=False))[-1].squeeze(3).squeeze(2)
                            fB2 = model.encoders[0](Variable(iB2, requires_grad=False))[-1].squeeze(3).squeeze(2)
                            fAB2 = model.encoders[0](Variable(iAB2, requires_grad=False))[-1].squeeze(3).squeeze(2)

                            fA3 = model.encoders[0](Variable(iA3, requires_grad=False))[-1].squeeze(3).squeeze(2)
                            fB3 = model.encoders[0](Variable(iB3, requires_grad=False))[-1].squeeze(3).squeeze(2)
                            fAB3 = model.encoders[0](Variable(iAB3, requires_grad=False))[-1].squeeze(3).squeeze(2)

                            #diff = (fAB - (fA+fB)).pow(2).sum(1).data[0]
                            #diff2 = (fAB2 - (fA2+fB2)).pow(2).sum(1).data[0]
                            #diff3 = (fAB3 - (fA3+fB3)).pow(2).sum(1).data[0]
                            diff = diffFn((fA+fB).unsqueeze(2), fAB.unsqueeze(2))
                            diff2 = diffFn((fA2+fB2).unsqueeze(2), fAB2.unsqueeze(2))
                            diff3 = diffFn((fA3+fB3).unsqueeze(2), fAB3.unsqueeze(2))

                            diff = max([diff, diff2, diff3])
                            res.append((diff, bi,segi, nbri))


                res.sort(key=lambda x: x[0])
                res2.sort(key=lambda x: x[0])

                resImg = []
                for pi, (diff, bi, segi, nbri) in enumerate(res):
                    segs = model.forVis['support_segs'][bi,ti]
                    img = normImg(model.forVis['support_set_images'][bi,ti])
                    adjGraph = graph.RAG(segs.cpu().numpy())
                    iA = ((segs == int(segi)).float().unsqueeze(0)  * img) # [nch, nr, nc]
                    iB = ((segs == int(nbri)).float().unsqueeze(0)  * img) # [nch, nr, nc]
                    iC = iA + iB
                    iD = iA * 0

                    nodeMap = {k:k for k in adjGraph.nodes}
                    nodeMap[nbri] = segi
                    for (d2, bi2, segi2, nbri2) in res[pi+1:]:
                        if bi2 != bi: continue
                        kold = nodeMap[nbri2]
                        nodeMap[nbri2] = nodeMap[segi2]
                        for k in nodeMap:
                            if nodeMap[k] == nbri2 or nodeMap[k] == kold:
                                nodeMap[k] = nodeMap[nbri2]
                    segs2 = segs.clone()
                    for k in nodeMap:
                        k = int(k)
                        v = int(nodeMap[k])
                        a = (segs2 == k).long()
                        segs2 = a* v + segs2 * (1-a)
                    img2 = normImg(img.cpu()).permute(1,2,0).numpy()
                    islic = torch.from_numpy(mark_boundaries(img2, segs2.cpu().numpy(),  color=[1.0,1.0,0.1])).permute(2,0,1).float().cuda()
                    iD = ((segs2 == int(nodeMap[segi])).float().unsqueeze(0)  * img)

                    img = hstackImgs([
                        txtImg(support_image[bi,ti], str(diff)),
                        iA, iB, iC, iD, islic
                    ])
                    resImg.append(normImg(img))
                resImg = vstackImgs(resImg)
                visImgs['im_nbr_join_support'].append(resImg)

                resImg = []
                for pi, (diff, bi, segi) in enumerate(res2):
                    img = normImg(model.forVis['support_set_images'][bi,ti])
                    segs = model.forVis['support_segs'][bi,ti]
                    iA = ((segs == int(segi)).float().unsqueeze(0)  * img) # [nch, nr, nc]
                    for (_,bi2,segi2) in res2[pi:]:
                        k = int(segi2)
                        v = 999
                        a = (segs == k).long()
                        segs = a* v + segs * (1-a)
                    iD = ((segs == 999).float().unsqueeze(0)  * img)

                    resImg.append(normImg(hstackImgs([
                        txtImg(img, str(diff)),
                        img-iA,
                        iA,
                        iD
                    ])))
                resImg = vstackImgs(resImg)
                visImgs['im_nbr_subtract_support'].append(resImg)

            visImgs['im_nbr_join_support'] = [hstackImgs(visImgs['im_nbr_join_support'])]
            visImgs['im_nbr_subtract_support'] = [hstackImgs(visImgs['im_nbr_subtract_support'])]


        if True:
            ##################################################
            ### test the guidance provided by a support img on train img
            pass




        ## display
        for k in visImgs:
            if len(visImgs[k]) == 0: continue
            im = vstackImgs(visImgs[k]) # [nr, nc, nk]
            VIS_wins[k] = VIS.image(vstackImgs(visImgs[k]).permute(2,0,1).cpu().numpy(),
                                    opts = {'caption':k, 'title':k},
                                    **({'win':VIS_wins[k]} if k in VIS_wins else {}))
        VIS.save([VIS.env])

        if cargs.test:
            import ipdb; ipdb.set_trace()


        """
        combineTs = [(0,[0,1,2,5,6,12,13,16,22,23,24,27,28,29]), (0,[3,4,7,8,9,10,11,14,15,17,18,19,20,21,25,26]), (0, list(range(30)))] # for 3

        combineTs = [(0,[0,1,2,3,4,5,10,15,17,20,25,26,27,28,29,31,32]), (0,[6,7,8,9,11,12,13,14,16,18,19,21,22,23,24,30]), (0, list(range(33)))] # for 9
        """

def debugModel2(obj_Builder):
    model = obj_Builder.nnet
    VIS = visdom.Visdom(env=('main' if builtins.cargs.visMain else builtins.cargs.log_base ), use_incoming_socket=False)

    support_rag = Variable(model.forVis['support_rag'], requires_grad=False)
    target_rag = Variable(model.forVis['target_rag'], requires_grad=False)

    combineSs = [list(range(support_rag.size(2))) for si in range(support_rag.size(1))]
    combineTs = []
    VIS_wins = {}
    visImgs = {}

    while True:
        used = {}
        support_rag = Variable(model.forVis['support_rag'], requires_grad=False)
        new_support_rag = [[] for i in range(support_rag.size(1))]
        for si, lsis in enumerate(combineSs):
            tmp = []
            for lsi in lsis:
                used[(si, lsi)] = True
                tmp.append(support_rag[:, si, lsi])
            tmp = sum(tmp)
            new_support_rag[si].append(tmp)

        new_support_rag = torch.stack([torch.stack(x, dim=1) for x in new_support_rag], dim=1)

        used = {}
        target_rag = Variable(model.forVis['target_rag'], requires_grad=False)
        new_target_rag = [[] for i in range(target_rag.size(1))]
        for si in range(target_rag.size(1)):
            for lsi in range(target_rag.size(2)):
                new_target_rag[si].append(target_rag[:,si,lsi])
        for (si, lsis) in combineTs:
            tmp = []
            for lsi in lsis:
                used[(si, lsi)] = True
                tmp.append(target_rag[:, si, lsi])
            tmp = sum(tmp)
            new_target_rag[si].append(tmp)


        new_target_rag = torch.stack([torch.stack(x, dim=1) for x in new_target_rag], dim=1)

        support_rag = new_support_rag
        target_rag = new_target_rag

        ## without any combination

        support_rag = support_rag.sum(2).unsqueeze(2)

        nb, ns, nsegs, ndim, nr, nc = support_rag.size()
        support_rag_enc = model.encoders[0](support_rag.view(nb * ns * nsegs, ndim, nr, nc))[-1].view(nb, ns * nsegs, -1)

        nb, nt, nsegt, ndim, nr, nc = target_rag.size()
        target_rag_enc = model.encoders[0](target_rag.view(nb * nt * nsegt, ndim, nr, nc))[-1].view(nb, nt * nsegt, -1)

        ## find best match

        similarities = model.dn.sim(target_rag_enc.permute(0,2,1), support_rag_enc.permute(0,2,1)) # [nb, nt*nsegt, ns*nsegs]
        similarities = torch.stack(similarities, dim=1)
        similarities = similarities.view(nb, nt, nsegt, ns, nsegs) # [nb, nt, nsegt, ns, nsegs]
        similarities,_ = similarities.max(4) # [nb, nt, nsegt, ns]

        ## show the similarity grid sorted by the segment
        _, indices = torch.sort(similarities, dim=2, descending=True) # [nb, nt, nsegt, ns]
        indices = indices.data

        support_set_images = model.forVis['support_set_images']
        visImgs['similarity_grid'] = [normImg(hstackImgs([support_set_images[0,si] for si in range(support_rag.size(1))]))]
        for bi in range(indices.size(0)):
            for ti in range(indices.size(1)):
                for segi in range(indices.size(2)):
                    img = hstackImgs([target_rag[bi, ti, indices[bi,ti,segi,si],:3] for si in range(indices.size(3))])
                    visImgs['similarity_grid'].append(normImg(img))

        ## show individual rag components
        visImgs['target_rag'] = []
        for bi in range(target_rag.size(0)):
            for segi in range(target_rag.size(2)):
                img = hstackImgs([txtImg(target_rag[bi,0,segi,:3], str(segi))] + [
                    target_rag[bi,ti,segi,:3] for ti in range(target_rag.size(1))
                ])
                visImgs['target_rag'].append(normImg(img))

        visImgs['support_rag'] = []
        support_rag = model.forVis['support_rag']
        for bi in range(support_rag.size(0)):
            for segi in range(support_rag.size(2)):
                img = hstackImgs([txtImg(support_rag[bi,0,segi,:3], str(segi))] + [
                    support_rag[bi,ti,segi,:3] for ti in range(support_rag.size(1))
                ])
                visImgs['support_rag'].append(normImg(img))


        ## display
        for k in visImgs:
            im = vstackImgs(visImgs[k]) # [nr, nc, nk]
            VIS_wins[k] = VIS.image(vstackImgs(visImgs[k]).permute(2,0,1).cpu().numpy(),
                                    opts = {'caption':k},
                                    **({'win':VIS_wins[k]} if k in VIS_wins else {}))
        VIS.save([VIS.env])
        import ipdb; ipdb.set_trace()
        if combineSs is None:
            break


        """
        combineTs = [(0,[0,1,2,5,6,12,13,16,22,23,24,27,28,29]), (0,[3,4,7,8,9,10,11,14,15,17,18,19,20,21,25,26]), (0, list(range(30)))] # for 3

        combineTs = [(0,[0,1,2,3,4,5,10,15,17,20,25,26,27,28,29,31,32]), (0,[6,7,8,9,11,12,13,14,16,18,19,21,22,23,24,30]), (0, list(range(33)))] # for 9
        """

def visModel(obj_Builder, plts = {}, ncat=1):
    VIS = visdom.Visdom(env=('main' if builtins.cargs.visMain else builtins.cargs.log_base ), use_incoming_socket=False)
    VIS.close(win=None)
    model = obj_Builder.nnet
    dataset = obj_Builder.data
    VIS_wins = {}
    visImgs = {}
    for ncati in range(ncat):
        ## clear
        if (cargs.skipRecon): continue
        obj_Builder.nnet.vis(False)
        obj_Builder.nnet.vis(True)

        obj_Builder.run_epoch('test', 1, False)

        if 'target_image' not in model.forVis:
            model.vis(False)
            break

        ## set batch size to 1 for vis
        support_set_images = model.forVis['support_set_images'] # [nb, ns, nk, nr, nc]
        support_set_labels_one_hot = model.forVis['support_set_labels_one_hot']
        target_image = model.forVis['target_image'] # [nb, 1, nk, nr, nc]
        target_label = model.forVis['target_label']

        recon = []
        ptMapping = []

        nb, ns, nk, nr, nc = support_set_images.size()

        recon.append(normImg(hstackImgs([support_set_images[0,0]*0-0.5] + [support_set_images[0,i] for i in range(support_set_images.size(1))])))


        if 'target_image' in model.forVis:
            # [nb, 1, nk, nr, nc]
            tgt = model.forVis['target_image']
            imgs = vstackImgs([hstackImgs([tgt[bi, ti] for ti in range(tgt.size(1))]) for bi in range(tgt.size(0)) ])
            addImg(visImgs, 'target_image', normImg(imgs))

        if 'support_set_images' in model.forVis:
            # [nb, 1, nk, nr, nc]
            sup = model.forVis['support_set_images']
            imgs = vstackImgs([hstackImgs([sup[bi, ti] for ti in range(sup.size(1))]) for bi in range(sup.size(0)) ])
            addImg(visImgs, 'support_set_images', normImg(imgs))


            support_match = (model.forVis['support_set_labels_one_hot'].max(2)[1] == model.forVis['target_label']).max(1)[1]
            imgs = vstackImgs([sup[bi, support_match[bi]] for bi in range(sup.size(0))])
            addImg(visImgs, 'support_match', normImg(imgs))


        rsampSup = model.forVis['rsampSup'] if 'rsampSup' in model.forVis else None
        rsampTgt = model.forVis['rsampTgt'] if 'rsampTgt' in model.forVis else None
        if 'pointSimilarities' in model.forVis and 'rsampTgt' in model.forVis:
            for i in numpy.arange(target_image.size(1)):
                similaritiesVis = model.forVis['pointSimilarities'][i]
                similarities = model.forVis['similarities'][i]
                preds = model.forVis['preds'][i]
                # un-one-hot encode preds
                preds = torch.matmul(preds.repeat(1,1,1), support_set_labels_one_hot.transpose(1,2)).squeeze(1)
                if builtins.cargs.open_set:
                    preds = preds.narrow(1,0,preds.size(1)-1)
                ##################################################
                ## visualize reconstructions
                # similaritiesVis :: #[nb, ntgt, ns, nsup]
                wt, sel = F.softmax(Variable(similaritiesVis, requires_grad=False), dim=3).data.max(3)


                destInd = rsampSup.index_select(0,sel.view(-1)).view(*sel.size()) # [nb, ntgt, ns]
                destInd = destInd.permute(0,2,1).unsqueeze(2).repeat(1,1,3,1)# [nb, ns, 3, ntgt]
                res = target_image[:,i,:,:,:].contiguous().view(nb, target_image.size(2), nr*nc).clone().repeat(1,3//target_image.size(2),1).unsqueeze(1) # [nb, 1, 3, nr*nc]

                dt = res.index_select(3,rsampTgt).repeat(1,ns,1,1) *  F.softmax(Variable(preds, requires_grad=False), dim=1).data.view(nb,ns, 1,1) # [nb, ns, 3, ntgt]

                res = res.fill_(-0.5).repeat(1,ns,1,1).scatter_(3, destInd, dt).view(nb,ns,3,nr,nc)
                #rescount = res.clone().zero_.scatter_add_(2, destInd, 1).view(ns,nk,nr,nc).clamp(min=1)
                #res = res/rescount
                res = vstackImgs([hstackImgs([normImg(target_image[bi,i].clone()) , normImg(hstackImgs([res[bi,ii] for ii in range(res.size(1)) ]))]) for bi in range(res.size(0))])
                recon.append(res)


                ##################################################
                ## visualize individual point recon
                j_ = 0
                r2 = []
                badtries = 0
                while j_ < 3:
                    supi = preds.max(1)[1][0]
                    j = numpy.random.randint(0,rsampTgt.size(0))
                    jtgt = rsampTgt[j]
                    jsup = rsampSup[sel[0,j, supi]]

                    rtgt = jtgt // nc
                    ctgt = jtgt % nc

                    rsup = jsup // nc
                    csup = jsup % nc


                    tgt = setupImg(normImg(target_image[:,i,:,:,:].clone())).repeat(1,3//target_image.size(2),1,1)
                    # tgt2 = setupImg(normImg(target_image[:,i,:,:,:].clone())).repeat(1,3,1,1).view(1,3,nr*nc)
                    # tgt2[:,:,jtgt-1:jtgt+2] = 0
                    # tgt2[:,0,jtgt-1:jtgt+2] = 1
                    # tgt2 = tgt2.view(3,nr,nc)

                    ## best support image
                    sup = setupImg(normImg(support_set_images[:,supi])).repeat(1,3//support_set_images.size(2),1,1)

                    ## prob dist
                    pd = F.softmax(Variable(similaritiesVis[:,j,supi,:], requires_grad=False), dim=1).data
                    sup2 = sup.select(1,0).clone().fill_(0).view(nb, nr*nc)
                    sup2.scatter_(1,rsampSup.repeat(nb,1), pd)
                    sup2 = setupImg(normImg(sup2.view(nb, 1, nr, nc).repeat(1,3,1,1)))
                    try:

                        tgt.narrow(1,0,1).narrow(2,rtgt-1,3).narrow(3,ctgt-1,3).fill_(1)
                        tgt.narrow(1,1,2).narrow(2,rtgt-1,3).narrow(3,ctgt-1,3).fill_(0)

                        sup.narrow(1,0,1).narrow(2,rsup-1,3).narrow(3,csup-1,3).fill_(1)
                        sup.narrow(1,1,2).narrow(2,rsup-1,3).narrow(3,csup-1,3).fill_(0)
                    except:
                        badtries += 1
                        if badtries < 4:
                            continue
                        else:
                            tgt.narrow(1,0,1).narrow(2,rtgt,1).narrow(3,ctgt,1).fill_(1)
                            tgt.narrow(1,1,2).narrow(2,rtgt,1).narrow(3,ctgt,1).fill_(0)

                            sup.narrow(1,0,1).narrow(2,rsup,1).narrow(3,csup,1).fill_(1)
                            sup.narrow(1,1,2).narrow(2,rsup,1).narrow(3,csup,1).fill_(0)

                            badtries = 0
                            # just display the image without doing the extra boundary

                    #r2.append(padImg(hstackImgs([tgt[0], sup[0], sup2[0]]), fill=[1,1,1]))
                    r2.append(padImg(hstackImgs([vstackImgs([tgt[bi] for bi in range(tgt.size(0))]),
                                                 vstackImgs([sup[bi] for bi in range(tgt.size(0))]),
                                                 vstackImgs([sup2[bi] for bi in range(tgt.size(0))])]), fill=[1,1,1]))
                    j_ += 1


                ptMapping.append(hstackImgs(r2))

            recon = vstackImgs(recon) if len(recon) > 0 else None
            ptMapping = vstackImgs(ptMapping) if len(ptMapping) > 0 else None
            if 'recon' not in visImgs: visImgs['recon'] = []
            visImgs['recon'].append(recon)
            if 'ptMapping' not in visImgs: visImgs['ptMapping'] = []
            visImgs['ptMapping'].append(ptMapping)

            if 'smask' in model.forVis:
                smask = model.forVis['smask'] #[nb, 1, ns, nsup]
                bi = 0
                rres = []
                print('going to viz {}'.format(support_set_images.size(0)))
                for bi in range(support_set_images.size(0)):
                    res = [normImg(hstackImgs([support_set_images[bi, 0]*0] + [support_set_images[bi, si] for si in range(ns)]))]
                    for i in range(len(smask)):
                        s = (support_set_images[bi] * 0).view(ns,nk,nr*nc)
                        s = s.scatter_(2,rsampSup.repeat(ns,nk,1), smask[i][bi,0].unsqueeze(1).repeat(1,nk,1)).view(ns,nk,nr,nc)
                        #s = s.exp()
                        print('smask min, max, mean', s.min(), s.max(), s.mean())
                        t = normImg(target_image[bi,i].clone())
                        res.append(hstackImgs([t] + [normImg(s[si]) for si in range(s.size(0))]))
                    rres.append(vstackImgs(res))
                smask = vstackImgs(rres)
                if 'smask' not in visImgs: visImgs['smask'] = []
                visImgs['smask'].append(smask)



            if 'tmask' in model.forVis:
                tmask = model.forVis['tmask'] #[nb, ntgt, ns, 1]
                tmask = [x.permute(0,3,2,1) for x in tmask] # [nb, 1, ns, ntgt]
                bi = 0
                rres = []
                for bi in range(support_set_images.size(0)):
                    res = [normImg(hstackImgs([support_set_images[bi, 0]*0] + [support_set_images[bi, si] for si in range(ns)]))]
                    for i in range(len(tmask)):
                        s = (support_set_images[bi] * 0).view(ns,nk,nr*nc)
                        s = s.scatter_(2,rsampTgt.repeat(ns,nk,1), tmask[i][bi,0].unsqueeze(1).repeat(1,nk,1)).view(ns,nk,nr,nc)
                        s = normImg(s)
                        print('tmask mean', s.min(), s.max(), s.mean())
                        t = normImg(target_image[bi,i].clone())
                        res.append(hstackImgs([t] + [s[si] for si in range(s.size(0))]))
                    rres.append(vstackImgs(res))
                tmask = vstackImgs(rres)
                if 'tmask' not in visImgs: visImgs['tmask'] = []
                visImgs['tmask'].append(tmask)

        if 'separation_mask' in model.forVis:
            addImg(visImgs, 'separation_mask', vstackImgs(model.forVis['separation_mask']))
        if 'separation_mask_support' in model.forVis:
            addImg(visImgs, 'separation_mask_support', vstackImgs(model.forVis['separation_mask_support']))

        for k in model.forVis:
            if k.startswith('im_'):
                addImg(visImgs, k, vstackImgs(model.forVis[k]))

        if False:
            ### Visualize the point distance distribution
            if 'pointSimilarities' in model.forVis:
                for i in numpy.arange(target_image.size(1)):
                    similaritiesVis = model.forVis['pointSimilarities'][i]
                    similarities = model.forVis['similarities'][i]
                    preds = model.forVis['preds'][i]
                    # un-one-hot encode preds
                    preds = torch.matmul(preds.repeat(1,1,1), support_set_labels_one_hot.transpose(1,2)).squeeze(1)
                    if builtins.cargs.open_set:
                        preds = preds.narrow(1,0,preds.size(1)-1)
                    ##################################################
                    ## visualize reconstructions
                    # similaritiesVis :: #[nb, ntgt, ns, nsup]
                    wt, sel = F.softmax(Variable(similaritiesVis, requires_grad=False), dim=3).data.max(3)

                    for j in numpy.arange(support_set_images.size(1)):
                        dists = similaritiesVis.select(2,j).max(2)[0] # [nb, ntgt]
                        for bi in range(dists.size(0)):
                            idx = preds.max(1)[1]
                            VIS_wins['pdist{}_{}_{}__{}'.format(bi,i,j,idx[bi] == j)] = VIS.histogram(dists[bi].cpu(),
                                                                                                  opts={'caption':'pdist{}_{}_{}__{}'.format(bi,i,j,idx[bi] == j), 'title':'pdist{}_{}_{}__{}'.format(bi,i,j,idx[bi] == j)})


        if True and ('pointSimilarities' in model.forVis):
            try:
                imgs_ = []
                for i in numpy.arange(target_image.size(1)):
                    similaritiesVis = model.forVis['pointSimilarities'][i]
                    similarities = model.forVis['similarities'][i]
                    preds = model.forVis['preds'][i]
                    # un-one-hot encode preds
                    preds = torch.matmul(preds.repeat(1,1,1), support_set_labels_one_hot.transpose(1,2)).squeeze(1)
                    if builtins.cargs.open_set:
                        preds = preds.narrow(1,0,preds.size(1)-1)
                    ##################################################
                    ## visualize reconstructions
                    # similaritiesVis :: #[nb, ntgt, ns, nsup]
                    wt, sel = F.softmax(Variable(similaritiesVis, requires_grad=False), dim=3 ).data.max(3)

                    imgs_ = []
                    for bi in range(target_image.size(0)):
                        supi = preds.max(1)[1][bi]
                        fg = (target_image[bi,i,0].view(-1) > 0)
                        if fg.long().sum() > (~fg).long().sum(): fg = ~fg
                        jtgt = rsampTgt[fg]
                        jsup = rsampSup.index_select(0,sel[bi,:,supi][fg])

                        xt = torch.stack([jtgt/cargs.nr, jtgt%cargs.nr, jtgt*0+1]).float()
                        xs = torch.stack([jsup/cargs.nr, jsup%cargs.nr, jsup*0+1]).float()

                        gAffine = torch.matmul(torch.matmul(xs, xt.transpose(0,1)),
                                               torch.matmul(xt, xt.transpose(0,1)).inverse()) # (xs * xt') * (xt * xt')^-1

                        xs_hat = torch.matmul(gAffine, xt)

                        imgs = [target_image[bi,i].clone()]
                        for frac in numpy.arange(0,1.01,0.1):
                            xs_new = (xs * frac) + (xs_hat * (1-frac))
                            xs_new = xs_new.round().clamp(min=0,max=cargs.nr-1).long()
                            xs_new = xs_new[0] * cargs.nr + xs_new[1]
                            # reconstruct
                            rec = target_image[bi,i].clone()
                            rec = rec.fill_(0)
                            rec = rec.view(-1)
                            rec.scatter_(0, xs_new, 1)
                            imgs.append(rec.view(cargs.nch, cargs.nr, cargs.nr))
                        imgs.append(support_set_images[bi,supi])

                        imgs = hstackImgs(imgs)
                        imgs_.append(imgs)

                addImg(visImgs, 'interp', normImg(vstackImgs(imgs_)))
            except:
                pass



        if 'expt_imgs' in model.forVis:
            # expt_imgs : [nb, ns, naff, nch, nr, nc]
            eb = model.forVis['expt_imgs']
            nb, ns, naff, nch, nr, nc = eb.size()
            imgs = vstackImgs([hstackImgs([hstackImgs([eb[bi, si, ai] for ai in range(naff)]) for si in range(ns)]) for bi in range(nb)])
            print(imgs.size())
            #imgs = padImg(normImg(vstackImgs(imgs)), fill=[0,0,0])
            imgs = normImg(imgs)
            addImg(visImgs, 'expt_imgs', imgs)

        if 'nbr_blocks' in model.forVis:
            # nbrs     : [ndir, nb, ns, nexpt, nch, mr, mc]
            # base     : [nb, nt+ns, nexpt, nch, mr, mc],
            # nbrs     : [ndir, nb, nt+ns, nexpt, ndim ]
            # nbrLabel : [ndir, nexpt] -> 1 if actually nbr 0 otherwise
            eb = model.forVis['nbr_blocks']
            base = model.forVis['base_blocks']
            nbrLabel = model.forVis['nbr_labels']
            ndir, nb, ns, nexpt, nch, mr, mc =  eb.size()
            for idir in range(ndir):
                imgs = []
                for ei in range(nexpt):
                    # dr, dc = [(0,-1), (0,1), (-1,0), (1,0)][idir]
                    timg = None
                    if idir == 0:
                        timg = torch.cat([eb[idir].select(2,ei),
                                          base.select(2,ei)], dim=4)
                    elif idir == 1:
                        timg = torch.cat([base.select(2,ei),
                                          eb[idir].select(2,ei)], dim=4)
                    elif idir == 2:
                        timg = torch.cat([eb[idir].select(2,ei),
                                          base.select(2,ei)], dim=3)
                    elif idir == 3:
                        timg = torch.cat([base.select(2,ei),
                                          eb[idir].select(2,ei)], dim=3)
                    l = nbrLabel[idir, ei]
                    timg = padImg(vstackImgs([ vstackImgs([ timg[bi, si] for si in range(ns)]) for bi in range(nb)]),
                                  fill = [l,0,0])
                    imgs.append(timg)
                addImg(visImgs, 'expt_blocks{}'.format(idir), normImg(hstackImgs(imgs)))



    if False: #isinstance(model, BitNet):
        ##################################################
        ## visualize masks
        msks = []
        for m in model.g.modules():
            if isinstance(m, BitMask):
                msks.append(hstackImgs([padImg(F.sigmoid(m.weight[i,0].unsqueeze(2)).data,
                                               padWidth=1,
                                               fill=[0.2,0,0]) for i in range(m.weight.size(0))]))
        msks = vstackImgs(msks)
        if len(plts['train']) == 0:
            plts['train'].append( (-1, {}))
        plts['train'][-1][1]['mmin'] = msks.min()
        plts['train'][-1][1]['mmax'] = msks.max()
        plts['train'][-1][1]['mmean'] = msks.mean()
        addImg(visImgs, 'bitmasks', msks)

        ##################################################
        ## visualize density over brushes
        brshEntropy = []
        for m in model.g.modules():
            if isinstance(m, Brush):
                entropy = F.softmax(m.weight, dim=1).data
                mxEntropy = math.log(entropy.size(1))/math.log(2)
                entropy = -(entropy * (entropy+1e-12).log()/math.log(2)).sum(1)  #nb

                brshEntropy.append(entropy.cpu()/mxEntropy)


        for i,b in enumerate(brshEntropy):
            plts['train'][-1][1]['H_b{}'.format(i+1)] = b

        ##################################################
        ## visualize density over brush0
        sig = []
        for m in model.g.modules():
            if isinstance(m, Brush0):
                sig.append(m.sigma.data.cpu())

        for i,b in enumerate(sig):
            plts['train'][-1][1]['Sigma_b{}'.format(i+1)] = b

        sig = []
        for m in model.g.modules():
            if isinstance(m, Brush0):
                sig.append(m.weight.data.squeeze(1).cpu())

        for i,b in enumerate(sig):
            plts['train'][-1][1]['Mean_b{}'.format(i+1)] = b


    for k in visImgs:
        im = vstackImgs(visImgs[k]) # [nr, nc, nk]
        VIS_wins[k] = VIS.image(vstackImgs(visImgs[k]).permute(2,0,1).cpu().numpy(),
                                opts = {'caption':k, 'title':k},
                                **({'win':VIS_wins[k]} if k in VIS_wins else {}))



    for dset in ['train', 'val', 'test']:
        if (dset in plts) and (len(plts[dset]) > 0):
            for k in plts[dset][-1][1]:
                X = torch.Tensor([x[0] for x in plts[dset] if k in x[1]])
                Y = [x[1][k] for x in plts[dset] if k in x[1]]
                if len(Y) > 0 and type(Y[0]) == torch.FloatTensor: Y = torch.stack(Y)
                else: Y = torch.Tensor(Y)
                VIS_wins['{}_{}'.format(dset, k)] = VIS.line(X = X,
                                                             Y = Y,
                                                             opts = {'title':'{}_{}'.format(dset, k)},
                                                             **({'update':'replace',
                                                                 'win':VIS_wins['{}_{}'.format(dset, k)]} if '{}_{}'.format(dset, k) in VIS_wins else {}))



    VIS.text(builtins.cargs.log_dir, opts={'title': 'log_dir'})
    VIS.text(str(builtins.cargs.start_time), opts={'title': 'start_time'})
    VIS.save([VIS.env])

    if cargs.test:
        if hasattr(dataset, 'create_slic'):
            debugModelSLIC(obj_Builder)
        import ipdb; ipdb.set_trace()
