##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from datasets import omniglotNShot
from option import *
#from experiments.OneShotBuilder import OneShotBuilder
from experiments.OneShotBuilder import OneShotBuilder
import tqdm
from logger import Logger


'''
:param batch_size: Experiment batch_size
:param classes_per_set: Integer indicating the number of classes per set
:param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
'''

# Experiment Setup
mbfact = 4
batch_size = //mbfact
fce = False
classes_per_set = 20
samples_per_class = 5
channels = 1
# Training setup
total_epochs = 500
total_train_batches = 1000 * mbfact
total_val_batches = 100 * mbfact
total_test_batches = 250 * mbfact
# Parse other options
args = Options().parse()
args.dataroot = '/home/pareshmg/data/OmniglotNShot'

LOG_DIR = args.log_dir + '/omniglot_{}_run-batchSize_{}-fce_{}-classes_per_set{}-samples_per_class{}-channels{}' \
    .format(args.model, batch_size,fce,classes_per_set,samples_per_class,channels)

# create logger
logger = Logger(LOG_DIR)

data = omniglotNShot.OmniglotNShotDataset(dataroot=args.dataroot, batch_size = batch_size,
                                            classes_per_set=classes_per_set,
                                            samples_per_class=samples_per_class)

obj_oneShotBuilder = OneShotBuilder(data, args.model)
obj_oneShotBuilder.build_experiment(batch_size, classes_per_set, samples_per_class, channels, fce)

best_val = 0



##################################################
### Train model on subset
def trainModel():
    with tqdm.tqdm(total=total_epochs) as pbar_e:
        for e in range(0, total_epochs):
            total_c_loss, total_accuracy = obj_oneShotBuilder.run_training_epoch(total_train_batches=total_train_batches)
            logger.debug("Epoch {}: train_loss: {}, train_accuracy: {}".format(e, total_c_loss, total_accuracy))

            total_val_c_loss, total_val_accuracy = obj_oneShotBuilder.run_validation_epoch(
                total_val_batches=total_val_batches)
            logger.debug("Epoch {}: val_loss: {}, val_accuracy: {}".format(e, total_val_c_loss, total_val_accuracy))

            logger.log_value('train_loss', total_c_loss)
            logger.log_value('train_acc', total_accuracy)
            logger.log_value('val_loss', total_val_c_loss)
            logger.log_value('val_acc', total_val_accuracy)
            print('next')

            if total_val_accuracy >= best_val:  # if new best val accuracy -> produce test statistics
                best_val = total_val_accuracy
                total_test_c_loss, total_test_accuracy = obj_oneShotBuilder.run_testing_epoch(
                    total_test_batches=total_test_batches)
                logger.debug("Epoch {}: test_loss: {}, test_accuracy: {}".format(e, total_test_c_loss, total_test_accuracy))
                logger.log_value('test_loss', total_test_c_loss)
                logger.log_value('test_acc', total_test_accuracy)

                save_checkpoint({
                    'epoch': e,
                    'state_dict': obj_oneShotBuilder.model.state_dict(),
                    'optimizer' : obj_oneShotBuilder.optimizer.state_dict(),
                    'test_loss': total_test_c_loss,
                    'test_acc':, total_test_accuracy,
                }, False)
                print('next')


            else:
                total_test_c_loss = -1
                total_test_accuracy = -1

            pbar_e.update(1)
            logger.step()


##################################################
### cross validate and obtain f1-measure
def validateModel():
    ## find the distance between target and template. If using (cosine) similarity,
    pass
