##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from datasets import omniglotNShot
from option import *
from experiments.OneShotBuilder import OneShotBuilder
import tqdm
from logger import Logger
import torch
import pickle
import os
import builtins
import numpy
'''
:param batch_size: Experiment batch_size
:param classes_per_set: Integer indicating the number of classes per set
:param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
'''
builtins.args, logger = Options().parse('CIFAR10',
                                nr=28,
                                nc=28,
                                nch=3,
                                # Experiment Setup
                                total_train_batches = 1000,
                                total_val_batches = 500,
                                total_test_batches = 1000,
                                layer_size = 64
)

data = omniglotNShot.CIFAR10NShotDataset(dataroot=args.dataroot,
                                         batch_size = args.batch_size,
                                         classes_per_set=args.classes_per_set,
                                         samples_per_class=args.samples_per_class)



logger.debug('Starting with classes_per_set: {}  samples_per_class: {}'.format(args.classes_per_set,
                                                                               args.samples_per_class))
logger.debug('Logging to directory: {}'.format(args.log_dir))


obj_oneShotBuilder = OneShotBuilder(data, args.model)
obj_oneShotBuilder.build_experiment(args.batch_size,
                                    args.mini_batch_size,
                                    args.classes_per_set,
                                    args.samples_per_class,
                                    args.nch,
                                    args.fce)


##################################################
### run
runExperiment(obj_oneShotBuilder, logger)
