##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from datasets.omniglotNShot import MiniImageNetNShotDataset
#from datasets.omniglotNShot import MiniImageNetSLICNShotDataset as MiniImageNetNShotDataset
from option import *
from experiments.OneShotBuilder import OneShotBuilder
import tqdm
import torch
import os
import builtins
import numpy

'''
:param batch_size: Experiment batch_size
:param classes_per_set: Integer indicating the number of classes per set
:param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
'''

builtins.cargs, logger = Options().parse('miniImagenet',
                                nr=84,
                                nc=84,
                                nch=3,
                                # Experiment Setup
                                # total_train_batches = 1,
                                # total_val_batches = 1,
                                # total_test_batches = 2,
                                total_train_batches = 100,
                                total_val_batches = 100,
                                #total_test_batches = 600,
                                total_test_batches = 600,
                                layer_size = 64
)


data = MiniImageNetNShotDataset(dataroot=cargs.dataroot,
                                batch_size = cargs.batch_size,
                                classes_per_set=cargs.classes_per_set,
                                samples_per_class=cargs.samples_per_class)

logger.debug('Starting with classes_per_set: {}  samples_per_class: {}'.format(cargs.classes_per_set,
                                                                               cargs.samples_per_class))
logger.debug('Logging to directory: {}'.format(cargs.log_dir))


obj_oneShotBuilder = OneShotBuilder(data, cargs.model)
obj_oneShotBuilder.build_experiment(cargs.batch_size,
                                    cargs.mini_batch_size,
                                    cargs.classes_per_set,
                                    cargs.samples_per_class,
                                    cargs.nch,
                                    cargs.fce)


##################################################
### run
runExperiment(obj_oneShotBuilder, logger)
