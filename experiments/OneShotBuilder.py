##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import builtins
import torch
import torch.backends.cudnn as cudnn
import tqdm
from models.AllModels import *
from torch.autograd import Variable
import scipy.stats
import numpy

class OneShotBuilder(object):

    def __init__(self, data, model, datacross=None):
        """
        Initializes an OneShotBuilder object. The OneShotBuilder object takes care of setting up our experiment
        and provides helper functions such as run_training_epoch and run_validation_epoch to simplify out training
        and evaluation procedures.
        :param data: A data provider class
        """
        self.data = data
        self.model = model
        self.datacross = datacross
        self.openSet = cargs.open_set

    def set_batch_size(self, batch_size, mini_batch_size):
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.data.batch_size = batch_size


    def build_experiment(self, batch_size, mini_batch_size, classes_per_set, samples_per_class, channels, fce):

        """
        :param batch_size: The experiment batch size
        :param classes_per_set: An integer indicating the number of classes per support set
        :param samples_per_class: An integer indicating the number of samples per class
        :param channels: The image channels
        :param fce: Whether to use full context embeddings or not
        :return: a matching_network object, along with the losses, the training ops and the init op
        """
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class
        self.keep_prob = torch.FloatTensor(1)
        self.nnet = eval(self.model)(batch_size=batch_size,
                                            keep_prob=self.keep_prob, num_channels=channels,
                                            fce=fce,
                                            num_classes_per_set=classes_per_set,
                                            num_samples_per_class=samples_per_class,
                                            nClasses = 0, image_size = cargs.nr)
        self.nnet.parent = self

        self.encoders = self.nnet.getEncoders(channels, cargs.nr)

        self.optimizer = 'adam'
        self.lr = cargs.lr
        self.current_lr = cargs.lr
        self.lr_decay = 1e-6
        self.wd = 1e-4
        self.total_train_iter = 0
        self.isCudaAvailable = torch.cuda.is_available()
        if self.isCudaAvailable:
            cudnn.benchmark = True
            torch.cuda.manual_seed_all(0)
            self.nnet.cuda()
            for x in self.encoders: x.cuda()

    def get_state_dict(self):
        return [self.nnet.state_dict()] + [x.state_dict() for x in self.encoders]

    def load_state_dict(self, checkpoint):
        sdict = checkpoint['state_dict']
        for m,d in zip([self.nnet] + self.encoders, sdict):
            m.load_state_dict(d, strict=False)

    def getMB(self, t, mbi):
        """
        t : tensor / list input with batch on dimension 0
        mbi: starting index to select the minibatch from. Selects [mbi:mbi+mini_batch_size]
        """
        if type(t) == list:
            return [self.getMB(x, mbi) for x in t]
        return t.narrow(0, mbi, self.mini_batch_size)

    def run_epoch(self, mode, total_batches, rotate_flag=True, dataSource=None):
        """
        Runs one training epoch
        :param total_batches: Number of batches to train on
        :return: mean_training_categorical_crossentropy_loss and mean_training_accuracy
        """
        if dataSource is None: dataSource = self.data

        accumulators = None
        total_c_loss = 0.
        total_accuracy = 0.
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0


        # Create the optimizer
        optimizer = None
        if mode == 'train':
            optimizer = self.__create_optimizer([self.nnet] + self.encoders, self.lr)
            self.nnet.train()
            for x in self.encoders: x.train()
        else:
            self.nnet.eval()
            for x in self.encoders: x.eval()

        self.mode_type = mode

        with tqdm.tqdm(total=total_batches) as pbar:
            accs = []
            for i in range(total_batches):  # train epoch
                x_support_set, y_support_set, x_target, y_target, ind_support, ind_target = dataSource.get_batch(str_type = mode,
                                                                                                      rotate_flag = rotate_flag)
                x_other_set,_,_,_,_,_ = dataSource.get_batch(str_type = 'val' if mode == 'train' else 'train',
                                                                  rotate_flag = rotate_flag)

                x_support_set = Variable(torch.from_numpy(x_support_set)).float()
                x_other_set = Variable(torch.from_numpy(x_other_set)).float()
                y_support_set = Variable(torch.from_numpy(y_support_set),requires_grad=False).long()
                x_target = Variable(torch.from_numpy(x_target)).float()
                y_target = Variable(torch.from_numpy(y_target),requires_grad=False).long()

                ind_target = Variable(torch.from_numpy(ind_target), requires_grad=False).long()
                ind_support = Variable(torch.from_numpy(ind_support), requires_grad=False).long()

                # y_support_set: Add extra dimension for the one_hot
                y_support_set = torch.unsqueeze(y_support_set, 2)
                sequence_length = y_support_set.size()[1]
                batch_size = y_support_set.size()[0]
                y_support_set_one_hot = torch.FloatTensor(batch_size, sequence_length,
                                                          x_support_set.size(1)).zero_()
                y_support_set_one_hot.scatter_(2, y_support_set.data, 1)
                y_support_set_one_hot = Variable(y_support_set_one_hot)

                # Reshape channels
                size = x_support_set.size()
                #x_support_set = x_support_set.view(size[0],size[1],size[4],size[2],size[3])
                x_support_set = x_support_set.permute(0,1,4,2,3)
                x_other_set = x_other_set.permute(0,1,4,2,3)
                size = x_target.size()
                #x_target = x_target.view(size[0],size[1],size[4],size[2],size[3])
                x_target = x_target.permute(0,1,4,2,3)

                if self.openSet:
                    x_support_set = x_support_set.narrow(1,0,x_support_set.size(1)-1)

                if self.isCudaAvailable:
                    x_support_set = x_support_set.cuda()
                    y_support_set_one_hot = y_support_set_one_hot.cuda()
                    x_target = x_target.cuda()
                    y_target = y_target.cuda()
                    x_other_set = x_other_set.cuda()
                    ind_support = ind_support.cuda()
                    ind_target = ind_target.cuda()

                ## embed the image and extract the filter outputs of each layer
                support_enc = [self.encoders[0](x_support_set[:,i,:,:,:])
                         for i in np.arange(x_support_set.size(1))]
                ## If we have separate encoders, use the second encoder to embed the target
                target_enc = [self.encoders[0 if len(self.encoders) == 1 else 1](x_target[:,i,:,:,:])
                         for i in np.arange(x_target.size(1))]

                if hasattr(self.nnet, 'nref'):
                    other_enc = [self.encoders[0 if len(self.encoders) == 1 else 1](x_other_set[:,i,:,:,:])
                                 for i in np.arange(x_other_set.size(1))]

                if mode == 'train':
                    # Before the backward pass, use the optimizer object to zero all of the
                    # gradients for the variables it will update (which are the learnable weights
                    # of the model)
                    optimizer.zero_grad()

                acc = 0
                c_loss_value = 0
                if x_support_set.size(0) != self.batch_size:
                    print(self.batch_size, x_support_set.size())
                assert(x_support_set.size(0) == self.batch_size)
                assert(self.batch_size % self.mini_batch_size == 0)

                for mbi in range(0, self.batch_size, self.mini_batch_size):
                    if hasattr(self.nnet, 'nref'):
                        mb_res = self.nnet(self.getMB(x_support_set, mbi),
                                           self.getMB(y_support_set_one_hot, mbi),
                                           self.getMB(x_target, mbi),
                                           self.getMB(y_target, mbi),
                                           self.getMB(support_enc, mbi),
                                           self.getMB(target_enc, mbi),
                                           self.getMB(x_other_set, mbi),
                                           self.getMB(other_enc, mbi),
                        )
                    else:
                        mb_res = self.nnet(self.getMB(x_support_set, mbi),
                                           self.getMB(y_support_set_one_hot, mbi),
                                           self.getMB(x_target, mbi),
                                           self.getMB(y_target, mbi),
                                           self.getMB(support_enc, mbi),
                                           self.getMB(target_enc, mbi),
                                           target_ind=self.getMB(ind_target, mbi),
                                           support_ind=self.getMB(ind_support, mbi))
                    mb_res['count'] = 1
                    if accumulators is None:
                        accumulators = {k:(mb_res[k].item() if isinstance(mb_res[k], Variable) else mb_res[k])
                                        for k in mb_res.keys()}
                    else:
                        accumulators = {k:(mb_res[k].item() if isinstance(mb_res[k], Variable) else mb_res[k]) + accumulators[k]
                                        for k in mb_res.keys()}

                    mb_acc = mb_res['acc']
                    mb_loss = mb_res['loss']
                    mb_acc *= self.mini_batch_size / self.batch_size
                    mb_loss *= self.mini_batch_size / self.batch_size

                    if mode == 'train':
                        # Backward pass: compute gradient of the loss with respect to model parameters
                        mb_loss.backward(retain_graph=True)
                    elif self.nnet.visMode: break


                    acc += mb_acc.item()
                    c_loss_value += mb_loss.item()

                    true_positive += mb_res['TP']
                    false_positive += mb_res['FP']
                    true_negative += mb_res['TN']
                    false_negative += mb_res['FN']
                    del mb_res
                    del mb_loss
                    del mb_acc


                if mode == 'train':
                    # Calling the step function on an Optimizer makes an update to its parameters
                    optimizer.step()
                    # update the optimizer learning rate
                    self.__adjust_learning_rate(optimizer)

                iter_out = "{} loss: {:.8f}, accuracy: {:.5f}".format(mode,
                                                                      accumulators['loss'] / accumulators['count'],
                                                                      accumulators['acc'] / accumulators['count'])
                pbar.set_description(iter_out, refresh=False)
                pbar.update(1)
                total_c_loss += c_loss_value
                total_accuracy += acc
                accs.append(acc)
                if mode == 'train':
                    self.total_train_iter += 1
                    ## FIXME: switch this back
                    # if self.total_train_iter % 2000 == 0:
                    #     self.lr /= 2
                    #     print("change learning rate", self.lr)
                    # if self.total_train_iter % 10000 == 0:
                    #     self.lr /= 2
                    #     print("FIXME: change learning rate", self.lr)

        total_c_loss = total_c_loss / total_batches
        total_accuracy = total_accuracy / total_batches
        try:
            f1 = (2*true_positive) / (2*true_positive + false_negative + false_positive)
        except:
            f1 = 0
        accumulators = {k:accumulators[k]/accumulators['count'] for k in accumulators}
        accumulators['f1'] = f1
        accs = numpy.array(accs)
        accs = accs.reshape(len(accs)//builtins.cargs.episode_size, builtins.cargs.episode_size).mean(1)
        accumulators['95Interval'] = numpy.mean(accs) - scipy.stats.t.interval(0.95, len(accs)-1, loc=numpy.mean(accs), scale=scipy.stats.sem(accs))[0]

        return accumulators




    def run_mnist_testing_epoch(self, total_test_batches, rotate_flag=True):
        """
        Runs one testing epoch
        :param total_test_batches: Number of batches to train on
        :param sess: Session object
        :return: mean_testing_categorical_crossentropy_loss and mean_testing_accuracy
        """
        total_test_c_loss = 0.
        total_test_accuracy = 0.

        self.nnet.eval()
        with tqdm.tqdm(total=total_test_batches) as pbar:
            for i in range(total_test_batches):
                x_support_set, y_support_set, x_target, y_target = \
                    self.datacross.get_batch(str_type='test', rotate_flag=rotate_flag)
                x_support_set = Variable(torch.from_numpy(x_support_set), volatile=True).float()
                y_support_set = Variable(torch.from_numpy(y_support_set), volatile=True).long()
                x_target = Variable(torch.from_numpy(x_target), volatile=True).float()
                y_target = Variable(torch.from_numpy(y_target), volatile=True).long()

                # y_support_set: Add extra dimension for the one_hot
                y_support_set = torch.unsqueeze(y_support_set, 2)
                sequence_length = y_support_set.size()[1]
                batch_size = y_support_set.size()[0]
                y_support_set_one_hot = torch.FloatTensor(batch_size, sequence_length, 10).zero_()
                y_support_set_one_hot.scatter_(2, y_support_set.data, 1)
                y_support_set_one_hot = Variable(y_support_set_one_hot)

                # Reshape channels
                size = x_support_set.size()
                #x_support_set = x_support_set.view(size[0], size[1], size[4], size[2], size[3])
                x_support_set = x_support_set.permute(0,1,4,2,3)
                size = x_target.size()
                #x_target = x_target.view(size[0],size[1],size[4],size[2],size[3])
                x_target = x_target.permute(0,1,4,2,3)
                if self.isCudaAvailable:
                    mb_res = self.nnet(x_support_set.cuda(), y_support_set_one_hot.cuda(),
                                              x_target.cuda(), y_target.cuda())
                else:
                    mb_res = self.nnet(x_support_set, y_support_set_one_hot,
                                              x_target, y_target)
                acc = mb_res['acc']
                c_loss_value = mb_res['loss']

                iter_out = "mnist_test_loss: {}, mnist_test_accuracy: {}".format(c_loss_value.data[0], acc.data[0])
                pbar.set_description(iter_out)
                pbar.update(1)

                total_test_c_loss += c_loss_value.data[0]
                total_test_accuracy += acc.data[0]
        total_test_c_loss = total_test_c_loss / total_test_batches
        total_test_accuracy = total_test_accuracy / total_test_batches
        return total_test_c_loss, total_test_accuracy


    def __adjust_learning_rate(self,optimizer):
        """Updates the learning rate given the learning rate decay.
        The routine has been implemented according to the original Lua SGD optimizer
        """
        for group in optimizer.param_groups:
            if 'step' not in group:
                group['step'] = 0
            group['step'] += 1

            group['lr'] = self.lr / (1 + group['step'] * self.lr_decay)

    def __create_optimizer(self,models, new_lr):
        # setup optimizer
        #allParams = model.parameters()
        allParams = [x for model in models for x in model.parameters() if x.requires_grad]
        if self.optimizer == 'sgd':
            optimizer = torch.optim.SGD(allParams, lr=new_lr,
                                  momentum=0.9, dampening=0.9,
                                  weight_decay=self.wd)
        elif self.optimizer == 'adam':
            optimizer = torch.optim.Adam(allParams, lr=new_lr,
                                   weight_decay=self.wd)
        else:
            raise Exception('Not supported optimizer: {0}'.format(self.optimizer))
        return optimizer


    # def run_vis(self):
    #     """
    #     Runs one testing epoch
    #     :param total_test_batches: Number of batches to train on
    #     :param sess: Session object
    #     :return: mean_testing_categorical_crossentropy_loss and mean_testing_accuracy
    #     """
    #     self.nnet.eval()

    #     x_targets = []
    #     y_targets = []

    #     x_support_set, y_support_set, x_target, y_target = self.data.get_batch(str_type='test', rotate_flag=False)
    #     x_support_set = Variable(torch.from_numpy(x_support_set), volatile=True).float()
    #     y_support_set = Variable(torch.from_numpy(y_support_set), volatile=True).long()

    #     x_targets.append(torch.from_numpy(x_target))
    #     y_targets.append(torch.from_numpy(y_target))
    #     for i in range(4):
    #         _, _, x_target, y_target = self.data.get_batch(str_type='test', rotate_flag=False)
    #         x_targets.append(torch.from_numpy(x_target))
    #         y_targets.append(torch.from_numpy(y_target))

    #     x_target = Variable(torch.cat(x_targets, dim=1), volatile=True).float()
    #     y_target = Variable(torch.cat(y_targets, dim=1), volatile=True).long()

    #     # y_support_set: Add extra dimension for the one_hot
    #     y_support_set = torch.unsqueeze(y_support_set, 2)
    #     sequence_length = y_support_set.size()[1]
    #     batch_size = y_support_set.size()[0]
    #     y_support_set_one_hot = torch.FloatTensor(batch_size, sequence_length,
    #                                               self.classes_per_set).zero_()
    #     y_support_set_one_hot.scatter_(2, y_support_set.data, 1)
    #     y_support_set_one_hot = Variable(y_support_set_one_hot)

    #     # Reshape channels
    #     size = x_support_set.size()
    #     #x_support_set = x_support_set.view(size[0], size[1], size[4], size[2], size[3])
    #     x_support_set = x_support_set.permute(0,1,4,2,3)
    #     size = x_target.size()
    #     #x_target = x_target.view(size[0],size[1],size[4],size[2],size[3])
    #     x_target = x_target.permute(0,1,4,2,3)
    #     res = None
    #     if self.isCudaAvailable:
    #         res = self.nnet.forwardVis(x_support_set.cuda(), y_support_set_one_hot.cuda(),
    #                                                         x_target.cuda(), y_target.cuda())
    #     else:
    #         res = self.nnet.forwardVis(x_support_set, y_support_set_one_hot,
    #                                                         x_target, y_target)


    #     return res




class ClassificationBuilder:
    def __init__(self, data, model, datacross=None):
        """
        Initializes an OneShotBuilder object. The OneShotBuilder object takes care of setting up our experiment
        and provides helper functions such as run_training_epoch and run_validation_epoch to simplify out training
        and evaluation procedures.
        :param data: A data provider class
        """
        self.data = data
        self.model = model
        self.datacross = datacross
        self.openSet = cargs.open_set

    def set_batch_size(self, batch_size, mini_batch_size):
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.data.batch_size = batch_size


    def build_experiment(self, batch_size, mini_batch_size, channels, fce):

        """
        :param batch_size: The experiment batch size
        :param classes_per_set: An integer indicating the number of classes per support set
        :param samples_per_class: An integer indicating the number of samples per class
        :param channels: The image channels
        :param fce: Whether to use full context embeddings or not
        :return: a matching_network object, along with the losses, the training ops and the init op
        """
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.keep_prob = torch.FloatTensor(1)
        self.nnet = eval(self.model)(batch_size=batch_size,
                                     keep_prob=self.keep_prob, num_channels=channels,
                                     fce=fce,
                                     nClasses = self.data.nClasses, image_size = cargs.nr)
        self.nnet.parent = self

        self.encoders = self.nnet.getEncoders(channels, cargs.nr)

        self.optimizer = 'adam'
        self.lr = cargs.lr
        self.current_lr = cargs.lr
        self.lr_decay = 1e-6
        self.wd = 1e-4
        self.total_train_iter = 0
        self.isCudaAvailable = torch.cuda.is_available()
        if self.isCudaAvailable:
            cudnn.benchmark = True
            torch.cuda.manual_seed_all(0)
            self.nnet.cuda()
            for x in self.encoders: x.cuda()

        self.optimizer = self.__create_optimizer([self.nnet] + self.encoders, self.lr)

    def get_state_dict(self):
        return [self.nnet.state_dict()] + [x.state_dict() for x in self.encoders]

    def load_state_dict(self, checkpoint):
        sdict = checkpoint['state_dict']
        for m,d in zip([self.nnet] + self.encoders, sdict):
            m.load_state_dict(d)

    def getMB(self, t, mbi):
        """
        t : tensor / list input with batch on dimension 0
        mbi: starting index to select the minibatch from. Selects [mbi:mbi+mini_batch_size]
        """
        if type(t) == list:
            return [self.getMB(x, mbi) for x in t]
        return t.narrow(0, mbi, self.mini_batch_size)

    def run_epoch(self, mode, total_batches, rotate_flag=True, dataSource=None):
        """
        Runs one training epoch
        :param total_batches: Number of batches to train on
        :return: mean_training_categorical_crossentropy_loss and mean_training_accuracy
        """
        if dataSource is None: dataSource = self.data

        accumulators = None
        total_c_loss = 0.
        total_accuracy = 0.
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0


        # Create the optimizer
        optimizer = None
        self.mode = mode
        if mode == 'train':
            optimizer = self.optimizer
            self.nnet.train()
            for x in self.encoders: x.train()
        else:
            self.nnet.eval()
            for x in self.encoders: x.eval()

        with tqdm.tqdm(total=total_batches) as pbar:
            for i in range(total_batches):  # train epoch
                x_target_low, x_target_high, y_target, one_hot = dataSource.get_batch(str_type = mode,
                                                                                      rotate_flag = rotate_flag)
                x_target_low = Variable(x_target_low).float()
                x_target_high = Variable(x_target_high).float()
                y_target = Variable(y_target,requires_grad=False).long()
                one_hot = Variable(one_hot,requires_grad=False).float()


                size = x_target_low.size()
                if self.isCudaAvailable:
                    x_target_low = x_target_low.cuda()
                    x_target_high = x_target_high.cuda()
                    y_target = y_target.cuda()
                    one_hot = one_hot.cuda()

                if mode == 'train':
                    # Before the backward pass, use the optimizer object to zero all of the
                    # gradients for the variables it will update (which are the learnable weights
                    # of the model)
                    optimizer.zero_grad()

                acc = 0
                c_loss_value = 0
                #assert(self.batch_size % self.mini_batch_size == 0)

                for mbi in [0]: #range(0, self.batch_size, self.mini_batch_size):
                    mb_res = self.nnet( x_target_low,
                                        x_target_high,
                                        y_target,
                                        one_hot)
                    mb_res['count'] = 1
                    if accumulators is None:
                        accumulators = {k:(mb_res[k].data[0] if isinstance(mb_res[k], Variable) else mb_res[k])
                                        for k in mb_res.keys()}
                    else:
                        accumulators = {k:(mb_res[k].data[0] if isinstance(mb_res[k], Variable) else mb_res[k]) + accumulators[k]
                                        for k in mb_res.keys()}

                    mb_acc = mb_res['acc']
                    mb_loss = mb_res['loss']
                    #mb_acc *= self.mini_batch_size / self.batch_size
                    #mb_loss *= self.mini_batch_size / self.batch_size
                    acc += mb_acc #.data[0]
                    c_loss_value += mb_loss.data[0]

                    if mode == 'train':
                        # Backward pass: compute gradient of the loss with respect to model parameters
                        mb_loss.backward(retain_graph=True)
                    elif self.nnet.visMode: break

                if mode == 'train':
                    # Calling the step function on an Optimizer makes an update to its parameters
                    optimizer.step()
                    # update the optimizer learning rate
                    self.__adjust_learning_rate(optimizer)

                iter_out = "{} loss: {:.8f}, accuracy: {:.5f}".format(mode,
                                                                      accumulators['loss'] / accumulators['count'],
                                                                      accumulators['acc'] / accumulators['count'])
                pbar.set_description(iter_out, refresh=False)
                pbar.update(1)
                total_c_loss += c_loss_value
                total_accuracy += acc
                if mode == 'train':
                    self.total_train_iter += 1
                    ## FIXME: switch this back
                    # if self.total_train_iter % 2000 == 0:
                    #     self.lr /= 2
                    #     print("change learning rate", self.lr)
                    # if self.total_train_iter % 10000 == 0:
                    #     self.lr /= 2
                    #     print("FIXME: change learning rate", self.lr)

        total_c_loss = total_c_loss / total_batches
        total_accuracy = total_accuracy / total_batches
        accumulators = {k:accumulators[k]/accumulators['count'] for k in accumulators}
        return accumulators


    def __adjust_learning_rate(self,optimizer):
        """Updates the learning rate given the learning rate decay.
        The routine has been implemented according to the original Lua SGD optimizer
        """
        for group in optimizer.param_groups:
            if 'step' not in group:
                group['step'] = 0
            group['step'] += 1

            group['lr'] = self.lr / (1 + group['step'] * self.lr_decay)

    def __create_optimizer(self,models, new_lr):
        # setup optimizer
        #allParams = model.parameters()
        allParams = [x for model in models for x in model.parameters() if x.requires_grad]
        if self.optimizer == 'sgd':
            optimizer = torch.optim.SGD(allParams, lr=new_lr,
                                  momentum=0.9, dampening=0.9,
                                  weight_decay=self.wd)
        elif self.optimizer == 'adam':
            optimizer = torch.optim.Adam(allParams, lr=new_lr,
                                   weight_decay=self.wd)
        else:
            raise Exception('Not supported optimizer: {0}'.format(self.optimizer))
        return optimizer




class OneShotMaskBuilder(OneShotBuilder):
    def __init__(self, data, model, datacross=None):
        super(OneShotMaskBuilder, self).__init__(data, model, datacross)

    def getMB(self, t, mbi):
        """
        t : tensor / list input with batch on dimension 0
        mbi: starting index to select the minibatch from. Selects [mbi:mbi+mini_batch_size]
        """
        if type(t) == list:
            return [self.getMB(x, mbi) for x in t]
        return t.narrow(0, mbi, self.mini_batch_size)

    def run_epoch(self, mode, total_batches, rotate_flag=True, dataSource=None):
        """
        Runs one training epoch
        :param total_batches: Number of batches to train on
        :return: mean_training_categorical_crossentropy_loss and mean_training_accuracy
        """
        if dataSource is None: dataSource = self.data

        accumulators = None
        total_c_loss = 0.
        total_accuracy = 0.
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0


        # Create the optimizer
        optimizer = None
        if mode == 'train':
            optimizer = self.__create_optimizer([self.nnet] + self.encoders, self.lr)
            self.nnet.train()
            for x in self.encoders: x.train()
        else:
            self.nnet.eval()
            for x in self.encoders: x.eval()

        self.mode_type = mode

        with tqdm.tqdm(total=total_batches) as pbar:
            accs = []
            for i in range(total_batches):  # train epoch
                x_support_set, y_support_set, x_target, y_target, ind_support, ind_target, target_mask = dataSource.get_batch(str_type = mode, rotate_flag = rotate_flag)
                x_other_set,_,_,_,_,_,_ = dataSource.get_batch(str_type = 'val' if mode == 'train' else 'train',
                                                               rotate_flag = rotate_flag)

                x_support_set = Variable(torch.from_numpy(x_support_set)).float()
                x_other_set = Variable(torch.from_numpy(x_other_set)).float()
                y_support_set = Variable(torch.from_numpy(y_support_set),requires_grad=False).long()
                x_target = Variable(torch.from_numpy(x_target)).float()
                y_target = Variable(torch.from_numpy(y_target),requires_grad=False).long()

                ind_support = Variable(torch.from_numpy(ind_support), requires_grad=False).long()
                ind_target = Variable(torch.from_numpy(ind_target), requires_grad=False).long()
                target_mask = Variable(torch.from_numpy(target_mask), requires_grad=False).float()


                # y_support_set: Add extra dimension for the one_hot
                y_support_set = torch.unsqueeze(y_support_set, 2)
                sequence_length = y_support_set.size()[1]
                batch_size = y_support_set.size()[0]
                y_support_set_one_hot = torch.FloatTensor(batch_size, sequence_length,
                                                          x_support_set.size(1)).zero_()
                y_support_set_one_hot.scatter_(2, y_support_set.data, 1)
                y_support_set_one_hot = Variable(y_support_set_one_hot)

                # Reshape channels
                size = x_support_set.size()
                #x_support_set = x_support_set.view(size[0],size[1],size[4],size[2],size[3])
                x_support_set = x_support_set.permute(0,1,4,2,3)
                x_other_set = x_other_set.permute(0,1,4,2,3)
                size = x_target.size()
                #x_target = x_target.view(size[0],size[1],size[4],size[2],size[3])
                x_target = x_target.permute(0,1,4,2,3)

                if self.openSet:
                    x_support_set = x_support_set.narrow(1,0,x_support_set.size(1)-1)

                if self.isCudaAvailable:
                    x_support_set = x_support_set.cuda()
                    y_support_set_one_hot = y_support_set_one_hot.cuda()
                    x_target = x_target.cuda()
                    y_target = y_target.cuda()
                    x_other_set = x_other_set.cuda()
                    ind_support = ind_support.cuda()
                    ind_target = ind_target.cuda()
                    target_mask = target_mask.cuda()

                if cargs.masktype == 'img': x_target = x_target * target_mask.unsqueeze(2)

                ## embed the image and extract the filter outputs of each layer
                support_enc = [self.encoders[0](x_support_set[:,i,:,:,:])
                         for i in np.arange(x_support_set.size(1))]
                ## If we have separate encoders, use the second encoder to embed the target
                target_enc = [self.encoders[0 if len(self.encoders) == 1 else 1](x_target[:,i,:,:,:])
                         for i in np.arange(x_target.size(1))]

                if hasattr(self.nnet, 'nref'):
                    other_enc = [self.encoders[0 if len(self.encoders) == 1 else 1](x_other_set[:,i,:,:,:])
                                 for i in np.arange(x_other_set.size(1))]

                if mode == 'train':
                    # Before the backward pass, use the optimizer object to zero all of the
                    # gradients for the variables it will update (which are the learnable weights
                    # of the model)
                    optimizer.zero_grad()

                acc = 0
                c_loss_value = 0
                if x_support_set.size(0) != self.batch_size:
                    print(self.batch_size, x_support_set.size())
                assert(x_support_set.size(0) == self.batch_size)
                assert(self.batch_size % self.mini_batch_size == 0)

                for mbi in range(0, self.batch_size, self.mini_batch_size):
                    if hasattr(self.nnet, 'nref'):
                        mb_res = self.nnet(self.getMB(x_support_set, mbi),
                                           self.getMB(y_support_set_one_hot, mbi),
                                           self.getMB(x_target, mbi),
                                           self.getMB(y_target, mbi),
                                           self.getMB(support_enc, mbi),
                                           self.getMB(target_enc, mbi),
                                           self.getMB(x_other_set, mbi),
                                           self.getMB(other_enc, mbi),
                        )
                    else:
                        mb_res = self.nnet(self.getMB(x_support_set, mbi),
                                           self.getMB(y_support_set_one_hot, mbi),
                                           self.getMB(x_target, mbi),
                                           self.getMB(y_target, mbi),
                                           self.getMB(support_enc, mbi),
                                           self.getMB(target_enc, mbi),
                                           target_ind=self.getMB(ind_target, mbi),
                                           support_ind=self.getMB(ind_support, mbi),
                                           target_mask=self.getMB(target_mask, mbi),
                        )
                    mb_res['count'] = 1
                    if accumulators is None:
                        accumulators = {k:(mb_res[k].item() if isinstance(mb_res[k], Variable) else mb_res[k])
                                        for k in mb_res.keys()}
                    else:
                        accumulators = {k:(mb_res[k].item() if isinstance(mb_res[k], Variable) else mb_res[k]) + accumulators[k]
                                        for k in mb_res.keys()}

                    mb_acc = mb_res['acc']
                    mb_loss = mb_res['loss']
                    mb_acc *= self.mini_batch_size / self.batch_size
                    mb_loss *= self.mini_batch_size / self.batch_size

                    if mode == 'train':
                        # Backward pass: compute gradient of the loss with respect to model parameters
                        mb_loss.backward(retain_graph=True)
                    elif self.nnet.visMode: break


                    acc += mb_acc.item()
                    c_loss_value += mb_loss.item()

                    true_positive += mb_res['TP']
                    false_positive += mb_res['FP']
                    true_negative += mb_res['TN']
                    false_negative += mb_res['FN']
                    del mb_res
                    del mb_loss
                    del mb_acc


                if mode == 'train':
                    # Calling the step function on an Optimizer makes an update to its parameters
                    optimizer.step()
                    # update the optimizer learning rate
                    self.__adjust_learning_rate(optimizer)

                iter_out = "{} loss: {:.8f}, accuracy: {:.5f}".format(mode,
                                                                      accumulators['loss'] / accumulators['count'],
                                                                      accumulators['acc'] / accumulators['count'])
                pbar.set_description(iter_out, refresh=False)
                pbar.update(1)
                total_c_loss += c_loss_value
                total_accuracy += acc
                accs.append(acc)
                if mode == 'train':
                    self.total_train_iter += 1
                    ## FIXME: switch this back
                    # if self.total_train_iter % 2000 == 0:
                    #     self.lr /= 2
                    #     print("change learning rate", self.lr)
                    # if self.total_train_iter % 10000 == 0:
                    #     self.lr /= 2
                    #     print("FIXME: change learning rate", self.lr)

        total_c_loss = total_c_loss / total_batches
        total_accuracy = total_accuracy / total_batches
        try:
            f1 = (2*true_positive) / (2*true_positive + false_negative + false_positive)
        except:
            f1 = 0
        accumulators = {k:accumulators[k]/accumulators['count'] for k in accumulators}
        accumulators['f1'] = f1
        accs = numpy.array(accs)
        accs = accs.reshape(len(accs)//builtins.cargs.episode_size, builtins.cargs.episode_size).mean(1)
        accumulators['95Interval'] = numpy.mean(accs) - scipy.stats.t.interval(0.95, len(accs)-1, loc=numpy.mean(accs), scale=scipy.stats.sem(accs))[0]

        return accumulators


    def __adjust_learning_rate(self,optimizer):
        """Updates the learning rate given the learning rate decay.
        The routine has been implemented according to the original Lua SGD optimizer
        """
        for group in optimizer.param_groups:
            if 'step' not in group:
                group['step'] = 0
            group['step'] += 1

            group['lr'] = self.lr / (1 + group['step'] * self.lr_decay)

    def __create_optimizer(self,models, new_lr):
        # setup optimizer
        #allParams = model.parameters()
        allParams = [x for model in models for x in model.parameters() if x.requires_grad]
        if self.optimizer == 'sgd':
            optimizer = torch.optim.SGD(allParams, lr=new_lr,
                                  momentum=0.9, dampening=0.9,
                                  weight_decay=self.wd)
        elif self.optimizer == 'adam':
            optimizer = torch.optim.Adam(allParams, lr=new_lr,
                                   weight_decay=self.wd)
        else:
            raise Exception('Not supported optimizer: {0}'.format(self.optimizer))
        return optimizer
