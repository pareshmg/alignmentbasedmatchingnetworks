##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import builtins
import torch
import torch.backends.cudnn as cudnn
import tqdm
from models.MatchingNetwork import *
from models.BitNet import *
from torch.autograd import Variable
import torch.nn.functional as F


class ClassificationBuilder:

    def __init__(self, data, model, datacross=None):
        """
        Initializes an ClassificationBuilder object. The ClassificationBuilder object takes care of setting up our experiment
        and provides helper functions such as run_training_epoch and run_validation_epoch to simplify out training
        and evaluation procedures.
        :param data: A data provider class
        """
        self.data = data
        self.model = model
        self.datacross = datacross
        self.openSet = args.open_set

    def set_batch_size(self, batch_size, mini_batch_size):
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.data.batch_size = batch_size


    def build_experiment(self, batch_size, mini_batch_size, channels):

        """
        :param batch_size: The experiment batch size
        :param classes_per_set: An integer indicating the number of classes per support set
        :param samples_per_class: An integer indicating the number of samples per class
        :param channels: The image channels
        :param fce: Whether to use full context embeddings or not
        :return: a matching_network object, along with the losses, the training ops and the init op
        """
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.keep_prob = torch.FloatTensor(1)
        self.nnet = eval(self.model)(batch_size=batch_size,
                                     keep_prob=self.keep_prob, num_channels=channels,
                                     nClasses = self.data.nClasses, image_size = args.nr)
        self.nnet.parent = self


        self.optimizer = 'adam'
        self.lr = args.lr
        self.current_lr = args.lr
        self.lr_decay = 1e-6
        self.wd = 1e-4
        self.total_train_iter = 0
        self.isCudaAvailable = torch.cuda.is_available()
        if self.isCudaAvailable:
            cudnn.benchmark = True
            torch.cuda.manual_seed_all(builtins.args.seed)
            self.nnet.cuda()

        self.optimizerMod = self.__create_optimizer([self.nnet], self.lr)


    def get_state_dict(self):
        return [self.nnet.state_dict()]

    def load_state_dict(self, checkpoint):
        sdict = checkpoint['state_dict']
        for m,d in zip([self.nnet], sdict):
            m.load_state_dict(d)

    def getMB(self, t, mbi):
        """
        t : tensor / list input with batch on dimension 0
        mbi: starting index to select the minibatch from. Selects [mbi:mbi+mini_batch_size]
        """
        if type(t) == list:
            return [self.getMB(x, mbi) for x in t]
        return t.narrow(0, mbi, self.mini_batch_size)

    def run_epoch(self, mode, total_batches, rotate_flag=True, dataSource=None):
        """
        Runs one training epoch
        :param total_batches: Number of batches to train on
        :return: mean_training_categorical_crossentropy_loss and mean_training_accuracy
        """
        if dataSource is None: dataSource = self.data

        accumulators = None
        total_c_loss = 0.
        total_accuracy = 0.
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0


        # Create the optimizer
        optimizer = self.optimizerMod
        if mode == 'train':
            self.nnet.train()
        else:
            self.nnet.eval()


        with tqdm.tqdm(total=total_batches) as pbar:
            for i in range(total_batches):  # train epoch
                try:
                    xs, ys = dataSource.get_batch(str_type = mode,
                                                  rotate_flag = rotate_flag)
                except StopIteration:
                    break
                xs = Variable(xs).float()
                ys = Variable(ys,requires_grad=False).long()

                # Reshape channels
                size = xs.size()

                if self.isCudaAvailable:
                    xs = xs.cuda()
                    ys = ys.cuda()

                if mode == 'train':
                    # Before the backward pass, use the optimizer object to zero all of the
                    # gradients for the variables it will update (which are the learnable weights
                    # of the model)
                    optimizer.zero_grad()

                acc = 0
                c_loss_value = 0
                if xs.size(0) != self.batch_size: break
                assert(xs.size(0) == self.batch_size) , str((self.batch_size, xs.size()))
                assert(self.batch_size % self.mini_batch_size == 0)

                for mbi in range(0, self.batch_size, self.mini_batch_size):
                    mb_res = self.nnet(self.getMB(xs, mbi),
                                       self.getMB(ys, mbi))
                    mb_res['count'] = 1
                    if accumulators is None:
                        accumulators = {k:(mb_res[k].data[0] if isinstance(mb_res[k], Variable) else mb_res[k])
                                        for k in mb_res.keys()}
                    else:
                        accumulators = {k:(mb_res[k].data[0] if isinstance(mb_res[k], Variable) else mb_res[k]) + accumulators[k]
                                        for k in mb_res.keys()}

                    mb_acc = mb_res['acc']
                    mb_loss = mb_res['loss']
                    mb_acc *= self.mini_batch_size / self.batch_size
                    mb_loss *= self.mini_batch_size / self.batch_size
                    acc += mb_acc #.data[0]
                    c_loss_value += mb_loss.data[0]

                    if 'TP' in mb_res:
                        true_positive += mb_res['TP']
                        false_positive += mb_res['FP']
                        true_negative += mb_res['TN']
                        false_negative += mb_res['FN']

                    if mode == 'train':
                        # Backward pass: compute gradient of the loss with respect to model parameters
                        mb_loss.backward(retain_graph=True)
                    elif self.nnet.visMode: break

                if mode == 'train':
                    # Calling the step function on an Optimizer makes an update to its parameters
                    optimizer.step()
                    # update the optimizer learning rate
                    self.__adjust_learning_rate(optimizer)

                iter_out = "{}_loss: {:.8f}, tr_accuracy: {:.5f}".format(mode,
                                                                         accumulators['loss'] / accumulators['count'],
                                                                         accumulators['acc'] / accumulators['count'])

                pbar.set_description(iter_out, refresh=False)
                pbar.update(1)
                total_c_loss += c_loss_value
                total_accuracy += acc
                if mode == 'train':
                    self.total_train_iter += 1
                    ## FIXME: switch this back
                    # if self.total_train_iter % 2000 == 0:
                    #     self.lr /= 2
                    #     print("change learning rate", self.lr)
                    # if self.total_train_iter % 10000 == 0:
                    #     self.lr /= 2
                    #     print("FIXME: change learning rate", self.lr)

        total_c_loss = total_c_loss / total_batches
        total_accuracy = total_accuracy / total_batches
        f1 = None
        try:
            f1 = (2*true_positive) / (2*true_positive + false_negative + false_positive)
        except:
            f1 = 0
        accumulators = {k:accumulators[k]/accumulators['count'] for k in accumulators}
        accumulators['f1'] = f1
        return accumulators

    def __adjust_learning_rate(self,optimizer):
        """Updates the learning rate given the learning rate decay.
        The routine has been implemented according to the original Lua SGD optimizer
        """
        for group in optimizer.param_groups:
            if 'step' not in group:
                group['step'] = 0
            group['step'] += 1

            group['lr'] = self.lr / (1 + group['step'] * self.lr_decay)

    def __create_optimizer(self,models, new_lr):
        # setup optimizer
        #allParams = model.parameters()
        allParams = [x for model in models for x in model.parameters() if x.requires_grad]
        if self.optimizer == 'sgd':
            optimizer = torch.optim.SGD(allParams, lr=new_lr,
                                  momentum=0.9, dampening=0.9,
                                  weight_decay=self.wd)
        elif self.optimizer == 'adam':
            optimizer = torch.optim.Adam(allParams, lr=new_lr,
                                   weight_decay=self.wd)
        else:
            raise Exception('Not supported optimizer: {0}'.format(self.optimizer))
        return optimizer
