##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import builtins
import torch
import torch.backends.cudnn as cudnn
import tqdm
from models.MatchingNetwork import *
from torch.autograd import Variable


class OpenSetBuilder:

    def __init__(self, data, model, datacross=None):
        """
        Initializes an OpenSetBuilder object. The OpenSetBuilder object takes care of setting up our experiment
        and provides helper functions such as run_training_epoch and run_validation_epoch to simplify out training
        and evaluation procedures.
        :param data: A data provider class
        """
        self.data = data
        self.model = model
        self.datacross = datacross

    def set_batch_size(self, batch_size, mini_batch_size):
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.data.batch_size = batch_size


    def build_experiment(self, batch_size, mini_batch_size, classes_per_set, samples_per_class, channels, fce):

        """
        :param batch_size: The experiment batch size
        :param classes_per_set: An integer indicating the number of classes per support set
        :param samples_per_class: An integer indicating the number of samples per class
        :param channels: The image channels
        :param fce: Whether to use full context embeddings or not
        :return: a matching_network object, along with the losses, the training ops and the init op
        """
        self.batch_size = batch_size
        self.mini_batch_size = mini_batch_size
        self.classes_per_set = classes_per_set
        self.samples_per_class = samples_per_class
        self.keep_prob = torch.FloatTensor(1)
        self.matchingNet = eval(self.model)(batch_size=batch_size,
                                            keep_prob=self.keep_prob, num_channels=channels,
                                            fce=fce,
                                            num_classes_per_set=classes_per_set,
                                            num_samples_per_class=samples_per_class,
                                            nClasses = 0, image_size = args.nr)

        self.encoders = self.matchingNet.getEncoders(channels, args.nr)

        self.optimizer = 'adam'
        self.lr = args.lr
        self.current_lr = args.lr
        self.lr_decay = 1e-6
        self.wd = 1e-4
        self.total_train_iter = 0
        self.isCudaAvailable = torch.cuda.is_available()
        if self.isCudaAvailable:
            cudnn.benchmark = True
            torch.cuda.manual_seed_all(0)
            self.matchingNet.cuda()
            for x in self.encoders: x.cuda()

    def get_state_dict(self):
        return [self.matchingNet.state_dict()] + [x.state_dict() for x in self.encoders]

    def load_state_dict(self, fname):
        checkpoint = torch.load(fname)
        sdict = checkpoint['state_dict']
        for m,d in zip([self.matchingNet] + self.encoders, sdict):
            m.load_state_dict(d)

    def getMB(self, t, mbi):
        """
        t : tensor / list input with batch on dimension 0
        mbi: starting index to select the minibatch from. Selects [mbi:mbi+mini_batch_size]
        """
        if type(t) == list:
            return [self.getMB(x, mbi) for x in t]
        return t.narrow(0, mbi, self.mini_batch_size)


    def run_test(self, nSupportPerClass, nTargetPerClass, dataSource = None):
        if dataSource is None: dataSource = self.data


    def run_fit(self, mode, nSupportPerClass, rotate_flag=True, dataSource=None):
        if dataSource is None: dataSource = self.data

        total_c_loss = 0.
        total_accuracy = 0.
        # Create the optimizer
        optimizer = None
        self.matchingNet.eval()
        for x in self.encoders: x.eval()

        ## fix the dataset of samples being trained
        x_support_set = {i:self.data.x_train[i,:nSupportPerClass] for i in range(5)}

        # for pairs, find 1 vs all distances

        with tqdm.tqdm(total=nTargetPerClass) as pbar:
            for bi in range(nTargetPerClass // self.mini_batch_size):

                x_support_set, y_support_set, x_target, y_target = dataSource.get_batch(str_type = mode,
                                                                                        rotate_flag = rotate_flag)

                x_support_set = Variable(torch.from_numpy(x_support_set)).float()
                y_support_set = Variable(torch.from_numpy(y_support_set),requires_grad=False).long()
                x_target = Variable(torch.from_numpy(x_target)).float()
                y_target = Variable(torch.from_numpy(y_target),requires_grad=False).long()

                # y_support_set: Add extra dimension for the one_hot
                y_support_set = torch.unsqueeze(y_support_set, 2)
                sequence_length = y_support_set.size()[1]
                batch_size = y_support_set.size()[0]
                y_support_set_one_hot = torch.FloatTensor(batch_size, sequence_length,
                                                               self.classes_per_set).zero_()
                y_support_set_one_hot.scatter_(2, y_support_set.data, 1)
                y_support_set_one_hot = Variable(y_support_set_one_hot)

                # Reshape channels
                size = x_support_set.size()
                #x_support_set = x_support_set.view(size[0],size[1],size[4],size[2],size[3])
                x_support_set = x_support_set.permute(0,1,4,2,3)
                size = x_target.size()
                #x_target = x_target.view(size[0],size[1],size[4],size[2],size[3])
                x_target = x_target.permute(0,1,4,2,3)


                if self.isCudaAvailable:
                    x_support_set = x_support_set.cuda()
                    y_support_set_one_hot = y_support_set_one_hot.cuda()
                    x_target = x_target.cuda()
                    y_target = y_target.cuda()


                ## embed the image and extract the filter outputs of each layer
                support_enc = [self.encoders[0](x_support_set[:,i,:,:,:])
                         for i in np.arange(x_support_set.size(1))]
                ## If we have separate encoders, use the second encoder to embed the target
                target_enc = [self.encoders[0 if len(self.encoders) == 1 else 1](x_target[:,i,:,:,:])
                         for i in np.arange(x_target.size(1))]

                if mode == 'train':
                    # Before the backward pass, use the optimizer object to zero all of the
                    # gradients for the variables it will update (which are the learnable weights
                    # of the model)
                    optimizer.zero_grad()

                acc = 0
                c_loss_value = 0
                if x_support_set.size(0) != self.batch_size:
                    print(self.batch_size, x_support_set.size())
                assert(x_support_set.size(0) == self.batch_size)
                assert(self.batch_size % self.mini_batch_size == 0)

                for mbi in range(0, self.batch_size, self.mini_batch_size):
                    mb_acc, mb_loss = self.matchingNet(self.getMB(x_support_set, mbi),
                                                       self.getMB(y_support_set_one_hot, mbi),
                                                       self.getMB(x_target, mbi),
                                                       self.getMB(y_target, mbi),
                                                       self.getMB(support_enc, mbi),
                                                       self.getMB(target_enc, mbi))
                    mb_acc *= self.mini_batch_size / self.batch_size
                    mb_loss *= self.mini_batch_size / self.batch_size
                    acc += mb_acc.data[0]
                    c_loss_value += mb_loss.data[0]
                    if mode == 'train':
                        # Backward pass: compute gradient of the loss with respect to model parameters
                        mb_loss.backward(retain_graph=True)
                    elif self.matchingNet.visMode: break

                if mode == 'train':
                    # Calling the step function on an Optimizer makes an update to its parameters
                    optimizer.step()
                    # update the optimizer learning rate
                    self.__adjust_learning_rate(optimizer)

                iter_out = "{}_loss: {:.8f}, tr_accuracy: {:.5f}".format(mode, c_loss_value, acc)
                pbar.set_description(iter_out)

                pbar.update(1)
                total_c_loss += c_loss_value
                total_accuracy += acc
                if mode == 'train':
                    self.total_train_iter += 1
                    ## FIXME: switch this back
                    # if self.total_train_iter % 2000 == 0:
                    #     self.lr /= 2
                    #     print("change learning rate", self.lr)
                    # if self.total_train_iter % 10000 == 0:
                    #     self.lr /= 2
                    #     print("FIXME: change learning rate", self.lr)

        total_c_loss = total_c_loss / total_batches
        total_accuracy = total_accuracy / total_batches
        return total_c_loss, total_accuracy
