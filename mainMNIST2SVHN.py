##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

from datasets.omniglotNShot import *
from option import *
#from experiments.OneShotMiniImageNetBuilder80 import miniImageNetBuilder80 as miniImageNetBuilder
from experiments.OneShotBuilder import OneShotBuilder
#from experiments.OneShotMiniImageNetBuilder import miniImageNetBuilder
import tqdm
from logger import Logger
import torch
import pickle
import os
import builtins
import numpy
from tu.imageUtils import *
"""
:param batch_size: Experiment batch_size
:param classes_per_set: Integer indicating the number of classes per set
:param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
"""
opt = Options()
opt.parser.add_argument('--resume2', type=str, default=None, help='resume from checkpoint')
builtins.args = opt.parse()

# Experiment Setup
mbfact = 1
batch_size = //mbfact
fce = False # FIXME: should be true
classes_per_set = args.classes_per_set
samples_per_class = args.samples_per_class
channels = 1
# Training setup
total_epochs = 200
total_train_batches = 100 * mbfact
total_val_batches = 100 * mbfact
total_test_batches = 250 * mbfact
# Parse other options
args.nr = 28
args.nc = 28
args.nch = 1
args.nr2 = 28
args.nc2 = 28
args.nch2 = 3
args.layer_size=64

args.dataroot = '/home/pareshmg/data/MNIST{}'.format(args.nr)

LOG_DIR = args.log_dir + '/mnist2svhn{}_{}_run-batchSize_{}-fce_{}-classes_per_set{}-samples_per_class{}-channels{}' \
    .format(args.nr,args.model, batch_size,fce,classes_per_set,samples_per_class,channels)

args.outDir = LOG_DIR

# create logger
logger = Logger(LOG_DIR)

##################################################
### Write code settings
with open(os.path.join(LOG_DIR, 'settings.pkl'), 'wb') as f:
    pickle.dump([args], f)

os.system('git rev-parse HEAD > {}'.format(os.path.join(LOG_DIR, 'git.log')))
os.system('git diff >> {}'.format(os.path.join(LOG_DIR, 'git.log')))

##################################################
### Setup

## set random seed
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
numpy.random.seed(args.seed)


#args.dataroot = '/home/aberenguel/Dataset/miniImagenet'
args.nch=1
mnist = MNISTNShotDataset(dataroot='/home/pareshmg/data/MNIST{}'.format(args.nr),
                          batch_size = batch_size,
                          classes_per_set=classes_per_set,
                          samples_per_class=samples_per_class)
args.nch = 3
svhn = SVHNNShotDataset(dataroot='/home/pareshmg/data/SVHN{}'.format(args.nr),
                        batch_size = batch_size,
                        classes_per_set=classes_per_set,
                        samples_per_class=samples_per_class)
args.nch=1


data = CrossNShotDataset(mnist, svhn,
                         batch_size = batch_size,
                         classes_per_set=classes_per_set,
                         samples_per_class=samples_per_class)




logger.debug('Starting with classes_per_set: {}  samples_per_class: {}'.format(classes_per_set, samples_per_class))
logger.debug('Logging to directory: {}'.format(LOG_DIR))


obj_oneShotBuilder = OneShotBuilder(data, args.model)
obj_oneShotBuilder.build_experiment(batch_size, classes_per_set, samples_per_class, channels, fce)

# for i in range(10):
#     res, res2 = obj_oneShotBuilder.run_vis()
#     makeImg(res).save(os.path.join(LOG_DIR, 'vis1_{}.png'.format(i)))
#     makeImg(res2).save(os.path.join(LOG_DIR, 'vis2_{}.png'.format(i)))
# exit(1)

# checkpoint = torch.load(args.resume)
# logger.debug('loading support from '+ args.resume)
# checkpoint['state_dict'] = {x[2:]: checkpoint['state_dict'][x] for x in checkpoint['state_dict'].keys()}
# obj_oneShotBuilder.matchingNet.gSup.load_state_dict(checkpoint['state_dict'], strict=False)
# checkpoint = torch.load(args.resume2)
#logger.debug('loading target from '+ args.resume2)
#checkpoint['state_dict'] = {x[2:]: checkpoint['state_dict'][x] for x in checkpoint['state_dict'].keys()}
#obj_oneShotBuilder.matchingNet.gTgt.load_state_dict(checkpoint['state_dict'], strict=False)
#del checkpoint
logger.debug('Loaded checkpoints')

#obj_oneShotBuilder.matchingNet.gSup.weights_freeze()
#obj_oneShotBuilder.matchingNet.gTgt.weights_freeze()

#obj_oneShotBuilder = miniImageNetBuilder(dataTrain,dataVal,dataTest, args.model)
#obj_oneShotBuilder.build_experiment(batch_size, classes_per_set, samples_per_class, channels, fce)

##################################################
### run


best_val = 0.
with tqdm.tqdm(total=total_epochs) as pbar_e:
    for e in range(0, total_epochs):
        total_c_loss, total_accuracy = obj_oneShotBuilder.run_training_epoch(total_train_batches=total_train_batches)
        logger.debug("Epoch {}: train_loss: {}, train_accuracy: {}".format(e, total_c_loss, total_accuracy))

        total_val_c_loss, total_val_accuracy = obj_oneShotBuilder.run_validation_epoch(
            total_val_batches=total_val_batches)
        logger.debug("Epoch {}: val_loss: {}, val_accuracy: {}".format(e, total_val_c_loss, total_val_accuracy))

        logger.log_value('train_loss', total_c_loss)
        logger.log_value('train_acc', total_accuracy)
        logger.log_value('val_loss', total_val_c_loss)
        logger.log_value('val_acc', total_val_accuracy)
        print('next')

        if total_val_accuracy >= best_val:  # if new best val accuracy -> produce test statistics
            best_val = total_val_accuracy
            total_test_c_loss, total_test_accuracy = obj_oneShotBuilder.run_testing_epoch(
                total_test_batches=total_test_batches)
            logger.debug("Epoch {}: test_loss: {}, test_accuracy: {}".format(e, total_test_c_loss, total_test_accuracy))
            logger.log_value('test_loss', total_test_c_loss)
            logger.log_value('test_acc', total_test_accuracy)

            save_checkpoint({
                'epoch': e,
                'args': args,
                'state_dict': obj_oneShotBuilder.matchingNet.state_dict(),
                'test_loss': total_test_c_loss,
                'test_acc': total_test_accuracy,
            }, False, LOG_DIR)
            logger.debug('saved model to {}'.format(LOG_DIR))
            print('next')
        else:
            total_test_c_loss = -1
            total_test_accuracy = -1

        pbar_e.update(1)
        logger.step()
