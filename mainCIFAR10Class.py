from datasets import omniglotNShot
from option import *
#from experiments.OneShotMiniImageNetBuilder80 import miniImageNetBuilder80 as miniImageNetBuilder
from experiments.OneShotBuilder import ClassificationBuilder
#from experiments.OneShotMiniImageNetBuilder import miniImageNetBuilder
import tqdm
import torch
import os
import builtins
import numpy

'''
:param batch_size: Experiment batch_size
:param classes_per_set: Integer indicating the number of classes per set
:param samples_per_class: Integer indicating samples per class
        e.g. For a 20-way, 1-shot learning task, use classes_per_set=20 and samples_per_class=1
             For a 5-way, 10-shot learning task, use classes_per_set=5 and samples_per_class=10
'''

builtins.cargs, logger = Options().parse('CIFAR10',
                                nr=28,
                                nc=28,
                                nch=3,
                                # Experiment Setup
                                # total_train_batches = 1,
                                # total_val_batches = 1,
                                # total_test_batches = 2,
                                total_train_batches = 1000,
                                total_val_batches = 500,
                                total_test_batches = 1000,
                                layer_size = 64
)

data = omniglotNShot.CIFAR10Dataset(root=cargs.dataroot,
                                    batch_size = cargs.batch_size)


logger.debug('Starting with classes_per_set: {}  samples_per_class: {}'.format(cargs.classes_per_set,
                                                                               cargs.samples_per_class))
logger.debug('Logging to directory: {}'.format(cargs.log_dir))


objBuilder = ClassificationBuilder(data, cargs.model)
objBuilder.build_experiment(cargs.batch_size,
                            cargs.mini_batch_size,
                            cargs.nch,
                            cargs.fce)


##################################################
### run
runExperiment(objBuilder, logger)
