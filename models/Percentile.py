import builtins
import torch
import torch.nn as nn
import unittest
import os
import numpy as np
from models.BidirectionalLSTM import BidirectionalLSTM
from models.Classifier import *
from models.DistanceNetwork import *
from models.AttentionalClassify import AttentionalClassify
import torch.nn.functional as F
from torch.autograd import Variable, Function
from models.vgg import *

class PercentileSelectHigh(Function):
    def __init__(self, pct, dim):
        super(PercentileSelectHigh, self).__init__()
        self.pct = pct
        self.dim = dim

    def forward(self, x):
        y,i = x.sort(self.dim)
        n = int(x.size(self.dim) * self.pct)
        i = i.narrow(self.dim, x.size(self.dim)-n,n)
        y = y.narrow(self.dim, x.size(self.dim)-n,n)
        self.save_for_backward(x,i)

        return y,i

    def backward(self, grad_output, _):
        x,i, = self.saved_tensors
        res = x.clone().zero_()
        res.scatter_(self.dim, i, grad_output)
        return res


class PercentileSelectLow(Function):
    def __init__(self, pct, dim):
        super(PercentileSelectLow, self).__init__()
        self.pct = pct
        self.dim = dim

    def forward(self, x):
        y,i = x.sort(self.dim)
        n = int(x.size(self.dim) * self.pct)
        i = i.narrow(self.dim, 0, n)
        y = y.narrow(self.dim, 0, n)
        self.save_for_backward(x,i)

        return y,i

    def backward(self, grad_output, _):
        x,i, = self.saved_tensors
        res = x.clone().zero_()
        res.scatter_(self.dim, i, grad_output)
        return res
