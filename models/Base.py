import torch
from torch.nn import Parameter
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
import unittest
import numpy
import numpy as np
import math
import torchsample

class MyModule(nn.Module):
    def __init__(self, **kwargs):
        super(MyModule, self).__init__(**kwargs)
        self.visMode = False
        self.forVis = {}

    def vis(self, flag=True):
        self.forVis = {} # free mem
        if self.visMode == flag: return # nothing to do
        self.visMode = flag
        for m in self.modules():
            try:
                m.vis(flag)
            except:
                pass


    def load_state_dict(self, state_dict, strict=True):
        """Copies parameters and buffers from :attr:`state_dict` into
        this module and its descendants. If :attr:`strict` is ``True`` then
        the keys of :attr:`state_dict` must exactly match the keys returned
        by this module's :func:`state_dict()` function.

        Arguments:
            state_dict (dict): A dict containing parameters and
                persistent buffers.
            strict (bool): Strictly enforce that the keys in :attr:`state_dict`
                match the keys returned by this module's `:func:`state_dict()`
                function.
        """
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, Parameter):
                    # backwards compatibility for serialized parameters
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    raise RuntimeError('While copying the parameter named {}, '
                                       'whose dimensions in the model are {} and '
                                       'whose dimensions in the checkpoint are {}.'
                                       .format(name, own_state[name].size(), param.size()))
            elif strict:
                raise KeyError('unexpected key "{}" in state_dict'
                               .format(name))
        if strict:
            missing = set(own_state.keys()) - set(state_dict.keys())
            if len(missing) > 0:
                raise KeyError('missing keys in state_dict: "{}"'.format(missing))



class Identity(MyModule):
    def forward(self, x):
        return x

class Identity2(MyModule):
    def forward(self, x, y):
        return x, y





class RBM(MyModule):
    """
    linear, relu, linear, relu, ... , relu, linear

    Last layer does not get relu
    """
    def __init__(self, kerns):
        super(RBM, self).__init__()
        layers = []
        for i in range(1,len(kerns)):
            layers.append(nn.Linear(kerns[i-1], kerns[i]))
            if i < len(kerns)-1: layers.append(nn.ReLU())
        self.mod = nn.Sequential(*layers)
    def forward(self, x):
        return self.mod(x)


class StackEmbed(MyModule):
    def __init__(self, kerns):
        super(StackEmbed, self).__init__()
        self.mod = RBM(kerns)

    def forward(self, x):
        """
        Produces pdfs over the support set classes for the target set image.
        :param x: The fv  of input image :: [batch_size, ndim, nsamples]
        :return: res :: [batch_size, ndim2, nsamples]
        """
        nb, ndim, ns = x.size()
        x = self.mod(x.permute(0,2,1)).permute(0,2,1)
        return x

class GetBlocks2D(MyModule):
    """
    [nb,.., nr, nc]
    """
    def __init__(self, width, stride=None, dim0=2, dim1=3, padding=0):
        super(GetBlocks2D, self).__init__()
        self.dim0 = dim0
        self.dim1 = dim1
        self.width = width
        self.stride = width if stride is None else stride
        self.pd = nn.ZeroPad2d(padding) if padding > 0 else Identity()
    def forward(self, x):
        x = self.pd(x)
        res = []
        s = self.stride
        w = self.width
        for i1 in range(0,x.size(self.dim1)-w+1,s):
            x1 = x.narrow(self.dim1, i1, w)
            res0 = []
            for i0 in range(0,(x.size(self.dim0)-w+1), s):
                x0 = x1.narrow(self.dim0, i0, w)
                res0.append(x0)
            res.append(torch.stack(res0, dim=-4))
        res = torch.stack(res, dim=-4)
        #print('getblocks res', res.size())
        return res


class RandomAffine(MyModule):
    def __init__(self, naffine):
        super(RandomAffine, self).__init__()
        self.naffine = naffine

    def forward(self, img):
        """
        img :: [nb, ns, ndim, nr, nc]
        """
        #print('RandomAffine', img.size())
        #return img.unsqueeze(2)
        res = []
        for i in range(self.naffine):
            r = torchsample.transforms.Rotate(numpy.random.randint(-45,45))
            z = torchsample.transforms.Zoom(numpy.random.randint(100,125)/100.0)
            s = torchsample.transforms.Shear(numpy.random.randint(-30,30))
            a = torchsample.transforms.AffineCompose([r,z,s])

            nb, ns, ndim, nr, nc = img.size()

            res.append( a(img.data.cpu().view(nb*ns*ndim, nr, nc)).view(nb, ns, ndim, nr,nc).cuda() )

        res = Variable(torch.stack(res, dim=2), requires_grad=True)
        #print('RandomAffine res', res.size())
        return res
