import torch
from torch.nn import Parameter
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
import unittest
import numpy
import numpy as np
import math
import torchsample
from models.Base import *
from models.Classifier import convLayer
import torch.nn.functional as F



class ParamGenCNN(MyModule):
    def __init__(self, kerns, genKerns):
        """
        kerns :: embDim = K0 -> ... -> Kn-1 -> self.nparams (added on in end)
        args.nch -> k0 -> ... -> kn-1 = self.nparams

        genKerns :: kerns to generate params for
        blockEmbDim -> K0 -> K1 -> ... -> Kn-1 = 1

        """
        super(ParamGenCNN, self).__init__()
        self.kernel_size = 3
        self.stride = 1
        self.padding = 1

        self.genKerns = genKerns
        self.nb = self.genKerns[1:]
        self.nw = [self.genKerns[i] * self.genKerns[i-1] * self.kernel_size * self.kernel_size for i in range(1,len(self.genKerns))]
        self.nparams = sum(self.nb) + sum(self.nw)
        self.paramGen = RBM(kerns + [ self.nparams])

        self.initWeights()


    def initWeights(self):
        m = list(self.paramGen.modules())[-2]
        m.weight.data *= 1e-5
        ps = m.bias
        pi = 0
        for i in range(len(self.nw)):
            k0 = self.genKerns[i]
            k1 = self.genKerns[i+1]
            nk = self.nw[i]
            w = ps.narrow(0,pi,nk).view(k1, k0, self.kernel_size, self.kernel_size)
            init.xavier_uniform(w, gain=np.sqrt(2))
            pi += nk

            b = ps.narrow(0,pi,k1) # [nb, k1]
            init.constant(b, 0)
            pi += k1
        print('initialized CNN gen weights : ', ps.size())

    def forward(self, inp):
        """
        inp: [nb, ndim]
        """
        #print('kernGen', inp.size())
        nb = inp.size(0)
        #print(self.paramGen)
        ps = self.paramGen(inp).contiguous().view(nb, self.nparams) # [nb,  nparams]
        res = []
        pi = 0
        for i in range(len(self.nw)):
            k0 = self.genKerns[i]
            k1 = self.genKerns[i+1]
            nk = self.nw[i]
            w = ps.narrow(1,pi,nk).contiguous().view(nb, k1, k0, self.kernel_size, self.kernel_size) # [nb,k1,k0,w,w]
            pi += nk

            b = ps.narrow(1,pi,k1) # [nb, k1]
            pi += k1
            res.append((w.contiguous(),b.contiguous()))
        return res



class ParamCNN(MyModule):
    def __init__(self, kerns, stride=1, padding=1, dilation=1, batchNorm=True):
        super(ParamCNN, self).__init__()
        self.stride = stride
        self.padding = padding
        self.dilation = dilation
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.kerns = kerns
        self.bns = None
        if batchNorm:
            self.bns = nn.ModuleList([nn.BatchNorm2d(k) for k in self.kerns[1:]])
            for m in self.bns:
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, params, x):
        """
        params: [ (W, b) ]
        W : [nb, k2, k1, w, w]
        b: [nb, k2]

        x: [nb, nch, nr, nc]

        """
        nb, nch, nr, nc = x.size()
        x = x.contiguous() #.view(1, nb*nch, nr, nc)

        for (i, (W, b)) in enumerate(params):
            nb2, k2, k1, w1, w2 = W.size()
            W = W.view(nb2*k2, k1, w1, w2)
            b = b.view(nb2*k2)
            x = x.view(1,nb*k1, x.size(2), x.size(3))
            x = torch.nn.functional.conv2d(x, W, bias=b, stride=self.stride, padding=self.padding, dilation = self.dilation, groups = nb)
            #x = torch.cat([torch.nn.functional.conv2d(x[bi:bi+1], W[bi], bias=b[bi], stride=self.stride, padding=self.padding, dilation=self.dilation, groups=self.groups) for bi in range(x.size(0))], dim=0)
            if self.bns is not None: x = self.bns[i](x.view(nb, k2, x.size(2), x.size(3)))
            x = F.relu(x)
            x = self.maxpool(x)



        return x



class ParamGenRBM(MyModule):
    def __init__(self, kerns, genKerns):
        """
        kerns :: embDim = K0 -> ... -> Kn-1 -> self.nparams (added on in end)
        args.nch -> k0 -> ... -> kn-1 = self.nparams

        genKerns :: kerns to generate params for
        blockEmbDim -> K0 -> K1 -> ... -> Kn-1 = 1

        """
        super(ParamGenRBM, self).__init__()
        self.genKerns = genKerns
        self.nb = self.genKerns[1:]
        self.nw = [self.genKerns[i-1] * self.genKerns[i] for i in range(1,len(self.genKerns))]
        self.nparams = sum(self.nb) + sum(self.nw)

        self.paramGen = RBM(kerns + [self.nparams])
        self.initWeights()

    def initWeights(self):
        m = list(self.paramGen.modules())[-2]
        m.weight.data *= 1e-3
        ps = m.bias
        print('initializing RBM gen weights : ', ps.size())
        pi = 0
        for i in range(len(self.nw)):
            k0 = self.genKerns[i]
            k1 = self.genKerns[i+1]
            nk = self.nw[i]
            mod = nn.Linear(k0,k1)
            w = ps.narrow(0,pi,nk).view(k1, k0, self.kernel_size, self.kernel_size)
            w.data.copy_(mod.weight.data)
            pi += nk

            b = ps.narrow(0,pi,k1) # [nb, k1]
            b.data.zero_() #copy_(mod.bias.data)
            pi += k1

    def forward(self, inp):
        """
        inp: [nb, ndim]
        """
        #print('kernGen', inp.size())
        nb = inp.size(0)
        #print(self.paramGen)
        ps = self.paramGen(inp).contiguous().view(nb, self.nparams) # [nb,nparams]
        res = []
        pi = 0
        for i in range(len(self.nw)):
            k0 = self.genKerns[i]
            k1 = self.genKerns[i+1]
            w = ps.narrow(1,pi, k0*k1).contiguous().view(nb, k0, k1) # [nb, k0, k1]
            pi += k0*k1

            b = ps.narrow(1,pi,k1) # [nb, k1]
            pi += k1
            res.append((w,b))
        return res

class ParamRBM(MyModule):
    def forward(self, params, x):
        """
        params: [ (W, b) ]
        W : [nb, k1, k2]
        b: [nb, k2]

        x: [nb, ndim]
        res : [nb, ndim] 0->1

        net: ndim*2 -> k1 -> k2 ... -> kn =1

        """
        x = x.unsqueeze(1) # [nb, 1, ndim]
        for (i, (W, b)) in enumerate(params):
            x = torch.matmul(x, W) + b.unsqueeze(1)
            if i < (len(params)-1): x = F.relu(x)

        return x.squeeze(1)
