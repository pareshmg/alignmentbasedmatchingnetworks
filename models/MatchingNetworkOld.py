import builtins
import torch
import torch.nn as nn
import unittest
import os
import numpy as np
from models.BidirectionalLSTM import BidirectionalLSTM
from models.Classifier import *
from models.DistanceNetwork import *
from models.AttentionalClassify import *
import torch.nn.functional as F
from torch.autograd import Variable, Function
from models.vgg import *
from models.Base import *
from models.Jigsaw import *
from models.ParamNet import *

class GenNet(MyModule):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(GenNet, self).__init__()

        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param is_training: Flag indicating whether we are training or evaluating
        :param rotate_flag: Flag indicating whether to rotate the images
        :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
        :param num_classes_per_set: Integer indicating the number of classes per set
        :param num_samples_per_class: Integer indicating the number of samples per class
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.fce = fce
        if fce:
            self.lstm = BidirectionalLSTM(layer_sizes=[32], batch_size=self.batch_size, vector_dim = self.g.outSize)
        self.dn = DistanceNetwork()
        self.classify = AttentionalClassify()

        self.keep_prob = keep_prob
        self.num_classes_per_set = num_classes_per_set
        self.num_samples_per_class = num_samples_per_class
        self.learning_rate = learning_rate
        self.embedder = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                                   nClasses= 0, image_size = image_size )
        cnnKerns = [args.nch,32,64,64,64] if image_size < 80 else [args.nch, 32, 64,64,64,64,64]
        self.kernGen = ParamGenCNN(kerns=[64, 128, 256], genKerns = cnnKerns)
        self.guessNet = ParamCNN(cnnKerns)


    def getEncoders(self, num_channels, image_size):
        gSup = Identity()
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size,1,  n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        accuracy = 0
        testAccuracy = 0
        accuracy_guess = 0
        loss_guess = 0
        nb, ns, nch, nr, nc = support_set_images.size()
        nt = target_image.size(1)
        # produce embeddings for support set images


        # get target emb
        for i in range(target_image.size(1)):
            target_emb0 = self.embedder(target_image.select(1,i))[-1].squeeze(3).squeeze(2) # [nb, ndim]
            params =  self.kernGen(target_emb0)
            support_emb = torch.stack([self.guessNet(params, support_set_images[:,si]).squeeze(3).squeeze(2) for si in range(ns)], dim=0) # [ns, nb, ndim]
            target_emb = self.guessNet(params, target_image[:,i]).squeeze(3).squeeze(2)


            similarities = self.dn(support_set = support_emb,
                                   target_image = target_emb)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()



        return {'acc':  accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class GenReducedNet(MyModule):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(GenReducedNet, self).__init__()

        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param is_training: Flag indicating whether we are training or evaluating
        :param rotate_flag: Flag indicating whether to rotate the images
        :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
        :param num_classes_per_set: Integer indicating the number of classes per set
        :param num_samples_per_class: Integer indicating the number of samples per class
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.fce = fce
        if fce:
            self.lstm = BidirectionalLSTM(layer_sizes=[32], batch_size=self.batch_size, vector_dim = self.g.outSize)
        self.dn = DistanceNetwork()
        self.classify = AttentionalClassify()

        self.keep_prob = keep_prob
        self.num_classes_per_set = num_classes_per_set
        self.num_samples_per_class = num_samples_per_class
        self.learning_rate = learning_rate
        self.embedder = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                                   nClasses= 0, image_size = image_size )
        cnnKerns = [args.nch,8,8,8,8] if image_size < 80 else [args.nch, 8, 8, 8, 8, 8, 8]
        self.kernGen = ParamGenCNN(kerns=[64, 128, 256], genKerns = cnnKerns)
        self.guessNet = ParamCNN(cnnKerns)


    def getEncoders(self, num_channels, image_size):
        gSup = Identity()
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size,1,  n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        accuracy = 0
        testAccuracy = 0
        accuracy_guess = 0
        loss_guess = 0
        nb, ns, nch, nr, nc = support_set_images.size()
        nt = target_image.size(1)
        # produce embeddings for support set images


        # get target emb
        for i in range(target_image.size(1)):
            target_emb0 = self.embedder(target_image.select(1,i))[-1].squeeze(3).squeeze(2) # [nb, ndim]
            params =  self.kernGen(target_emb0)
            support_emb = torch.stack([self.guessNet(params, support_set_images[:,si]).squeeze(3).squeeze(2) for si in range(ns)], dim=0) # [ns, nb, ndim]
            target_emb = self.guessNet(params, target_image[:,i]).squeeze(3).squeeze(2)


            similarities = self.dn(support_set = support_emb,
                                   target_image = target_emb)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()



        return {'acc':  accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class BinaryGenNet(MyModule):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(BinaryGenNet, self).__init__()

        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param is_training: Flag indicating whether we are training or evaluating
        :param rotate_flag: Flag indicating whether to rotate the images
        :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
        :param num_classes_per_set: Integer indicating the number of classes per set
        :param num_samples_per_class: Integer indicating the number of samples per class
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.fce = fce
        if fce:
            self.lstm = BidirectionalLSTM(layer_sizes=[32], batch_size=self.batch_size, vector_dim = self.g.outSize)
        #self.classify = OneHotClassify()
        self.classify = AttentionalClassify()

        self.keep_prob = keep_prob
        self.num_classes_per_set = num_classes_per_set
        self.num_samples_per_class = num_samples_per_class
        self.learning_rate = learning_rate
        self.embedder = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                                   nClasses= 0, image_size = image_size )
        self.genCNN = ParamGenCNN(kerns=[64, 128, 256],
                                  genKerns=[args.nch,32,64,64,64] if image_size < 80 else [args.nch, 32, 64,64,64,64,64])
        self.guessEnc = ParamCNN()

        self.genRBM = ParamGenRBM(kerns=[64,128],
                                  genKerns=[64,1])
        self.guessNet = ParamRBM()


    def getEncoders(self, num_channels, image_size):
        gSup = Identity()
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size,1,  n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        accuracy = 0
        testAccuracy = 0
        accuracy_guess = 0
        loss_guess = 0
        nb, ns, nch, nr, nc = support_set_images.size()
        nt = target_image.size(1)
        # produce embeddings for support set images


        # get target emb
        for i in range(target_image.size(1)):
            target_emb0 = self.embedder(target_image.select(1,i))[-1].squeeze(3).squeeze(2) # [nb, ndim]
            cnnparams = self.genCNN(target_emb0)
            rbmparams = self.genRBM(target_emb0)
            support_prob = torch.stack([self.guessNet(rbmparams,
                                                      self.guessEnc(cnnparams, support_set_images[:,si]).squeeze(3).squeeze(2)).squeeze(1) for si in range(ns)], dim=1) # [nb, ns]
            target_prob = self.guessNet(rbmparams,
                                        self.guessEnc(cnnparams, target_image[:,i]).squeeze(3).squeeze(2)).squeeze(1) # [nb]

            support_prob = F.sigmoid(support_prob)
            lp0 = (1-support_prob+1e-12).log() # [nb, ns]
            lp0s = lp0.sum(1) # [nb]
            lp1 = (support_prob+1e-12).log() # [nb, ns]
            preds = lp1 + lp0s.unsqueeze(1) - lp0 # [nb, ns]
            lself = (1-F.sigmoid(target_prob)+1e-12).log() # [nb]
            lself = lself * 0
            # produce predictions for target probabilities
            preds = self.classify(preds,support_set_y=support_set_labels_one_hot) # [nb, ns]

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long()) + lself.mean()
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long()) + lself.mean()


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()



        return {'acc':  accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}

class GenDiffNet(MyModule):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(GenDiffNet, self).__init__()

        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param is_training: Flag indicating whether we are training or evaluating
        :param rotate_flag: Flag indicating whether to rotate the images
        :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
        :param num_classes_per_set: Integer indicating the number of classes per set
        :param num_samples_per_class: Integer indicating the number of samples per class
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.fce = fce
        if fce:
            self.lstm = BidirectionalLSTM(layer_sizes=[32], batch_size=self.batch_size, vector_dim = self.g.outSize)
        self.dn = DistanceNetwork()
        self.classify = AttentionalClassify()

        self.keep_prob = keep_prob
        self.num_classes_per_set = num_classes_per_set
        self.num_samples_per_class = num_samples_per_class
        self.learning_rate = learning_rate
        self.embedder = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                                   nClasses= 0, image_size = image_size )
        cnnKerns = [args.nch,32,64,64,64] if image_size < 80 else [args.nch, 32, 64,64,64,64,64]
        self.kernGen = ParamGenCNN(kerns=[64, 128, 256], genKerns=cnnKerns)
        self.kernGen0 = ParamGenCNN(kerns=[1], genKerns = cnnKerns)
        self.guessNet = ParamCNN(cnnKerns)


    def getEncoders(self, num_channels, image_size):
        gSup = Identity()
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size,1,  n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        accuracy = 0
        testAccuracy = 0
        accuracy_guess = 0
        loss_guess = 0
        nb, ns, nch, nr, nc = support_set_images.size()
        nt = target_image.size(1)
        # produce embeddings for support set images

        params0 = self.kernGen0(target_image[:,0:1,0,0,0] * 0)
        #params0 = self.kernGen(target_image.view(nb, -1).narrow(1,0,args.layer_size) * 0)

        # get target emb
        for i in range(target_image.size(1)):
            #target_emb0 = self.embedder(target_image.select(1,i))[-1].squeeze(3).squeeze(2) # [nb, ndim]
            target_emb0 = self.guessNet(params0, target_image.select(1,i)).squeeze(3).squeeze(2) # [nb, ndim]

            params =  self.kernGen(target_emb0)
            #params = [(W0+W1, (b0+b1)/2) for ((W0,b0), (W1,b1)) in zip(params0, params)]

            support_emb = torch.stack([self.guessNet(params, support_set_images[:,si]).squeeze(3).squeeze(2) for si in range(ns)], dim=0) # [ns, nb, ndim]
            target_emb = self.guessNet(params, target_image[:,i]).squeeze(3).squeeze(2)


            similarities = self.dn(support_set = support_emb,
                                   target_image = target_emb)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()



        return {'acc':  accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class JigsawNet(MyModule):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(JigsawNet, self).__init__()

        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param is_training: Flag indicating whether we are training or evaluating
        :param rotate_flag: Flag indicating whether to rotate the images
        :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
        :param num_classes_per_set: Integer indicating the number of classes per set
        :param num_samples_per_class: Integer indicating the number of samples per class
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.fce = fce
        if fce:
            self.lstm = BidirectionalLSTM(layer_sizes=[32], batch_size=self.batch_size, vector_dim = self.g.outSize)
        self.dn = DistanceNetwork()
        self.classify = MaxClassify()
        #self.classify = AttentionalClassify()
        self.keep_prob = keep_prob
        self.num_classes_per_set = num_classes_per_set
        self.num_samples_per_class = num_samples_per_class
        self.learning_rate = learning_rate
        self.partWidth = 14
        self.nExptAffine = 1
        self.nExptGuess = 8
        self.imgTransform = RandomAffine(self.nExptAffine)
        self.imgBlocks = GetBlocks2D(stride=self.partWidth,
                                     dim0=4,
                                     dim1=5)
        self.embedder = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                                   nClasses= 0, image_size = image_size )
        block_layer_size = args.layer_size
        self.blockEmbeder = BlockClassifier(layer_size = block_layer_size, num_channels=num_channels,
                                            nClasses= 0, image_size = self.partWidth )
        self.kernGen = nn.ModuleList([KernGen(1,
                                              [args.layer_size, 2*args.layer_size, 4*args.layer_size],
                                              [block_layer_size*2, 128, 1]) for i in range(4)])
        # self.kernGen = nn.ModuleList([KernGen([args.layer_size],
        #                                       [block_layer_size*2, 1]) for i in range(4)])

        self.jigsawExpt = JigsawExperimentBuilder(self.nExptGuess)
        self.guessNet = NeighborGuessNet()



        self.testGuesser = RBM([block_layer_size*2, 128,1])
        #self.testGuesser = nn.Linear(block_layer_size*2,1)


        self.w12 = Parameter(nn.Linear(block_layer_size*2,128).weight.transpose(0,1).data) #Parameter(torch.randn(1,block_layer_size*2, 128))
        self.b2 = Parameter(nn.Linear(block_layer_size*2,128).bias.data)
        self.w23 = Parameter(nn.Linear(128,1).weight.transpose(0,1).data) #Parameter(torch.randn(1,block_layer_size*2, 128))
        self.b3 = Parameter(nn.Linear(128,1).bias.data)

        #self.b2 = Parameter(torch.zeros(1,128))
        #self.w23 = Parameter(torch.randn(1, 128, 1))
        #self.b3 = Parameter(torch.randn(1,1))


    def getEncoders(self, num_channels, image_size):
        gSup = Identity()
        return [gSup]


    def getLoss(self, res, pred):
        """
        nb, ns, nb2
        """

        assert (pred.data.max() <= 1), "{}".format(pred.data.max())
        assert (pred.data.min() >= 0), "{}".format(pred.data.min())
        assert (res.data.max() <= 1), "{}".format(res.data.max())
        assert (res.data.min() >= 0), "{}".format(res.data.min())
        loss = pred * (res+1e-12).log() + (1-pred) * (1-res+1e-12).log()
        loss = -loss.mean(2)
        return loss


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size,1,  n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        accuracy = 0
        testAccuracy = 0
        accuracy_guess = 0
        loss_guess = 0
        nb, ns, nch, nr, nc = support_set_images.size()
        nt = target_image.size(1)
        # produce embeddings for support set images


        # random affine images

        expt_imgs = torch.cat([target_image, support_set_images], dim=1)
        #expt_imgs = self.imgTransform(expt_imgs) # [nb, ns, naff, nch, nr, nc]
        expt_imgs = expt_imgs.unsqueeze(2) # [nb, ns, naff, nch, nr, nc]
        expt_blocks = self.imgBlocks(expt_imgs) # [nb, ns, naff,  nr/m,nc/m, nch, m,m]


        nb, ns, naff, nr, nc, nch, mr, mc = expt_blocks.size()
        base_blocks, nbr_blocks, nbrLabels = self.jigsawExpt(expt_blocks.view(nb, ns, naff, nr, nc, nch*mr*mc))
        # nbr_blocks [nb, nt+ns, nexpt, nch*mr*mc]

        nexpt = nbr_blocks.size(3)
        ndir = nbr_blocks.size(0)

        if self.visMode:
            self.forVis['expt_imgs'] = expt_imgs.data.clone()
            self.forVis['nbr_blocks'] = nbr_blocks.data.view(ndir,nb, ns, nexpt, nch, mr, mc).clone()
            self.forVis['base_blocks'] = base_blocks.data.view(nb, ns, nexpt, nch, mr, mc).clone()
            self.forVis['nbr_labels'] = nbrLabels.data.clone()

        nbr_blocks = nbr_blocks.view(ndir * nb * ns * nexpt, nch, mr, mc)
        base_blocks = base_blocks.view(nb * ns * nexpt, nch, mr, mc)
        nbrs = self.blockEmbeder(nbr_blocks).view(ndir, nb, ns, nexpt, -1).contiguous() # [nb, ns, nexpt, ndim]
        base = self.blockEmbeder(base_blocks).view(nb, ns, nexpt, -1).contiguous() # [nb, ns, nexpt, ndim]

        # random guesses
        # base     : [nb, nt+ns, nexpt, ndim],
        # nbrs     : [ndir, nb, nt+ns, nexpt, ndim ]
        # nbrLabel : [ndir, nexpt] -> 1 if actually nbr 0 otherwise

        # get target emb
        for i in range(target_image.size(1)):
            target_emb = self.embedder(target_image.select(1,i))[-1].squeeze(3).squeeze(2)

            ## generate params
            guessLoss = None
            guess_acc = 0
            guess_num = 0
            testGuessLoss = None
            for iDir in range(nbrs.size(0)):
                params =  self.kernGen[iDir](target_emb)
                res = torch.cat([base, nbrs[iDir]], dim=3)
                res = self.guessNet(params, res) # [nb, nt+ns, nexpt, nnets, 1]
                #res = self.testGuesser(res)
                res = F.sigmoid(res).squeeze(3).squeeze(3)
                lab = nbrLabels[iDir].view(1,1,-1).repeat(nb, ns, 1)
                testRes = res
                if guessLoss is None:
                    guessLoss = self.getLoss(res, lab) # [nb, nt+ns]
                    testGuessLoss = F.binary_cross_entropy(testRes, lab)
                else:
                    guessLoss += self.getLoss(res, lab)  # [nb, nt+ns]
                    testGuessLoss += F.binary_cross_entropy(testRes, lab)
                guess_acc += (lab.data == res.data.round()).sum()
                guess_num += lab.data.nelement()
                testAccuracy += (lab.data == testRes.data.round()).sum()

            #print('guessloss', guessLoss.size(), target_image.size(1), support_set_images.size(1))
            ##################################################
            # compute model loss
            # produce predictions for target probabilities
            tguessLoss = guessLoss.narrow(1,0,target_image.size(1)) # [nb, nt]

            similarities = -guessLoss.narrow(1,nt, support_set_images.size(1)) # [nb, ns]
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)
            #pmask = (preds * 0).scatter_(1, target_label[:,i], 1)
            #preds = (preds * pmask) - (preds * (1-pmask))

            # sguessLoss : minimize loss of correct label and maximize loss of incorrect label
            #mul = preds * 0 + 1
            #mul[target_label[:,i]] = -1
            sguessLoss = -preds.index_select(1,target_label[:,i])
            a2 = sguessLoss.mean()
            a1 = tguessLoss.mean()
            a3 = F.cross_entropy(preds, target_label[:,i].long())
            testGuessLoss = a3
            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                #accuracy = guess_acc / guess_num
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float()).data[0]
                accuracy_guess = testAccuracy / guess_num
                #crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long()) + 10 * tguessLoss
                crossentropy_loss = testGuessLoss.mean()
                loss_guess = (a1 + a2).data.mean()
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float()).data[0]
                #accuracy += guess_acc / guess_num
                accuracy_guess += testAccuracy / guess_num
                #crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long()) + 10 * tguessLoss
                crossentropy_loss += testGuessLoss.mean()
                loss_guess += (a1 + a2).data.mean()


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()


            if self.visMode:
                self.forVis['base'] = base.data.clone()
                self.forVis['nbrs'] = nbrs.data.clone()
                self.forVis['nbrLabels'] = nbrLabels.data.clone()
                self.forVis['support_set_images'] = support_set_images.data.clone()
                self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
                self.forVis['target_image'] = target_image.data.clone()
                self.forVis['target_label'] = target_label.data.clone()



        #print(self.w12.data.mean(), self.w12.data.max())
        return {'acc':  accuracy/target_image.size(1),
                'guess_acc': accuracy_guess / target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'guess_loss': loss_guess / target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}




class MatchingNetworkPWNeighborhoods(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWNeighborhoods, self).__init__(*args, **kwargs)
        from tu.modules.ccl import PairwiseDiffL2
        self.g = StackFV(kwargs.get('image_size'))
        self.dn = DistanceNetworkPWNeighborhood(kwargs.get('image_size'))
        self.delta = 2
        self.pd = PairwiseDiffL2()

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        # produce embeddings for support set images
        nb, ns, nk, nr, nc = support_set_images.size()


        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= 0.2)).select(1,0), requires_grad=False) # sample 10%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= 0.1)).select(1,0), requires_grad=False) # sample 20%

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()

        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0


        ntgt = rsampTgt.size(0)
        tgtr = rsampTgt.data / nc # [ntgt]
        tgtc = rsampTgt.data % nc # [ntgt]


        nbrhood = (torch.abs(tgtr.view(1,ntgt) - tgtr.view(ntgt, 1)) <= self.delta) & (torch.abs(tgtc.view(1,ntgt) - tgtc.view(ntgt, 1)) <= self.delta) & (torch.eye(ntgt).cuda() != 1)


        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)


        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]
            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)

            # get similarity between support set embeddings and target
            #similarities = self.dn(support_set=suppFV, target_image=targetFV)
            similarities = self.dn(supportSetFV=suppFV,
                                   targetFV=targetFV,
                                   rsampSup=rsampSup,
                                   rsampTgt=rsampTgt,
                                   nbrhood = nbrhood)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long())

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])


        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}

class MatchingNetworkPWNeighborhoodDists(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWNeighborhoodDists, self).__init__(*args, **kwargs)
        from tu.modules.ccl import PairwiseDiffL2
        self.g = StackFV(kwargs.get('image_size'))
        self.dn = DistanceNetworkPWNeighborhoodDists(kwargs.get('image_size'))
        self.delta = 2
        self.pd = PairwiseDiffL2()

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        # produce embeddings for support set images
        nb, ns, nk, nr, nc = support_set_images.size()


        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= 0.2)).select(1,0), requires_grad=False) # sample 10%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= 0.1)).select(1,0), requires_grad=False) # sample 20%

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()

        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0


        ntgt = rsampTgt.size(0)
        tgtr = rsampTgt.data / nc # [ntgt]
        tgtc = rsampTgt.data % nc # [ntgt]
        tgtxy = Variable(torch.stack([tgtr, tgtc], dim=1).float(), requires_grad=False) # [ntgt, 2]

        nsup = rsampSup.size(0)
        supr = rsampSup.data / nc # [nsup]
        supc = rsampSup.data % nc # [nsup]
        supxy = Variable(torch.stack([supr, supc], dim=1).float(), requires_grad=False) # [nsup, 2]


        nnbr = 16
        assert(ntgt >= nnbr)


        dii = self.pd(tgtxy.unsqueeze(0), tgtxy.unsqueeze(0)).squeeze(0).mul_(2).sqrt()  # [ntgt, nnbr]
        djj = self.pd(supxy.unsqueeze(0), supxy.unsqueeze(0)).squeeze(0).mul_(2).sqrt() # [nsup, nsup]


        nbrhood = dii.sort(dim=1)[1].narrow(1,0,nnbr)

        dii = dii.gather(1,nbrhood) ## select the nbr nodes


        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)


        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]
            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)

            # get similarity between support set embeddings and target
            #similarities = self.dn(support_set=suppFV, target_image=targetFV)
            similarities = self.dn(supportSetFV=suppFV,
                                   targetFV=targetFV,
                                   rsampSup=rsampSup,
                                   rsampTgt=rsampTgt,
                                   nbrhood = nbrhood,
                                   dii = dii,
                                   djj = djj)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long())

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])


        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}




class MatchingNetworkPWAttnReg(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWAttnReg, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnReg(builtins.args.stackFVSize)
        self.g = StackFV(kwargs.get('image_size'))

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.randperm(nr*nc)[:50].cuda(), requires_grad=False)
        rsampTgt = Variable(torch.randperm(nr*nc)[:75].cuda(), requires_grad=False)

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)

            # get similarity between support set embeddings and target
            similarities, tmaskreg = self.dn(support_set=suppFV, target_image=targetFV)
            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long()) + 10 * tmaskreg
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long()) + 10 * tmaskreg

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}

class MatchingNetworkPWAttnRegFullMask(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWAttnRegFullMask, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnReg(builtins.args.stackFVSize)
        self.g = StackFV(kwargs.get('image_size'))
        ndim = builtins.args.stackFVSize
        self.mask = StackEmbed([ndim, 128, 1])

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.randperm(nr*nc)[:50].cuda(), requires_grad=False)
        rsampTgt = Variable(torch.randperm(nr*nc)[:75].cuda(), requires_grad=False)

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        supMask = torch.stack([self.mask(x) for x in suppFV])

        import ipdb; ipdb.set_trace()


        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            tgtMask = self.mask(targetFV)

            # get similarity between support set embeddings and target
            similarities, tmaskreg = self.dn(support_set=suppFV, target_image=targetFV)
            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long()) + 10 * tmaskreg
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long()) + 10 * tmaskreg

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}




class MatchingNetworkMaskedEmbed(MyModule):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(MatchingNetworkMaskedEmbed, self).__init__()
        self.batch_size = batch_size
        self.fce = fce
        self.classify = AttentionalClassify()
        self.keep_prob = keep_prob
        self.num_classes_per_set = num_classes_per_set
        self.num_samples_per_class = num_samples_per_class
        self.learning_rate = learning_rate

        self.emb = MaskedEmbed(layer_size = args.layer_size, num_channels=num_channels,
                             nClasses= 0, image_size = image_size )

    def getEncoders(self, num_channels, image_size):
        gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size )
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        # produce embeddings for support set images
        embSim = self.emb(target_image, support_set_images)

        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            # get similarity between support set embeddings and target
            similarities = embSim[:,i,:]


            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()



        return {'acc':  accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class MatchingNetworkPWMaskedCl(MatchingNetworkPW):
    def __init__(self, **kwargs):
        super(MatchingNetworkPWMaskedCl, self).__init__(**kwargs)
        self.gEnc = 1

    def getEncoders(self, num_channels, image_size):
        gSup = MaskedClassifier(layer_size = args.layer_size, num_channels=num_channels,
                                nClasses= 0, image_size = image_size )
        return [gSup]

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        res = super(MatchingNetworkPWMaskedCl, self).forward(support_set_images, support_set_labels_one_hot,
                                                       target_image, target_label,
                                                       support_enc, target_enc)
        if self.visMode:
            genc = self.parent.encoders[0]
            genc.visMode = True
            self.forVis['tmask'] = []
            for i in range(target_image.size(1)):
                genc(target_image.select(1,i))
                mask = genc.forVis['mask']
                mask = mask.view(mask.size(0), mask.size(2)*mask.size(3),1,1).repeat(1,1,support_set_images.size(1),1)
                self.forVis['tmask'].append(mask.clone())


            self.forVis['smask'] = []
            for i in range(support_set_images.size(1)):
                genc(support_set_images.select(1,i))
                mask = genc.forVis['mask']
                mask = mask.view(mask.size(0), 1,  mask.size(2)*mask.size(3)) # [nb, 1, nsup]
                self.forVis['smask'].append(mask.clone())
            self.forVis['smask'] = [torch.stack(self.forVis['smask'], dim=2)] * target_image.size(1) # [nb, 1, ns, nsup]

            genc.visMode = False
        encoded_support = torch.stack([x[-1].view(x[-1].size(0), x[-1].size(1)) for x in support_enc])

        return res




class MatchingNetworkMaskedCl(MatchingNetwork):
    def __init__(self, **kwargs):
        super(MatchingNetworkMaskedCl, self).__init__(**kwargs)
        self.gEnc = 1

    def getEncoders(self, num_channels, image_size):
        gSup = MaskedClassifier(layer_size = args.layer_size, num_channels=num_channels,
                                nClasses= 0, image_size = image_size )
        return [gSup]

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        res = super(MatchingNetworkMaskedCl, self).forward(support_set_images, support_set_labels_one_hot,
                                                       target_image, target_label,
                                                       support_enc, target_enc)
        if self.visMode:
            genc = self.parent.encoders[0]
            genc.visMode = True
            self.forVis['tmask'] = []
            for i in range(target_image.size(1)):
                genc(target_image.select(1,i))
                mask = genc.forVis['mask']
                mask = mask.view(mask.size(0), mask.size(2)*mask.size(3),1,1).repeat(1,1,support_set_images.size(1),1)
                self.forVis['tmask'].append(mask.clone())


            self.forVis['smask'] = []
            for i in range(support_set_images.size(1)):
                genc(support_set_images.select(1,i))
                mask = genc.forVis['mask']
                mask = mask.view(mask.size(0), 1,  mask.size(2)*mask.size(3)) # [nb, 1, nsup]
                self.forVis['smask'].append(mask.clone())
            self.forVis['smask'] = [torch.stack(self.forVis['smask'], dim=2)] * target_image.size(1) # [nb, 1, ns, nsup]

            genc.visMode = False
        encoded_support = torch.stack([x[-1].view(x[-1].size(0), x[-1].size(1)) for x in support_enc])

        return res
