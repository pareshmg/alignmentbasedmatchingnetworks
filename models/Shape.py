class DistanceNetworkPWShape(DistanceNetworkPW):
    def __init__(self, **kwargs):
        super(DistanceNetworkPWShape, self).__init__(**kwargs)


    def forward(self, support_set, target_image):
        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, ndim, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, ndim, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        ## compute content based similarity distance
        eps = 1e-10
        similarities = []
        for support_image in support_set:
            # support_image = [batch_size, ndim, nsamples1]
            sum_support = torch.sum(torch.pow(support_image, 2), 1) # [batch_size, ndim, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = target_image.permute(0,2,1).bmm(support_image)  #[batch_size, nsamples2 , nsamples1]
            cosine_similarity = dot_product * support_magnitude.unsqueeze(1)
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[batch_size, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities, dim=1)
        similarities = similarities.view(nb, ntgt, ns, nsup)

        ## within each image, make sure points can be discriminated



        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]
