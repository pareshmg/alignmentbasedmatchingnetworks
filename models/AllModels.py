from models.MatchingNetwork import *
from models.Occlusion import *
from models.ABMNets import *
from models.MatchingRegs import *
from models.LinearAdditiveModel import *
from models.RangeNetworks import *
MatchingNetworkPWLocal = MatchingNetworkPWSelfLoss

class MatchingNetworkPWSym(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSym, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWSym()
class MatchingNetworkPWEmb(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWEmb, self).__init__(*args, **kwargs)
        ndim = builtins.args.stackFVSize
        self.emb = StackEmbed([ndim, ndim, ndim])
        for m in self.emb.modules():
            if isinstance(m, nn.Linear):
                m.bias.data.zero_()
                m.weight.data *= 1e-2
                m.weight.data += torch.eye(ndim)

class MatchingNetworkPWEmbLocal(MatchingNetworkPWLocal):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWEmbLocal, self).__init__(*args, **kwargs)
        ndim = builtins.args.stackFVSize
        self.emb = StackEmbed([ndim, ndim, ndim])
        for m in self.emb.modules():
            if isinstance(m, nn.Linear):
                m.bias.data.zero_()
                m.weight.data *= 1e-2
                m.weight.data += torch.eye(ndim)

class MatchingNetworkPWStack4(MatchingNetworkPW):
    def __init__(self, **kwargs):
        super(MatchingNetworkPWStack4, self).__init__(**kwargs)
        self.g = StackFV(kwargs.get('image_size'), nstack=4)
##################################################
### L1 diff

class MatchingNetworkL1(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkL1, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkL1()

class MatchingNetworkL2(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkL2, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkL2()


class MatchingNetworkPWL1(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWL1, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWL1()

class MatchingNetworkPWL2(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWL2, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWL2()

class MatchingNetworkPWHalf(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWHalf, self).__init__(*args, **kwargs)
        self.g = StackFVHalf(kwargs.get('image_size'))


class MatchingNetworkPWSubSample(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSubSample, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPW(subsample=True)


class MatchingNetworkPWNotLast(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWNotLast, self).__init__(*args, **kwargs)
        self.g = StackFVNotLast(kwargs.get('image_size'))

class MatchingNetworkPWNoLSM(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWNoLSM, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWNoLSM()


class MatchingNetworkPWOpenSet(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSet, self).__init__(*args, **kwargs)
        self.classify = OpenAttentionalClassify()


class MatchingNetworkPWOpenSetMoment(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSetMoment, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassify(nMoments=8)
        self.dn = DistanceNetworkPWMoments(nMoments=8)


class MatchingNetworkPWOpenSetMoment1(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSetMoment1, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassify(nMoments=1)
        self.dn = DistanceNetworkPWMoments(nMoments=1)
class MatchingNetworkPWOpenSetMoment2(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSetMoment2, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassify(nMoments=2)
        self.dn = DistanceNetworkPWMoments(nMoments=2)
class MatchingNetworkPWOpenSetMoment4(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSetMoment4, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassify(nMoments=4+2)
        self.dn = DistanceNetworkPWMoments(nMoments=4)

class MatchingNetworkPWLocalOpenSetMoment4(MatchingNetworkPWLocal):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWLocalOpenSetMoment4, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassify(nMoments=4+2)
        self.dn = DistanceNetworkPWMoments(nMoments=4)

class MatchingNetworkPWOpenSetMoment8(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSetMoment8, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassify(nMoments=10)
        self.dn = DistanceNetworkPWMoments(nMoments=8)


class MatchingNetworkPWOpenSetMoment4x8(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSetMoment4x8, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassifyx81(nMoments=4)
        self.dn = DistanceNetworkPWMoments(nMoments=4)
class MatchingNetworkPWOpenSetMoment4x8(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWOpenSetMoment4x8, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassifyx81(nMoments=4+2)
        self.dn = DistanceNetworkPWMoments(nMoments=4)


class MatchingNetworkPWMoment(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWMoment, self).__init__(*args, **kwargs)
        self.classify = MomentAttentionalClassify(nMoments=8)
        self.dn = DistanceNetworkPWMoments(nMoments=8)


class MatchingNetworkPWCosOpenSet(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWCosOpenSet, self).__init__(*args, **kwargs)
        self.classify = OpenAttentionalClassify()
        self.dn = DistanceNetworkPWCos()

class MatchingNetworkOpenSet(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkOpenSet, self).__init__(*args, **kwargs)
        self.classify = OpenAttentionalClassify()


class MatchingNetworkL2OpenSet(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkL2OpenSet, self).__init__(*args, **kwargs)
        self.classify = OpenAttentionalClassify()
        self.dn = DistanceNetworkL2()

class MatchingNetworkOpenSetMoment1(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkOpenSetMoment1, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassify(nMoments=1)
        self.dn = DistanceNetworkMoments()
class MatchingNetworkOpenSetMoment2(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkOpenSetMoment2, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassifyx21(nMoments=1)
        self.dn = DistanceNetworkMoments()
class MatchingNetworkOpenSetMoment4(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkOpenSetMoment4, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassifyx41(nMoments=1)
        self.dn = DistanceNetworkMoments()
class MatchingNetworkOpenSetMoment8(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkOpenSetMoment8, self).__init__(*args, **kwargs)
        self.classify = OpenMomentAttentionalClassifyx81(nMoments=1)
        self.dn = DistanceNetworkMoments()

class MatchingNetworkMarginalConv(MatchingNetwork):
    def getEncoders(self, num_channels, image_size):
        import models.Occlusion
        gSup = models.Occlusion.MaskedClassifier(layer_size = args.layer_size, num_channels=num_channels,
                                                 nClasses= 0, image_size = image_size )
        return [gSup]





##################################################
## Decoupled
class MatchingNetworkPWDecoupled(MatchingNetworkPW):
    def getEncoders(self, *args, **kwargs):
        return MatchingNetworkDecoupled.getEncoders(self, *args, **kwargs)


class MatchingNetworkPWNotLastDecoupled(MatchingNetworkPW):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(MatchingNetworkPWNotLastDecoupled, self).__init__(keep_prob, batch_size, num_channels,
                                                         learning_rate, fce, num_classes_per_set,
                                                         num_samples_per_class, nClasses, image_size)
        self.gSup = EncoderPhi1NotLast(layer_size = builtins.args.layer_size, num_channels=1,
                                nClasses= nClasses, image_size = image_size )
        self.gTgt = EncoderPhi1NotLast(layer_size = builtins.args.layer_size, num_channels=3,
                                nClasses= nClasses, image_size = image_size )


class MatchingNetworkDecoupled(MatchingNetwork):
    def getEncoders(self, num_channels, image_size):
        gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size )
        gTgt = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size )
        return [gSup, gTgt]



class MatchingNetworkPWPhi(MatchingNetworkPW):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(MatchingNetworkPWPhi, self).__init__(keep_prob = keep_prob,
                                                   num_channels = num_channels,
                                                   learning_rate = learning_rate,
                                                   fce = fce,
                                                   num_classes_per_set = num_classes_per_set,
                                                   num_samples_per_class = num_samples_per_class,
                                                   nClasses = nClasses,
                                                   image_size = image_size)
        self.gSup = EncoderPhi1(layer_size = args.layer_size, num_channels=num_channels,
                                nClasses= nClasses, image_size = image_size )
        self.gTgt = self.gSup




# class MatchingNetworkPWAttn(MatchingNetworkPW):
#     def __init__(self, *args, **kwargs):
#         super(MatchingNetworkPWAttn, self).__init__(*args, **kwargs)
#         self.dn = DistanceNetworkPWAttn(builtins.args.stackFVSize) # 355 for imagenet

class MatchingNetworkPWAttnSigm(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWAttnSigm, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnSigm(builtins.args.stackFVSize) # 355 for imagenet

class MatchingNetworkPWAttnPair(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWAttnPair, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnPair(builtins.args.stackFVSize) # 355 for imagenet


class MatchingNetworkPWAttnNormProd(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWAttnNormProd, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnNormProd(builtins.args.stackFVSize) # 355 for imagenet

class MatchingNetworkPWLocalAttnProd(MatchingNetworkPWLocal):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWLocalAttnProd, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnProd(builtins.args.stackFVSize) # 355 for imagenet


class MatchingNetworkPWLocalAttnProdL2(MatchingNetworkPWLocal):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWLocalAttnProdL2, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnProdL2(builtins.args.stackFVSize) # 355 for imagenet

class MatchingNetworkPWLocalVectorMask(MatchingNetworkPWLocal):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWLocalVectorMask, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWVectorMask(builtins.args.stackFVSize) # 355 for imagenet

class MatchingNetworkPWAttnNormProdSym(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWAttnNormProdSym, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttnNormProdSym(builtins.args.stackFVSize) # 355 for imagenet
