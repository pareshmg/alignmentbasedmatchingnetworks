import builtins
from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg, unnormImg
from models.MatchingNetwork import *
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import data, segmentation, filters, color
from skimage import io
import matplotlib.pyplot as plt
from skimage.future import graph
from skimage.future.graph import RAG, merge_hierarchical
import scipy

def weight_boundary(graph, src, dst, n):
    """
    Handle merging of nodes of a region boundary region adjacency graph.

    This function computes the `"weight"` and the count `"count"`
    attributes of the edge between `n` and the node formed after
    merging `src` and `dst`.


    Parameters
    ----------
    graph : RAG
        The graph under consideration.
    src, dst : int
        The vertices in `graph` to be merged.
    n : int
        A neighbor of `src` or `dst` or both.

    Returns
    -------
    data : dict
        A dictionary with the "weight" and "count" attributes to be
        assigned for the merged node.

    """
    default = {'weight': 0.0, 'count': 0}

    count_src = graph[src].get(n, default)['count']
    count_dst = graph[dst].get(n, default)['count']

    weight_src = graph[src].get(n, default)['weight']
    weight_dst = graph[dst].get(n, default)['weight']

    count = count_src + count_dst
    return {
        'count': count,
        'weight': (count_src * weight_src + count_dst * weight_dst)/count
    }


def merge_boundary(graph, src, dst):
    """Call back called before merging 2 nodes.

    In this case we don't need to do any computation here.
    """
    pass


class ImageComposer(MyModule):
    def __init__(self):
        super(ImageComposer, self).__init__()
        self.composer = RBM([2*k, 2*k, 4*k, k])
        self.dim = 1

    def forward(self, enc1, enc2):
        enc = torch.concat([enc1, enc2], self.dim)
        res = self.composer(enc)
        return res


# class MatchingNetworkComposer(MatchingNetwork):
#     def __init__(self, *args, **kwargs):
#         super(MatchingNetworkComposer, self).__init__(*args, **kwargs)
#         self.composer = ImageComposer(args.layer_size)
#         self.encoder = None

#     def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
#         res = super(MatchingNetworkComposer, self).forward(support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc)

#         ### regularize the encoder and train the composer such that images can be added
#         target_segs, target_ulabs = self.parent.data.get_rag(self.parent.mode_type, target_ind)
#         import ipdb; ipdb.set_trace()
#         ## find m1, m2 as some subsets of neighboring slic superpixels
#         for ti in range(target_image.size(1)):
#             for bi in range(target_image.size(0)):
#                 adjGraph = RAG(target_slic[bi,ti])


#         ## generate im12 = im1 + im2
#         im1 = m1 * target_image
#         im2 = m2 * target_image
#         im12 = im1 + im2

#         ## encode im1, im2, im12 to e1, e2, e12
#         e1 = self.encoder(im1)[-1]
#         e2 = self.encoder(im2)[-1]
#         e12 = self.encoder(im12)[-1]

#         ## require composer(e1, e2) = e12
#         ehat12 = self.composer(e1, e2)
#         loss = (ehat12 - e12).pow(2).mean()

#         ## add loss to overall loss
#         wt = 0.1
#         res['composer_loss'] = loss.data
#         res['loss'] += wt * loss

#         return res

class MakeRGBA(MyModule):
    def __init__(self, dim=1):
        super(MakeRGBA, self).__init__()
        self.dim = dim

    def forward(self, inp):
        if inp.size(self.dim) == 4: return inp.narrow(self.dim, 0, 3)
        if inp.size(self.dim) == 3: return inp #torch.cat([inp, inp.narrow(self.dim, 0,1)*0+1], dim=self.dim)
        else: raise RuntimeError('bad input to make RGBA' + str(inp.size()))


class MatchingNetworkRAG1(MatchingNetwork):
    """
    take SLIC + merge_hierarchical segments.

    simply try to find the best matching alignment encoding a segment as one image.
    """
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkRAG1, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkSigmoid()
    def getEncoders(self, num_channels, image_size):
        # gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels + 1,
        #                   nClasses= 0, image_size = image_size)
        gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size, relu_last=False)
        gSup = nn.Sequential(MakeRGBA(), gSup)
        self.encoders = [gSup]
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        if self.visMode:
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        if True:
            support_segs, support_ulabs = self.parent.data.get_rag(self.parent.mode_type, support_ind)
            support_rag = self.parent.data.make_rag(support_set_images, support_segs, support_ulabs) # [nb, ns, nsegs, ndim+1, nr, nc]
            target_segs, target_ulabs = self.parent.data.get_rag(self.parent.mode_type, target_ind)
            target_rag = self.parent.data.make_rag(target_image, target_segs, target_ulabs) # [nb, nt, nsegt, ndim+1, nr, nc]


            if self.visMode:
                self.forVis['support_rag'] = support_rag.data.clone()
                self.forVis['target_rag'] = target_rag.data.clone()
                self.forVis['target_segs'] = target_segs.data.clone()
                self.forVis['support_segs'] = support_segs.data.clone()
                self.forVis['target_ulabs'] = target_ulabs.data.clone()



        # FIXME: remove
        #support_rag = torch.cat([support_rag.sum(2).unsqueeze(2), support_rag], dim=2)
        #target_rag = torch.cat([target_rag.sum(2).unsqueeze(2), target_rag], dim=2)
        #support_rag = support_rag.sum(2).unsqueeze(2)
        #target_rag = target_rag.sum(2).unsqueeze(2)
        #target_rag = torch.cat([target_rag.sum(2).unsqueeze(2), target_rag.narrow(2,0,1)], dim=2)
        #support_rag = torch.cat([support_rag.sum(2).unsqueeze(2), support_rag.narrow(2,0,1)], dim=2)
        #support_rag = torch.cat([support_rag.sum(2).unsqueeze(2), support_rag], dim=2)
        self_loss = 0
        if True:
            support_rag = support_rag.narrow(0,0,1)
            target_rag = target_rag.narrow(0,0,1)
            ##################################################
            ## Regularize identifiability of superpixels

            encFn = lambda x: torch.cat([y.max(3)[0].max(2)[0] for y in self.encoders[0](x)], dim=1)

            ## without any combination
            nb, ns, nsegs, ndim, nr, nc = support_rag.size()
            support_rag_enc = encFn(support_rag.view(nb * ns * nsegs, ndim, nr, nc)).view(nb, ns * nsegs, -1)

            nb, nt, nsegt, ndim, nr, nc = target_rag.size()
            target_rag_enc = encFn(target_rag.view(nb * nt * nsegt, ndim, nr, nc)).view(nb, nt * nsegt, -1)

            ## find best match
            selfSimilarity = self.dn.sim(target_rag_enc.permute(0,2,1), target_rag_enc.permute(0,2,1)) # [nb, nt, nt]
            selfLabel = Variable(torch.arange(0,nt*nsegt).long().cuda(), requires_grad=False)
            #self_loss += F.cross_entropy(selfSimilarity.view(nb*nt*nsegt, nt*nsegt), selfLabel.repeat(nb))

        additivity_loss = 0
        if True:
            combine = lambda a,b, ab: (torch.stack([a,b], dim=0).max(0)[0] - ab).abs().mean()
            ##################################################
            ## regularize linear additivity of non-adjacent superpixels
            target_rag_enc = target_rag_enc.view(nb, nt, nsegt, -1)
            nad = 0
            for bi in range(nb):
                for ti in range(nt):
                    ## find set of non-adjacent superpixels or superpixel groups
                    adjGraph = graph.RAG(target_segs[bi,ti].data.cpu().numpy())
                    for samplei in range(5):
                        a = numpy.random.randint(0,len(adjGraph.nodes))
                        b = numpy.random.randint(0,len(adjGraph.nodes))
                        ntries = 0
                        while adjGraph.has_edge(a,b):
                            b = numpy.random.randint(0,len(adjGraph.nodes))
                            ntries += 1
                            if ntries > 10: break
                        if ntries > 10: break

                        ea = target_rag_enc[bi,ti,a]
                        eb = target_rag_enc[bi,ti,b]
                        eab = encFn((target_rag[bi,ti,a] + target_rag[bi,ti,b]).unsqueeze(0)).view(-1)
                        additivity_loss += combine(ea, eb, eab)
                        nad += 1
            if nad > 0: additivity_loss /= nad



        encoded_support = torch.stack([x[-1].view(x[-1].size(0), x[-1].size(1)) for x in support_enc])
        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = target_enc[i][-1].view(target_enc[i][-1].size(0), target_enc[i][-1].size(1))

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=encoded_support, target_image=encoded_target)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

        if self.visMode:
            nb, nt, nch, nr, nc = target_image.size()
            self.forVis['im_t_slic'] = []
            for bi in range(nb):
                tmp = []
                for si in range(nt):
                    img = normImg(target_image[bi,si].data.cpu()).permute(1,2,0).numpy()
                    seg = target_segs[bi,si].data.cpu().numpy()
                    islic = torch.from_numpy(mark_boundaries(img, seg, color=[0.0, 0.1, 0.1]))
                    tmp.append(islic)
                self.forVis['im_t_slic'].append(hstackImgs(tmp))

            nb, ns, nch, nr, nc = support_set_images.size()
            self.forVis['im_s_slic'] = []
            for bi in range(nb):
                tmp = []
                for si in range(ns):
                    img = normImg(support_set_images[bi,si].data.cpu()).permute(1,2,0).numpy()
                    seg = support_segs[bi,si].data.cpu().numpy()
                    islic = torch.from_numpy(mark_boundaries(img, seg, color=[0, 0.1, 0.1]))
                    tmp.append(islic)
                self.forVis['im_s_slic'].append(hstackImgs(tmp))


        crossentropy_loss *= 1/target_image.size(1)
        self_loss *= 0/target_image.size(1)
        additivity_loss *= 1e-2
        loss = crossentropy_loss + self_loss + additivity_loss
        return {'acc':  accuracy/target_image.size(1),
                'loss': loss,
                'additivity_loss': additivity_loss,
                'self_loss': self_loss,
                'crossentropy_loss': crossentropy_loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class MatchingNetworkRAG2(MatchingNetwork):
    """
    SLIC -> gating

    simply try to find the best matching alignment encoding a segment as one image.
    """
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkRAG2, self).__init__(*args, **kwargs)
        self.dn = DistanceNetwork()
        #self.dn.sim = CosineSimilaritiesUnit()
        self.dn.sim = L2Similarity()
        self.ngates = 5
        self.gate = RBM([0*64 + 3*419,4*419,5])
    def getEncoders(self, num_channels, image_size):
        # gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels + 1,
        #                   nClasses= 0, image_size = image_size)
        gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size, relu_last=False)
        gSup = nn.Sequential(MakeRGBA(), gSup)
        self.encoders = [gSup]
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        additivity_loss = 0
        crossentropy_loss = 0
        self_loss = 0

        if self.visMode:
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        if True:
            support_segs, support_ulabs = self.parent.data.get_rag(self.parent.mode_type, support_ind)
            support_rag = self.parent.data.make_rag(support_set_images, support_segs, support_ulabs) # [nb, ns, nsegs, ndim+1, nr, nc]
            target_segs, target_ulabs = self.parent.data.get_rag(self.parent.mode_type, target_ind)
            target_rag = self.parent.data.make_rag(target_image, target_segs, target_ulabs) # [nb, nt, nsegt, ndim+1, nr, nc]


            if self.visMode:
                self.forVis['support_rag'] = support_rag.data.clone()
                self.forVis['target_rag'] = target_rag.data.clone()
                self.forVis['target_segs'] = target_segs.data.clone()
                self.forVis['support_segs'] = support_segs.data.clone()
                self.forVis['target_ulabs'] = target_ulabs.data.clone()
                self.forVis['im_weighted_slic'] = []
            support_rag = support_rag.narrow(3,0,3).contiguous()
            target_rag = target_rag.narrow(3,0,3).contiguous()


        # encFn = lambda x: self.encoders[0](x)[-1].squeeze(3).squeeze(2)
        enc_0 = torch.cat([y.max(3)[0].max(2)[0] for y in self.encoders[0](support_rag[0,0,0:1])], dim=1)
        encFn = lambda x: F.relu(torch.cat([y.max(3)[0].max(2)[0] for y in self.encoders[0](x)], dim=1) - enc_0)
        self_loss = 0
        if True:
            #support_rag = support_rag.narrow(0,0,1) # [nb, ns, nsegs, nch, nr, nc]
            #target_rag = target_rag.narrow(0,0,1) # [nb, nt, nsegt, nch, nr, nc]
            ##################################################
            ## Regularize identifiability of superpixels


            ## without any combination
            nb, ns, nsegs, ndim, nr, nc = support_rag.size()
            support_rag_enc = encFn(support_rag.view(nb * ns * nsegs, ndim, nr, nc)).view(nb, ns, nsegs, -1)
            #support_rag__enc = encFn((support_set_images.narrow(0,0,1).unsqueeze(2) - support_rag).view(nb * ns * nsegs, ndim, nr, nc)).view(nb, ns, nsegs, -1)

            nb, nt, nsegt, ndim, nr, nc = target_rag.size()
            target_rag_enc = encFn(target_rag.view(nb * nt * nsegt, ndim, nr, nc)).view(nb, nt, nsegt, -1) # [nb, nt, nsegt, ndim]
            #target_rag__enc = encFn((target_image.narrow(0,0,1) - target_rag).view(nb * nt * nsegt, ndim, nr, nc)).view(nb, nt, nsegt, -1)

            ## find best match
            tr = target_rag_enc.view(nb, nt * nsegt, -1).permute(0,2,1)
            selfSimilarity = self.dn.sim(tr, tr) # [nb, nt, nt]
            selfLabel = Variable(torch.arange(0,nt*nsegt).long().cuda(), requires_grad=False)
            #self_loss += F.cross_entropy(selfSimilarity.view(nb*nt*nsegt, nt*nsegt), selfLabel.repeat(nb))



        encoded_support = torch.stack([x[-1].view(x[-1].size(0), x[-1].size(1)) for x in support_enc]) # [ns, nb, ndim]
        encoded_support_all = torch.stack([F.relu(torch.cat([y.max(3)[0].max(2)[0] for y in support_enc[supi]], dim=1) - enc_0) for supi in range(len(support_enc))])
        # produce embeddings for target images
        for i in range(target_image.size(1)):
            encoded_target = target_enc[i][-1].view(target_enc[i][-1].size(0), target_enc[i][-1].size(1)) # [nb, ndim]
            encoded_target_all = F.relu(torch.cat([y.max(3)[0].max(2)[0] for y in target_enc[i]], dim=1) - enc_0)

            similarities = []
            for supi in range(encoded_support.size(0)):
                targetSuperpixelWeight = torch.stack([self.gate(torch.cat([
                    encoded_target_all,
                    encoded_support_all[supi],
                    target_rag_enc[:,i, segi],
                    #target_rag__enc[:,i, segi]
                ], dim=1)) for segi in range(target_rag_enc.size(2))], dim=2) # [nb, 1, nseg]
                supportSuperpixelWeight = torch.stack([self.gate(torch.cat([
                    encoded_support_all[supi],
                    encoded_target_all,
                    support_rag_enc[:,supi, segi],
                    #support_rag__enc[:,supi, segi]
                ], dim=1)) for segi in range(support_rag_enc.size(2))], dim=2) # [nb, 1, nseg]

                targetSuperpixelWeight = F.sigmoid(targetSuperpixelWeight).unsqueeze(3).unsqueeze(4).unsqueeze(5)
                supportSuperpixelWeight = F.sigmoid(supportSuperpixelWeight).unsqueeze(3).unsqueeze(4).unsqueeze(5)
                targetWeightedImage = (target_rag.select(1,i).unsqueeze(1) * targetSuperpixelWeight).sum(2) # [nb, ngates, nch, nr, nc]
                supportWeightedImage = (support_rag.select(1,supi).unsqueeze(1) * supportSuperpixelWeight).sum(2) # [nb, ngates, nch, nr, nc]

                nb, ng, nch, nr, nc = targetWeightedImage.size()

                if self.visMode:
                    self.forVis['im_weighted_slic'].append(
                        hstackImgs([
                            hstackImgs([target_image[0, i]] + [targetWeightedImage[0,_i,:] for _i in range(targetWeightedImage.size(1))  ]),
                            hstackImgs([support_set_images[0, supi]] + [supportWeightedImage[0,_i,:] for _i in range(supportWeightedImage.size(1))])]))

                targetEncodedWeighted = encFn(targetWeightedImage.view(nb*ng, nch, nr, nc))  # [nb, ndim]
                supportEncodedWeighted = encFn(supportWeightedImage.view(nb*ng, nch, nr, nc)) # [nb, ndim]

                similarities.append(self.dn.sim(targetEncodedWeighted.unsqueeze(2),
                                                supportEncodedWeighted.unsqueeze(2)).view(nb,ng).max(1)[0])

            # get similarity between support set embeddings and target
            similarities = torch.stack(similarities, dim=1) # [nb, ns]
            #similarities = self.dn(support_set=encoded_support, target_image=encoded_target)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

        if self.visMode:
            self.forVis['im_weighted_slic'] = [normImg(vstackImgs(self.forVis['im_weighted_slic']))]
            nb, nt, nch, nr, nc = target_image.size()
            self.forVis['im_t_slic'] = []
            for bi in range(nb):
                tmp = []
                for si in range(nt):
                    img = normImg(target_image[bi,si].data.cpu()).permute(1,2,0).numpy()
                    seg = target_segs[bi,si].data.cpu().numpy()
                    islic = torch.from_numpy(mark_boundaries(img, seg, color=[0.0, 0.1, 0.1]))
                    tmp.append(islic)
                self.forVis['im_t_slic'].append(hstackImgs(tmp))

            nb, ns, nch, nr, nc = support_set_images.size()
            self.forVis['im_s_slic'] = []
            for bi in range(nb):
                tmp = []
                for si in range(ns):
                    img = normImg(support_set_images[bi,si].data.cpu()).permute(1,2,0).numpy()
                    seg = support_segs[bi,si].data.cpu().numpy()
                    islic = torch.from_numpy(mark_boundaries(img, seg, color=[0, 0.1, 0.1]))
                    tmp.append(islic)
                self.forVis['im_s_slic'].append(hstackImgs(tmp))


        crossentropy_loss *= 1/target_image.size(1)
        self_loss *= 0/target_image.size(1)
        additivity_loss *= 1e-2
        loss = crossentropy_loss + self_loss + additivity_loss
        return {'acc':  accuracy/target_image.size(1),
                'loss': loss,
                'additivity_loss': additivity_loss,
                'self_loss': self_loss,
                'crossentropy_loss': crossentropy_loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class MatchingNetworkRAG3(MatchingNetwork):
    """
    SLIC -> gating via generation of set of directions -> cosine similarity to directions gives weight

    simply try to find the best matching alignment encoding a segment as one image.
    """
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkRAG3, self).__init__(*args, **kwargs)
        self.dn = DistanceNetwork()
        #self.dn.sim = CosineSimilaritiesUnit()
        #self.dn.sim = L2Similarity()
        self.ngates = 5
        self.gate = RBM([0*64 + 2*419,419,self.ngates * 419])
        self.sim = CosineSimilaritiesUnit()

    def getEncoders(self, num_channels, image_size):
        # gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels + 1,
        #                   nClasses= 0, image_size = image_size)
        gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size, relu_last=False)
        gSup = nn.Sequential(MakeRGBA(), gSup)
        self.encoders = [gSup]
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        additivity_loss = 0
        crossentropy_loss = 0
        self_loss = 0

        if self.visMode:
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        if True:
            support_segs, support_ulabs = self.parent.data.get_rag(self.parent.mode_type, support_ind)
            support_rag = self.parent.data.make_rag(support_set_images, support_segs, support_ulabs) # [nb, ns, nsegs, ndim+1, nr, nc]
            target_segs, target_ulabs = self.parent.data.get_rag(self.parent.mode_type, target_ind)
            target_rag = self.parent.data.make_rag(target_image, target_segs, target_ulabs) # [nb, nt, nsegt, ndim+1, nr, nc]

            if self.visMode:
                self.forVis['support_rag'] = support_rag.data.clone()
                self.forVis['target_rag'] = target_rag.data.clone()
                self.forVis['target_segs'] = target_segs.data.clone()
                self.forVis['support_segs'] = support_segs.data.clone()
                self.forVis['target_ulabs'] = target_ulabs.data.clone()
                self.forVis['im_weighted_slic'] = []
            support_rag = support_rag.narrow(3,0,3).contiguous()
            target_rag = target_rag.narrow(3,0,3).contiguous()


        # encFn = lambda x: self.encoders[0](x)[-1].squeeze(3).squeeze(2)
        enc_0 = torch.cat([y.max(3)[0].max(2)[0] for y in self.encoders[0](support_rag[0,0,0:1])], dim=1)
        encFn = lambda x: F.relu(torch.cat([y.max(3)[0].max(2)[0] for y in self.encoders[0](x)], dim=1) - enc_0)
        encFnTop = lambda x: self.encoders[0](x)[-1].max(3)[0].max(2)[0]
        self_loss = 0
        if True:
            #support_rag = support_rag.narrow(0,0,1) # [nb, ns, nsegs, nch, nr, nc]
            #target_rag = target_rag.narrow(0,0,1) # [nb, nt, nsegt, nch, nr, nc]
            ##################################################
            ## Regularize identifiability of superpixels


            ## without any combination
            nb, ns, nsegs, ndim, nr, nc = support_rag.size()
            support_rag_enc = encFn(support_rag.view(nb * ns * nsegs, ndim, nr, nc)).view(nb, ns, nsegs, -1)
            #support_rag__enc = encFn((support_set_images.narrow(0,0,1).unsqueeze(2) - support_rag).view(nb * ns * nsegs, ndim, nr, nc)).view(nb, ns, nsegs, -1)

            nb, nt, nsegt, ndim, nr, nc = target_rag.size()
            target_rag_enc = encFn(target_rag.view(nb * nt * nsegt, ndim, nr, nc)).view(nb, nt, nsegt, -1) # [nb, nt, nsegt, ndim]
            #target_rag__enc = encFn((target_image.narrow(0,0,1) - target_rag).view(nb * nt * nsegt, ndim, nr, nc)).view(nb, nt, nsegt, -1)

            ## find best match
            tr = target_rag_enc.view(nb, nt * nsegt, -1).permute(0,2,1)
            selfSimilarity = self.dn.sim(tr, tr) # [nb, nt, nt]
            selfLabel = Variable(torch.arange(0,nt*nsegt).long().cuda(), requires_grad=False)
            #self_loss += F.cross_entropy(selfSimilarity.view(nb*nt*nsegt, nt*nsegt), selfLabel.repeat(nb))



        encoded_support = torch.stack([x[-1].view(x[-1].size(0), x[-1].size(1)) for x in support_enc]) # [ns, nb, ndim]
        encoded_support_all = torch.stack([F.relu(torch.cat([y.max(3)[0].max(2)[0] for y in support_enc[supi]], dim=1) - enc_0) for supi in range(len(support_enc))])
        # produce embeddings for target images
        for i in range(target_image.size(1)):
            encoded_target = target_enc[i][-1].view(target_enc[i][-1].size(0), target_enc[i][-1].size(1)) # [nb, ndim]
            encoded_target_all = F.relu(torch.cat([y.max(3)[0].max(2)[0] for y in target_enc[i]], dim=1) - enc_0)

            similarities = []
            for supi in range(encoded_support.size(0)):
                targetSuperpixelDirs = self.gate(torch.cat([
                    encoded_target_all,
                    encoded_support_all[supi]
                ], dim=1)).view(encoded_target.size(0), -1, self.ngates)  # [nb, ndim, ng]
                supportSuperpixelDirs = self.gate(torch.cat([
                    encoded_support_all[supi],
                    encoded_target_all
                ], dim=1)).view(encoded_target.size(0), -1, self.ngates)  # [nb, ndim, ng]

                targetSuperpixelWeight = self.sim(targetSuperpixelDirs,
                                                  target_rag_enc[:,i].permute(0,2,1)) # [nb, ng, nsegt]
                supportSuperpixelWeight = self.sim(supportSuperpixelDirs,
                                                   support_rag_enc[:,supi].permute(0,2,1)) # [nb, ng, nsegs]

                targetSuperpixelWeight = targetSuperpixelWeight.mean(1, keepdim=True) # [nb, 1, nsegt]
                supportSuperpixelWeight = supportSuperpixelWeight.mean(1, keepdim=True) # [nb, 1, nsegs]

                # targetSuperpixelWeight = (targetSuperpixelWeight - targetSuperpixelWeight.min(2, keepdim=True)[0]) / (targetSuperpixelWeight.max(2, keepdim=True)[0] - targetSuperpixelWeight.min(2,keepdim=True)[0] + 1e-3)
                # supportSuperpixelWeight = (supportSuperpixelWeight - supportSuperpixelWeight.min(2, keepdim=True)[0]) / (supportSuperpixelWeight.max(2, keepdim=True)[0] - supportSuperpixelWeight.min(2,keepdim=True)[0] + 1e-3)

                targetSuperpixelWeight = targetSuperpixelWeight.unsqueeze(3).unsqueeze(4).unsqueeze(5)
                supportSuperpixelWeight = supportSuperpixelWeight.unsqueeze(3).unsqueeze(4).unsqueeze(5)
                targetWeightedImage = (target_rag.select(1,i).unsqueeze(1) * targetSuperpixelWeight).sum(2) # [nb, ngates, nch, nr, nc]
                supportWeightedImage = (support_rag.select(1,supi).unsqueeze(1) * supportSuperpixelWeight).sum(2) # [nb, ngates, nch, nr, nc]

                nb, ng, nch, nr, nc = targetWeightedImage.size()

                if self.visMode:
                    self.forVis['im_weighted_slic'].append(
                        hstackImgs([
                            hstackImgs([target_image[0, i]] + [targetWeightedImage[0,_i,:] for _i in range(targetWeightedImage.size(1))  ]),
                            hstackImgs([support_set_images[0, supi]] + [supportWeightedImage[0,_i,:] for _i in range(supportWeightedImage.size(1))])]))

                targetEncodedWeighted = encFnTop(targetWeightedImage.view(nb*ng, nch, nr, nc))  # [nb*ng, ndim]
                supportEncodedWeighted = encFnTop(supportWeightedImage.view(nb*ng, nch, nr, nc)) # [nb*ng, ndim]

                similarities.append(self.dn.sim(targetEncodedWeighted.unsqueeze(2),
                                                supportEncodedWeighted.unsqueeze(2)).view(nb,ng).max(1)[0])

            # get similarity between support set embeddings and target
            similarities = torch.stack(similarities, dim=1) # [nb, ns]
            #similarities = self.dn(support_set=encoded_support, target_image=encoded_target)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

        if self.visMode:
            self.forVis['im_weighted_slic'] = [unnormImg(vstackImgs(self.forVis['im_weighted_slic']),
                                                         self.parent.data.mean,
                                                         self.parent.data.std)]
            nb, nt, nch, nr, nc = target_image.size()
            self.forVis['im_t_slic'] = []
            for bi in range(nb):
                tmp = []
                for si in range(nt):
                    img = normImg(target_image[bi,si].data.cpu()).permute(1,2,0).numpy()
                    seg = target_segs[bi,si].data.cpu().numpy()
                    islic = torch.from_numpy(mark_boundaries(img, seg, color=[0.0, 0.1, 0.1]))
                    tmp.append(islic)
                self.forVis['im_t_slic'].append(hstackImgs(tmp))

            nb, ns, nch, nr, nc = support_set_images.size()
            self.forVis['im_s_slic'] = []
            for bi in range(nb):
                tmp = []
                for si in range(ns):
                    img = normImg(support_set_images[bi,si].data.cpu()).permute(1,2,0).numpy()
                    seg = support_segs[bi,si].data.cpu().numpy()
                    islic = torch.from_numpy(mark_boundaries(img, seg, color=[0, 0.1, 0.1]))
                    tmp.append(islic)
                self.forVis['im_s_slic'].append(hstackImgs(tmp))


        crossentropy_loss *= 1/target_image.size(1)
        self_loss *= 0/target_image.size(1)
        additivity_loss *= 1e-2
        loss = crossentropy_loss + self_loss + additivity_loss
        return {'acc':  accuracy/target_image.size(1),
                'loss': loss,
                'additivity_loss': additivity_loss,
                'self_loss': self_loss,
                'crossentropy_loss': crossentropy_loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class AttentionModel1(MatchingNetwork):
    """
    (mid a, top a, top b)  -> attn

    """
    def __init__(self, *args, **kwargs):
        super(AttentionModel1, self).__init__(*args, **kwargs)
        self.dn = DistanceNetwork()
        self.ngates = 5
        self.attention = nn.Sequential(nn.Linear(3*64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]
        self.attnLayer = -3 # -3 = 5x5; -4= 10x10
    def getEncoders(self, num_channels, image_size):
        # gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels + 1,
        #                   nClasses= 0, image_size = image_size)
        gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size, relu_last=False)
        #gSup = nn.Sequential(MakeRGBA(), gSup)
        self.encoders = [gSup]
        return [gSup]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        top_a = top_a.unsqueeze(1).repeat(1,nrMid*ncMid, 1)
        top_b = top_b.unsqueeze(1).repeat(1,nrMid*ncMid, 1)

        vec = torch.cat([enc_mid_a, top_a, top_b], dim=2).view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.softmax(attn, dim=1)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]

        complement = (enc_mid_a * (1-attn)).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        additivity_loss = 0
        crossentropy_loss = 0
        self_loss = 0
        accuracy = 0

        if self.visMode:
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()
            self.forVis['im_attn_mask'] = []

        # produce embeddings for target images
        for tgti in range(len(target_enc)):
            nb, nTopDim, _, _ = target_enc[tgti][-1].size()
            encoded_target_top = target_enc[tgti][-1].view(nb, nTopDim) # [nb, ndim]
            encoded_target_mid = target_enc[tgti][self.attnLayer] # [nb, ndim, nr, nc]
            nb, nMidDim, nrMid, ncMid = encoded_target_mid.size()
            similarities = []
            for supi in range(len(support_enc)):
                encoded_support_top = support_enc[supi][-1].view(nb, nTopDim)
                encoded_support_mid = support_enc[supi][self.attnLayer] # [nb, ndim, nr, nc]

                encoded_target_attn, target_attn_mask = self.getAttnFV(encoded_target_mid, encoded_target_top, encoded_support_top) # [nb, ndim]
                encoded_support_attn, support_attn_mask = self.getAttnFV(encoded_support_mid, encoded_support_top, encoded_target_top) # [nb, ndim]

                if self.visMode:
                    ta = target_attn_mask[0] # [1, nr, nc]
                    sa = support_attn_mask[0] # [1, nr, nc]
                    ta = torch.from_numpy(scipy.misc.imresize(ta.data.cpu().numpy()[0], (84,84), mode='F')).unsqueeze(0).cuda()
                    sa = torch.from_numpy(scipy.misc.imresize(sa.data.cpu().numpy()[0], (84,84), mode='F')).unsqueeze(0).cuda()

                    self.forVis['im_attn_mask'].append(
                        hstackImgs([
                            hstackImgs([unnormImg(target_image[0, tgti], self.parent.data.mean, self.parent.data.std), ta]),
                            hstackImgs([unnormImg(support_set_images[0, supi], self.parent.data.mean, self.parent.data.std), sa])]))

                similarities.append(self.dn.sim(encoded_target_attn.unsqueeze(2),
                                                encoded_support_attn.unsqueeze(2)).view(nb))

            # get similarity between support set embeddings and target
            similarities = torch.stack(similarities, dim=1) # [nb, ns]
            #similarities = self.dn(support_set=encoded_support, target_image=encoded_target)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, tgti]).float())
            crossentropy_loss +=  F.cross_entropy(preds, target_label[:, tgti].long())


        if self.visMode:
            self.forVis['im_attn_mask'] = [vstackImgs(self.forVis['im_attn_mask'])]


        crossentropy_loss *= 1/target_image.size(1)
        loss = crossentropy_loss
        return {'acc':  accuracy/target_image.size(1),
                'loss': loss,
                'crossentropy_loss': crossentropy_loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class AttentionModel2(AttentionModel1):
    """
    (mid a, top a)  -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionModel2, self).__init__(*args, **kwargs)
        #self.attention = RBM([2*64, 64, 1])
        self.attention = nn.Sequential(nn.Linear(2*64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        top_a = top_a.unsqueeze(1).repeat(1,nrMid*ncMid, 1)

        vec = torch.cat([enc_mid_a, top_a], dim=2).view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.softmax(attn, dim=1)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)


class AttentionModel3(AttentionModel1):
    """
    (mid a)  -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionModel3, self).__init__(*args, **kwargs)
        self.attention = nn.Sequential(nn.Linear(64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        vec = enc_mid_a.contiguous().view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.softmax(attn, dim=1)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)

class AttentionModel4(AttentionModel1):
    """
    (mid a, top b)  -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionModel4, self).__init__(*args, **kwargs)
        self.attention = nn.Sequential(nn.Linear(2*64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        top_b = top_b.unsqueeze(1).repeat(1,nrMid*ncMid, 1)

        vec = torch.cat([enc_mid_a, top_b], dim=2).view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.softmax(attn, dim=1)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)


class AttentionModel1L4(AttentionModel1):
    def __init__(self, *args, **kwargs):
        super(AttentionModel1L4, self).__init__(*args, **kwargs)
        self.attnLayer = -4

class AttentionModel3L4(AttentionModel3):
    def __init__(self, *args, **kwargs):
        super(AttentionModel3L4, self).__init__(*args, **kwargs)
        self.attnLayer = -4



class AttentionModel3Sigmoid(AttentionModel1):
    """
    (mid a)  -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionModel3Sigmoid, self).__init__(*args, **kwargs)
        self.attention = nn.Sequential(nn.Linear(64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        vec = enc_mid_a.contiguous().view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.sigmoid(attn)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)

class AttentionModel2Sigmoid(AttentionModel1):
    """
    (mid a)  -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionModel2Sigmoid, self).__init__(*args, **kwargs)
        self.attention = nn.Sequential(nn.Linear(2*64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        top_a = top_a.unsqueeze(1).repeat(1,nrMid*ncMid, 1)

        vec = torch.cat([enc_mid_a, top_a], dim=2).view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.sigmoid(attn)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)


class AttentionModel1Sigmoid(AttentionModel1):
    """
    (mid a)  -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionModel1Sigmoid, self).__init__(*args, **kwargs)
        self.attention = nn.Sequential(nn.Linear(3*64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        top_a = top_a.unsqueeze(1).repeat(1,nrMid*ncMid, 1)
        top_b = top_b.unsqueeze(1).repeat(1,nrMid*ncMid, 1)

        vec = torch.cat([enc_mid_a, top_a, top_b], dim=2).view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.sigmoid(attn)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)


class AttentionVectorModel1(AttentionModel1):
    """
    (mid a, top a, top b)  -> vector -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionVectorModel1, self).__init__(*args, **kwargs)
        self.ngates = 5
        self.attention = RBM([2*64, 2*64, self.ngates*64])
        self.attnSim = CosineSimilaritiesUnit()

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1).contiguous() # [nb, nr*nc, ndim]

        vec = torch.cat([
            #enc_mid_a,
            top_a.unsqueeze(1).repeat(1,nrMid*ncMid, 1),
            top_b.unsqueeze(1).repeat(1,nrMid*ncMid, 1)
        ], dim=2).view(nb*nrMid*ncMid, -1)

        attn = self.attention(vec).view(nb * nrMid * ncMid, -1, self.ngates) # [nb * nr*nc, ndim, ngates]

        attn = self.attnSim(attn, enc_mid_a.view(nb*nrMid*ncMid, -1, 1)) # [nb*nr*nc, ngates,1]
        #attn = attn.max(1)[0] # [nb* nr*nc,1]
        attn = attn.mean(1) # [nb* nr*nc,1]
        attn = attn.view(nb, nrMid*ncMid,1)


        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)

class AttentionVectorModel1L4(AttentionVectorModel1):
    def __init__(self, *args, **kwargs):
        super(AttentionVectorModel1L4, self).__init__(*args, **kwargs)
        self.attnLayer = -4


class AttentionVectorModel4(AttentionModel1):
    """
    (mid a, top b)  -> attn
    """
    def __init__(self, *args, **kwargs):
        super(AttentionVectorModel4, self).__init__(*args, **kwargs)
        self.ngates = 5
        self.attention = RBM([64, 2*64, self.ngates*64])
        self.attnSim = CosineSimilaritiesUnit()


    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        attn = self.attention(top_b).view(nb, -1, self.ngates)

        import ipdb; ipdb.set_trace()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid) # [nb, ndim, nr*nc]
        attn = self.attnSim(attn, enc_mid_a) # [nb, ngates, nr*nc]

        attn = attn.mean(1, keepdim=True) # [nb, ngates, nr*nc]
        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid)



class AttentionComplementModel1(MatchingNetwork):
    """
    (mid a, top a, top b)  -> attn

    """
    def __init__(self, *args, **kwargs):
        super(AttentionComplementModel1, self).__init__(*args, **kwargs)
        self.dn = DistanceNetwork()
        self.ngates = 5
        self.attention = nn.Sequential(nn.Linear(3*64, 64),
                                       nn.ReLU(),
                                       nn.Linear(64, 1, bias=False)) # [sub-region A, full enc A, full enc B]
        self.attnLayer = -3 # -3 = 5x5; -4= 10x10

        self.nBkgndVectors = 8
        self.bkgndVectors = Parameter(torch.randn(64, self.nBkgndVectors).abs().cuda(), requires_grad=True)

    def getEncoders(self, num_channels, image_size):
        # gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels + 1,
        #                   nClasses= 0, image_size = image_size)
        gSup = Classifier(layer_size = args.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size, relu_last=False)
        #gSup = nn.Sequential(MakeRGBA(), gSup)
        self.encoders = [gSup]
        return [gSup]

    def getAttnFV(self, enc_mid_a, top_a, top_b):
        """
        enc_mid_a: nb, ndim, nr, nc
        top_a: nb, ndim
        top_b: nb, nmid
        """
        nb, nMidDim, nrMid, ncMid = enc_mid_a.size()
        enc_mid_a = enc_mid_a.view(nb, nMidDim, nrMid*ncMid).permute(0,2,1) # [nb, nr*nc, ndim]
        top_a = top_a.unsqueeze(1).repeat(1,nrMid*ncMid, 1)
        top_b = top_b.unsqueeze(1).repeat(1,nrMid*ncMid, 1)

        vec = torch.cat([enc_mid_a, top_a, top_b], dim=2).view(nb*nrMid*ncMid, -1)
        attn = self.attention(vec).view(nb, nrMid * ncMid, 1)
        attn = F.softmax(attn, dim=1)

        combined = (enc_mid_a * attn).sum(1) # [nb, ndim]

        complement = (enc_mid_a * (1-attn)).sum(1) # [nb, ndim]
        return combined, attn.view(nb, 1, nrMid, ncMid), complement


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0
        additivity_loss = 0
        crossentropy_loss = 0
        self_loss = 0
        bkgnd_loss = 0
        accuracy = 0

        if self.visMode:
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()
            self.forVis['im_attn_mask'] = []

        # produce embeddings for target images
        for tgti in range(len(target_enc)):
            nb, nTopDim, _, _ = target_enc[tgti][-1].size()
            encoded_target_top = target_enc[tgti][-1].view(nb, nTopDim) # [nb, ndim]
            encoded_target_mid = target_enc[tgti][self.attnLayer] # [nb, ndim, nr, nc]
            nb, nMidDim, nrMid, ncMid = encoded_target_mid.size()
            similarities = []
            similaritiesBkgnd = []
            for supi in range(len(support_enc)):
                encoded_support_top = support_enc[supi][-1].view(nb, nTopDim)
                encoded_support_mid = support_enc[supi][self.attnLayer] # [nb, ndim, nr, nc]

                encoded_target_attn, target_attn_mask, encoded_target_complement = self.getAttnFV(encoded_target_mid, encoded_target_top, encoded_support_top) # [nb, ndim]
                encoded_support_attn, support_attn_mask, encoded_support_complement = self.getAttnFV(encoded_support_mid, encoded_support_top, encoded_target_top) # [nb, ndim]

                if self.visMode:
                    ta = target_attn_mask[0] # [1, nr, nc]
                    sa = support_attn_mask[0] # [1, nr, nc]
                    ta = torch.from_numpy(scipy.misc.imresize(ta.data.cpu().numpy()[0], (84,84), mode='F')).unsqueeze(0).cuda()
                    sa = torch.from_numpy(scipy.misc.imresize(sa.data.cpu().numpy()[0], (84,84), mode='F')).unsqueeze(0).cuda()

                    self.forVis['im_attn_mask'].append(
                        hstackImgs([
                            hstackImgs([unnormImg(target_image[0, tgti], self.parent.data.mean, self.parent.data.std), ta]),
                            hstackImgs([unnormImg(support_set_images[0, supi], self.parent.data.mean, self.parent.data.std), sa])]))

                similarities.append(self.dn.sim(encoded_target_attn.unsqueeze(2),
                                                encoded_support_attn.unsqueeze(2)).view(nb))

                bkgndSim = CosineSimilaritiesUnit()(encoded_target_mid.view(nb, nMidDim, nrMid*ncMid),
                                                    self.bkgndVectors.unsqueeze(0).repeat(nb,1,1)) # [nb, nrMid*ncMid, nbkgnd]
                bkgndSim = bkgndSim.max(2)[0].mean(1) # [nb]
                bkgnd_loss += -bkgndSim.mean()

            # get similarity between support set embeddings and target
            similarities = torch.stack(similarities, dim=1) # [nb, ns]
            #similarities = self.dn(support_set=encoded_support, target_image=encoded_target)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)


            # calculate accuracy and crossentropy loss
            _, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, tgti]).float())
            crossentropy_loss +=  F.cross_entropy(preds, target_label[:, tgti].long())

        if self.visMode:
            self.forVis['im_attn_mask'] = [vstackImgs(self.forVis['im_attn_mask'])]


        crossentropy_loss *= 1/target_image.size(1)
        bkgnd_loss *= 0.1/target_image.size(1)
        loss = crossentropy_loss + bkgnd_loss
        return {'acc':  accuracy/target_image.size(1),
                'loss': loss,
                'crossentropy_loss': crossentropy_loss,
                'bkgnd_loss': bkgnd_loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}
