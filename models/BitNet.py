import torch.nn.functional as F
from models.Base import *
from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg

class BitMask(MyModule):
    def __init__(self, nmasks, width=3, stride=2):
        """
        nm : number of masks to create
        """
        super(BitMask, self).__init__()
        self.w = width
        self.stride = stride
        self.nm = nmasks
        self.weight = Parameter(torch.randn(self.nm, 1, self.w, self.w))
        self.mp = nn.MaxPool2d(self.stride,self.stride)

    def forward(self, inp):
        """
        inp: [nb, nbl, nr, nc]
        convolve over nr, nc : m * br1 + (1-m) * br0
        """
        nb, nbl, nr, nc = inp.size()
        inp = inp.view(nb*nbl, 1, nr, nc)
        m = F.sigmoid(self.weight) # [1, self.nm, w, w]

        myconv = lambda xinp, xm: self.mp(F.conv2d(xinp, weight=xm, stride=1, padding=self.w//2))

        nr2 = nr // self.stride
        nc2 = nc // self.stride
        f1 = myconv(inp,   m)
        f2 = myconv(inp, 1-m)
        nr2 = f1.size(2)
        nc2 = f1.size(3)
        f1 = f1.view(nb, nbl, 1, self.nm, nr2, nc2) # [nb, nbl, nm, 1, nr, nc]
        f2 = f2.view(nb, 1, nbl, self.nm, nr2, nc2) # [nb, 1, nbl, nm, nr, nc]

        f = f1 + f2  # [nb, nbl, nbl, nm, nr, nc]
        f = f.view(nb, nbl*nbl*self.nm, nr2, nc2) # [nb, nl, nr, nc] nl = nbl*nbl*nm

        assert(f.data.min() >= 0), "Need #bits to be non-negative for bitmask. Got {}".format(f.min())
        return f


class BrushWithoutMask(MyModule):
    def __init__(self, nbl, nbl_1, nml_1):
        """
        nbl: number of brushes to create at this level
        nml_1: number of masks in previous level
        nbl_1: number of brushes in previous level

        generates distribution over:
        Single brush over full area or Masked fill with 2 brushes
        (diag or off-diag)
        nbl_1 + nml_1 * nbl_1 * nbl_1

        """
        super(Brush, self).__init__()
        self.nbl = nbl
        self.nbl_1 = nbl_1
        self.nml_1 = nml_1
        self.weight = Parameter(torch.randn(nbl, nbl_1 * nbl_1))

        self.initializeWeights()

    def initializeWeights(self):
        mb = torch.randn(self.nbl, self.nbl_1, self.nbl_1)

        eps = 0.3
        for i in range(self.nbl_1):
            mb[1:, i,i] -= 10  # mask 0 diag will be the default for same brush fill

        self.weight.data.copy_(mb.view(*self.weight.data.size()))



    def forward(self, inp):
        """
        nl_1 = br0l_1 * br1l_1 * nml_1
        inp : [batch, nl_1, nr, nc]
        """
        nb, nl_1, nr, nc = inp.size()
        p = F.softmax(self.weight, dim=1) # [nbl, nl_1]
        p = p.view(1,p.size(0), p.size(1), 1,1,1) # [1, nbl, nl_1, 1, 1, 1]
        bp = -(p+1e-12).log()
        bits = inp.view(nb, 1, self.nbl_1 * self.nbl_1, nl_1//(self.nbl_1*self.nbl_1), nr, nc) + bp # [nb, nbl, nbl_1**2, nml_1, nr, nc]
        bits = bits.view(nb, self.nbl, nl_1, nr, nc)
        #ebits = (bits * p).sum(2) # [nb, nbl, nr, nc]
        ebits = bits.min(2)[0]
        assert(ebits.data.min() >= 0), "Need #bits to be non-negative for brush. Got {}".format(ebits.min())
        return ebits



class BrushWithMask(MyModule):
    def __init__(self, nbl, nbl_1, nml_1):
        """
        nbl: number of brushes to create at this level
        nml_1: number of masks in previous level
        nbl_1: number of brushes in previous level

        generates distribution over:
        Single brush over full area or Masked fill with 2 brushes
        (diag or off-diag)
        nbl_1 + nml_1 * nbl_1 * nbl_1

        """
        super(Brush, self).__init__()
        self.nbl = nbl
        self.nbl_1 = nbl_1
        self.nml_1 = nml_1
        self.weight = Parameter(torch.randn(nbl, nbl_1 * nbl_1 * nml_1))

        self.initializeWeights()

    def initializeWeights(self):
        mb = torch.randn(self.nbl, self.nbl_1, self.nbl_1, self.nml_1)

        eps = 0.3
        for i in range(self.nbl_1):
            mb[1:, i,i,:] -= 10  # mask 0 diag will be the default for same brush fill

        self.weight.data.copy_(mb.view(*self.weight.data.size()))



    def forward(self, inp):
        """
        nl_1 = br0l_1 * br1l_1 * nml_1
        inp : [batch, nl_1, nr, nc]
        """
        p = F.softmax(self.weight, dim=1) # [nbl, nl_1]
        p = p.view(1,p.size(0), p.size(1), 1,1) # [1, nbl, nl_1, 1, 1]
        bp = -(p+1e-12).log()
        bits = inp.unsqueeze(1) + bp # [nb, nbl, nl_1, nr, nc]
        #ebits = (bits * p).sum(2) # [nb, nbl, nr, nc]
        ebits = bits.min(2)[0]
        assert(ebits.data.min() >= 0), "Need #bits to be non-negative for brush. Got {}".format(ebits.min())
        return ebits


Brush = BrushWithoutMask


class Brush0(MyModule):
    def __init__(self, nbrush, nch):
        """
        nbl: number of brushes to create at this level
        nch: num channels

        colors are in unit cube of size nch. Uniformly sample that cube

        model: normal distribution with sigma centered at random points in unit cube
        """
        super(Brush0, self).__init__()
        self.nbrush = nbrush
        self.nch = nch

        # since image is normalized, color can be negative
        self.weight = Parameter(torch.randn(self.nbrush, self.nch))  if self.nbrush > 2 else Parameter(torch.Tensor([0.0, 1.0]).view(2,1))

        self.sigma = Parameter(torch.zeros(self.nbrush).fill_(0.5))

    def forward(self, inp):
        """
        nl_1 = br0l_1 * br1l_1 * nml_1
        inp : [batch, nch, nr, nc]
        """
        d2 = (inp.unsqueeze(1) - self.weight.view(1,self.nbrush, self.nch, 1, 1)).pow(2).sum(2) # [nb, nbrush, nr, nc]

        d2 = d2/((self.sigma.pow(2) + 0.01).view(1,self.nbrush, 1,1))

        bits = d2 + (self.sigma+0.1).log().view(1,self.nbrush,1,1) + 0.5*math.log(2*math.pi) # [nb, nbrush, nr, nc]
        bits = F.relu(bits)
        #assert(bits.data.min() >= 0), "Need #bits to be non-negative for brush0. Got {}, {}, {}".format(bits.min(), self.weight.data, self.sigma.data)
        return bits



class BitClassifier(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        super(BitClassifier, self).__init__()
        #nm = 32
        #nb = [256, 32,64,64,64,64,64,64]
        nm = 8
        maskWidth=5
        stride = 2
        nch = num_channels

        nlayers = math.ceil(math.log(image_size) / math.log(stride) - 1e-6)

        nb = [2 if nch == 1 else 16] + [8]*(nlayers-1)
        self.layers = [Brush0(nb[0], nch)]
        for i in range(1,len(nb)):
            self.layers.append(self.bitlayer(nm, nb[i-1], nb[i], maskWidth))

        self.layers = nn.ModuleList(self.layers[:nlayers])
        self.ll  = nn.Linear(8, nClasses)

    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [nb, nch, nr, nc]
        :return: Embeddings of size [batch_size, 64]
        """
        xs = [image_input]
        x = image_input
        for i,l in enumerate(self.layers):
            x = l(x)
            xs.append(x)

        ll = self.ll(x.squeeze(3).squeeze(2))
        return xs, ll

    def bitlayer(self, nm, nbl_1, nbl, width=3):
        return nn.Sequential(BitMask(nm, width),
                             Brush(nbl, nbl_1, nm)
        )





class BitNet(MyModule):
    def __init__(self, keep_prob=1, batch_size=100, num_channels=1, learning_rate=0.001,
                 nClasses = 0, image_size = 28):
        super(BitNet, self).__init__()
        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.g = BitClassifier(layer_size = 64, nClasses = nClasses,
                               num_channels = num_channels, image_size=image_size)
        self.keep_prob = keep_prob
        self.learning_rate = learning_rate

    def getEncoders(self, num_channels, image_size):
        gSup = Identity()
        return [gSup]


    def forward(self, xs, ys):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param xs
        :param ys
        """
        # produce embeddings for support set images


        gbits, ll = self.g(xs)
        bits = gbits[-1]
        bits = bits.squeeze(3).squeeze(2)
        preds = F.softmax(ll, dim=1)


        # calculate accuracy and crossentropy loss
        values, indices = preds.max(1)

        accuracy = torch.mean((indices.squeeze() == ys).float())
        crossentropy_loss = F.cross_entropy(preds, ys.long())
        #crossentropy_loss = preds.index_select(1,ys).pow(2).mean()

        #minBitWeights = [0.5**i for i in range(10)]
        minBitWeights = [1 for i in range(10)]

        #minBitLoss = [gbits[i].min(1)[0].mean() * minBitWeights[i] for i in range(1,len(gbits))]
        minBitLoss = [(gbits[i] * F.softmax(-gbits[i], dim=1)).sum() * minBitWeights[i] for i in range(1,len(gbits))]

        minBitLoss = sum(minBitLoss)

        loss = minBitLoss #+ crossentropy_loss


        return {'acc':  accuracy,
                'loss': loss,
                'bitsmean': gbits[0].mean(),
                'bitsmax': gbits[0].max(),
                'bitsmin': gbits[0].min(),
        }




class BitClassifierSharedMask(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        super(BitClassifierSharedMask, self).__init__()
        #nm = 32
        #nb = [256, 32,64,64,64,64,64,64]
        nm = 8
        maskWidth=3
        nch = num_channels

        nlayers = math.ceil(math.log(image_size) / math.log(maskWidth) - 1e-6) + 2
        self.bitmask = BitMask(nm, width)
        nb = [2 if nch == 1 else 16] + [8]*(nlayers-2) + [nClasses]
        self.layers = [Brush0(nb[0], nch)]
        for i in range(1,len(nb)):
            self.layers.append(bitlayer(nm, nb[i-1], nb[i], maskWidth))

        self.layers = nn.ModuleList(self.layers[:nlayers])

    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [nb, nch, nr, nc]
        :return: Embeddings of size [batch_size, 64]
        """
        xs = [image_input]
        x = image_input
        for i,l in enumerate(self.layers):
            x = l(x)
            xs.append(x)
            if x.size(3) == 1: break
        return xs


    def bitlayer(self, nm, nbl_1, nbl, width=3):
        return nn.Sequential(self.bitmask,
                             Brush(nbl, nbl_1, nm)
        )




class BitNetSharedMask(MyModule):
    def __init__(self, keep_prob=1, batch_size=100, num_channels=1, learning_rate=0.001,
                 nClasses = 0, image_size = 28):
        super(BitNet, self).__init__()
        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.g = BitClassifier(layer_size = 64, nClasses = nClasses,
                               num_channels = num_channels, image_size=image_size)
        self.keep_prob = keep_prob
        self.learning_rate = learning_rate

    def getEncoders(self, num_channels, image_size):
        gSup = Identity()
        return [gSup]


    def forward(self, xs, ys):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param xs
        :param ys
        """
        # produce embeddings for support set images


        bits = self.g(xs)[-1]
        bits = bits.squeeze(3).squeeze(2)
        preds = F.softmax(-bits, dim=1)


        # calculate accuracy and crossentropy loss
        values, indices = preds.max(1)

        accuracy = torch.mean((indices.squeeze() == ys).float())
        #crossentropy_loss = F.cross_entropy(preds, ys.long())
        crossentropy_loss = preds.index_select(ys).pow(2).mean()


        return {'acc':  accuracy,
                'loss': crossentropy_loss,
                'bitsmean': bits.mean(),
                'bitsmax': bits.max(),
                'bitsmin': bits.min(),
        }
