from models.ABMNets import *
from tu.imageUtils import makeImg, setupImg, hstackImgs, vstackImgs, padImg, normImg
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import data, segmentation, filters, color
from skimage import io
import matplotlib.pyplot as plt
from skimage.future import graph
from skimage.future.graph import RAG, merge_hierarchical
from models.LinearAdditiveModel import weight_boundary, merge_boundary
import gc
import GPUtil

class MatchingNetworkPWSelfLoss(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfLoss, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPW()
        self.dnSelf = self.dn
        self.g = StackFV(kwargs.get('image_size'))
        self.emb = Identity()

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None, target_mask=None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :param target_mask :: [nb, nr, nc]

        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.supSamp)).select(1,0), requires_grad=False) # sample 20%

        if cargs.masktype == 'img':
            # masking is done in oneshotbuilder
            rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.tgtSamp)).select(1,0), requires_grad=False) # sample 10%
        else:
            assert(nb == 1)
            rsampTgt = torch.nonzero(target_mask.view(nb, nr*nc))[:,1]

        accuracy = 0
        crossentropy_loss = 0

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])
        self_loss = 0

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)
            tMask = None
            if target_mask is not None: tMask = target_mask.view(nb, nr*nc).index_select(1, rsampTgt)

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV, target_mask=tMask)

            ## get self similarity
            selfSimilarity = None
            if True:
                selfSimilarity = self.dn.sim(targetFV, targetFV) #[batch_size, ntgt , ntgt]

                nb, ntgt,_ = selfSimilarity.size()
                selfSimilarity = F.softmax(selfSimilarity, dim=2) #[batch_size, ntgt , ntgt]
                selfLabel = Variable(torch.arange(0,ntgt).long().cuda(), requires_grad=False)

                self_loss += F.cross_entropy(selfSimilarity.view(nb * ntgt, ntgt), selfLabel.repeat(nb))

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)
            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
            crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long()) + 0.25 * self_loss
            cel = F.cross_entropy(preds, target_label[:, i].long())
            if numpy.isnan(cel.item()):
                import ipdb; ipdb.set_trace()


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class MatchingNetworkPWHierarchicalSelfLoss(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWHierarchicalSelfLoss, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPW()
        self.g = StackFV(kwargs.get('image_size'))
        self.emb = Identity()
        self.selfLoss = SelfLossHierarchical()
    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.supSamp)).select(1,0), requires_grad=False) # sample 20%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.tgtSamp)).select(1,0), requires_grad=False) # sample 10%

        accuracy = 0
        crossentropy_loss = 0

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV)


            ## get self similarity
            self_loss = self.selfLoss(target_enc[i])

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
            crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long()) + 0.25 * self_loss

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class MatchingNetworkPWSelfMaskPair(MatchingNetworkPW):
    """
    self regularization + ability to split im12 vs im1, im2  with single encoder
    """
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfMaskPair, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPW()
        self.g = StackFV(kwargs.get('image_size'))
        self.emb = Identity()
        self.sim = CosineSimilarities()
        self.maskGen = RandomVOCMask('/home/pareshmg/data/VOC', transform=transforms.Compose([filenameToPILImage,
                                                                                              PiLImageResizeNearest,
                                                                                              numpy.array,
                                                                                              torch.from_numpy]))

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.supSamp)).select(1,0), requires_grad=False) # sample 20%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.tgtSamp)).select(1,0), requires_grad=False) # sample 10%


        accuracy = 0
        crossentropy_loss = 0

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        ntgt = rsampTgt.size()[0]
        nsup = rsampSup.size()[0]



        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])
        self_loss = 0
        separation_loss = 0

        ## Mix matching for separability
        if True:
            im1 = suppFV[0] # [nb, ndim, nsup]
            im2 = suppFV[1]
            im3 = suppFV[2]
            msk = Variable(self.maskGen[random.randrange(0, len(self.maskGen.masks))].float().cuda(), requires_grad=False)
            msk = msk.view(-1).index_select(0, rsampSup).unsqueeze(0).unsqueeze(1)
            im12 = msk * im1 + (1-msk) * im2
            ims = [im1, im2, im3] # for contrast


            separation_similarities = [] # [ [nb, nsup, nsup] ]
            for im in ims:
                separation_similarities.append(self.sim(im, im12))

            separation_similarities = torch.stack(separation_similarities, dim=2) # [nb, nsup, nims, nsup]
            nb, sup, nims, _ = separation_similarities.size()
            separation_similarities = separation_similarities.view(nb*nsup, nims*nsup)
            separation_similarities = F.softmax(separation_similarities, dim=1)
            trueMatch = Variable(torch.arange(0,nsup).long().cuda(), requires_grad=False)
            trueMatch = trueMatch + (1-msk.long()) * nsup

            separation_loss += F.cross_entropy(separation_similarities, trueMatch.squeeze().repeat(nb))



        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV)

            ## Self matching for identifiability
            if True:
                eps = 1e-10
                selfSimilarity = self.sim(targetFV, targetFV) #[batch_size, ntgt , ntgt]
                nb, ntgt,_ = selfSimilarity.size()
                selfSimilarity = F.softmax(selfSimilarity, dim=2) #[batch_size, ntgt , ntgt]
                selfLabel = Variable(torch.arange(0,ntgt).long().cuda(), requires_grad=False)

                self_loss += F.cross_entropy(selfSimilarity.view(nb * ntgt, ntgt), selfLabel.repeat(nb))



            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
            crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long())

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        loss = (crossentropy_loss + 0.25 * self_loss)/target_image.size(1) + separation_loss

        return {'acc' : accuracy/target_image.size(1),
                'self_loss': self_loss / target_image.size(1),
                'crossentropy_loss': crossentropy_loss / target_image.size(1),
                'separation_loss': separation_loss,
                'loss': loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class MatchingNetworkPWSelfMaskPair2(MatchingNetworkPW):
    """
    selfmaskpair with dual encoders - one for separation and one for mask
    """
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfMaskPair2, self).__init__(*args, **kwargs)
        self.g = StackFV(kwargs.get('image_size'))

        self.emb = Identity()
        self.embSplit = Classifier(layer_size = builtins.cargs.layer_size, num_channels=kwargs['num_channels'],
                                   nClasses= 0, image_size = kwargs['image_size'] )

        self.normalize_mask_weight = False
        self.use_sim = False
        self.dn = DistanceNetworkPWSeparation(embSplit = self.embSplit, normalize_mask_weight=self.normalize_mask_weight, use_sim=self.use_sim)
        self.sim = CosineSimilarities()
        self.l2 = L2Distance()
        self.maskGen = RandomVOCMask('/home/pareshmg/data/VOC', transform=transforms.Compose([filenameToPILImage,
                                                                                              PiLImageResizeNearest,
                                                                                              numpy.array,
                                                                                              torch.from_numpy]))
        self.nref = 5
        self.image_only_match = True

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, other_set_images):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [nb, ns, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [nb, ns, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [nb, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.supSamp)).select(1,0), requires_grad=False) # sample 20%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.tgtSamp)).select(1,0), requires_grad=False) # sample 10%


        accuracy = 0
        crossentropy_loss = 0

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        ntgt = rsampTgt.size(0)
        nsup = rsampSup.size(0)



        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [ns, nb, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])
        self_loss = 0
        self_acc = 0
        separation_loss = 0
        separation_acc = 0

        ## Mix matching for separability
        other_emb = [self.g(self.embSplit(other_set_images.select(1,i))).view(nb, ndim, nr * nc).index_select(2, rsampSup) for i in range(other_set_images.size(1))]
        if False:
            im1 = support_set_images.select(1,0)
            im2 = support_set_images.select(1,1)
            im3 = support_set_images.select(1,2)
            ims = [im1, im2, im3] # for contrast
            msk = Variable(self.maskGen[random.randrange(0, len(self.maskGen.masks))].float().cuda(), requires_grad=False) # [nr, nc]
            msk = msk.unsqueeze(0).unsqueeze(1)
            assert(msk.data.min() >= 0), 'got {}'.format(msk.data.min())
            assert(msk.data.max() <= 1), 'got {}'.format(msk.data.max())
            im12 = msk * support_set_images.select(1,1) + (1-msk) * support_set_images.select(1,0)

            if self.visMode:
                self.forVis['separation_mask'] = [vstackImgs([
                    normImg(hstackImgs([im[bi].data.clone() for im in ims] +
                                        [im12.data.clone()[bi]])),
                    hstackImgs([(1-msk[0]).data.clone(),
                                msk[0].data,
                                msk[0].data*0])]) for bi in range(nb)]


            im12 = self.g(self.embSplit(im12)).view(nb, ndim, nr*nc).index_select(2, rsampSup)
            ims = [self.g(self.embSplit(x)).view(nb, ndim, nr*nc).index_select(2, rsampSup) for x in ims]

            msk = msk.view(-1).index_select(0, rsampSup).unsqueeze(0).unsqueeze(1) # [1,1,nsup]

            separation_similarities = [] # [ [nb, nsup, nsup] ]
            for im in ims:
                #separation_similarities.append(-self.l2(im12, im)) # [nb, nsup, nsup]
                separation_similarities.append(self.sim(im12, im)) # [nb, nsup, nsup]

            separation_similarities = torch.stack(separation_similarities, dim=2) # [nb, nsup, nims, nsup]
            nb, nsup, nims, _ = separation_similarities.size()

            if self.image_only_match: # match only to image, not to image, pixel
                separation_similarities = separation_similarities.max(3)[0]
                separation_similarities = F.softmax(separation_similarities, dim=2)
                separation_similarities = separation_similarities.view(nb*nsup, nims) # [nb*nsup, nims]
                trueMatch = msk.squeeze(1).repeat(nb, 1).long()

                if self.visMode:
                    predmask = separation_similarities.view(nb, nsup, nims).data
                    for bi in range(nb):
                        self.forVis['separation_mask'][bi] = vstackImgs([self.forVis['separation_mask'][bi],
                                                                         hstackImgs([predmask[bi,:,oi].clone().view(nr,nc) for oi in range(nims)])])
            else: # match image, pixel
                separation_similarities = separation_similarities.view(nb*nsup, nims*nsup) # [nb*nsup, nims*nsup]
                separation_similarities = F.softmax(separation_similarities, dim=1)
                trueMatch = Variable(torch.arange(0,nsup).long().cuda(), requires_grad=False)
                trueMatch = trueMatch.unsqueeze(0) + (1-msk.squeeze(1).repeat(nb, 1).long()) * nsup
                if self.visMode:
                    predmask = separation_similarities.view(nb, nsup, nims, nsup).data.sum(3)
                    for bi in range(nb):
                        self.forVis['separation_mask'][bi] = vstackImgs([self.forVis['separation_mask'][bi],
                                                                         hstackImgs([predmask[bi,:,oi].clone().view(nr,nc) for oi in range(nims)])])


            trueMatch = trueMatch.view(-1)
            separation_loss += F.cross_entropy(separation_similarities, trueMatch)
            _, indices = separation_similarities.max(1)
            separation_acc += torch.mean((indices.squeeze() == trueMatch).float())

        # produce embeddings for target images
        for i in range(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            target_image_other = self.g(self.embSplit(target_image.select(1,i)))
            target_image_other = target_image_other.view(target_image_other.size(0),
                                                         target_image_other.size(1), nr*nc)
            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV,
                                   target_image_other = target_image_other.index_select(2, rsampTgt),
                                   reference_other_set = other_emb)

            ## Self matching for identifiability
            if True:
                eps = 1e-10
                selfSimilarity = self.sim(targetFV, targetFV) #[batch_size, ntgt , ntgt]
                nb, ntgt,_ = selfSimilarity.size()
                selfSimilarity = F.softmax(selfSimilarity, dim=2) #[batch_size, ntgt , ntgt]
                selfLabel = Variable(torch.arange(0,ntgt).long().cuda().repeat(nb), requires_grad=False)
                selfSimilarity = selfSimilarity.view(nb * ntgt, ntgt)
                self_loss += F.cross_entropy(selfSimilarity, selfLabel)

                _, indices = selfSimilarity.max(1)
                self_acc += torch.mean((indices.squeeze() == selfLabel).float())

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
            crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long())

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())

                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        loss = (crossentropy_loss + 0.25 * self_loss)/target_image.size(1) + separation_loss
        return {'acc' : accuracy/target_image.size(1),
                'self_loss': self_loss / target_image.size(1),
                'crossentropy_loss': crossentropy_loss / target_image.size(1),
                'separation_loss': separation_loss,
                'self_acc': self_acc / target_image.size(1),
                'separation_acc': separation_acc,
                'loss': loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class MatchingNetworkPWSelfMaskPair2Full(MatchingNetworkPWSelfMaskPair2):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfMaskPair2Full, self).__init__(*args, **kwargs)
        self.image_only_match = False


class MatchingNetworkPWSelfMaskPair2Norm(MatchingNetworkPWSelfMaskPair2):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfMaskPair2Norm, self).__init__(*args, **kwargs)
        self.image_only_match = True
        self.normalize_mask_weight = True
        self.dn = DistanceNetworkPWSeparation(embSplit = self.embSplit, normalize_mask_weight=self.normalize_mask_weight)

class MatchingNetworkPWSelfMaskPair2Sim(MatchingNetworkPWSelfMaskPair2):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfMaskPair2Sim, self).__init__(*args, **kwargs)
        self.image_only_match = True
        self.normalize_mask_weight = False
        self.use_sim = True
        self.dn = DistanceNetworkPWSeparation(embSplit = self.embSplit, normalize_mask_weight=self.normalize_mask_weight, use_sim = self.use_sim)




class MatchingNetworkPWSelfMaskQuad2(MatchingNetworkPW):
    """
    selfmaskpair with dual encoders - one for separation and one for mask
    """
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfMaskQuad2, self).__init__(*args, **kwargs)
        self.g = StackFV(kwargs.get('image_size'))

        self.emb = Identity()
        self.embSplit = Classifier(layer_size = builtins.cargs.layer_size, num_channels=kwargs['num_channels'],
                                   nClasses= 0, image_size = kwargs['image_size'] )

        self.normalize_mask_weight = False
        self.dn = DistanceNetworkPWSeparation(embSplit = self.embSplit, normalize_mask_weight=self.normalize_mask_weight)
        self.sim = CosineSimilarities()
        self.l2 = L2Distance()
        self.maskGen = RandomVOCMask('/home/pareshmg/data/VOC', transform=transforms.Compose([filenameToPILImage,
                                                                                              PiLImageResizeNearest,
                                                                                              numpy.array,
                                                                                              torch.from_numpy]))
        self.nref = 5

        self.image_only_match = True

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, other_set_images):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [nb, ns, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [nb, ns, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [nb, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.supSamp)).select(1,0), requires_grad=False) # sample 20%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.tgtSamp)).select(1,0), requires_grad=False) # sample 10%


        accuracy = 0
        crossentropy_loss = 0

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        ntgt = rsampTgt.size(0)
        nsup = rsampSup.size(0)



        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [ns, nb, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])
        self_loss = 0
        self_acc = 0
        separation_loss = 0
        separation_acc = 0

        ## Mix matching for separability
        other_emb = [self.g(self.embSplit(other_set_images.select(1,i))).view(nb, ndim, nr * nc).index_select(2, rsampSup) for i in range(other_set_images.size(1))]
        if True:
            im1 = support_set_images.select(1,0)
            im2 = support_set_images.select(1,1)
            im3 = support_set_images.select(1,2)
            im4 = support_set_images.select(1,3)
            msk = Variable(self.maskGen[random.randrange(0, len(self.maskGen.masks))].float().cuda(), requires_grad=False) # [nr, nc]

            ims = [im4]
            msk = msk.unsqueeze(0).unsqueeze(1)
            assert(msk.data.min() >= 0), 'got {}'.format(msk.data.min())
            assert(msk.data.max() <= 1), 'got {}'.format(msk.data.max())
            im12 = msk * support_set_images.select(1,1) + (1-msk) * support_set_images.select(1,0)
            im32 = msk * support_set_images.select(1,2) + (1-msk) * support_set_images.select(1,0)

            if self.visMode:
                self.forVis['separation_mask'] = [vstackImgs([
                    normImg(hstackImgs([im[bi].data.clone() for im in ims] +
                                        [im12.data.clone()[bi]])),
                    hstackImgs([(1-msk[0]).data.clone(),
                                msk[0].data,
                                msk[0].data*0])]) for bi in range(nb)]


            im12 = self.g(self.embSplit(im12)).view(nb, ndim, nr*nc).index_select(2, rsampSup)
            im32 = self.g(self.embSplit(im32)).view(nb, ndim, nr*nc).index_select(2, rsampSup)
            ims = [im32] + [self.g(self.embSplit(x)).view(nb, ndim, nr*nc).index_select(2, rsampSup) for x in ims]

            msk = msk.view(-1).index_select(0, rsampSup).unsqueeze(0).unsqueeze(1) # [1,1,nsup]

            separation_similarities = [] # [ [nb, nsup, nsup] ]
            for im in ims:
                #separation_similarities.append(-self.l2(im12, im)) # [nb, nsup, nsup]
                separation_similarities.append(self.sim(im12, im)) # [nb, nsup, nsup]

            separation_similarities = torch.stack(separation_similarities, dim=2) # [nb, nsup, nims, nsup]
            nb, nsup, nims, _ = separation_similarities.size()

            if self.image_only_match: # match only to image, not to image, pixel
                separation_similarities = separation_similarities.max(3)[0]
                separation_similarities = F.softmax(separation_similarities, dim=2)
                separation_similarities = separation_similarities.view(nb*nsup, nims) # [nb*nsup, nims]
                trueMatch = msk.squeeze(1).repeat(nb, 1).long()

                if self.visMode:
                    predmask = separation_similarities.view(nb, nsup, nims).data
                    for bi in range(nb):
                        self.forVis['separation_mask'][bi] = vstackImgs([self.forVis['separation_mask'][bi],
                                                                         hstackImgs([predmask[bi,:,oi].clone().view(nr,nc) for oi in range(nims)])])
            else: # match image, pixel
                separation_similarities = separation_similarities.view(nb*nsup, nims*nsup) # [nb*nsup, nims*nsup]
                separation_similarities = F.softmax(separation_similarities, dim=1)
                trueMatch = Variable(torch.arange(0,nsup).long().cuda(), requires_grad=False)
                trueMatch = trueMatch.unsqueeze(0) + (1-msk.squeeze(1).repeat(nb, 1).long()) * nsup
                if self.visMode:
                    predmask = separation_similarities.view(nb, nsup, nims, nsup).data.sum(3)
                    for bi in range(nb):
                        self.forVis['separation_mask'][bi] = vstackImgs([self.forVis['separation_mask'][bi],
                                                                         hstackImgs([predmask[bi,:,oi].clone().view(nr,nc) for oi in range(nims)])])


            trueMatch = trueMatch.view(-1)
            separation_loss += F.cross_entropy(separation_similarities, trueMatch)
            _, indices = separation_similarities.max(1)
            separation_acc += torch.mean((indices.squeeze() == trueMatch).float())

        # produce embeddings for target images
        for i in range(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            target_image_other = self.g(self.embSplit(target_image.select(1,i)))
            target_image_other = target_image_other.view(target_image_other.size(0),
                                                         target_image_other.size(1), nr*nc)
            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV,
                                   target_image_other = target_image_other.index_select(2, rsampTgt),
                                   reference_other_set = other_emb)

            ## Self matching for identifiability
            if True:
                eps = 1e-10
                selfSimilarity = self.sim(targetFV, targetFV) #[batch_size, ntgt , ntgt]
                nb, ntgt,_ = selfSimilarity.size()
                selfSimilarity = F.softmax(selfSimilarity, dim=2) #[batch_size, ntgt , ntgt]
                selfLabel = Variable(torch.arange(0,ntgt).long().cuda().repeat(nb), requires_grad=False)
                selfSimilarity = selfSimilarity.view(nb * ntgt, ntgt)
                self_loss += F.cross_entropy(selfSimilarity, selfLabel)

                _, indices = selfSimilarity.max(1)
                self_acc += torch.mean((indices.squeeze() == selfLabel).float())

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
            crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long())

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())

                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        loss = (crossentropy_loss + 0.25 * self_loss)/target_image.size(1) + separation_loss
        return {'acc' : accuracy/target_image.size(1),
                'self_loss': self_loss / target_image.size(1),
                'crossentropy_loss': crossentropy_loss / target_image.size(1),
                'separation_loss': separation_loss,
                'self_acc': self_acc / target_image.size(1),
                'separation_acc': separation_acc,
                'loss': loss,
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class MatchingNetworkPWSelfLossVisMask(MatchingNetworkPWSelfLoss):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfLossVisMask, self).__init__(*args, **kwargs)
        self.nref = 5
        self.use_sim = True
        self.sim = CosineSimilarities()
        self.l2 = L2Distance()

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, other_set_images, other_enc):
        res = super(MatchingNetworkPWSelfLossVisMask, self).forward(support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc)


        if self.visMode:
            encoded_other = torch.stack([self.g(x) for x in other_enc]) # [ns, nb, ndim, nr, nc]
            ns, nb, ndim, nr, nc = encoded_other.size()
            encoded_other = encoded_other.view(ns, nb, ndim, nr*nc)
            encoded_support = [self.g(x).view(nb, ndim, nr*nc) for x in support_enc]
            self.forVis['tmask'] = []
            self.forVis['separation_mask'] = []
            self.forVis['separation_mask_support'] = []
            for i in range(target_image.size(1)):
                target_image_other = self.g(target_enc[i]).view(nb, ndim, nr*nc)
                ## obtain mask
                other_sim = []
                ## obtain mask
                for other_image in encoded_other:
                    if self.use_sim:
                        other_sim.append(self.sim(target_image_other, other_image).max(2)[0]) # [nb, ntgt]
                    else:
                        other_sim.append(-self.l2(target_image_other, other_image).min(2)[0]) # [nb, ntgt]
                other_sim = torch.stack(other_sim, dim=2) # [nb, ntgt, nref]

                selfsim = (self.sim(target_image_other, target_image_other).max(2)[0] * 0.7).exp()  if self.use_sim else 0.15
                other_sim_base = other_sim.exp()
                other_sim_norm = other_sim_base.sum(2) + selfsim # [nb, ntgt]
                m_ = selfsim/other_sim_norm # [nb, ntgt]
                m = m_.data.clone().unsqueeze(2).unsqueeze(3) # [nb, ntgt, 1, 1]
                self.forVis['separation_mask'].extend([vstackImgs([
                    normImg(hstackImgs([target_image[bi,0].data] +
                                       [other_set_images[bi,si].data.clone() for si in range(other_set_images.size(1))])),
                    hstackImgs([m[bi].view(nr,nc).clone()] +
                               [(other_sim_base[bi,:,si]/other_sim_norm).view(nr,nc) for si in range(other_set_images.size(1))])])
                                                  for bi in range(nb)])


                print('support', support_set_images.size())
                print('other', other_set_images.size())
                for bi in range(support_set_images.size(0)):
                    itop = normImg(hstackImgs([target_image[bi,0].data, target_image[bi,0].data * 0] +
                                              [other_set_images[bi,si].data.clone() for si in range(other_set_images.size(1))]))
                    iside = normImg(vstackImgs([support_set_images[bi, si].data.clone() for si in range(support_set_images.size(1))]))
                    imids = []

                    for si in range(support_set_images.size(1)):
                        selfsim = (self.sim(target_image_other, encoded_support[si]).max(2)[0] ).exp()  if self.use_sim else (-self.l2(target_image_other, encoded_support[si])).max(2)[0].exp()
                        other_sim_norm = other_sim_base.sum(2) + selfsim # [nb, ntgt]
                        m_ = selfsim/other_sim_norm # [nb, ntgt]
                        m = m_.data.clone().unsqueeze(2).unsqueeze(3) # [nb, ntgt, 1, 1]
                        imids.append(hstackImgs([m[bi].view(nr,nc).clone()] +
                                                [(other_sim_base[bi,:,si2]/other_sim_norm).view(nr,nc) for si2 in range(other_set_images.size(1))]))


                    osum = sum(imids)/len(imids)
                    osum = hstackImgs([target_image[bi,0].data*0, osum])

                    self.forVis['separation_mask_support'].append(vstackImgs([itop, hstackImgs([iside, vstackImgs(imids)]), osum]))


                if True: # SLIC target
                    # load the image and convert it to a floating point data type
                    allSegs = [50]
                    self.forVis['im_slic'] = []
                    self.forVis['im_rag2'] = []
                    for bi in range(target_image.size(0)):
                        image = normImg(target_image[bi, i].data.cpu()).permute(1,2,0).numpy()
                        # loop over the number of segments
                        islic = []
                        irag2 = []
                        for numSegments in allSegs:
                            # apply SLIC and extract (approximately) the supplied number
                            # of segments
                            segments = slic(image, n_segments = numSegments, compactness=10, sigma = 1)
                            # show the output of SLIC
                            islic.append(torch.from_numpy(mark_boundaries(image, segments, color=[0.1,0.1,0])))

                            for thresh in [0.05, 0.07, 0.075, 0.08, 0.125]:
                                edge_map = filters.sobel(color.rgb2gray(image))
                                adjGraph = graph.rag_boundary(segments, edge_map)
                                seg2 = merge_hierarchical(labels=segments,
                                                          rag=adjGraph,
                                                          thresh=thresh,
                                                          rag_copy=True,
                                                          in_place_merge=True,
                                                          merge_func=merge_boundary,
                                                          weight_func=weight_boundary
                                )

                                irag2.append(torch.from_numpy(mark_boundaries(image, seg2, color=[0.1,0.1,0])))

                        self.forVis['im_slic'].append(hstackImgs(islic))
                        self.forVis['im_rag2'].append(hstackImgs(irag2))


        return res





class MatchingNetworkPWSelfLossL2(MatchingNetworkPWSelfLoss):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfLossL2, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWL2()
        self.use_sim = False

class MatchingNetworkPWSelfLossVisMaskL2(MatchingNetworkPWSelfLossVisMask):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfLossVisMaskL2, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWL2()
        self.use_sim = False


class DistanceNetworkPWClassMean(DistanceNetworkPW):
    def __init__(self, subsample=False):
        super(DistanceNetworkPWClassMean, self).__init__()
        self.subsample = subsample
        self.sim = CosineSimilarities()

    def forward(self, support_set, target_image, support_set_labels_one_hot):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [ns, nb, ndim, nsup]
        :param target_image: The embedding of the target image, tensor of shape [nb, ndim, ntgt]
        :return: Softmax pdf. Tensor with cosine similarities of shape [nb, ns]
        """
        eps = 1e-10
        similarities = []
        for support_image in support_set:
            cosine_similarity = self.sim(target_image, support_image) #[nb, ntgt , nsup]
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[nb, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        maxmatch = similarities.max(3)[1] # [nb, ntgt, ns]
        maxmatch = maxmatch.permute(2,0,1)  #[ns, nb, ntgt]
        supSel = support_set.gather(3,maxmatch.unsqueeze(2).repeat(1,1,support_set.size(2),1)) # [ns, nb, ndim, ntgt]

        supSel = supSel.permute(1,3,2,0) # [nb, ntgt, ndim, ns]
        ss2 = support_set_labels_one_hot.narrow(2,0,builtins.cargs.classes_per_set).unsqueeze(1) # [nb, 1, ns, nred]

        ## split supSel according to one-hot encoding
        supSel = torch.matmul(supSel, ss2)/builtins.cargs.samples_per_class # [nb, ntgt, ndim, ncl]
        similarities = []
        ns = builtins.cargs.classes_per_set
        eps=1e-10
        for ci in range(ns):
            support_image = supSel.select(3,ci) # [nb, ntgt, ndim]
            sum_support = torch.sum(torch.pow(support_image, 2), 2) # [nb, ntgt]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt()
            va = target_image.permute(0,2,1).unsqueeze(2) # [nb, ntgt, 1, ndim]
            dot_product = torch.matmul(va, support_image.unsqueeze(3)).squeeze(3).squeeze(2) # [nb, ntgt]
            cosine_similarity = dot_product * sum_support # [nb, ntgt]
            similarities.append(cosine_similarity)

        similarities = torch.stack(similarities, dim=2) # [nb, ntgt, ncl]
        similarities = F.log_softmax(similarities, dim=2)

        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]


class MatchingNetworkPWSelfLossClassMean(MatchingNetworkPWSelfLoss):

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.supSamp)).select(1,0), requires_grad=False) # sample 20%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.tgtSamp)).select(1,0), requires_grad=False) # sample 10%

        accuracy = 0
        crossentropy_loss = 0

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])
        self_loss = 0
        gc.collect()
        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            ## get self similarity
            selfSimilarity = None
            if True:
                selfSimilarity = self.dn.sim(targetFV, targetFV) #[batch_size, ntgt , ntgt]

                nb, ntgt,_ = selfSimilarity.size()
                selfSimilarity = F.softmax(selfSimilarity, dim=2) #[batch_size, ntgt , ntgt]
                selfLabel = Variable(torch.arange(0,ntgt).long().cuda(), requires_grad=False)

                self_loss += F.cross_entropy(selfSimilarity.view(nb * ntgt, ntgt), selfLabel.repeat(nb))


            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV) # [nb, ns]
            ncl = builtins.cargs.classes_per_set
            ss2 = support_set_labels_one_hot.narrow(2,0,ncl) # [nb, ns, ncl]
            similarities = torch.matmul(similarities.unsqueeze(1), ss2).squeeze(1)/ncl # [nb, ncl]
            # produce predictions for target probabilities
            preds = F.softmax(similarities, dim=1)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
            crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long()) + 0.25 * self_loss

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = values.data == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode and False:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class MatchingNetworkPWSelfPartial(MatchingNetworkPWSelfLoss):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfPartial, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPW()
        self.dnSelf = self.dn
        self.g = StackFV(kwargs.get('image_size'))
        self.gSelf = StackFVPartial(kwargs.get('image_size'), 4 if kwargs.get('image_size') > 80 else 2)
        self.emb = Identity()

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.supSamp)).select(1,0), requires_grad=False) # sample 20%
        rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.cargs.tgtSamp)).select(1,0), requires_grad=False) # sample 10%

        accuracy = 0
        crossentropy_loss = 0

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])
        self_loss = 0

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV)

            ## get self similarity
            selfSimilarity = None
            if True:
                targetFVSelf = self.gSelf(target_enc[i])
                targetFVSelf = self.emb(targetFVSelf.view(nb, targetFVSelf.size(1), -1).index_select(2, rsampTgt))
                selfSimilarity = self.dn.sim(targetFVSelf, targetFVSelf) #[batch_size, ntgt , ntgt]

                nb, ntgt,_ = selfSimilarity.size()
                selfSimilarity = F.softmax(selfSimilarity, dim=2) #[batch_size, ntgt , ntgt]
                selfLabel = Variable(torch.arange(0,ntgt).long().cuda(), requires_grad=False)

                self_loss += F.cross_entropy(selfSimilarity.view(nb * ntgt, ntgt), selfLabel.repeat(nb))

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)

            accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
            crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long()) + 0.25 * self_loss

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}


class StackAndTransformLower(MyModule):
    def __init__(self, **kwargs):
        super(StackAndTransformLower, self).__init__()
        self.nLower = 3 + 32 + 3*64
        self.dimsize = 419
        self.TRBM = RBM([self.dimsize, 2*self.nLower, self.nLower])
        self.stack = StackFV(kwargs.get('image_size'))

    def forward(self, emb):
        emb = self.stack(emb)
        nb, ndim, nr, nc = emb.size()
        emb = emb.view(nb, ndim, nr*nc)
        emb= torch.cat([emb.narrow(1,self.nLower, self.dimsize-self.nLower),
                        self.TRBM(emb.permute(0,2,1)).permute(0,2,1)], dim=1)
        emb = emb.view(nb, ndim, nr, nc)
        return emb


class MatchingNetworkPWSelfLowerTransform(MatchingNetworkPWSelfPartial):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWSelfLowerTransform, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPW()
        self.dnSelf = self.dn

        self.g = StackAndTransformLower(image_size=kwargs.get('image_size'))
        self.gSelf = self.g
        self.emb = Identity()
