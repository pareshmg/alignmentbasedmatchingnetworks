class StructurePos(MyModule):
    def __init__(self, np, w, stride=2):
        self.np = np
        self.w = w
        self.stride = stride
        self.weight = Parameter(torch.rand(np,1, w * w))

    def rebase(self):
        self.weight.data -= self.weight.data.max(2,keepdim=True)[0]

    def forward(self, h):
        """
        h :: [nb, nch, nr, nc]
        """
        nb, nch, nr, nc = h.size()
        h = h.view(nb*nch, 1, nr, nc) # [nb*nch, 1, nr, nc]

        w = F.softmax(self.weight, dim=2).view(self.np, 1, self.w, self.w)

        # do conv
        res = F.conv2d(h, weight=w, padding=self.w//2, stride=self.stride) # [nb*nch, np, nr, nc]

        return res.view(nb, nch, self.np, nr//self.stride, nc//self.stride) # [nb*nch, np, nr', nc']




class StructurePredictor(MyModule):
    def __init__(self, kw, stride):
        self.np = np
        self.ng = ng
        self.kw = kw
        self.stride = stride
        self.emb = Classifier(stride=stride)

    def forward(self, h, w):
        """
        h :: [nb, nk1, nr, nc]
        w :: [nk2, nk1, kw, kw]
        """
        nb, nk1, nr, nc = h.size()
        hemb = self.emb(h.view(nb*nk1, 1, nr, nc)) # [nb*nk1, ndim1, mr, mc]
        _,ndim1, mr, mc = hemb.size()
        hemb = hemb.view(nb, 1, nk1, ndim, mr, mc) # [nb, 1, nk1, ndim1, mr, mc]
        nk2,_,kwr, kwc = w.size()
        w = w.view(1,nk2, nk1, kwr * kwc, 1, 1)

        phi = torch.cat([hemb,
                         w], dim=3) # [nb, nk2, nk1, ndim, mr,mc]

        pdist # [nb, nk2, nk1, kw * kw, mr, mc]
        blocks # [nb, nk1, kw*kw, mr, mc]

        eh # [nb, nk2, nk1, mr, mc]


        resConv # [nb, nk2, mr, mc]




class StructureConv(MyModule):
    def __init__(self, nk1, nk2, w):
        self.nk1 = nk1
        self.nk2 = nk2
        self.w = w
        self.weight = Paramter(torch.rand(nk1 * w * w, nk2))
        self.bias = Paramter(torch.zeros(nk2))
        self.emb = Classifier()

    def forward(self, h):
        """
        hg :: [nb, k1, nr, nc]
        """


        hg = hg.permute(0,1,2,3,2) # [nb, ng, nr, nc, nk1*w*w]
        res = torch.mm(hg, self.weight.view(1,1,1,*self.weight.size())) # [nb, ng, nr, nc, nk2]
        res = res.max(1)[0] # [nb, nr, nc, nk2]
        res = res + self.bias.view(1,1,1,self.nk2) # [nb, nr, nc, nk2]
        res = res.permute(0,3,1,2) # [nb, nk2, nr, nc]
        return res # [nb, nk2, nr, nc]
