import torch
import torch.cuda
from torch.autograd import Variable, Function



class NbrNormV0(Function):
    def __init__(self, dii, djj, nbrhood):
        """
        : nbrhood [ntgt, nnbr]
        : dii [ntgt, nnbr]
        : djj [nsup, nsup]
        """
        super(NbrNormV0, self).__init__()
        self.dii = dii
        self.djj = djj
        self.nbrhood = nbrhood
        self.means = None

    def forward(self, similarities):
        """ similarities are not in log domain
        """
        dii = self.dii
        djj = self.djj
        nbrhood = self.nbrhood
        ntgt, nnbr = nbrhood.size()
        nsup = djj.size(0)
        nbrSimilarity = []
        means = []
        for ti in range(ntgt):
            pmij = similarities.index_select(1, nbrhood[ti]) #[nb, nnbr, ns, nsup]
            edij = torch.matmul(pmij, djj.view(1,1,nsup, nsup)) #[nb, nnbr, ns, nsup]
            edijmean = edij.mean(1) #[nb, ns, nsup]
            means.append(edijmean)
            edij = edij.div(edijmean.unsqueeze(1))

            res = -(edij - dii[ti].view(1,nnbr,1,1)).pow(2).sum(1)  #[nb, ns, nsup]

            nbrSimilarity.append(res)
        nbrSimilarity = torch.stack(nbrSimilarity, dim=1) # [nb, ntgt, ns, nsup]
        means = torch.stack(means, dim=1) #[nb, ntgt, ns, nsup]
        self.save_for_backward(similarities, nbrSimilarity)
        self.means = means
        return nbrSimilarity

    def backward(self, gout):
        """
        gout [nb, ntgt, ns, nsup]
        """
        means = self.means
        dii = self.dii
        djj = self.djj
        nbrhood = self.nbrhood
        similarities, nbrSimilarity = self.saved_variables
        res = gout * 0
        nb, ntgt, ns, nsup = gout.size()
        nnbr = nbrhood.size(1)
        for ti in range(ntgt):
            pmij = similarities.data.index_select(1, nbrhood[ti]) #[nb, nnbr, ns, nsup]
            edij = torch.matmul(pmij, djj.view(1,1,nsup, nsup)) #[nb, nnbr, ns, nsup]
            edij = edij.div(means.select(1,ti).unsqueeze(1))

            g = gout.select(1,ti) # [nb, ns, nsup]
            g = g.unsqueeze(1).repeat(1,nnbr,1,1) # [nb, nnbr, ns, nsup]
            g *=  -(2)*(edij - dii[ti].view(1,nnbr,1,1))  # [nb, nnbr, ns, nsup]
            g /= means.narrow(1,ti,1) # [nb, nnbr, ns, nsup]
            g = torch.matmul(g, self.djj)  # g * djj^T but djj is symmetric [nb, nnbr, ns, nsup]

            nbh = nbrhood[ti].view(1,nnbr, 1, 1).repeat(nb, 1, ns, nsup)
            res.scatter_add_(1,nbh,  g)
        return res


class NbrNormV2(Function):
    def __init__(self, dii, djj, nbrhood, sigma2 = 4*4):
        """
        : nbrhood [ntgt, nnbr]
        : dii [ntgt, nnbr]
        : djj [nsup, nsup]
        """
        from tu.functions.ccl import DLSEFunction
        super(NbrNormV2, self).__init__()
        self.dii = dii
        self.djj = djj
        self.nbrhood = nbrhood
        self.dlse = DLSEFunction(sigma2)

    def forward(self, similarities):
        """ similarities are not in log domain
        """
        dii = self.dii
        djj = self.djj
        nbrhood = self.nbrhood
        ntgt, nnbr = nbrhood.size()
        nsup = djj.size(0)
        nbrSimilarity = []
        for ti in range(ntgt):
            si_j_ = similarities.index_select(1, nbrhood[ti]) #[nb, nnbr i_, ns, nsup j_]
            djj_ = djj #[nsup j, nsup j_]
            di_ = dii[ti] # [nnbr i_]
            out = self.dlse.forward(si_j_, di_, djj_) # [nb, nnbr i_, ns, nsup j]
            res = out.mean(1) #[nb, ns,  nsup j]
            nbrSimilarity.append(res)
        nbrSimilarity = torch.stack(nbrSimilarity, dim=1) # [nb, ntgt, ns, nsup]

        self.save_for_backward(similarities)
        return nbrSimilarity

    def backward(self, gout):
        """
        gout [nb, ntgt, ns, nsup]
        """
        dii = self.dii
        djj = self.djj
        nbrhood = self.nbrhood
        similarities = self.saved_variables[0]
        res = gout * 0
        nb, ntgt, ns, nsup = gout.size()
        nnbr = nbrhood.size(1)
        for ti in range(ntgt):
            si_j_ = similarities.data.index_select(1, nbrhood[ti]) #[nb, nnbr i_, ns, nsup j_]
            djj_ = djj #[nsup j, nsup j_]
            di_ = dii[ti] # [nnbr i_]

            g0 = gout.select(1,ti)
            g = self.dlse.backward(g0, si_j_, di_, djj_)
            nbh = nbrhood[ti].view(1,nnbr, 1, 1).repeat(nb, 1, ns, nsup)
            res.scatter_add_(1,nbh,  g)
        return res / nnbr



class LSE(Function):
    def __init__(self, dim):
        super(LSE, self).__init__()
        self.dim = dim

    def forward(self, s):
        m, _ = torch.max(s, dim=self.dim, keepdim=True)
        s0 = s - m
        m = m.squeeze(self.dim)
        return m + torch.log(torch.sum(torch.exp(s0), dim=self.dim))

    def backward(self, go, s):
        """
        go : #[nb, nnbr i_, ns,  nsup j]
        s : #[nb, nnbr i_, ns, nsup j, nsup j_]
        """
        m, _ = torch.max(s, dim=self.dim, keepdim=True)
        s0 = s - m
        t0 = m + torch.log(torch.sum(torch.exp(s0), dim=self.dim, keepdim = True))
        return torch.exp(s - t0) * go.unsqueeze(4)
