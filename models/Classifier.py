import torch
from torch.nn import Parameter
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
import unittest
import numpy
import numpy as np
import math
import builtins
from models.Base import *
import torch.nn.functional as F




def convLayer(in_planes, out_planes, useDropout = False):
    "3x3 convolution with padding"
    seq = nn.Sequential(
        nn.Conv2d(in_planes, out_planes, kernel_size=3,
                  stride=1, padding=1, bias=True),
        nn.BatchNorm2d(out_planes),
        #nn.MaxPool2d(kernel_size=2, stride=2)
    )
    if useDropout: # Add dropout module
        list_seq = list(seq.modules())[1:]
        list_seq.append(nn.Dropout(0.1))
        seq = nn.Sequential(*list_seq)

    return seq


class Transpose(MyModule):
    def __init__(self, d1, d2):
        super(Transpose, self).__init__()
        self.d1 = d1
        self.d2 = d2

    def forward(self, inp):
        return torch.transpose(inp, self.d1, self.d2)


class UpFitExp(MyModule):
    """
    add 1 so that it becomes divisible by 2 so max pool upsample does the right thing
    [batch , chan, row, col]
    """
    def __init__(self, in_w, out_w):
        super(UpFit, self).__init__()
        self.w = out_w
        self.in_w = in_w
        self.upsample = nn.Upsample((math.ceil(out_w/in_w)*in_w,math.ceil(out_w/in_w)*in_w), mode='nearest')


    def forward(self, inp):
        res = self.upsample(inp)
        if res.size(2) != self.w: res = res.narrow(2,0,self.w)
        if res.size(3) != self.w: res = res.narrow(3,0,self.w)
        return res

class UpFitCopy(MyModule):
    """
    add 1 so that it becomes divisible by 2 so max pool upsample does the right thing
    [batch , chan, row, col]
    """
    def __init__(self, in_w, out_w):
        super(UpFitCopy, self).__init__()
        self.w = out_w
        self.scale = out_w // in_w
        self.upsample = nn.Upsample(scale_factor=self.scale)


    def forward(self, inp):
        res = self.upsample(inp)
        if res.size(2) < self.w:
            res = torch.cat([res, res.narrow(2,res.size(2)-1,1).repeat(1,1,self.w-res.size(2),1)], dim=2)
        if res.size(3) < self.w:
            res = torch.cat([res, res.narrow(3,res.size(3)-1,1).repeat(1,1,1,self.w-res.size(3))], dim=3)
        return res

UpFit = UpFitCopy

class Classifier(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28, relu_last=True):
        super(Classifier, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.relu_last = relu_last
        self.l1 = convLayer(num_channels, layer_size//2, useDropout)
        self.l2 = convLayer(layer_size//2, layer_size, useDropout) # layer 2
        self.l3 = convLayer(layer_size, layer_size, useDropout) # layer 3
        self.l4 = convLayer(layer_size, layer_size, useDropout) # layer 4
        self.l5 = None
        self.l6 = None
        if image_size >= 30:
            self.l5 = convLayer(layer_size, layer_size, useDropout) # layer 5
        if image_size >= 80:
            self.l6 = convLayer(layer_size, layer_size, useDropout) # layer 6

        self.useClassification = False

        self.weights_init(self.l1)
        self.weights_init(self.l2)
        self.weights_init(self.l3)
        self.weights_init(self.l4)
        self.weights_init(self.l5)
        self.weights_init(self.l6)


    def weights_init(self,module):
        if module is None: return
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform_(m.weight, gain=np.sqrt(2))
                init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 28, 28, 1]
        :return: Embeddings of size [batch_size, 64]
        """
        xs = [image_input]
        x = image_input
        layers = [self.l1, self.l2, self.l3, self.l4, self.l5, self.l6]
        for li,l in enumerate(layers):
            if l is None: continue
            x = l(x)
            xs.append(x)
            if self.relu_last or (li < len(layers)-1): x = F.relu(x)
            x = nn.MaxPool2d(kernel_size=2,stride=2)(x)
        xs.append(x)
        return xs





class StackFV(MyModule):
    def __init__(self, image_size = 28, nstack=100):
        super(StackFV, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.up1 = Identity() #UpFit(image_size, image_size)
        self.up2 = UpFit(image_size//2, image_size)
        self.up3 = UpFit(image_size//2//2, image_size)
        self.up4 = UpFit(image_size//2//2//2, image_size)
        self.up5 = None
        self.up6 = None
        self.up7 = None
        if image_size >= 30:
            self.up5 = UpFit(image_size//2//2//2//2, image_size)
        if image_size >= 80:
            self.up6 = UpFit(image_size//2//2//2//2//2, image_size)
            self.up7 = UpFit(image_size//2//2//2//2//2//2, image_size)

        #self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')
        self.nstack = nstack

    def forward(self, fstack):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        #pxe = [m(x) for (m,x) in zip([Identity(), self.up1, self.up2, self.up3, self.up4, self.up4, self.up5, self.up6], fstack)]
        ups = [self.up1, self.up1, self.up2, self.up3, self.up4, self.up5, self.up6, self.up7]
        if len(ups) > self.nstack:
            ups = ups[-self.nstack:]
            fstack = fstack[-self.nstack:]
        pxe = [m(x) for (m,x) in zip(ups, fstack) if m is not None]
        return torch.cat(pxe, 1)





class StackFVPartial(MyModule):
    def __init__(self, image_size = 28, nstack=4):
        super(StackFVPartial, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.up1 = Identity() #UpFit(image_size, image_size)
        self.up2 = UpFit(image_size//2, image_size)
        self.up3 = UpFit(image_size//2//2, image_size)
        self.up4 = UpFit(image_size//2//2//2, image_size)
        self.up5 = None
        self.up6 = None
        self.up7 = None
        if image_size >= 30:
            self.up5 = UpFit(image_size//2//2//2//2, image_size)
        if image_size >= 80:
            self.up6 = UpFit(image_size//2//2//2//2//2, image_size)
            self.up7 = UpFit(image_size//2//2//2//2//2//2, image_size)

        #self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')
        self.nstack = nstack

    def forward(self, fstack):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        #pxe = [m(x) for (m,x) in zip([Identity(), self.up1, self.up2, self.up3, self.up4, self.up4, self.up5, self.up6], fstack)]
        ups = [self.up1, self.up1, self.up2, self.up3, self.up4, self.up5, self.up6, self.up7]
        pxe = [m(x) for (m,x) in zip(ups, fstack) if m is not None]
        pxe = pxe[-self.nstack:]
        return torch.cat(pxe, 1)








class StackFVPartialOld(MyModule):
    def __init__(self, image_size = 28, nstack=100):
        super(StackFVPartial, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.up1 = Identity() #UpFit(image_size, image_size)
        self.up2 = UpFit(image_size//2//2//2//2//2, image_size//2//2//2//2)
        self.up3 = UpFit(image_size//2//2//2//2//2//2, image_size//2//2//2//2)

        #self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')


    def forward(self, fstack):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        #pxe = [m(x) for (m,x) in zip([Identity(), self.up1, self.up2, self.up3, self.up4, self.up4, self.up5, self.up6], fstack)]
        ups = [self.up1, self.up2, self.up3]
        pxe = [m(x) for (m,x) in zip(ups, fstack) if m is not None]
        return torch.cat(pxe, 1)


class StackFVNotLast(StackFV):
    def forward(self, fstack):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        pxe = [m(x) for (m,x) in zip([Identity(), self.up1, self.up2, self.up3, self.up4, self.up5, self.up6], fstack[:-1])]
        return torch.cat(pxe, 1)



class EncoderPhi1(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(EncoderPhi1, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """

        def getPhi(nch, lsz, isz, nch2=None):
            if nch2 is None: nch2 = nch
            if lsz == isz:
                return nn.Sequential(
                    Transpose(1,3),
                    nn.Linear(nch, nch2),
                    nn.ReLU(True),
                    Transpose(3,1)
                )
            return nn.Sequential(
                Transpose(1,3),
                nn.Linear(nch, nch2),
                nn.ReLU(True),
                Transpose(3,1),
                UpFit(lsz, isz)
            )

        self.layer1 = convLayer(num_channels, layer_size//2, useDropout)
        self.layer2 = convLayer(layer_size//2, layer_size, useDropout)
        self.layer3 = convLayer(layer_size, layer_size, useDropout)
        self.layer4 = convLayer(layer_size, layer_size, useDropout)
        self.layer5 = None
        self.layer6 = None
        if image_size >= 30:
            self.layer5 = convLayer(layer_size, layer_size, useDropout) # layer 5
        if image_size >= 80:
            self.layer6 =convLayer(layer_size, layer_size, useDropout) # layer 5

        finalSize = image_size // (2 * 2 * 2 * 2)
        self.outSize = finalSize * finalSize * layer_size

        self.weights_init(self.layer1)
        self.weights_init(self.layer2)
        self.weights_init(self.layer3)
        self.weights_init(self.layer4)
        if self.layer5 is not None: self.weights_init(self.layer5)
        if self.layer6 is not None: self.weights_init(self.layer6)

        self.up0 = getPhi(num_channels, image_size,image_size, 1)
        self.up1 = getPhi(layer_size//2,image_size//2, image_size)
        self.up2 = getPhi(layer_size, image_size//2//2, image_size)
        self.up3 = getPhi(layer_size, image_size//2//2//2, image_size)
        self.up4 = getPhi(layer_size, image_size//2//2//2//2, image_size)
        if self.layer5 is not None:
            self.up5 = getPhi(layer_size, image_size//2//2//2//2//2, image_size)
        if self.layer6 is not None:
            self.up6 = getPhi(layer_size, image_size//2//2//2//2//2//2, image_size)

        #self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')

    def weights_init(self,module):
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform_(m.weight, gain=numpy.sqrt(2))
                init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def weights_freeze(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.BatchNorm2d):
                for param in m.parameters():
                    param.requires_grad = False


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        pxe = [self.up0(image_input)]
        x = self.layer1(image_input)
        pxe.append(self.up1(x))
        x = self.layer2(x)
        pxe.append(self.up2(x))
        x = self.layer3(x)
        pxe.append(self.up3(x))
        x = self.layer4(x)
        pxe.append(self.up4(x))
        if self.layer5 is not None:
            x = self.layer5(x)
            pxe.append(self.up5(x))
        if self.layer6 is not None:
            x = self.layer6(x)
            pxe.append(self.up6(x))
        x = x.view(x.size(0), -1)
        return x, torch.cat(pxe, 1).view(image_input.size(0), -1, image_input.size(2) * image_input.size(3))


class EncoderPhi1NotLast(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(EncoderPhi1NotLast, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """

        def getPhi(nch, lsz, isz, nch2=None):
            if nch2 is None: nch2 = nch
            if lsz == isz:
                return nn.Sequential(
                    Transpose(1,3),
                    nn.Linear(nch, nch2),
                    nn.ReLU(True),
                    Transpose(3,1)
                )
            return nn.Sequential(
                Transpose(1,3),
                nn.Linear(nch, nch2),
                nn.ReLU(True),
                Transpose(3,1),
                UpFit(lsz, isz)
            )

        self.layer1 = convLayer(num_channels, layer_size//2, useDropout)
        self.layer2 = convLayer(layer_size//2, layer_size, useDropout)
        self.layer3 = convLayer(layer_size, layer_size, useDropout)
        self.layer4 = convLayer(layer_size, layer_size, useDropout)
        self.layer5 = None
        self.layer6 = None
        if image_size >= 30:
            self.layer5 = convLayer(layer_size, layer_size, useDropout) # layer 5
        if image_size >= 80:
            self.layer6 =convLayer(layer_size, layer_size, useDropout) # layer 5

        finalSize = image_size // (2 * 2 * 2 * 2)
        self.outSize = finalSize * finalSize * layer_size

        self.weights_init(self.layer1)
        self.weights_init(self.layer2)
        self.weights_init(self.layer3)
        self.weights_init(self.layer4)
        if self.layer5 is not None: self.weights_init(self.layer5)
        if self.layer6 is not None: self.weights_init(self.layer6)

        self.up0 = getPhi(num_channels, image_size,image_size, 1)
        self.up1 = getPhi(layer_size//2,image_size//2, image_size)
        self.up2 = getPhi(layer_size, image_size//2//2, image_size)
        self.up3 = getPhi(layer_size, image_size//2//2//2, image_size)
        self.up4 = getPhi(layer_size, image_size//2//2//2//2, image_size)
        if self.layer5 is not None:
            self.up5 = getPhi(layer_size, image_size//2//2//2//2//2, image_size)
        if self.layer6 is not None:
            self.up6 = getPhi(layer_size, image_size//2//2//2//2//2//2, image_size)

        #self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')

    def weights_init(self,module):
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform_(m.weight, gain=numpy.sqrt(2))
                init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def weights_freeze(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.BatchNorm2d):
                for param in m.parameters():
                    param.requires_grad = False


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        pxe = [self.up0(image_input)]
        x = self.layer1(image_input)
        pxe.append(self.up1(x))
        x = self.layer2(x)
        pxe.append(self.up2(x))
        x = self.layer3(x)
        pxe.append(self.up3(x))
        x = self.layer4(x)
        pxe.append(self.up4(x))
        if self.layer5 is not None:
            x = self.layer5(x)
            pxe.append(self.up5(x))
        if self.layer6 is not None:
            x = self.layer6(x)
            pxe.append(self.up6(x))
        x = x.view(x.size(0), -1)
        pxe.pop()
        return x, torch.cat(pxe, 1).view(image_input.size(0), -1, image_input.size(2) * image_input.size(3))


# class Encoder6(MyModule):
#     def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
#         super(Encoder6, self).__init__()

#         """
#         Builds a CNN to produce embeddings
#         :param layer_sizes: A list of length 4 containing the layer sizes
#         :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
#         :param num_channels: Number of channels of images
#         :param useDroput: use Dropout with p=0.1 in each Conv block
#         """
#         self.layer1 = convLayer(num_channels, layer_size, useDropout)
#         self.layer2 = convLayer(layer_size, layer_size, useDropout)
#         self.layer3 = convLayer(layer_size, layer_size, useDropout)
#         self.layer4 = convLayer(layer_size, layer_size, useDropout)
#         self.layer5 = convLayer(layer_size, layer_size, useDropout)
#         self.layer6 = convLayer(layer_size, layer_size, useDropout)

#         finalSize = int(math.floor(image_size / (2 * 2 * 2 * 2)))
#         self.outSize = finalSize * finalSize * layer_size
#         if nClasses>0: # We want a linear
#             self.useClassification = True
#             self.layer7 = nn.Linear(self.outSize,nClasses)
#             self.outSize = nClasses
#         else:
#             self.useClassification = False

#         self.weights_init(self.layer1)
#         self.weights_init(self.layer2)
#         self.weights_init(self.layer3)
#         self.weights_init(self.layer4)
#         self.weights_init(self.layer5)
#         self.weights_init(self.layer6)
#         self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')

#     def weights_init(self,module):
#         for m in module.modules():
#             if isinstance(m, nn.Conv2d):
#                 init.xavier_uniform_(m.weight, gain=numpy.sqrt(2))
#                 init.constant_(m.bias, 0)
#             elif isinstance(m, nn.BatchNorm2d):
#                 m.weight.data.fill_(1)
#                 m.bias.data.zero_()


#     def forward(self, image_input):
#         """
#         Runs the CNN producing the embeddings and the gradients.
#         :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
#         :return: Embeddings of size [batch_size, 64]
#         """
#         pxe = [image_input]
#         x = self.layer1(image_input)
#         pxe.append(self.upsample(x))
#         x = self.layer2(x)
#         pxe.append(self.upsample(x))
#         x = self.layer3(x)
#         pxe.append(self.upsample(x))
#         x = self.layer4(x)
#         pxe.append(self.upsample(x))
#         x = self.layer5(x)
#         pxe.append(self.upsample(x))
#         x = self.layer6(x)
#         pxe.append(self.upsample(x))
#         x = x.view(x.size(0), -1)
#         if self.useClassification:
#             x = self.layer7(x)
#         return x, torch.cat(pxe, 1).view(image_input.size(0), -1, image_input.size(2) * image_input.size(3))



class EncoderAttn(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(Encoder, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.layer1 = convLayer(num_channels, layer_size, useDropout)
        self.layer2 = convLayer(layer_size, layer_size, useDropout)
        self.layer3 = convLayer(layer_size, layer_size, useDropout)
        self.layer4 = convLayer(layer_size, layer_size, useDropout)

        finalSize = int(math.floor(image_size / (2 * 2 * 2 * 2)))
        self.outSize = finalSize * finalSize * layer_size
        if nClasses>0: # We want a linear
            self.useClassification = True
            self.layer5 = nn.Linear(self.outSize,nClasses)
            self.outSize = nClasses
        else:
            self.useClassification = False

        self.weights_init(self.layer1)
        self.weights_init(self.layer2)
        self.weights_init(self.layer3)
        self.weights_init(self.layer4)
        self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')

        # def attnConv(in_planes, out_planes):
        #     seq = nn.Sequential(
        #         nn.Conv2d(in_planes, out_planes, kernel_size=3,
        #                   stride=1, padding=1, bias=True),
        #         nn.BatchNorm2d(out_planes),
        #         nn.ReLU(True),
        #         ## FIXME : here


        self.attnl1 = convLayer(num_channels + 4*layer_size, layer_size, useDropout)


    def weights_init(self,module):
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform_(m.weight, gain=numpy.sqrt(2))
                init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        pxe = [image_input]
        x = self.layer1(image_input)
        pxe.append(self.upsample(x))
        x = self.layer2(x)
        pxe.append(self.upsample(x))
        x = self.layer3(x)
        pxe.append(self.upsample(x))
        x = self.layer4(x)
        pxe.append(self.upsample(x))
        x = x.view(x.size(0), -1)
        if self.useClassification:
            x = self.layer5(x)
        return x, torch.cat(pxe, 1).view(image_input.size(0), -1, image_input.size(2) * image_input.size(3))



class Encoder2(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(Encoder2, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.layer1 = convLayer(num_channels, layer_size, useDropout)
        self.layer2 = convLayer(layer_size, layer_size, useDropout)
        self.layer3 = convLayer(layer_size, layer_size, useDropout)
        self.layer4 = convLayer(layer_size, layer_size, useDropout)

        finalSize = int(math.floor(image_size / (2 * 2 * 2 * 2)))
        self.outSize = finalSize * finalSize * layer_size
        if nClasses>0: # We want a linear
            self.useClassification = True
            self.layer5 = nn.Linear(self.outSize,nClasses)
            self.outSize = nClasses
        else:
            self.useClassification = False


        self.enc2 = nn.Sequential(
            nn.Linear(num_channels + layer_size*4, 64),
            #nn.BatchNorm1d(128),
            nn.ReLU(True),
            nn.Linear(64, 64))


        self.weights_init(self.layer1)
        self.weights_init(self.layer2)
        self.weights_init(self.layer3)
        self.weights_init(self.layer4)
        self.upsample = nn.Upsample(size=(image_size, image_size), mode='nearest')

    def weights_init(self,module):
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform_(m.weight, gain=numpy.sqrt(2))
                init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 1, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        pxe = [image_input]
        x = self.layer1(image_input)
        pxe.append(self.upsample(x))
        x = self.layer2(x)
        pxe.append(self.upsample(x))
        x = self.layer3(x)
        pxe.append(self.upsample(x))
        x = self.layer4(x)
        pxe.append(self.upsample(x))
        x = x.view(x.size(0), -1)
        if self.useClassification:
            x = self.layer5(x)

        pxe = torch.cat(pxe, 1).view(image_input.size(0), -1, image_input.size(2) * image_input.size(3))
        pxe = pxe.permute(0,2,1) # batch * samples * dim
        pxe = self.enc2(pxe)
        pxe = pxe.permute(0,2,1) # batch * dim * samples
        return x, pxe



class MaskedClassifier(Classifier):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(MaskedClassifier, self).__init__(layer_size, nClasses , num_channels, useDropout, image_size )
        """
        embed, produce mask, dot product with original input, re-embed
        """
        ## re-embed last 3 layers

        #self.mask = StackEmbed([ndim, 128, num_channels])
        self.mask = RBM([layer_size*3, 128, 1])

        self.up6 = UpFit(image_size//2//2//2//2//2//2, image_size//2//2//2//2)
        self.up5 = UpFit(image_size//2//2//2//2//2, image_size//2//2//2//2)
        self.up4 = Identity()
        self.mp = nn.MaxPool2d(kernel_size=2,stride=2)

    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, nch, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        xs = [image_input]
        x = image_input
        for l in [self.l1, self.l2, self.l3, self.l4, self.l5, self.l6]:
            if l is None: continue
            x = l(x)
            xs.append(x)
            x = F.relu(x)
            x = self.mp(x)
        xs.append(x)
        xs = xs[-3:]

        xs[1] = self.up5(xs[1])
        xs[2] = self.up6(xs[2])

        m = torch.cat(xs, dim=1) # [nb, ndim, nr, nc]
        m = F.sigmoid(self.mask(m.permute(0,2,3,1))) # [nb, nr, nc, 1]
        m = m.squeeze(3).unsqueeze(1)
        if self.visMode:
            self.forVis = {}
            self.forVis['mask'] = m.data.clone()
        xs = xs[0] * m

        xs = self.mp(F.relu(self.l5(xs)))
        xs = self.mp(F.relu(self.l6(xs)))

        return [xs]



class MaskedEmbed(Classifier):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(MaskedEmbed, self).__init__(layer_size, nClasses , num_channels, useDropout, image_size )
        """
        embed, produce mask, dot product with original input, re-embed
        """
        ## re-embed last 3 layers

        #self.mask = StackEmbed([ndim, 128, num_channels])
        self.mask = RBM([layer_size*3 + layer_size, 128, 1])

        self.up6 = UpFit(image_size//2//2//2//2//2//2, image_size//2//2//2//2)
        self.up5 = UpFit(image_size//2//2//2//2//2, image_size//2//2//2//2)
        self.up4 = Identity()
        self.mp = nn.MaxPool2d(kernel_size=2,stride=2)

        self.l1_2 = convLayer(layer_size, layer_size, useDropout) # layer 4
        self.l2_2 = convLayer(layer_size, layer_size, useDropout) # layer 4


    def forward(self, target_image, support_set_images):
        """
        target: [nb, nt, nch, nr, nc]
        support_set_images: [nb, ns, nch, nr, nc]
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, nch, 28, 28]
        :return: Embeddings of size [batch_size, 64]
        """
        nb, ns, nch, nr, nc = support_set_images.size()
        nt = target_image.size(1)
        inp = torch.cat([target_image, support_set_images], dim=1).view(nb*(nt+ns), nch, nr, nc)
        xs = [inp]
        x = inp
        for l in [self.l1, self.l2, self.l3, self.l4, self.l5, self.l6]:
            if l is None: continue
            x = l(x)
            xs.append(x)
            x = F.relu(x)
            x = self.mp(x)
        xs.append(x)

        emb = x.view(nb, nt+ns, -1, 1, 1) # [nb, nt+ns, ndim, 1, 1]
        embt = emb.narrow(1, 0, nt).unsqueeze(2) # [nb, nt, 1, ndim, 1, 1]
        embs = emb.narrow(1,nt,ns).unsqueeze(1) # [nb, 1, ns, ndim, 1, 1]

        xs = xs[-3:]
        xs[1] = self.up5(xs[1])
        xs[2] = self.up6(xs[2])

        m = torch.cat(xs, dim=1) # [nb*(nt+ns), ndim, mr, mc]
        m = m.view(nb, nt+ns, m.size(1), m.size(2), m.size(3)) # [nb, nt+ns, ndim, mr, mc]
        mr = xs[0].size(2)
        mc = xs[1].size(3)
        mt = m.narrow(1,0,nt).unsqueeze(2) # [nb, nt, 1, ndim, mr, mc]
        ms = m.narrow(1,nt,ns).unsqueeze(1) # [nb, 1, ns, ndim, mr, mc]

        mt = torch.cat([mt.repeat(1,1,ns,1,1,1), embs.repeat(1, nt, 1, 1, mr, mc)], dim=3) # [nb, nt, ns, ndim, mr, mc]
        ms = torch.cat([ms.repeat(1,nt,1,1,1,1), embt.repeat(1, 1, ns, 1, mr, mc)], dim=3) # [nb, nt, ns, ndim, mr, mc]

        mt = F.softmax(self.mask(mt.view(nb * nt * ns, -1, mr * mc).permute(0, 2,1)), dim=1).view(nb*nt*ns,1,mr,mc) # [nb *nt * ns, 1,  mr, mc]
        ms = F.softmax(self.mask(ms.view(nb * nt * ns, -1, mr * mc).permute(0, 2,1)), dim=1).view(nb*nt*ns,1,mr,mc) # [nb *nt * ns, 1,  mr, mc]


        xs = xs[0].view(nb, nt+ns, -1, mr, mc)
        xt = xs.narrow(1,0,nt).unsqueeze(2).repeat(1,1,ns,1,1,1).view(nb*nt*ns, -1, mr, mc) # [nb*nt*ns, ndim', mr, mc]
        xs = xs.narrow(1,nt,ns).unsqueeze(1).repeat(1,nt,1,1,1,1).view(nb*nt*ns, -1, mr, mc) # [nb*nt*ns, ndim', mr, mc]

        xt = xt * mt
        xs = xs * ms

        xt = self.mp(F.relu(self.l1_2(xt)))
        xt = self.mp(F.relu(self.l2_2(xt)))
        xt = xt.view(nb, nt, ns, -1)

        xs = self.mp(F.relu(self.l1_2(xs)))
        xs = self.mp(F.relu(self.l2_2(xs)))
        xs = xs.view(nb, nt, ns, -1)
        eps = 1e-10
        similarities = []
        for ti in range(nt):
            sims = []
            for si in range(ns):
                sup = xs[:,ti,si,:] # [nb, ndim]
                tgt = xt[:,ti,si,:] # [nb, ndim]
                sum_support = torch.sum(torch.pow(sup, 2),1) # [nb]
                support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [nb]

                dot_product = tgt.unsqueeze(1).bmm(sup.unsqueeze(2)).squeeze(2).squeeze(1) # [nb]
                cosine_similarity = dot_product * support_magnitude
                sims.append(cosine_similarity)
            similarities.append(torch.stack(sims, dim=1)) # [nb, ns]

        similarities = torch.stack(similarities, dim=1) # [nb, nt, ns]
        return similarities






class ClassifierTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_forward(self):
        pass

if __name__ == '__main__':
    unittest.main()
