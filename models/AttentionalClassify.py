##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import builtins
from models.Base import *
import torch.nn as nn
import unittest
import torch.nn.functional as F

class AttentionalClassify(nn.Module):
    def __init__(self):
        super(AttentionalClassify, self).__init__()

    def forward(self, similarities, support_set_y):

        """
        Produces pdfs over the support set classes for the target set image.
        :param similarities: A tensor with cosine similarities of size [sequence_length, batch_size]
        :param support_set_y: A tensor with the one hot vectors of the targets for each support set image
                                                                            [sequence_length,  batch_size, num_classes]
        :return: Softmax pdf
        """
        preds = similarities.unsqueeze(1).bmm(support_set_y).squeeze(1)
        return preds

class AttentionalClassifyOld(nn.Module):
    def __init__(self):
        super(AttentionalClassify, self).__init__()

    def forward(self, similarities, support_set_y):

        """
        Produces pdfs over the support set classes for the target set image.
        :param similarities: A tensor with cosine similarities of size [sequence_length, batch_size]
        :param support_set_y: A tensor with the one hot vectors of the targets for each support set image
                                                                            [sequence_length,  batch_size, num_classes]
        :return: Softmax pdf
        """
        softmax = nn.Softmax(dim=1)
        softmax_similarities = softmax(similarities)
        preds = softmax_similarities.unsqueeze(1).bmm(support_set_y).squeeze(1)
        return preds


class OneHotClassify(nn.Module):
    def __init__(self):
        super(OneHotClassify, self).__init__()

    def forward(self, similarities, support_set_y):

        """
        Produces pdfs over the support set classes for the target set image.
        :param similarities: A tensor with cosine similarities of size [sequence_length, batch_size]
        :param support_set_y: A tensor with the one hot vectors of the targets for each support set image
                                                                            [sequence_length,  batch_size, num_classes]
        :return: Softmax pdf
        """
        preds = similarities.unsqueeze(1).bmm(support_set_y).squeeze(1)
        return preds


class OpenRelativeDistsAttentionalClassify(nn.Module):
    def __init__(self):
        super(OpenRelativeDistsAttentionalClassify, self).__init__()
        self.toP = RBM([builtins.args.classes_per_set-1, 32,64,1])
        self.softmax = nn.Softmax(dim=1)


    def forward(self, similarities, support_set_y):

        sortedSimilarities,indices = torch.sort(similarities, dim=1)
        p = F.sigmoid(self.toP(sortedSimilarities))

        softmax_similarities = self.softmax(similarities)
        softmax_similarities = torch.cat([softmax_similarities * (1-p), p], dim=1)
        preds = softmax_similarities.unsqueeze(1).bmm(support_set_y).squeeze(1)
        return preds


class OpenAttentionalClassify(nn.Module):
    def __init__(self):
        super(OpenAttentionalClassify, self).__init__()
        self.softmax = nn.Softmax(dim=1)


    def forward(self, similarities, support_set_y):
        similarities = torch.cat([similarities , similarities.narrow(1,0,1)*0 + 1], dim=1)

        softmax_similarities = self.softmax(similarities)
        preds = softmax_similarities.unsqueeze(1).bmm(support_set_y).squeeze(1)
        return preds



class OpenMomentAttentionalClassify(nn.Module):
    def __init__(self, nMoments, openSetConst=0):
        super(OpenMomentAttentionalClassify, self).__init__()
        self.toP = RBM([nMoments, 1])
        self.softmax = nn.Softmax(dim=1)
        self.openSetConst = openSetConst


    def forward(self, similarities, support_set_y):
        similarities = self.toP(similarities).squeeze(2) # [nb, ns]
        similarities = torch.cat([similarities, similarities.narrow(1,0,1)*0 + self.openSetConst], dim=1) # [nb, ns+1]

        softmax_similarities = self.softmax(similarities)
        preds = softmax_similarities.unsqueeze(1).bmm(support_set_y).squeeze(1)
        return preds

class OpenMomentAttentionalClassifyx21(OpenMomentAttentionalClassify):
    def __init__(self, nMoments, openSetConst=0):
        super(OpenMomentAttentionalClassifyx21, self).__init__(nMoments, openSetConst)
        self.toP = RBM([nMoments, 2,1])
class OpenMomentAttentionalClassifyx41(OpenMomentAttentionalClassify):
    def __init__(self, nMoments, openSetConst=0):
        super(OpenMomentAttentionalClassifyx41, self).__init__(nMoments, openSetConst)
        self.toP = RBM([nMoments, 4,1])

class OpenMomentAttentionalClassifyx81(OpenMomentAttentionalClassify):
    def __init__(self, nMoments, openSetConst=0):
        super(OpenMomentAttentionalClassifyx81, self).__init__(nMoments, openSetConst)
        self.toP = RBM([nMoments, 8,1])


class MomentAttentionalClassify(nn.Module):
    def __init__(self, nMoments):
        super(MomentAttentionalClassify, self).__init__()
        self.toP = RBM([nMoments, 32,64,1])
        self.softmax = nn.Softmax(dim=1)


    def forward(self, similarities, support_set_y):
        similarities = self.toP(similarities).squeeze(2) # [nb, ns]
        softmax_similarities = self.softmax(similarities)
        preds = softmax_similarities.unsqueeze(1).bmm(support_set_y).squeeze(1)
        return preds





class AttentionalClassifyTest(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_forward(self):
        pass

if __name__ == '__main__':

    unittest.main()
