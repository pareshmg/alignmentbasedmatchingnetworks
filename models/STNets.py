import torch
import torch.nn as nn

class GuessTNet(nn.Module):
    def __init__(self):
        super(GuessTNet, self).__init__()


    def forward(self, topTarget, topSupport):
        """
        topTarget :: [nbatch, ndim]
        topSupport :: [batch, nsupport, ndim]
        """
        topTarget = topTarget.unsqueeze(1).repeat(1,topSupport.size(1), 1)
        enc = torch.cat([topTarget, topSupport], dim=2) # nbach, nsupport, ndim

class GuessFGNet(nn.Module):
    def __init__(self):
        super(GuessFGNet, self).__init__()


    def forward(self, topTarget, topSupport):
        """
        topTarget :: [nbatch, ndim]
        topSupport :: [batch, nsupport, ndim]
        """
        topTarget = topTarget.unsqueeze(1).repeat(1,topSupport.size(1), 1)
        enc = torch.cat([topTarget, topSupport], dim=2) # nbach, nsupport, ndim
