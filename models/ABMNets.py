from models.MatchingNetwork import *


class MatchingNetworkPW(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPW, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPW()
        self.dnSelf = self.dn
        self.g = StackFV(kwargs.get('image_size'))
        self.emb = Identity()

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind, target_ind):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.args.supSamp)).select(1,0), requires_grad=False) # sample 20%
        #rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.args.tgtSamp)).select(1,0), requires_grad=False) # sample 10%
        #if rsampTgt.size(0) == 0:
        rsampTgt = Variable(torch.LongTensor([numpy.random.randint(0,nr*nc)]).cuda(), requires_grad=False) # sample 10%

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])


        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [batch_size, ndim, nr, nc]

            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV)
            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long())

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



# class MatchingNetworkPWHierarchicalNbrhood(MatchingNetworkPWSelfLoss):
#     def __init__(self, *args, **kwargs):
#         super(MatchingNetworkPWHierarchicalNbrhood, self).__init__(*args, **kwargs)
#         self.dn = DistanceNetworkPWHierarchical()
#         self.g = Identity()
#         self.emb = Identity()
#         self.selfLoss = SelfLossHierarchical()


#     def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc):
#         """
#         Builds graph for Matching Networks, produces losses and summary statistics.
#         :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
#         :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
#         :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
#         :param target_label: A tensor containing the target label [batch_size, 1]
#         :return:
#         """
#         nb, ns, nk, nr, nc = support_set_images.size()
#         accuracy = 0
#         crossentropy_loss = 0

#         if self.visMode:
#             self.forVis['support_set_images'] = support_set_images.data.clone()
#             self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
#             self.forVis['target_image'] = target_image.data.clone()
#             self.forVis['target_label'] = target_label.data.clone()

#         true_positive = 0
#         false_positive = 0
#         true_negative = 0
#         false_negative = 0

#         self_loss = 0

#         # produce embeddings for target images
#         for i in np.arange(target_image.size(1)):
#             # get similarity between support set embeddings and target
#             similarities = self.dn(support_set=support_enc, target_image=target_enc[i])

#             ## get self similarity
#             self_loss = self.selfLoss(target_enc[i])

#             # produce predictions for target probabilities
#             preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

#             # calculate accuracy and crossentropy loss
#             values, indices = preds.max(1)

#             accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
#             crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long()) + 0.25 * self_loss

#             nsup = support_set_labels_one_hot.size(1)
#             actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
#             declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
#             true_positive += (actual_open_set & declared_open_set).sum()
#             false_positive += ((~actual_open_set) & declared_open_set).sum()
#             true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
#             false_negative += (actual_open_set & (~declared_open_set)).sum()

#             if self.visMode:
#                 if 'similarities' not in self.forVis:
#                     self.forVis['similarities'] = []
#                     self.forVis['preds'] = []
#                     for k in self.dn.forVis:
#                         self.forVis[k] = []
#                 self.forVis['similarities'].append(similarities.data.clone())
#                 self.forVis['preds'].append(preds.data.clone())
#                 for k in self.dn.forVis:
#                     self.forVis[k].append(self.dn.forVis[k])

#         return {'acc' : accuracy/target_image.size(1),
#                 'loss': crossentropy_loss/target_image.size(1),
#                 'TP': true_positive,
#                 'FP': false_positive,
#                 'TN': true_negative,
#                 'FN': false_negative}



class MatchingNetworkPWAttn(MatchingNetworkPW):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPWAttn, self).__init__(*args, **kwargs)
        self.dn = DistanceNetworkPWAttn()
        self.attnLayer = -3

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind, target_ind):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        nb, ns, nk, nr, nc = support_set_images.size()
        # sample points
        rsampSup = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.args.supSamp)).select(1,0), requires_grad=False) # sample 20%
        #rsampTgt = Variable(torch.nonzero((torch.rand(nr*nc).cuda() <= builtins.args.tgtSamp)).select(1,0), requires_grad=False) # sample 10%
        #if rsampTgt.size(0) == 0:
        rsampTgt = Variable(torch.LongTensor([numpy.random.randint(0,nr*nc)]).cuda(), requires_grad=False) # sample 10%

        if self.visMode:
            rsampSup = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            rsampTgt = Variable(torch.arange(0,nr*nc).long().cuda(), requires_grad=False)
            self.forVis['rsampSup'] = rsampSup.data.clone()
            self.forVis['rsampTgt'] = rsampTgt.data.clone()
            self.forVis['support_set_images'] = support_set_images.data.clone()
            self.forVis['support_set_labels_one_hot'] = support_set_labels_one_hot.data.clone()
            self.forVis['target_image'] = target_image.data.clone()
            self.forVis['target_label'] = target_label.data.clone()


        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for support set images
        encoded_support = torch.stack([self.g(x) for x in support_enc]) # [sequence_size, batch_size, ndim, nr, nc]
        ndim = encoded_support.size(2)
        suppFV = encoded_support.view(ns,nb, ndim, nr*nc).index_select(3, rsampSup)
        suppFV = torch.stack([self.emb(x) for x in suppFV])


        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = self.g(target_enc[i]) # [nb, ndim, nr, nc]
            targetFV = encoded_target.view(nb, ndim, nr*nc).index_select(2, rsampTgt)
            targetFV = self.emb(targetFV)

            target_attn_input = targetFV.narrow(1, targetFV.size(1)-3*64, 64)

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=suppFV, target_image=targetFV, target_attn_input = target_attn_input)
            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss += F.cross_entropy(preds, target_label[:, i].long())

            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

            if self.visMode:
                if 'similarities' not in self.forVis:
                    self.forVis['similarities'] = []
                    self.forVis['preds'] = []
                    for k in self.dn.forVis:
                        self.forVis[k] = []
                self.forVis['similarities'].append(similarities.data.clone())
                self.forVis['preds'].append(preds.data.clone())
                for k in self.dn.forVis:
                    self.forVis[k].append(self.dn.forVis[k])

        return {'acc' : accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}
