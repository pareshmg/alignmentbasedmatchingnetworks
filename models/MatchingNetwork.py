import builtins
import torch
import torch.nn as nn
import unittest
import os
import numpy as np
from models.BidirectionalLSTM import BidirectionalLSTM
from models.Classifier import *
from models.DistanceNetwork import *
from models.AttentionalClassify import *
import torch.nn.functional as F
from torch.autograd import Variable, Function
from models.vgg import *
from models.Base import *
from models.Jigsaw import *
from models.ParamNet import *
from datasets.dset import *
import torchvision.transforms as transforms

class MatchingNetwork(MyModule):
    def __init__(self, keep_prob, \
                 batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
                 num_samples_per_class=1, nClasses = 0, image_size = 28):
        super(MatchingNetwork, self).__init__()

        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param is_training: Flag indicating whether we are training or evaluating
        :param rotate_flag: Flag indicating whether to rotate the images
        :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
        :param num_classes_per_set: Integer indicating the number of classes per set
        :param num_samples_per_class: Integer indicating the number of samples per class
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.batch_size = batch_size
        self.fce = fce
        if fce:
            self.lstm = BidirectionalLSTM(layer_sizes=[32], batch_size=self.batch_size, vector_dim = self.g.outSize)
        self.dn = DistanceNetwork()
        self.classify = AttentionalClassify()
        self.keep_prob = keep_prob
        self.num_classes_per_set = num_classes_per_set
        self.num_samples_per_class = num_samples_per_class
        self.learning_rate = learning_rate
        self.encoders = None

    def getEncoders(self, num_channels, image_size):
        gSup = Classifier(layer_size = cargs.layer_size, num_channels=num_channels,
                          nClasses= 0, image_size = image_size )
        self.encoders = [gSup]
        return [gSup]


    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None, target_mask = None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        # produce embeddings for support set images
        encoded_support = torch.stack([x[-1].view(x[-1].size(0), x[-1].size(1)) for x in support_enc])

        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = target_enc[i][-1].view(target_enc[i][-1].size(0), target_enc[i][-1].size(1))

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=encoded_support, target_image=encoded_target)

            # produce predictions for target probabilities
            preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

        return {'acc':  accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class MatchingNetworkPrototypical(MatchingNetwork):
    def __init__(self, *args, **kwargs):
        super(MatchingNetworkPrototypical, self).__init__(*args, **kwargs)

        """
        Builds a matching network, the training and evaluation ops as well as data augmentation routines.
        :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
        :param batch_size: The batch size for the experiment
        :param num_channels: Number of channels of the images
        :param is_training: Flag indicating whether we are training or evaluating
        :param rotate_flag: Flag indicating whether to rotate the images
        :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
        :param num_classes_per_set: Integer indicating the number of classes per set
        :param num_samples_per_class: Integer indicating the number of samples per class
        :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
        :param image_input: size of the input image. It is needed in case we want to create the last FC classification
        """
        self.dn = DistanceNetworkPrototypicalNetworks()

    def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        # produce embeddings for support set images
        encoded_support = torch.stack([x[-1].view(x[-1].size(0), x[-1].size(1)) for x in support_enc])

        true_positive = 0
        false_positive = 0
        true_negative = 0
        false_negative = 0

        # produce embeddings for target images
        for i in np.arange(target_image.size(1)):
            encoded_target = target_enc[i][-1].view(target_enc[i][-1].size(0), target_enc[i][-1].size(1))

            # get similarity between support set embeddings and target
            similarities = self.dn(support_set=encoded_support, target_image=encoded_target, support_set_labels_one_hot=support_set_labels_one_hot)

            # produce predictions for target probabilities
            preds = F.softmax(similarities, dim=1)

            # calculate accuracy and crossentropy loss
            values, indices = preds.max(1)
            if i == 0:
                accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
                crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
            else:
                accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
                crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


            nsup = support_set_labels_one_hot.size(1)
            actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
            declared_open_set = indices.data == (nsup - 1)
            true_positive += (actual_open_set & declared_open_set).sum()
            false_positive += ((~actual_open_set) & declared_open_set).sum()
            true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
            false_negative += (actual_open_set & (~declared_open_set)).sum()

        return {'acc':  accuracy/target_image.size(1),
                'loss': crossentropy_loss/target_image.size(1),
                'TP': true_positive,
                'FP': false_positive,
                'TN': true_negative,
                'FN': false_negative}



class MatchingNetworkTest(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def test_accuracy(self):
        pass


if __name__ == '__main__':
    unittest.main()
