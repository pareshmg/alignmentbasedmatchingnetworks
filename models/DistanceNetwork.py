from models.Base import *
import torch
import torch.nn as nn
import unittest
import torch.nn.functional as F
import numpy
from torch.autograd import Variable, Function
from models.Percentile import *
from models.NbrNorm import *
from tu.modules.ccl import PairwiseDiffL2

class DistanceNetwork(MyModule):
    def __init__(self):
        super(DistanceNetwork, self).__init__()
        self.sim = CosineSimilarities()

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [ns, nb, ndim]
        :param target_image: The embedding of the target image, tensor of shape [nb, ndim]
        :return: Softmax pdf. Tensor with cosine similarities of shape [nb, ns]
        """
        eps=1e-10
        similarities = []
        for support_image in support_set:
            sum_support = torch.sum(torch.pow(support_image, 2), 1)
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt()
            dot_product = target_image.unsqueeze(1).bmm(support_image.unsqueeze(2)).squeeze(2).squeeze(1)
            cosine_similarity = dot_product * support_magnitude
            similarities.append(cosine_similarity)
            #cosine_similarity = self.sim(target_image.unsqueeze(2), support_image.unsqueeze(2))
            #cosine_similarity = self.sim(support_image.unsqueeze(2), target_image.unsqueeze(2))
            #similarities.append(cosine_similarity.squeeze(2).squeeze(1))
        similarities = torch.stack(similarities, dim=1)
        return similarities

class DistanceNetworkPrototypicalNetworks(DistanceNetwork):
    def __init__(self):
        super(DistanceNetworkPrototypicalNetworks, self).__init__()
        self.sim = CosineSimilarities()

    def forward(self, support_set, target_image, support_set_labels_one_hot):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [ns, nb, ndim]
        :param target_image: The embedding of the target image, tensor of shape [nb, ndim]
        :return: Softmax pdf. Tensor with cosine similarities of shape [nb, ns]
        """
        support_set = support_set.permute(1,2,0) # [nb, ndim, ns]
        ss2 = support_set_labels_one_hot.narrow(2,0,builtins.args.classes_per_set) # [nb, ns, ncl]
        support_set = torch.matmul(support_set, ss2)/builtins.args.samples_per_class # [nb, ndim, ncl]
        eps=1e-10
        similarities = []
        for si in range(support_set.size(2)):
            support_image = support_set.select(2,si)
            sum_support = torch.sum(torch.pow(support_image, 2), 1)
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt()
            dot_product = target_image.unsqueeze(1).bmm(support_image.unsqueeze(2)).squeeze(2).squeeze(1)
            cosine_similarity = dot_product * support_magnitude
            similarities.append(cosine_similarity)
            #cosine_similarity = self.sim(target_image.unsqueeze(2), support_image.unsqueeze(2))
            #cosine_similarity = self.sim(support_image.unsqueeze(2), target_image.unsqueeze(2))
            #similarities.append(cosine_similarity.squeeze(2).squeeze(1))
        similarities = torch.stack(similarities, dim=1)
        return similarities


class DistanceNetworkSigmoid(MyModule):
    def __init__(self):
        super(DistanceNetworkSigmoid, self).__init__()
        self.sim = CosineSimilarities()

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.

        :param support_set: The embeddings of the support set images, tensor of shape [ns, nb, ndim]
        :param target_image: The embedding of the target image, tensor of shape [nb, ndim]
        :return: Softmax pdf. Tensor with cosine similarities of shape [nb, ns]
        """
        support_set = F.sigmoid(support_set).log()
        target_image = F.sigmoid(target_image).log()
        eps=1e-10
        similarities = []
        for support_image in support_set:
            sum_support = torch.sum(torch.pow(support_image, 2), 1)
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt()
            dot_product = target_image.unsqueeze(1).bmm(support_image.unsqueeze(2)).squeeze(2).squeeze(1)
            cosine_similarity = dot_product * support_magnitude
            similarities.append(cosine_similarity)
            #cosine_similarity = self.sim(target_image.unsqueeze(2), support_image.unsqueeze(2))
            #cosine_similarity = self.sim(support_image.unsqueeze(2), target_image.unsqueeze(2))
            #similarities.append(cosine_similarity.squeeze(2).squeeze(1))
        similarities = torch.stack(similarities, dim=1)
        return similarities


class DistanceNetworkL1(MyModule):
    def __init__(self):
        super(DistanceNetworkL1, self).__init__()

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, 64]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, 64]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        for support_image in support_set:
            similarity = - torch.abs(target_image - support_image).mean(1)
            similarities.append(similarity)
        similarities = torch.stack(similarities, dim=1)
        return similarities

class DistanceNetworkL2(MyModule):
    def __init__(self):
        super(DistanceNetworkL2, self).__init__()
        self.sim = L2Similarity()

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, 64]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, 64]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        similarities = []
        for support_image in support_set:
            similarity = self.sim(target_image, support_image)
            similarities.append(similarity)
        similarities = torch.stack(similarities, dim=1)
        return similarities


class DistanceNetworkMoments(DistanceNetworkL2):
    def __init__(self):
        super(DistanceNetworkMoments, self).__init__()

    def forward(self, support_set, target_image):
        return super(DistanceNetworkMoments, self).forward(support_set, target_image).unsqueeze(2)

class DistanceNetworkPW(MyModule):
    def __init__(self, subsample=False):
        super(DistanceNetworkPW, self).__init__()
        self.subsample = subsample
        self.sim = CosineSimilarities()

    def forward(self, support_set, target_image, target_mask=None):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, ndim, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, ndim, nsamples2]
        :param target_mask: [nb, nsamp2] binary mask
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        for support_image in support_set:
            cosine_similarity = self.sim(target_image, support_image) #[batch_size, nsamples2 , nsamples1]
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[nb, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        ###
        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities, dim=1)
        similarities = similarities.view(nb, ntgt, ns, nsup)
        ### END
        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]

        #if self.subsample:
        #    similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        if target_mask is not None:
            similarities = similarities * target_mask.unsqueeze(2)
            similarities = similarities / target_mask.sum(1, keepdim=True).unsqueeze(2)
            similarities = similarities.sum(1)
        else:
            similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]

class DistanceNetworkPWNoLSM(MyModule):
    def __init__(self, subsample=False):
        super(DistanceNetworkPWNoLSM, self).__init__()
        self.subsample = subsample

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, ndim, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, ndim, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        for support_image in support_set:
            # support_image = [batch_size, ndim, nsamples1]
            sum_support = torch.sum(torch.pow(support_image, 2), 1) # [batch_size, ndim, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = target_image.permute(0,2,1).bmm(support_image)  #[batch_size, nsamples2 , nsamples1]
            cosine_similarity = dot_product * support_magnitude.unsqueeze(1)
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[batch_size, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]



class DistanceNetworkPWSym(MyModule):
    def __init__(self, subsample=False):
        super(DistanceNetworkPWSym, self).__init__()
        self.subsample = subsample

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, ndim, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, ndim, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        similaritiesRev = []
        for support_image in support_set:
            # support_image = [batch_size, ndim, nsamples1]
            sum_support = torch.sum(torch.pow(support_image, 2), 1) # [batch_size, ndim, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            sum_target = torch.sum(torch.pow(target_image, 2), 1) # [batch_size, ndim, nsamples1]
            target_magnitude = sum_target.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = target_image.permute(0,2,1).bmm(support_image)  #[batch_size, nsamples2 , nsamples1]
            similarities.append(dot_product * support_magnitude.unsqueeze(1))
            similaritiesRev.append(dot_product * target_magnitude.unsqueeze(2))
        similarities = torch.stack(similarities, dim=2) + torch.stack(similaritiesRev, dim=2) #[batch_size, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        ### FIXME
        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities)
        similarities = similarities.view(nb, ntgt, ns, nsup)
        ### END
        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]



class DistanceNetworkPWMoments(MyModule):
    def __init__(self, subsample=False, nMoments=8):
        super(DistanceNetworkPWMoments, self).__init__()
        from tu.modules.ccl import PairwiseDiffL2
        from tu.modules.add import LogSum
        self.pd = PairwiseDiffL2()
        self.logsum = LogSum(1)
        self.subsample = subsample
        self.nMoments = nMoments

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, ndim, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, ndim, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        target_image = target_image.permute(0,2,1)

        for support_image in support_set:
            # support_image = [batch_size, ndim, nsamples1]
            support_image = support_image.permute(0,2,1)
            pdiff = self.pd(target_image.double(), support_image.double()).float()
            similarity = -pdiff
            similarities.append(similarity)
        similarities = torch.stack(similarities, dim=2) #[batch_size, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        # ### FIXME
        # similarities = similarities.view(nb*ntgt, ns*nsup)
        # similarities = F.log_softmax(similarities)
        # similarities = similarities.view(nb, ntgt, ns, nsup)
        # ### END
        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        dists = - similarities
        ldists = dists.log().clamp(-10)
        dmin = ldists.min(1)[0]
        dmax = ldists.max(1)[0]
        ldistsMoment = torch.stack([dmin, dmax] + [self.logsum(i*ldists - numpy.log(ldists.size(1)))/i for i in range(1,self.nMoments + 1)], dim=2)
        #similarities = torch.stack([(-similarities).pow(i).mean(1).log()/i for i in range(1,self.nMoments+1)], dim=2)
        #similarities = similarities.mean(1) # [nb, ns]
        return -ldistsMoment # [nb, ns, nMoments]


class DistanceNetworkPWCos(MyModule):
    def __init__(self, subsample=False):
        super(DistanceNetworkPWCos, self).__init__()
        self.subsample = subsample

    def forward(self, support_set, target_image):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, ndim, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, ndim, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        for support_image in support_set:
            # support_image = [batch_size, ndim, nsamples1]
            sum_support = torch.sum(torch.pow(support_image, 2), 1) # [batch_size, ndim, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            sum_target = torch.sum(torch.pow(target_image, 2), 1) # [batch_size, ndim, nsamples1]
            target_magnitude = sum_target.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = target_image.permute(0,2,1).bmm(support_image)  #[batch_size, nsamples2 , nsamples1]
            cosine_similarity = dot_product * support_magnitude.unsqueeze(1) * target_magnitude.unsqueeze(2)
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[batch_size, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        ### FIXME
        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities)
        similarities = similarities.view(nb, ntgt, ns, nsup)
        ### END
        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]


class DistanceNetworkPWNeighborhood(MyModule):
    def __init__(self, nr, subsample=False):
        super(DistanceNetworkPWNeighborhood, self).__init__()
        self.nnbrhoods = 20
        self.delta = 2
        self.nsamp = 5
        self.mp2d = nn.MaxPool2d(2*(self.delta+1)+1,1,self.delta+1)
        self.subsample = subsample
        assert(self.nsamp < (2*self.delta+1)**2)
        self.nr = nr
        self.nc = nr

    def train(self, mode):
        super(DistanceNetworkPWNeighborhood, self).train(mode)
        self.nnbrhoods = 20
    def eval(self, mode):
        super(DistanceNetworkPWNeighborhood, self).eval(mode)
        self.nnbrhoods = 20

    def forward(self, supportSetFV, targetFV, rsampSup, rsampTgt, nbrhood, dii, djj):

        """
        Produces pdfs over the support set classes for the target set image.
        :param supportSetFV: The embeddings of the support set images, tensor of shape [sequence_length, nb, 64, nr,nc]
        :param targetFV: The embedding of the target image, tensor of shape [nb, 64, nr,nc]
        :return: Softmax pdf. Tensor with cosine similarities of shape [nb, sequence_length]
        """
        eps = 1e-10
        delta = self.delta
        ns, nb, nk, nsup = supportSetFV.size()
        ntgt = targetFV.size(2)
        w = 2*delta+1
        res = []
        similarities = []

        for si, supportFV in enumerate(supportSetFV):
            sum_support = torch.sum(torch.pow(supportFV, 2), 1) # [batch_size, 64, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = targetFV.permute(0,2,1).bmm(supportFV)  #[batch_size, nsamples2 , nsamples1]
            cosine_similarity = dot_product * support_magnitude.unsqueeze(1)
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[nb, ntgt, ns, nsup]

        ## lsm
        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities)
        similarities = similarities.view(nb, ntgt, ns, nsup) #[nb, ntgt, ns, nsup]

        ## scatter to full image
        similaritiesFull = Variable(torch.zeros(nb, ntgt, ns, self.nr*self.nc).cuda()) # [nb, ntgt, ns, nr*nc]
        similaritiesFull.scatter_(3,rsampSup.repeat(nb,ntgt,ns,1), similarities)
        similaritiesFull = similaritiesFull.view(nb, ntgt * ns, self.nr, self.nc) # [nb, ntgt * ns, nr, nc]

        ## find highest value in neighborhood
        nbrc = similaritiesFull # [nb, ntgt*ns, nr, nc]
        nbrc = self.mp2d(nbrc) # [nb, nnbr*ns, nr, nc] - max in nbrhood
        nbrc = nbrc.view(nb, ntgt, ns, self.nr*self.nc) # [nb, nnbr, ns, nr*nc]
        nbrc = nbrc.index_select(3, rsampSup) # [nb, ntgt, ns, nsup]

        # [nb, ntgt, ns, nsup] x [1,1,ntgt,ntgt] -> #[nb, ntgt, ns, nsup]
        nbrc = torch.matmul(nbrc.permute(0,2,3,1), nbrhood.repeat(1,1,1,1).float()).permute(0,3,1,2)
        similarities = similarities + nbrc

        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]


class DistanceNetworkPWNeighborhoodDists(MyModule):
    def __init__(self, nr, subsample=False):
        super(DistanceNetworkPWNeighborhoodDists, self).__init__()
        self.nnbrhoods = 20
        self.delta = 2
        self.nsamp = 5
        self.mp2d = nn.MaxPool2d(2*(self.delta+1)+1,1,self.delta+1)
        self.subsample = subsample
        assert(self.nsamp < (2*self.delta+1)**2)
        self.nr = nr
        self.nc = nr

    def forward(self, supportSetFV, targetFV, rsampSup, rsampTgt, nbrhood, dii, djj):

        """
        Produces pdfs over the support set classes for the target set image.
        :param supportSetFV: The embeddings of the support set images, tensor of shape [sequence_length, nb, 64, nr,nc]
        :param targetFV: The embedding of the target image, tensor of shape [nb, 64, nr,nc]
        : nbrhood [ntgt, nnbr]
        : dii [ntgt, nnbr]
        : djj [nsup, nsup]
        :return: Softmax pdf. Tensor with cosine similarities of shape [nb, sequence_length]

        """
        eps = 1e-10
        delta = self.delta
        ns, nb, nk, nsup = supportSetFV.size()
        ntgt = targetFV.size(2)
        w = 2*delta+1
        res = []
        similarities = []

        for si, supportFV in enumerate(supportSetFV):
            sum_support = torch.sum(torch.pow(supportFV, 2), 1) # [batch_size, 64, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = targetFV.permute(0,2,1).bmm(supportFV)  #[batch_size, nsamples2 , nsamples1]
            cosine_similarity = dot_product * support_magnitude.unsqueeze(1)
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[nb, ntgt, ns, nsup]

        ## lsm
        #lsmsimilarities = F.log_softmax(similarities,dim=3)#[nb, ntgt, ns, nsup]
        lsmsimilarities = similarities - similarities.max(3, keepdim=True)[0] #[nb, ntgt, ns, nsup]

        similarities = similarities.view(nb,ntgt, ns*nsup)
        similarities = F.log_softmax(similarities, dim=2)
        similarities = similarities.view(nb, ntgt, ns, nsup) #[nb, ntgt, ns, nsup]



        nbrSimilarity = NbrNormV2(dii.data,djj.data,nbrhood.data, sigma2=4*4)(lsmsimilarities)

        #similarities = similarities +  0.01 * nbrSimilarity # interp -> 0.01
        similarities = similarities +  nbrSimilarity # interp1
        #similarities = nbrSimilarity # nbronly -> interp0
        #similarities = similarities + 0.2 * nbrSimilarity # interp0.2
        #similarities = nbrSimilarity # interp0NbrNorm
        if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}

        similarities = similarities.max(3)[0]   # [batch_size, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        similarities = similarities.mean(1) + similarities.min(1)[0] # [nb, ns]
        return similarities # [nb, ns]





class CosineDistance(MyModule):
    def __init__(self, normalize_first=False):
        super(CosineDistance, self).__init__()
        self.sim = CosineSimilarities(normalize_first)

    def forward(self, v1, v2):
        """
        v1 :: [nb, ndim, n1]
        v2 :: [nb, ndim, n2]
        res :: [nb, n1, n2]
        """
        cosine_similarity = self.sim(v1, v2)
        return -cosine_similarity

class L2DistanceOld(MyModule):
    def __init__(self):
        super(L2Distance, self).__init__()
        self.pd = PairwiseDiffL2()
    def forward(self, v1, v2):
        """
        v1 :: [nb, ndim, n1]
        v2 :: [nb, ndim, n2]
        res :: [nb, n1, n2]
        """
        return self.pd(v1.permute(0,2,1).contiguous(), v2.permute(0,2,1).contiguous())

class L2Distance(MyModule):
    def forward(self, v1, v2):
        """
        v1 :: [nb, ndim, n1]
        v2 :: [nb, ndim, n2]
        res :: [nb, n1, n2]
        """
        ndim = v1.size(1)
        v1_norm = (v1 ** 2).sum(1).unsqueeze(2) # [nb, n1, 1]
        v2_norm = (v2 ** 2).sum(1).unsqueeze(1) # [nb, 1, n2]
        dist = v1_norm + v2_norm - 2.0 * torch.matmul(v1.transpose(1,2), v2)
        return dist/ndim


class L2Similarity(MyModule):
    def __init__(self):
        super(L2Similarity, self).__init__()
        self.pd = L2Distance()
    def forward(self, v1, v2):
        """
        v1 :: [nb, ndim, n1]
        v2 :: [nb, ndim, n2]
        res :: [nb, n1, n2]
        """
        return -self.pd(v1, v2)


class CosineSimilarities(MyModule):
    def __init__(self, normalize_first=False):
        super(CosineSimilarities, self).__init__()
        self.normalize_first = normalize_first

    def forward(self, v1, v2):
        """
        v1 : [nb, ndim, n1]
        v2 : [nb, ndim, n2]
        res: [nb, n1, n2]
        """
        eps = 1e-10
        vsum = torch.sum(torch.pow(v1 if self.normalize_first else v2, 2), 1) # [nb, ndim, n2]
        vmag = vsum.clamp(eps, float("inf")).rsqrt() # [nb, n2]
        dot_product = v1.permute(0,2,1).bmm(v2)  #[nb, n1, n2]
        cosine_similarity = dot_product * vmag.unsqueeze(1)
        return cosine_similarity

class CosineSimilaritiesUnit(MyModule):
    def __init__(self, normalize_first=False):
        super(CosineSimilaritiesUnit, self).__init__()
        self.normalize_first = normalize_first

    def forward(self, v1, v2):
        """
        v1 : [nb, ndim, n1]
        v2 : [nb, ndim, n2]
        res: [nb, n1, n2]
        """
        eps = 1e-10
        vsum = torch.sum(torch.pow(v1 if self.normalize_first else v2, 2), 1) # [nb, ndim, n2]
        vmag = vsum.clamp(eps, float("inf")).rsqrt() # [nb, n2]
        vsum2 = torch.sum(torch.pow(v2 if self.normalize_first else v1, 2), 1) # [nb, ndim, n2]
        vmag2 = vsum2.clamp(eps, float("inf")).rsqrt() # [nb, n2]
        dot_product = v1.permute(0,2,1).bmm(v2)  #[nb, n1, n2]
        cosine_similarity = dot_product * vmag.unsqueeze(1) * vmag2.unsqueeze(2)
        return cosine_similarity


class CosineSimilaritiesPerDim(MyModule):
    def __init__(self, normalize_first=False):
        super(CosineSimilaritiesPerDim, self).__init__()
        self.normalize_first = normalize_first

    def forward(self, v1, v2):
        """
        v1 : [nb, ndim, n1]
        v2 : [nb, ndim, n2]
        """
        v1,v2 = (v2,v1) if self.normalize_first else (v1,v2)
        eps = 1e-10
        vsum = torch.sum(torch.pow(v2, 2), 1) # [nb, ndim, n2]
        vmag = vsum.clamp(eps, float("inf")).rsqrt() # [nb, n2]

        v2 = v2 * vmag.unsqueeze(1) # [nb, ndim, n2]

        simperdim = v1.unsqueeze(2) * v2.unsqueeze(2) # [nb, ndim, n1, n2]
        return simperdim


class DistanceNetworkPWL2(DistanceNetworkPW):
    def __init__(self):
        super(DistanceNetworkPWL2, self).__init__()
        self.sim = L2Similarity()


class DistanceNetworkPWVectorMask(DistanceNetworkPW):
    """
    hypercolum -> Foreground/background mask -> product with similarity (no normalization)
    """
    def __init__(self, ndim, subsample=False, usePair=False):
        super(DistanceNetworkPWVectorMask, self).__init__(subsample)
        self.mask = StackEmbed([ndim, 128, ndim])
        self.usePair = usePair
    def forward(self, support_set, target_image):
        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, 64, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, 64, nsamples2]

        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        tmask = []
        smask = []
        tmask0 = F.sigmoid(self.mask(target_image).squeeze(1)) # [nb, ndim, ntgt]
        target_image = target_image * tmask0
        for support_image in support_set:
            tmask.append(tmask0)
            smask.append(F.sigmoid(self.mask(support_image))) # [nb, ndim, nsup]
            #support_image = support_image * tmask0  # automatically taken care of in dot product
            # support_image = [batch_size, ndim, nsamples1]
            sum_target = torch.sum(torch.pow(target_image, 2), 1) # [batch_size, ndim, ntgt]
            target_magnitude = sum_target.clamp(eps, float("inf")).rsqrt() # [batch_size, nsup]
            dot_product = support_image.permute(0,2,1).bmm(target_image)  #[batch_size, nsup , ntgt]
            cosine_similarity = dot_product * target_magnitude.unsqueeze(1)
            cosine_similarity = cosine_similarity.permute(0,2,1) #[batch_size, ntgt , nsup]
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[batch_size, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        ### LSM
        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities, dim=1)
        similarities = similarities.view(nb, ntgt, ns, nsup)
        ### END

        if self.visMode:
            self.forVis = {'pointSimilarities':similarities.data.clone(), #[nb, ntgt, ns, nsup]
                           #'smask': smask.data.clone(),
                           #'tmask': tmask.data.clone() #[nb, ntgt, ns, 1]
            }

        similarities = similarities.max(3)[0]   # [nb, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [nb, ntgt/2, ns]

        similarities = similarities.sum(1)
        return similarities # [nb, ns]



class DistanceNetworkPWL1(MyModule):
    def __init__(self):
        super(DistanceNetworkPWL1, self).__init__()
        from tu.modules.ccl import PairwiseDiffL1
        self.pd = PairwiseDiffL1()

    def forward(self, support_set, target_image):
        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, 64, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, 64, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        target_image = target_image.permute(0,2,1)
        for support_image in support_set:
            # support_image = [batch_size, 64, nsamples1]
            support_image = support_image.permute(0,2,1)
            pdiff = self.pd(target_image.double(), support_image.double()).float()
            similarity = -pdiff
            similarities.append(similarity.max(2)[0].mean(1))
        similarities = torch.stack(similarities, dim=1)
        return similarities



class DistanceNetworkPWST(MyModule):
    def __init__(self):
        super(DistanceNetworkPWST, self).__init__()

    def forward(self, support_set, target_image, matchProb):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, 64, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, 64, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        for support_image in support_set:
            # support_image = [batch_size, 64, nsamples1]
            sum_support = torch.sum(torch.pow(support_image, 2), 1) # [batch_size, 64, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = target_image.permute(0,2,1).bmm(support_image)  #[batch_size, nsamples2 , nsamples1]
            cosine_similarity = dot_product * support_magnitude.unsqueeze(1)

            ## do a weighted combination based on the expected location based on the transformer
            cosine_similarity = cosine_similarity * matchProb # [batch_size, nsamples2, nsamples1, numT]
            cosine_similarity = cosine_similarity.sum(2).mean(1).max(1)[0]

        similarities = torch.stack(similarities)
        return similarities



class DistanceNetworkMaskedCl(DistanceNetworkPW):
    def __init__(self, ndim, subsample=False):
        super(DistanceNetworkMaskedCl, self).__init__(subsample)
        self.cl = Classifier()

        self.mask = StackEmbed([ndim+64, 128, 1])
        self.emb = StackEmbed([ndim, ndim, ndim])

    def forward(self, support_set, target_image):
        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, 64, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, 64, nsamples2]

        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []

        self.targetFV = self.emb(target_image)

        smask = []
        tmask = []
        for support_image in support_set:
            supportFV = self.emb(support_image)
            smask.append(-self.mask(torch.cat([support_image,
                                              target_image.narrow(2,0,1).narrow(1,target_image.size(1)-64,64).repeat(1,1,support_image.size(2))], dim=1))) # [nb,1, nsup])

            tmask.append(-self.mask(torch.cat([target_image,
                                              support_image.narrow(2,0,1).narrow(1,support_image.size(1)-64,64).repeat(1,1,target_image.size(2))], dim=1)).squeeze(1).unsqueeze(2)) #[nb, ntgt, 1]


            # support_image = [batch_size, ndim, nsamples1]
            sum_support = torch.sum(torch.pow(support_image, 2), 1) # [batch_size, ndim, nsamples1]
            support_magnitude = sum_support.clamp(eps, float("inf")).rsqrt() # [batch_size, nsamples1]
            dot_product = target_image.permute(0,2,1).bmm(support_image)  #[batch_size, nsamples2 , nsamples1]
            cosine_similarity = dot_product * support_magnitude.unsqueeze(1)
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[batch_size, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        ### LSM
        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities, dim=1)
        similarities = similarities.view(nb, ntgt, ns, nsup)
        ### END

        smask = F.sigmoid(torch.stack(smask, dim=2))
        tmask = F.sigmoid(torch.stack(tmask, dim=2)) #[nb, ntgt, ns, 1]
        similarities = similarities * tmask#[nb, ntgt, ns, nsup]

        if self.visMode:
            self.forVis = {'pointSimilarities':similarities.data.clone(), #[nb, ntgt, ns, nsup]
                           'smask':smask.data.clone(), #[nb, 1, ns, nsup]
                           'tmask':tmask.data.clone()} #[nb, ntgt, ns, 1]

        similarities = similarities.max(3)[0]   # [nb, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [nb, ntgt/2, ns]

        similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]




class DistanceNetworkPWHierarchical(MyModule):
    def __init__(self, subsample=False, selfReg = False):
        super(DistanceNetworkPWHierarchical, self).__init__()
        self.subsample = subsample
        self.selfReg = selfReg
        self.dist = CosineDistance()


    def forward(self, support_set, target_image):
        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: emb :: [[nb, ns, ndim, nr, nc] x nlayers]
        :param target_image: emb :: [[nb, ndim, nr, nc] x nlayers]
        """
        eps = 1e-10
        w = 2
        nlayers = len(target_image)
        t0, s0 = None, None
        alignmentCosts = []
        alignmentLayers = []
        for si in range(len(support_set)):
            support_image = support_set[si]
            nb, ndim, nr, nc = target_image[-1].size()

            t0 = target_image[-1].view(nb, ndim,nr*nc) # [nb, ndim, ntgt=1]
            s0 = support_image[-1].view(nb, ndim,1, nr*nc).repeat(1,1,nr*nc,1)# [nb, ndim, ntgt, nsup=1]
            sim = -self.dist(s0, t0) # [nb, ntgt, nsup]
            match, matchIdx = sim.max(-1)  # [nb, ntgt]

            matchIdx = [matchIdx%nc * 2, matchIdx/nc * 2]
            allMatches = torch.stack([torch.stack([matchIdx[0]+dr, matchIdx[1]+dc], dim=1) # [nb, 2, ntgt]
                                      for dr in range(-1,w+1)
                                      for dc in range(-1,w+1)], dim=1) # [nb, nopt, 2, ntgt]
            allMatches = allMatches.view(nb, -1, 2, nr,nc) # [nb, nopt, 2, nr, nc]
            match = match.view(nb, 1, nr, nc)

            for li in range(nlayers-2, 0, -1):
                nb, ndim, nr, nc = target_image[li].size()
                t0 = target_image[li].view(nb, ndim, nr*nc) # [nb, ndim, ntgt]
                allMatches = allMatches.clamp(min=0, max=nr-1)
                allMatches = allMatches.select(2,0)*nc + allMatches.select(2,1) # [nb, nopt, nr, nc]

                ##################################################
                ## upsample from previous layer

                allMatches = UpFitCopy(nr//2,nr)(allMatches.float()).long() # [nb, nopt, nr, nc]
                match = UpFitCopy(nr//2,nr)(match) # [nb, 1, nr, nc]

                nopt = allMatches.size(1)

                allMatches = allMatches.view(nb, 1,nopt* nr*nc).repeat(1,ndim,1)
                s0 = support_image[li].view(nb,ndim, nr*nc)
                s0 = s0.gather(2,allMatches) # [nb, ndim, nopt * ntgt]
                s0 = s0.view(nb, ndim, nopt, nr*nc)
                s0 = s0.permute(0,1,3,2) # [nb, ndim, ntgt, nopt]
                sim = -self.dist(s0, t0) # [nb, ntgt, nopt]
                match2, matchIdx = sim.max(-1)  # [nb, ntgt]
                match = match + match2.view(nb, 1, nr, nc) # [nb, 1, nr, nc]

                matchIdx = allMatches[:,0,:].contiguous().view(nb, nopt, nr*nc).gather(1,matchIdx.view(nb, 1, nr*nc))

                matchIdx = [matchIdx%nc * 2, matchIdx/nc * 2]
                allMatches = torch.stack([torch.stack([matchIdx[0]+dr, matchIdx[1]+dc], dim=1) # [nb, 2, ntgt]
                                          for dr in range(-1,w+1)
                                          for dc in range(-1,w+1)], dim=1) # [nb, nopt, 2, ntgt]
                allMatches = allMatches.view(nb, -1, 2, nr,nc) # [nb, nopt, 2, nr, nc]

            match = match.view(nb, -1).mean(1)
            alignmentCosts.append(match)
        alignmentCosts = torch.stack(alignmentCosts, dim=1) # [nb, ns]

        #if self.visMode: self.forVis = {'pointSimilarities':similarities.data.clone()}
        similarities = alignmentCosts
        return similarities # [nb, ns]


class SelfLossHierarchical(MyModule):
    def __init__(self):
        super(SelfLossHierarchical, self).__init__()
        self.sim = CosineSimilarities()
        self.blocks = GetBlocks2D()

    def forward(self, target_image):
        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: emb :: [[nb, ns, ndim, nr, nc] x nlayers]
        :param target_image: emb :: [[nb, ndim, nr, nc] x nlayers]
        """
        self_loss = 0
        eps = 1e-10
        w = 2
        nlayers = len(target_image)
        for li in range(nlayers-2, 1, -1):
            ## extract chunks

            targetFV = self.blocks(target_image[li]) # chunks [nb * nchunks, ndim, mr * mc]
            nb, mr, mc, ndim, wr, wc = targetFV.size()
            targetFV = targetFV.view(nb*mr*mc, ndim, wr * wc)


            eps = 1e-10
            sum_target = torch.sum(torch.pow(targetFV, 2), 1) # [nb, ndim, ntgt]
            target_magnitude = sum_target.clamp(eps, float("inf")).rsqrt() # [nb, ntgt]
            dot_product = targetFV.permute(0,2,1).bmm(targetFV)  #[nb, ntgt , ntgt]
            selfSimilarity = dot_product * target_magnitude.unsqueeze(1) #[nb, ntgt , ntgt]

            nb, ntgt,_ = selfSimilarity.size()
            selfSimilarity = F.softmax(selfSimilarity, dim=2) #[batch_size, ntgt , ntgt]
            selfLabel = Variable(torch.arange(0,ntgt).long().cuda(), requires_grad=False)

            self_loss += F.cross_entropy(selfSimilarity.view(nb * ntgt, ntgt), selfLabel.repeat(nb))

        return self_loss / (nlayers-2) # [nb, ns]



class DistanceNetworkPWSeparation(MyModule):
    def __init__(self, embSplit, subsample=False, normalize_mask_weight=False, use_sim=False):
        super(DistanceNetworkPWSeparation, self).__init__()
        self.subsample = subsample
        self.embSplit = embSplit
        self.l2 = L2Distance()
        self.sim = CosineSimilarities()
        self.normalize_mask_weight = normalize_mask_weight
        self.use_sim = use_sim

    def forward(self, support_set, target_image, target_image_other, reference_other_set):

        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [sequence_length, batch_size, ndim, nsamples1]
        :param target_image: The embedding of the target image, tensor of shape [batch_size, ndim, nsamples2]
        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10

        if True:
            ## obtain mask
            other_l2 = []
            ## obtain mask
            for other_image in reference_other_set:
                if self.use_sim:
                    other_l2.append(self.sim(target_image_other, other_image).max(2)[0]) # [nb, ntgt]
                else:
                    other_l2.append(-self.l2(target_image_other, other_image).min(2)[0]) # [nb, ntgt]
            other_l2 = torch.stack(other_l2, dim=2) # [nb, ntgt, nref]

            selfsim = self.sim(target_image_other, target_image_other).max(2)[0] * 5 if self.use_sim else 1

            other_l2 = other_l2.exp().sum(2) + selfsim # [nb, ntgt]
            m_ = selfsim/other_l2 # [nb, ntgt]

        similarities = []
        for support_image in support_set:
            cosine_similarity = self.sim(target_image, support_image)
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[nb, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()


        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities, dim=1)
        similarities = similarities.view(nb, ntgt, ns, nsup)

        if self.visMode:
            m = m_.data.clone().unsqueeze(2).unsqueeze(3) # [nb, ntgt, 1, 1]
            m = m.repeat(1,1,ns,1) # [nb, ntgt, ns, 1]
            self.forVis = {'pointSimilarities':similarities.data.clone(),
                           'tmask': m}

        similarities = similarities.max(3)[0]   # [nb, ntgt, ns]
        similarities = similarities * m_.unsqueeze(2)
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [batch_size, ntgt/2, ns]

        if self.normalize_mask_weight:
            similarities = similarities.sum(1)/m_.sum(1).unsqueeze(1) # [nb, ns]
        else:
            similarities = similarities.mean(1) # [nb, ns]
        return similarities # [nb, ns]





class DistanceNetworkPWAttn(DistanceNetworkPW):
    """
    hypercolum -> Foreground/background mask -> product with similarity (no normalization)
    """
    def __init__(self, subsample=False, usePair=False):
        super(DistanceNetworkPWAttn, self).__init__(subsample)
        self.mask = StackEmbed([64, 64, 1])
        self.usePair = usePair
        self.sim = CosineSimilarities()
    def forward(self, support_set, target_image, target_attn_input, support_attn_input=None):
        """
        Produces pdfs over the support set classes for the target set image.
        :param support_set: The embeddings of the support set images, tensor of shape [ns, nb, ndim, nsup]
        :param target_image: The embedding of the target image, tensor of shape [nb, ndim, ntgt]

        :param target_attn_input: [nb, ndim_attn, ntgt]

        :return: Softmax pdf. Tensor with cosine similarities of shape [batch_size, sequence_length]
        """
        eps = 1e-10
        similarities = []
        tmask = []
        smask = []
        tmask0 = F.softmax(self.mask(target_attn_input).squeeze(1), dim=1) # [nb, ntgt]
        for support_image in support_set:
            cosine_similarity = self.sim(target_image, support_image) # [nb, ntgt, nsup]
            similarities.append(cosine_similarity)
        similarities = torch.stack(similarities, dim=2) #[nb, ntgt, ns, nsup]
        nb, ntgt, ns, nsup = similarities.size()

        similarities = similarities * tmask0.unsqueeze(2).unsqueeze(3) # [nb, ntgt, ns, nsup]


        ### LSM
        similarities = similarities.view(nb*ntgt, ns*nsup)
        similarities = F.log_softmax(similarities, dim=1)
        similarities = similarities.view(nb, ntgt, ns, nsup)
        ### END

        if self.visMode:
            self.forVis = {'pointSimilarities':similarities.data.clone(), #[nb, ntgt, ns, nsup]
                           #'smask': smask.data.clone(),
                           'tmask': tmask0.data.view(nb, ntgt, 1, 1).repeat(1,1,len(support_set),1).clone() #[nb, ntgt, ns, 1]
            }

        similarities = similarities.max(3)[0]   # [nb, ntgt, ns]
        if self.subsample:
            similarities,_ = PercentileSelectHigh(0.5, 1)(similarities) # [nb, ntgt/2, ns]

        similarities = similarities.sum(1)
        return similarities # [nb, ns]




class DistanceNetworkTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_forward(self):
        pass


if __name__ == '__main__':
    unittest.main()
