import torch
from torch.nn import Parameter
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
import unittest
import numpy
import numpy as np
import math
import builtins
from models.Base import *
import torch.nn.functional as F
from models.DistanceNetwork import *

from torch.nn.modules.utils import _single, _pair, _triple

class Conv2dMinMax(nn.Conv2d):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                     padding=0, dilation=1, groups=1, bias=True):
        super(Conv2dMinMax, self).__init__(
            in_channels, out_channels, kernel_size, stride, padding, dilation,
             groups, bias)

    def forward(self, inpLow, inpHigh):
        if False:
            y = super(Conv2dMinMax, self).forward(inpLow)
            return y, y
        wHigh = F.relu(self.weight)
        wLow = -F.relu(-self.weight)
        conv = lambda inp, wt, b: F.conv2d(inp, wt, b , self.stride,
                                           self.padding, self.dilation, self.groups)

        try:
            yHigh = conv(inpHigh, wHigh, self.bias) + conv(inpLow, wLow, self.bias*0)
            yLow = conv(inpLow, wHigh, self.bias) + conv(inpHigh, wLow, self.bias*0)
        except:
            import ipdb; ipdb.set_trace()

        return yLow, yHigh




class MaxPool2dMinMaxPickLow(nn.MaxPool2d):
    def forward(self, inpLow, inpHigh):
        resLow = F.max_pool2d(inpLow, self.kernel_size, self.stride,
                              self.padding, self.dilation, self.ceil_mode,
                              False)
        resHigh = F.max_pool2d(inpHigh, self.kernel_size, self.stride,
                               self.padding, self.dilation, self.ceil_mode,
                               False)
        return resLow, resLow

class MaxPool2dMinMaxPickMinMax(nn.MaxPool2d):
    def forward(self, inpLow, inpHigh):
        resLow = F.max_pool2d(inpLow, self.kernel_size, self.stride,
                              self.padding, self.dilation, self.ceil_mode,
                              False)
        resHigh = F.max_pool2d(inpHigh, self.kernel_size, self.stride,
                               self.padding, self.dilation, self.ceil_mode,
                               False)
        return resLow, resHigh

class MaxPool2dMinMaxDelta(nn.MaxPool2d):
    def forward(self, inpLow, inpHigh):
        resLow = F.max_pool2d(inpLow, self.kernel_size, self.stride,
                              self.padding, self.dilation, self.ceil_mode,
                              False)
        resHigh = F.max_pool2d(inpHigh, self.kernel_size, self.stride,
                               self.padding, self.dilation, self.ceil_mode,
                               False)
        delta = 0.05
        return resLow, resLow + F.relu(resHigh-resLow - delta)


class AvgPool2dMinMax(nn.AvgPool2d):
    def forward(self, inpLow, inpHigh):
        aLow = F.avg_pool2d(inpLow, self.kernel_size, self.stride,
                            self.padding, self.ceil_mode, self.count_include_pad)
        aHigh = F.avg_pool2d(inpHigh, self.kernel_size, self.stride,
                        self.padding, self.ceil_mode, self.count_include_pad)
        return aLow, aLow #aHigh

class AvgPool2dMinMaxDelta(nn.AvgPool2d):
    def forward(self, inpLow, inpHigh):
        aLow = F.avg_pool2d(inpLow, self.kernel_size, self.stride,
                            self.padding, self.ceil_mode, self.count_include_pad)
        aHigh = F.avg_pool2d(inpHigh, self.kernel_size, self.stride,
                        self.padding, self.ceil_mode, self.count_include_pad)
        delta = 0.5
        return aLow, aLow  + F.relu(aHigh - aLow - delta)




class MaxPool2dMinMax(AvgPool2dMinMaxDelta):
    pass


class LinearMinMax(nn.Linear):
    def forward(self, inpLow, inpHigh):
        wHigh = F.relu(self.weight)
        wLow = -F.relu(-self.weight)
        f = lambda inp, wt, b: F.linear(inp, wt, b)

        yHigh = f(inpHigh, wHigh, self.bias) + f(inpLow, wLow, self.bias*0)
        yLow = f(inpLow, wHigh, self.bias) + f(inpHigh, wLow, self.bias*0)

        return yLow, yHigh

class ReLUMinMax(nn.ReLU):
    def forward(self, inpLow, inpHigh):
        return F.relu(inpLow), F.relu(inpHigh)

class SigmoidMinMax(nn.Sigmoid):
    def forward(self, inpLow, inpHigh):
        return F.sigmoid(inpLow), F.sigmoid(inpHigh)


class SoftmaxMinMax(nn.Softmax):
    def forward(self, inpLow, inpHigh):
        """
        [nb, ndim]
        """
        resLow = []
        resHigh = []
        n = inpLow.size(self.dim)
        for di in range(n):
            stk = []
            if di > 0: stk.append(inpHigh.narrow(self.dim, 0, di))
            stk.append(inpLow.narrow(self.dim, di,1))
            if di < (n-1): stk.append(inpHigh.narrow(self.dim,di+1,n-di-1))

            stk = torch.cat(stk, dim=self.dim)
            resLow.append(F.softmax(stk, dim=self.dim))

            stk = []
            if di > 0: stk.append(inpLow.narrow(self.dim, 0, di))
            stk.append(inpHigh.narrow(self.dim, di,1))
            if di < (n-1): stk.append(inpLow.narrow(self.dim,di+1,n-di-1))

            stk = torch.cat(stk, dim=self.dim)
            resHigh.append(F.softmax(stk, dim=self.dim)) # [nb, ndim]

        resLow = torch.stack(resLow, dim=self.dim) # [nb, ndim, ndim]
        resHigh = torch.stack(resHigh, dim=self.dim)
        return resLow, resHigh

class ScoringMinMax(nn.Softmax):
    def forward(self, inpLow, inpHigh):
        """
        [nb, ndim]
        """
        resLow = []
        resHigh = []
        n = inpLow.size(self.dim)
        for di in range(n):
            stk = []
            if di > 0: stk.append(inpHigh.narrow(self.dim, 0, di))
            stk.append(inpLow.narrow(self.dim, di,1))
            if di < (n-1): stk.append(inpHigh.narrow(self.dim,di+1,n-di-1))

            stk = torch.cat(stk, dim=self.dim)
            resLow.append(stk)

            stk = []
            if di > 0: stk.append(inpLow.narrow(self.dim, 0, di))
            stk.append(inpHigh.narrow(self.dim, di,1))
            if di < (n-1): stk.append(inpLow.narrow(self.dim,di+1,n-di-1))

            stk = torch.cat(stk, dim=self.dim)
            resHigh.append(stk) # [nb, ndim]

        resLow = torch.stack(resLow, dim=self.dim) # [nb, ndim, ndim]
        resHigh = torch.stack(resHigh, dim=self.dim)
        return resLow, resHigh


def convLayer(in_planes, out_planes, useDropout = False):
    "3x3 convolution with padding"
    seq = Conv2dMinMax(in_planes, out_planes, kernel_size=3,
                       stride=1, padding=1, bias=True)
    #nn.BatchNorm2d(out_planes),
    #nn.MaxPool2d(kernel_size=2, stride=2)

    if useDropout: # Add dropout module
        assert(False)
        list_seq = list(seq.modules())[1:]
        list_seq.append(nn.Dropout(0.1))
        seq = nn.Sequential(*list_seq)

    return seq



class EncoderMinMax(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28, relu_last=True, returnLayer = -1):
        super(EncoderMinMax, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """
        self.relu_last = relu_last
        self.l1 = convLayer(num_channels, layer_size//2, useDropout)
        self.l2 = convLayer(layer_size//2, layer_size, useDropout) # layer 2
        self.l3 = convLayer(layer_size, layer_size, useDropout) # layer 3
        self.l4 = convLayer(layer_size, layer_size, useDropout) # layer 4
        self.l5 = None
        self.l6 = None
        if image_size >= 30:
            self.l5 = convLayer(layer_size, layer_size, useDropout) # layer 5
        if image_size >= 80:
            self.l6 = convLayer(layer_size, layer_size, useDropout) # layer 6

        self.useClassification = False

        self.ls = [self.l1, self.l2, self.l3, self.l4, self.l5, self.l6]
        self.ls = [x for x in self.ls if x is not None]
        for x in self.ls:
            self.weights_init(x)
        self.maxpool = MaxPool2dMinMax(kernel_size=2, stride=2)
        self.returnLayer = returnLayer

    def weights_init(self,module):
        if module is None: return
        for m in module.modules():
            if isinstance(m, Conv2dMinMax) or isinstance(m, nn.Conv2d):
                init.xavier_uniform(m.weight, gain=np.sqrt(2))
                init.constant(m.bias, 0)
                print(m.weight.data.max())
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, image_input_low, image_input_high):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 28, 28, 1]
        :return: Embeddings of size [batch_size, 64]
        """
        xs_low = [image_input_low]
        xs_high = [image_input_high]
        x_low = image_input_low
        x_high = image_input_high
        layers = self.ls
        for li,l in enumerate(layers):
            if l is None: continue
            #if li == 0: print('_', (x_high-x_low).data.min(), (x_high-x_low).data.max())
            x_low, x_high = l(x_low, x_high)
            if self.visMode:
                print('layer{}'.format(li), (x_high-x_low).data.min(), (x_high-x_low).data.max(), (x_high-x_low).data.mean())
            xs_low.append(x_low)
            xs_high.append(x_high)
            x_low = F.relu(x_low)
            x_high = F.relu(x_high)
            x_low, x_high = self.maxpool(x_low, x_high)
        xs_low.append(x_low)
        xs_high.append(x_high)
        #import ipdb; ipdb.set_trace()

        return xs_low[self.returnLayer], xs_high[self.returnLayer]


class ClassifierMinMaxMinProb(MyModule):
    def __init__(self, batch_size=100, num_channels=1, learning_rate=0.001,
                 nClasses = 10, image_size = 28, layer_size=64, **kwargs):
        super(ClassifierMinMaxMinProb, self).__init__()
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.encoders = None
        gSup = EncoderMinMax(layer_size = cargs.layer_size, num_channels=num_channels,
                                nClasses= nClasses, image_size = image_size )
        self.encoder = gSup
        self.ll = LinearMinMax(cargs.layer_size, nClasses)
        self.scoring = ScoringMinMax(dim=1)

    def getEncoders(self, num_channels, image_size):
        self.encoders = [Identity2()]
        return self.encoders


    def forward(self, target_image_low, target_image_high, target_label, target_ind=None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        target_enc_low, target_enc_high = self.encoder(target_image_low, target_image_high)
        nb, ndim, nr2, nc2 = target_enc_low.size()
        target_enc_low = target_enc_low.view(nb, ndim*nr2*nc2)
        target_enc_high = target_enc_high.view(nb, ndim*nr2*nc2)

        scores_low, scores_high = self.ll(target_enc_low, target_enc_high) # [nb, ndim]
        scores_low, scores_high = self.scoring(scores_low, scores_high) # [nb, ndim, ndim]

        # prob_low :: [nb, ndim, ndim]
        targ = target_label.unsqueeze(1).unsqueeze(2).repeat(1,1,scores_low.size(2))
        if True: # self.parent.mode == 'train':
            # FIXME: This is going to be true as only the first layer has variablility
            scores_low = scores_low.gather(1, targ).squeeze(1)
        else:
            scores_low = scores_low.mean(1)
        # if self.visMode:
        #     diff = []
        #     for i in range(prob_low.size(0)):
        #         diff.append(prob_high.data.select(0,i).diag() - prob_low.data.select(0,i).diag())
        #     diff = torch.stack(diff)
        #     print('softmax', diff.min(), diff.max(), diff.mean())
        prob_low = F.softmax(scores_low, dim=1)
        values, indices = prob_low.max(1)

        accuracy = torch.mean((indices.squeeze() == target_label).float())
        crossentropy_loss =  F.cross_entropy(scores_low, target_label.long())

        return {'acc':  accuracy,
                'loss': crossentropy_loss,
                '95Interval': 0,
                'TP': 0,
                'FP': 0,
                'TN': 0,
                'FN': 0}


class ClassifierMinMaxMeanProb(MyModule):
    def __init__(self, batch_size=100, num_channels=1, learning_rate=0.001,
                 nClasses = 10, image_size = 28, layer_size=64, **kwargs):
        super(ClassifierMinMaxMeanProb, self).__init__()
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.encoders = None
        gSup = EncoderMinMax(layer_size = cargs.layer_size, num_channels=num_channels,
                                nClasses= nClasses, image_size = image_size )
        self.encoder = gSup
        self.ll = LinearMinMax(cargs.layer_size, nClasses)
        self.softmax = SoftmaxMinMax(dim=1)

    def getEncoders(self, num_channels, image_size):
        self.encoders = [Identity2()]
        return self.encoders


    def forward(self, target_image_low, target_image_high, target_label, target_ind=None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        target_enc_low, target_enc_high = self.encoder(target_image_low, target_image_high)
        nb, ndim, nr2, nc2 = target_enc_low.size()
        target_enc_low = target_enc_low.view(nb, ndim*nr2*nc2)
        target_enc_high = target_enc_high.view(nb, ndim*nr2*nc2)

        scores_low, scores_high = self.ll(target_enc_low, target_enc_high)
        prob_low, prob_high = self.softmax(scores_low, scores_high)

        # prob_low :: [nb, ndim, ndim]
        crossentropy_loss = torch.stack([F.cross_entropy(prob_low[:,i,:].log().clamp(min=-100), target_label) for i in range(prob_low.size(1))]).mean()

        if self.visMode:
            diff = []
            for i in range(prob_low.size(0)):
                diff.append(prob_high.data.select(0,i).diag() - prob_low.data.select(0,i).diag())
            diff = torch.stack(diff)
            print('softmax', diff.min(), diff.max(), diff.mean())
        prob_low = prob_low.mean(1)
        values, indices = prob_low.max(1)

        accuracy = torch.mean((indices.squeeze() == target_label).float())

        return {'acc':  accuracy,
                'loss': crossentropy_loss,
                '95Interval': 0,
                'TP': 0,
                'FP': 0,
                'TN': 0,
                'FN': 0}



class RBMMinMax(MyModule):
    def __init__(self,kerns):
        super(RBMMinMax, self).__init__()
        self.rmm = ReLUMinMax()
        self.modlist = nn.ModuleList([LinearMinMax(kerns[i-1], kerns[i]) for i in range(1,len(kerns))])
    def forward(self, x_low, x_high):
        for m in self.modlist:
            x_low, x_high = m(x_low, x_high)
            x_low, x_high = self.rmm(x_low, x_high)
        return x_low, x_high

class AttentionClassifierMinMax(MyModule):
    def __init__(self, batch_size=100, num_channels=1, learning_rate=0.001,
                 nClasses = 10, image_size = 28, layer_size=64, **kwargs):
        super(AttentionClassifierMinMax, self).__init__()
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.encoders = None
        gSup = EncoderMinMax(layer_size = cargs.layer_size, num_channels=num_channels,
                                nClasses= nClasses, image_size = image_size , returnLayer=-3)
        self.encoder = gSup
        self.ll = LinearMinMax(cargs.layer_size, nClasses)
        self.attn = RBMMinMax([cargs.layer_size, cargs.layer_size, 1])
        self.softmax = SoftmaxMinMax(dim=1)

    def getEncoders(self, num_channels, image_size):
        self.encoders = [Identity2()]
        return self.encoders


    def forward(self, target_image_low, target_image_high, target_label, target_ind=None):
        """
        Builds graph for Matching Networks, produces losses and summary statistics.
        :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
        :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
        :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
        :param target_label: A tensor containing the target label [batch_size, 1]
        :return:
        """
        target_enc_low, target_enc_high = self.encoder(target_image_low, target_image_high)
        # target_enc_low :: [nb, ndim, nr, nc]
        nb, ndim, nr2, nc2 = target_enc_low.size()

        target_enc_low = target_enc_low.view(nb, ndim,nr2*nc2).permute(0,2,1).contiguous().view(nb*nr2*nc2, ndim)
        target_enc_high = target_enc_high.view(nb, ndim,nr2*nc2).permute(0,2,1).contiguous().view(nb*nr2*nc2, ndim)

        attn_low, attn_high = self.attn(target_enc_low, target_enc_high)
        attn_low = attn_low.view(nb, nr2*nc2)
        attn_high = attn_high.view(nb, nr2*nc2)

        spreadProb = (attn_low-attn_high).exp() # FIXME: parameterize the scaling here?,


        attn = F.softmax((attn_low+attn_high)/2, dim=1) # [nb, nr2*nc2]
        attn = attn * spreadProb
        attn = attn / (attn.sum(1, keepdim=True) + 1e-5)

        target_enc_low = (target_enc_low.view(nb, nr2*nc2, ndim) * attn.view(nb, nr2*nc2, 1)).sum(1) # [nb, ndim]
        target_enc_high = (target_enc_high.view(nb, nr2*nc2, ndim) * attn.view(nb, nr2*nc2, 1)).sum(1) # [nb, ndim]

        scores_low, scores_high = self.ll(target_enc_low, target_enc_high)
        prob_low, prob_high = self.softmax(scores_low, scores_high)

        # prob_low :: [nb, ndim, ndim]
        crossentropy_loss = torch.stack([F.cross_entropy(prob_low[:,i,:], target_label) for i in range(prob_low.size(1))]).mean()

        #prob_low = prob_low.gather(1, target_label.unsqueeze(1).unsqueeze(1).repeat(1,1,prob_low.size(2))).squeeze(1)
        prob_low = prob_low.mean(1)
        #assert((prob_low-prob_high).data.abs().max() < 1e-8)
        values, indices = prob_low.max(1)

        accuracy = torch.mean((indices.squeeze() == target_label).float())
        #crossentropy_loss =  F.cross_entropy(prob_low, target_label.long())

        return {'acc':  accuracy,
                'loss': crossentropy_loss,
                '95Interval': 0,
                'TP': 0,
                'FP': 0,
                'TN': 0,
                'FN': 0}

# class RangeNetworkOneShot(MyModule):
#     def __init__(self, keep_prob, \
#                  batch_size=100, num_channels=1, learning_rate=0.001, fce=False, num_classes_per_set=5, \
#                  num_samples_per_class=1, nClasses = 0, image_size = 28):
#         super(RangeNetwork, self).__init__()

#         """
#         Builds a matching network, the training and evaluation ops as well as data augmentation routines.
#         :param keep_prob: A tf placeholder of type tf.float32 denotes the amount of dropout to be used
#         :param batch_size: The batch size for the experiment
#         :param num_channels: Number of channels of the images
#         :param is_training: Flag indicating whether we are training or evaluating
#         :param rotate_flag: Flag indicating whether to rotate the images
#         :param fce: Flag indicating whether to use full context embeddings (i.e. apply an LSTM on the CNN embeddings)
#         :param num_classes_per_set: Integer indicating the number of classes per set
#         :param num_samples_per_class: Integer indicating the number of samples per class
#         :param nClasses: total number of classes. It changes the output size of the classifier g with a final FC layer.
#         :param image_input: size of the input image. It is needed in case we want to create the last FC classification
#         """
#         self.batch_size = batch_size
#         self.dn = DistanceNetwork()
#         self.classify = AttentionalClassify()
#         self.keep_prob = keep_prob
#         self.num_classes_per_set = num_classes_per_set
#         self.num_samples_per_class = num_samples_per_class
#         self.learning_rate = learning_rate
#         self.encoders = None

#     def getEncoders(self, num_channels, image_size):
#         gSup = ClassifierMinMax(layer_size = cargs.layer_size, num_channels=num_channels,
#                                 nClasses= 10, image_size = image_size )
#         self.encoders = [gSup]
#         return [gSup]


#     def forward(self, support_set_images, support_set_labels_one_hot, target_image, target_label, support_enc, target_enc, support_ind=None, target_ind=None):
#         """
#         Builds graph for Matching Networks, produces losses and summary statistics.
#         :param support_set_images: A tensor containing the support set images [batch_size, sequence_size, n_channels, 28, 28]
#         :param support_set_labels_one_hot: A tensor containing the support set labels [batch_size, sequence_size, n_classes]
#         :param target_image: A tensor containing the target image (image to produce label for) [batch_size, n_channels, 28, 28]
#         :param target_label: A tensor containing the target label [batch_size, 1]
#         :return:
#         """
#         # produce embeddings for support set images
#         encoded_support_low = torch.stack([x[-1].select(1,0).view(x[-1].size(0), x[-1].size(2)) for x in support_enc])
#         encoded_support_high = torch.stack([x[-1].select(1,1).view(x[-1].size(0), x[-1].size(2)) for x in support_enc])


#         true_positive = 0
#         false_positive = 0
#         true_negative = 0
#         false_negative = 0

#         # produce embeddings for target images
#         for i in np.arange(target_image.size(1)):
#             encoded_target_low = target_enc[i][-1].select(1,0).view(target_enc[i][-1].size(0), target_enc[i][-1].size(2))
#             encoded_target_high = target_enc[i][-1].select(1,0).view(target_enc[i][-1].size(0), target_enc[i][-1].size(2))

#             # get similarity between support set embeddings and target
#             similarities = self.dn(support_set=encoded_support, target_image=encoded_target)

#             # produce predictions for target probabilities
#             preds = self.classify(similarities,support_set_y=support_set_labels_one_hot)

#             # calculate accuracy and crossentropy loss
#             values, indices = preds.max(1)
#             if i == 0:
#                 accuracy = torch.mean((indices.squeeze() == target_label[:,i]).float())
#                 crossentropy_loss = F.cross_entropy(preds, target_label[:,i].long())
#             else:
#                 accuracy += torch.mean((indices.squeeze() == target_label[:, i]).float())
#                 crossentropy_loss +=  F.cross_entropy(preds, target_label[:, i].long())


#             nsup = support_set_labels_one_hot.size(1)
#             actual_open_set = support_set_labels_one_hot.select(1,nsup-1).data.max(1)[1] == target_label.data[:,i]
#             declared_open_set = torch.matmul(preds.data.unsqueeze(1), support_set_labels_one_hot.data.transpose(1,2)).squeeze(1).max(1)[1] == (nsup - 1)
#             true_positive += (actual_open_set & declared_open_set).sum()
#             false_positive += ((~actual_open_set) & declared_open_set).sum()
#             true_negative += ((~actual_open_set) & (~declared_open_set)).sum()
#             false_negative += (actual_open_set & (~declared_open_set)).sum()

#         return {'acc':  accuracy/target_image.size(1),
#                 'loss': crossentropy_loss/target_image.size(1),
#                 'TP': true_positive,
#                 'FP': false_positive,
#                 'TN': true_negative,
#                 'FN': false_negative}
