import torch
from torch.nn import Parameter
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
import unittest
import numpy
import numpy as np
import math
import torchsample
from models.Base import *
from models.Classifier import convLayer
import torch.nn.functional as F


class NeighborGuessNet(MyModule):
    def forward(self, params, x):
        """
        params: [ (W, b) ]
        W : [nb, nnets, k1, k2]
        b: [nb, nnets, k2]

        x: [nb, ns, nexpt, ndim]
        res : [nb, ns, nexpt, nnets] 0->1

        net: ndim*2 -> k1 -> k2 ... -> kn =1

        """
        x = x.unsqueeze(3) # [nb, ns, 1, nexpt, ndim]
        for (i, (W, b)) in enumerate(params):
            x = torch.matmul(x, W.unsqueeze(1)) + b.unsqueeze(1).unsqueeze(3)
            if i < (len(params)-1): x = F.relu(x)

        return x

class KernGen(MyModule):
    def __init__(self, nnets, kerns, genKerns):
        """
        kerns :: embDim = K0 -> ... -> Kn-1 -> self.nparams (added on in end)
        args.nch -> k0 -> ... -> kn-1 = self.nparams

        genKerns :: kerns to generate params for
        blockEmbDim -> K0 -> K1 -> ... -> Kn-1 = 1

        """
        super(KernGen, self).__init__()
        self.genKerns = genKerns
        self.nnets = nnets
        self.nb = self.genKerns[1:]
        self.nw = [self.genKerns[i-1] * self.genKerns[i] for i in range(1,len(self.genKerns))]
        self.nparams = sum(self.nb) + sum(self.nw)

        self.paramGen = RBM(kerns + [self.nnets * self.nparams])



    def forward(self, inp):
        """
        inp: [nb, ndim]
        """
        #print('kernGen', inp.size())
        nb = inp.size(0)
        #print(self.paramGen)
        ps = self.paramGen(inp).contiguous().view(nb, self.nnets, self.nparams) # [nb, nnets * nparams]
        res = []
        pi = 0
        for i in range(len(self.nw)):
            k0 = self.genKerns[i]
            k1 = self.genKerns[i+1]
            w = ps.narrow(2,pi, k0*k1).contiguous().view(nb, self.nnets, k0, k1) # [nb, nnets, k0, k1]
            pi += k0*k1

            b = ps.narrow(2,pi,k1) # [nb, nnets, k1]
            pi += k1
            res.append((w,b))
        #print('kernGen res', len(res))
        return res


class JigsawExperimentBuilder(MyModule):
    def __init__(self, numExperiments):
        super(JigsawExperimentBuilder, self).__init__()
        self.numExperiments = numExperiments
    def forward(self, blocks):
        """
        blocks :: [nb, ns, naff, nr, nc, ndim]
        # base     : [nb, ns, nexpt, ndim],
        # nbrs     : [ndir, nb, ns, nexpt, 1, ndim ]
        # nbrLabel : [ndir, nb, ns, nexpt, 1] -> 1 if actually nbr 0 otherwise

        """
        #print('exptbuilder', blocks.size())

        nb, ns, naff, nr, nc, ndim = blocks.size()
        #print(ndim)
        bases = []
        nbrs = []
        labels = []
        for iNexpt in range(self.numExperiments):
            # base
            br = numpy.random.randint(1,nr-1)
            bc = numpy.random.randint(1,nc-1)
            bases.append(blocks.select(4,bc).select(3,br)) # [nb, ns, naff, ndim]
            eNbrs = []
            eLabels = []
            for (dr,dc) in [(0,-1), (0,1), (-1,0), (1,0)]: #[l,r,u,d]
                # outcome
                label = numpy.random.randint(0,2)
                eLabels.append(label)
                # guess
                gr, gc = br, bc
                if label == 1: # actual nbr
                    gr = br + dr
                    gc = bc + dc
                else:
                    assert(label == 0)
                    while ((gr == br) and (gc == bc)) or ((gr == (br+dr)) and (gc == (bc+dc))):
                        gr = numpy.random.randint(0,nr)
                        gc = numpy.random.randint(0,nc)
                eNbrs.append(blocks.select(4,gc).select(3,gr)) #[nb, ns, naff, ndim]
            nbrs.append(torch.stack(eNbrs, dim=0)) #[ndir, nb, ns, naff, ndim]
            labels.append(eLabels) #[nexpt, ndir]
        labels = torch.Tensor(labels).cuda().transpose(0,1) #[ndir, nexpt]
        labels = labels.unsqueeze(2).repeat(1,1,naff).view(4, self.numExperiments*naff) # [ndim, nexpt * naff]
        nbrs = torch.cat(nbrs, dim=3).view(4, nb, ns, self.numExperiments*naff, ndim) #[ndir, nb, ns, nexpt * naff, ndim]
        bases = torch.stack(bases, dim=2).view(nb, ns, self.numExperiments*naff, ndim) # [nb, ns, nexpt* naff, ndim]
        labels = Variable(labels, requires_grad=False) #[ndir, nexpt]
        #print('exptbuilder res', bases.size(), nbrs.size(), labels.size())
        return bases, nbrs, labels


class BlockClassifier(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(BlockClassifier, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """

        self.l1 = convLayer(num_channels, layer_size//2, useDropout)
        self.l2 = convLayer(layer_size//2, layer_size, useDropout) # layer 2
        self.l3 = convLayer(layer_size, layer_size, useDropout) # layer 3


    def weights_init(self,module):
        if module is None: return
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform(m.weight, gain=np.sqrt(2))
                init.constant(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [nb, nch, nr, nc]
        :return: Embeddings of size [nb, 64]
        """
        #print('block embedder', image_input.size())
        x = image_input
        #print(0, x.size())
        for l in [self.l1, self.l2, self.l3]:
            if l is None: continue
            x = l(x)
            x = nn.MaxPool2d(kernel_size=2,stride=2)(x)
        x = x.squeeze(3).squeeze(2)
        #print('block embedder res', x.size())
        return x
