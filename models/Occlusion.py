from models.Base import *
import torch.nn.functional as F

class MaskedConv2d(MyModule):
    def __init__(self, in_planes, out_planes, kernel_size=3, stride=1, padding=0, bias=True):
        super(MaskedConv2d, self).__init__()
        """
        embed, produce mask, dot product with original input, re-embed

        The marginal here is a free parameter that can be set

        TODO: Compute the mini-batch-marginal and use that to update a running
        marginal value that can be used as a true marginal rather than a free parameter.

        """

        # for conv
        tmp = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, bias=bias)
        self.weight = tmp.weight
        self.bias = tmp.bias
        self.mask = RBM([in_planes*kernel_size*kernel_size, kernel_size*kernel_size, kernel_size*kernel_size])
        self.marginal = Parameter(tmp.weight.data[0].clone().zero_())
        self.blocks = GetBlocks2D(width=kernel_size, stride=stride, dim0=2, dim1=3,  padding=kernel_size//2)

    def forward(self, x):
        """
        x: [nb, nk, nr, nc]
        """

        xb = self.blocks(x)
        nb, nr, nc, nch, mr, mc = xb.size()
        xb = xb.view(nb*nr*nc, nch * mr * mc)

        xm = F.sigmoid(self.mask(xb)) # nb', mr * mc

        xb = xb.view(nb, nr, nc, nch,  mr, mc)
        xm = xm.view(nb, nr, nc, 1, mr, mc)

        xgate = xm * xb + (1-xm) * self.marginal.view(1,1,1,nch,mr,mc)

        res = torch.matmul(xgate.view(nb*nr*nc, nch*mr*mc), self.weight.view(self.weight.size(0), -1).transpose(0,1)) + self.bias.unsqueeze(0) # [nb', out_planes]
        nout = res.size(1)
        res = res.view(nb, nr, nc, nout)
        res = res.permute(0,3,1,2)
        return res.contiguous()


def convLayer(in_planes, out_planes, useDropout = False):
    "3x3 convolution with padding"
    seq = nn.Sequential(
        MaskedConv2d(in_planes, out_planes, kernel_size=3,
                     stride=1, padding=1, bias=True),
        nn.BatchNorm2d(out_planes),
        #nn.MaxPool2d(kernel_size=2, stride=2)
    )
    if useDropout: # Add dropout module
        list_seq = list(seq.modules())[1:]
        list_seq.append(nn.Dropout(0.1))
        seq = nn.Sequential(*list_seq)

    return seq


class MaskedClassifier(MyModule):
    def __init__(self, layer_size, nClasses = 0, num_channels = 1, useDropout = False, image_size = 28):
        super(MaskedClassifier, self).__init__()

        """
        Builds a CNN to produce embeddings
        :param layer_sizes: A list of length 4 containing the layer sizes
        :param nClasses: If nClasses>0, we want a FC layer at the end with nClasses size.
        :param num_channels: Number of channels of images
        :param useDroput: use Dropout with p=0.1 in each Conv block
        """

        self.l1 = convLayer(num_channels, layer_size//2, useDropout)
        self.l2 = convLayer(layer_size//2, layer_size, useDropout) # layer 2
        self.l3 = convLayer(layer_size, layer_size, useDropout) # layer 3
        self.l4 = convLayer(layer_size, layer_size, useDropout) # layer 4
        self.l5 = None
        self.l6 = None
        if image_size >= 30:
            self.l5 = convLayer(layer_size, layer_size, useDropout) # layer 5
        if image_size >= 80:
            self.l6 = convLayer(layer_size, layer_size, useDropout) # layer 6

        self.useClassification = False

        self.weights_init(self.l1)
        self.weights_init(self.l2)
        self.weights_init(self.l3)
        self.weights_init(self.l4)
        self.weights_init(self.l5)
        self.weights_init(self.l6)


    def weights_init(self,module):
        if module is None: return
        for m in module.modules():
            if isinstance(m, nn.Conv2d):
                init.xavier_uniform(m.weight, gain=np.sqrt(2))
                init.constant(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()


    def forward(self, image_input):
        """
        Runs the CNN producing the embeddings and the gradients.
        :param image_input: Image input to produce embeddings for. [batch_size, 28, 28, 1]
        :return: Embeddings of size [batch_size, 64]
        """
        xs = [image_input]
        x = image_input
        for l in [self.l1, self.l2, self.l3, self.l4, self.l5, self.l6]:
            if l is None: continue
            x = x.contiguous()
            x = l(x)
            xs.append(x)
            x = F.relu(x)
            x = nn.MaxPool2d(kernel_size=2,stride=2)(x)
        xs.append(x)
        return xs
