import os, errno
from tensorboard_logger import configure, log_value
import logging


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


class Logger(object):
    def __init__(self, log_dir, clear=True):
        # clean previous logged data under the same directory name
        if clear:
            self._remove(log_dir)

        # configure the project
        configure(log_dir)

        self.global_step = 0


        self.logger = logging.getLogger('net')
        fmt = '[%(levelname)s] <%(name)s> %(message)s'
        logFormatter = logging.Formatter(fmt)
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        self.logger.addHandler(consoleHandler)
        mkdir_p(log_dir)
        hdlr = logging.FileHandler(os.path.join(log_dir, 'runtext.log'))
        hdlr.setFormatter(logFormatter)
        self.logger.addHandler(hdlr)
        self.logger.setLevel(logging.DEBUG)



    def log_value(self, name, value):
        log_value(name, value, self.global_step)
        self.logger.info('{}:{}: {}'.format(self.global_step, name, value))
        return self

    def debug(self, s):
        self.logger.debug(s)

    def step(self):
        self.global_step += 1

    @staticmethod
    def _remove(path):
        """ param <path> could either be relative or absolute. """
        if os.path.isfile(path):
            os.remove(path)  # remove the file
        elif os.path.isdir(path):
            import shutil
            shutil.rmtree(path)  # remove dir and all contains

# class Logger(object):
#     def __init__(self, log_dir):
#         # clean previous logged data under the same directory name
#         self._remove(log_dir)

#         # configure the project
#         configure(log_dir)

#         self.global_step = 0

#     def log_value(self, name, value):
#         log_value(name, value, self.global_step)
#         return self

#     def step(self):
#         self.global_step += 1

#     @staticmethod
#     def _remove(path):
#         """ param <path> could either be relative or absolute. """
#         if os.path.isfile(path):
#             os.remove(path)  # remove the file
#         elif os.path.isdir(path):
#             import shutil
#             shutil.rmtree(path)  # remove dir and all contains
