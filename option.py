##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import argparse
import os
import torch
import shutil
import builtins
from logger import Logger
import pickle
import numpy
import visdom
import visFunctions
import smtplib, socket
import datetime

class Options():
    def __init__(self):
        # Training settings
        parser = argparse.ArgumentParser(description='Matching Network',
                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('--log-dir', default='/home/pareshmg/outs/logs/abmnets',
                            help='folder to output model checkpoints')

        parser.add_argument('--model', type=str, default='MatchingNetwork', help='use pairwise diff')
        parser.add_argument('--action', type=str, default=None, help='what action to do')
        parser.add_argument('--seed', type=int, default=1234, help='what action to do')
        parser.add_argument('--classes-per-set', type=int, default=5, help='classes per set (K-way)')
        parser.add_argument('--samples-per-class', type=int, default=1, help='samples per class (N-shot)')
        parser.add_argument('--resume', type=str, default=None, help='resume from checkpoint')
        parser.add_argument('--lr', type=float, default=1e-3, help='learning rate')
        parser.add_argument('--total-epochs', type=int, default=200, help='How many epochs to run')
        parser.add_argument('--batch-size', type=int, default=32, help='size of batch')
        parser.add_argument('--mini-batch-size', type=int, default=32, help='size of mini batch')
        parser.add_argument('--open-set', action='store_true', help='use open set simulation for training')
        parser.add_argument('--tag', type=str, default='', help='additional tag to add to log base')
        parser.add_argument('--visMain', action='store_true', help='use main visdom tag')
        parser.add_argument('--test', action='store_true', help='use main visdom tag')
        parser.add_argument('--skipRecon', action='store_true', help='skip recon')
        parser.add_argument('--test-sel', type=str, default='acc',  help='acc, f1')
        parser.add_argument('--supSamp', type=float, default=0.2,  help='pct sup to sample')
        parser.add_argument('--tgtSamp', type=float, default=0.1,  help='pct tgt to sample')
        parser.add_argument('--nvis', type=int, default=5,  help='number of test images to cat to show')
        parser.add_argument('--episode-size', type=int, default=1,  help='number of test images to cat to show')
        parser.add_argument('--masktype', type=str, default='img',  help='img / sample')
        self.parser = parser

    def parse(self, dset, nr, nc, nch, total_train_batches, total_val_batches, total_test_batches, layer_size):
        builtins.cargs = self.parser.parse_args()
        cargs.dset = dset
        cargs.nr = nr
        cargs.nc = nc
        cargs.nch = nch
        cargs.fce = False
        cargs.layer_size = layer_size

        cargs.total_train_batches = total_train_batches * cargs.episode_size
        cargs.total_val_batches = total_val_batches * cargs.episode_size
        cargs.total_test_batches = total_test_batches * cargs.episode_size

        cargs.start_time = datetime.datetime.now()

        if cargs.test:
            cargs.tag = 'test'
            cargs.visMain = True
            cargs.total_train_batches = 1
            cargs.total_val_batches = 1
            cargs.total_test_batches = 1


        if len(cargs.tag) > 0: cargs.tag = '_' + cargs.tag

        cargs.log_base = '{}{}{}_{}-nbatch_{}_{}-classes_per_set{}-samples_per_class{}{}'.format(dset, cargs.nr, 'openset' if cargs.open_set else '' , cargs.model, cargs.batch_size, cargs.mini_batch_size,cargs.classes_per_set,cargs.samples_per_class, cargs.tag)
        cargs.log_dir = os.path.join(cargs.log_dir, cargs.log_base)

        logger = Logger(cargs.log_dir, clear = cargs.resume is None)

        if dset.lower() == 'omniglot':
            cargs.dataroot = '/home/pareshmg/data/OmniglotNShot{}'.format(cargs.nr)
            cargs.stackFVSize = 225
        elif dset.lower() == 'miniimagenet':
            cargs.dataroot = '/home/pareshmg/data/miniImagenet'
            #cargs.stackFVSize = 355
            cargs.stackFVSize = 419
        elif dset.lower() == 'mnist':
            cargs.dataroot = '/home/pareshmg/data/MNIST{}'.format(cargs.nr)
            cargs.stackFVSize = 225
        elif dset.lower() == 'cifar10':
            cargs.dataroot = '/home/pareshmg/data/CIFAR10_{}'.format(cargs.nr)
            cargs.stackFVSize = 227
        elif dset.lower() == 'cifar100':
            cargs.dataroot = '/home/pareshmg/data/CIFAR100_{}'.format(cargs.nr)
            cargs.stackFVSize = 227
        elif dset.lower() == 'voc':
            cargs.dataroot = '/home/pareshmg/data/voc_{}'.format(cargs.nr)
            cargs.stackFVSize = 355


        ##################################################
        ### Write code settings
        with open(os.path.join(cargs.log_dir, 'settings.pkl'), 'wb') as f:
            pickle.dump([cargs], f)

        os.system('git rev-parse HEAD > {}'.format(os.path.join(cargs.log_dir, 'git.log')))
        os.system('git diff >> {}'.format(os.path.join(cargs.log_dir, 'git.log')))
        ##################################################
        ### Set seed
        torch.manual_seed(cargs.seed)
        torch.cuda.manual_seed(cargs.seed)
        numpy.random.seed(cargs.seed)

        ##################################################
        ### vis
        VIS = visdom.Visdom(env=('main' if cargs.visMain else cargs.log_base ), use_incoming_socket=False)
        VIS.close(win=None)

        return cargs, logger

def save_checkpoint(state, is_best, logdir, filename='checkpoint.pth.tar'):
    torch.save(state, os.path.join(logdir, filename))
    if is_best:
        shutil.copyfile(filename, os.path.join(logdir, 'model_best.pth.tar'))


def runExperiment(obj_Builder, logger):
    best_val = 0.
    best_val_test = {}
    acc_loss = {x:[] for x in ['train', 'val', 'test']}
    startEpoch = 0

    if cargs.resume is not None:
        print('Loading checkpoint from', cargs.resume)
        checkpoint = torch.load(cargs.resume)
        obj_Builder.load_state_dict(checkpoint)
        if 'acc_loss' in checkpoint: acc_loss = checkpoint['acc_loss']
        if 'epoch' in checkpoint: startEpoch = checkpoint['epoch'] + 1


    ncat = cargs.nvis

    if False:
        obj_Builder.set_batch_size(cargs.batch_size, 1)
        obj_Builder.nnet.vis(True)
        visFunctions.visModel(obj_Builder, plts=acc_loss, ncat=ncat)
        obj_Builder.nnet.vis(False)
        obj_Builder.set_batch_size(cargs.batch_size, cargs.mini_batch_size)

    e = 0
    if cargs.total_epochs == 0:
        total_test_res = obj_Builder.run_epoch('test', total_batches=cargs.total_test_batches)
        total_test_c_loss = total_test_res['loss']
        total_test_accuracy = total_test_res['acc']
        total_test_interval = total_test_res['95Interval']
        logger.debug(u"Epoch {}: test_loss: {}, test_accuracy: {} \u00B1 {}".format(e, total_test_c_loss, total_test_accuracy*100, total_test_interval*100))
        exit(1)

    for e in range(startEpoch, startEpoch + cargs.total_epochs):
        total_train_res = obj_Builder.run_epoch('train', total_batches=cargs.total_train_batches)
        total_train_c_loss = total_train_res['loss']
        total_train_accuracy = total_train_res['acc']
        total_train_interval = total_train_res['95Interval']
        logger.debug(u"Epoch {}: train_loss: {}, train_accuracy: {} \u00B1 {}".format(e, total_train_c_loss, total_train_accuracy*100, total_train_interval*100))

        acc_loss['train'].append([e, total_train_res])


        total_val_res = obj_Builder.run_epoch('val', total_batches=cargs.total_val_batches)
        total_val_c_loss = total_val_res['loss']
        total_val_accuracy = total_val_res['acc']
        total_val_interval = total_val_res['95Interval']
        logger.debug(u"Epoch {}: val_loss: {}, val_accuracy: {} \u00B1 {}".format(e, total_val_c_loss, total_val_accuracy*100, total_val_interval*100))
        acc_loss['val'].append([e, total_val_res])

        logger.log_value('train_loss', total_train_c_loss)
        logger.log_value('train_acc', total_train_accuracy)
        logger.log_value('val_loss', total_val_c_loss)
        logger.log_value('val_acc', total_val_accuracy)
        print('next')

        if total_val_res[cargs.test_sel] >= best_val:  # if new best val accuracy -> produce test statistics
            best_val = total_val_res[cargs.test_sel]
            total_test_res = obj_Builder.run_epoch('test', total_batches=cargs.total_test_batches)
            total_test_c_loss = total_test_res['loss']
            total_test_accuracy = total_test_res['acc']
            total_test_interval = total_test_res['95Interval']
            logger.debug(u"Epoch {}: test_loss: {}, test_accuracy: {} \u00B1 {}".format(e, total_test_c_loss, total_test_accuracy*100, total_test_interval*100))
            acc_loss['test'].append([e, total_test_res])

            logger.log_value('test_loss', total_test_c_loss)
            logger.log_value('test_acc', total_test_accuracy)
            best_val_test = total_test_res[cargs.test_sel]


            # total_testm_c_loss, total_testm_accuracy = obj_Builder.run_mnist_testing_epoch(
            #     total_test_batches=total_test_batches*2, rotate_flag=False)
            # logger.debug("Epoch {}: test_mnist_loss: {}, test_mnist_accuracy: {}".format(e, total_testm_c_loss, total_testm_accuracy))

            save_checkpoint({
                'epoch': e,
                'args': cargs,
                'state_dict': obj_Builder.get_state_dict(),
                'test_loss': total_test_c_loss,
                'test_acc': total_test_accuracy,
                'acc_loss': acc_loss,
            }, False, cargs.log_dir)
            logger.debug('saved model to {}'.format(cargs.log_dir))
            print('next')
        else:
            total_test_c_loss = -1
            total_test_accuracy = -1
        logger.step()

        ##################################################
        ## visualzie
        obj_Builder.set_batch_size(cargs.batch_size, 1)
        obj_Builder.nnet.vis(True)
        visFunctions.visModel(obj_Builder, plts= acc_loss, ncat=ncat)
        obj_Builder.nnet.vis(False)
        obj_Builder.set_batch_size(cargs.batch_size, cargs.mini_batch_size)
    logger.debug('Complete! best test is {} at val {}'.format(best_val_test, best_val))
    if not cargs.test:
        save_checkpoint({
            'epoch': e,
            'args': cargs,
            'state_dict': obj_Builder.get_state_dict(),
            'test_loss': total_test_c_loss,
            'test_acc': total_test_accuracy,
            'acc_loss': acc_loss,
        }, False, cargs.log_dir, filename="final.pth.tar")
        logger.debug('saved model to {}'.format(cargs.log_dir))

    if (not cargs.test) and (cargs.total_epochs > 2):
        SERVER = "localhost"
        FROM = "pareshmg@"+socket.gethostbyaddr(socket.gethostname())[0]

        TO = ["paresh.mg@gmail.com"]
        SUBJECT = "[Script Notification] " + socket.gethostname()
        TEXT = """Finished command
        {}

        Complete! best test is {} at val {}""".format(str(cargs.__dict__), best_val_test, best_val)

        message = """From: %s
To: %s
MIME-Version: 1.0
Content-type: text/html
Subject: %s

%s""" % (FROM, ", ".join(TO), SUBJECT, TEXT)

        server = smtplib.SMTP(SERVER)
        #server.set_debuglevel(3)
        server.sendmail(FROM, TO, message)
        server.quit()
