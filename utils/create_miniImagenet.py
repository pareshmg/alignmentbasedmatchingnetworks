##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## Created by: Albert Berenguel
## Computer Vision Center (CVC). Universitat Autonoma de Barcelona
## Email: aberenguel@cvc.uab.es
## Copyright (c) 2017
##
## This source code is licensed under the MIT-style license found in the
## LICENSE file in the root directory of this source tree
##+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

'''
This code creates the MiniImagenet dataset. Following the partitions given
by Sachin Ravi and Hugo Larochelle in
https://github.com/twitter/meta-learning-lstm/tree/master/data/miniImagenet
'''

import numpy as np
import csv
import glob, os
#from shutil import copyfile
import cv2
from tqdm import tqdm
import pickle
import ipdb
pathImageNet = '/home/pareshmg/data/Imagenet/train'
pathminiImageNet = '/home/pareshmg/data/miniImagenet'
filesCSVSachinRavi = [os.path.join(pathminiImageNet,'train.csv'),
                      os.path.join(pathminiImageNet,'val.csv'),
                      os.path.join(pathminiImageNet,'test.csv')]


# Check if the folder of images exist. If not create it.
for w in [80, 84]:
    pathImages = os.path.join(pathminiImageNet,'images{}/'.format(w))
    if not os.path.exists(pathImages):
        os.makedirs(pathImages)


allClasses = set()

for dataset, filename in zip(['train', 'val', 'test'], filesCSVSachinRavi):
    for w in [80, 84]:
        pathImages = os.path.join(pathminiImageNet,'images{}/'.format(w))
        if not os.path.exists(os.path.join(pathImages, dataset)):
            os.makedirs(os.path.join(pathImages, dataset))
    with open(filename) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=',')
        next(csv_reader, None)
        images = {}
        print('Reading IDs....')
        for row in tqdm(csv_reader):
            if row[1] in images.keys():
                images[row[1]].append(row[0])
            else:
                images[row[1]] = [row[0]]

        print('Writing photos....')
        for c in tqdm(images.keys()): # Iterate over all the classes
            allClasses.add(c)
            #os.chdir(pathImageNet) # TODO: change this line that is change the current folder.
            for w in [80, 84]:
                pathImages = os.path.join(pathminiImageNet,'images{}/'.format(w))
                if not os.path.exists(os.path.join(pathImages, dataset, c)):
                    os.makedirs(os.path.join(pathImages, dataset, c))

            lst_files = []
            for fn in glob.glob(os.path.join(pathImageNet, c, "*.*")):
                lst_files.append(fn)
            # TODO: Sort by name of by index number of the image???
            # I sort by the number of the image
            lst_index = [int(i[i.rfind('_')+1:i.rfind('.')]) for i in lst_files]
            index_sorted = sorted(range(len(lst_index)), key=lst_index.__getitem__)

            # Now iterate
            index_selected = [int(i[i.index('.') - 4:i.index('.')]) for i in images[c]]
            selected_images = np.array(index_sorted)[np.array(index_selected) - 1]

            for i in np.arange(len(selected_images)):
                # read file and resize to nrxncx3
                im = cv2.imread(os.path.join(pathImageNet,lst_files[selected_images[i]]))

                for w in [80, 84]:
                    pathImages = os.path.join(pathminiImageNet,'images{}/'.format(w))
                    im_resized = cv2.resize(im, (w, w), interpolation=cv2.INTER_AREA)
                    cv2.imwrite(os.path.join(pathImages, dataset, c, images[c][i]),im_resized)

                #copyfile(os.path.join(pathImageNet,lst_files[selected_images[i]]),os.path.join(pathImages, images[c][i]))

allClasses = list(allClasses)
allClasses.sort()
for w in [80, 84]:
    pathImages = os.path.join(pathminiImageNet,'images{}/'.format(w))
    with open(os.path.join(pathImages, 'classes.pkl'), 'wb') as f:
        pickle.dump(allClasses, f)
